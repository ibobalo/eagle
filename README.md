# eagle

tools for Cadsoft Eagle CAD

ulp scripts:

- mod-dup - duplicates the board placement and routing of a module
- mod-mname - adds alternatine (short) component names

scr scripts:

- print - print set of documentation to pdf

lib:

- main.lbr - well selected library of commonly used components
- main-demo.sch, main-demo.brd - demo board for main.lbr

dru, cams:
- PcbPrint* - production files for pcb24.com.ua
- SeedStudio* - production files for seeedstudio.com/fusion_pcb

py scripts:
- python script for production files preview (pdf) and pack. Gerber2pdf required
   for Windows [https://sourceforge.net/projects/gerber2pdf/files/Gerber2pdf.exe/download]
   for linux   [https://sourceforge.net/projects/gerber2pdf/files/Gerber2pdf/download]
