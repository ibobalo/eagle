<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="yes" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="yes" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="yes" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="yes" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="yes" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="yes" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="yes" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="yes" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="yes" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="yes" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="yes" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="yes" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="yes" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Hidden" color="15" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="12" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="main">
<packages>
<package name="0603">
<description>&lt;h3&gt;Common SMD Chip 0603&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 1.6 x 0.8 mm, Metric Code Size 1608&lt;/p&gt;
&lt;p&gt;power 0.100W&lt;p&gt;</description>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-1.397" y="0.762" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.6096" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.5494" y1="0.6858" x2="1.5494" y2="0.6858" width="0.0254" layer="51"/>
<wire x1="1.5494" y1="0.6858" x2="1.5494" y2="-0.6858" width="0.0254" layer="51"/>
<wire x1="1.5494" y1="-0.6858" x2="-1.5494" y2="-0.6858" width="0.0254" layer="51"/>
<wire x1="-1.5494" y1="-0.6858" x2="-1.5494" y2="0.6858" width="0.0254" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
</package>
<package name="0805">
<description>&lt;h3&gt;Common SMD Chip 0805&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 2.0 x 1.25 mm, Metric Code Size 2012&lt;/p&gt;
&lt;p&gt;power 0.125W&lt;p&gt;</description>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.651" y="1.016" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.778" y1="0.8128" x2="1.778" y2="0.8128" width="0.0254" layer="51"/>
<wire x1="1.778" y1="0.8128" x2="1.778" y2="-0.8128" width="0.0254" layer="51"/>
<wire x1="1.778" y1="-0.8128" x2="-1.778" y2="-0.8128" width="0.0254" layer="51"/>
<wire x1="-1.778" y1="-0.8128" x2="-1.778" y2="0.8128" width="0.0254" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="1.016" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.016" y1="-0.635" x2="-1.016" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="-0.635" x2="-1.016" y2="0.635" width="0.0508" layer="39"/>
<wire x1="-0.127" y1="0.635" x2="0.127" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.127" y1="-0.635" x2="-0.127" y2="-0.635" width="0.127" layer="21"/>
</package>
<package name="0402">
<description>&lt;h3&gt;Common SMD Chip 0402&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 1.0 x 0.5 mm, Metric Code Size 1005&lt;/p&gt;
&lt;p&gt;power 0.062W&lt;p&gt;</description>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-1.27" y="0.635" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.6096" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.1938" y1="0.508" x2="1.1938" y2="0.508" width="0.0254" layer="51"/>
<wire x1="1.1938" y1="0.508" x2="1.1938" y2="-0.508" width="0.0254" layer="51"/>
<wire x1="1.1938" y1="-0.508" x2="-1.1938" y2="-0.508" width="0.0254" layer="51"/>
<wire x1="-1.1938" y1="-0.508" x2="-1.1938" y2="0.508" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="0.508" y2="0.254" width="0.0508" layer="39"/>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.0508" layer="39"/>
<wire x1="0.508" y1="-0.254" x2="-0.508" y2="-0.254" width="0.0508" layer="39"/>
<wire x1="-0.508" y1="-0.254" x2="-0.508" y2="0.254" width="0.0508" layer="39"/>
</package>
<package name="1206">
<description>&lt;h3&gt;Common SMD Chip 1206&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 3.2 x 1.6 mm, Metric Code Size 3216&lt;/p&gt;
&lt;p&gt;power 0.250W&lt;p&gt;</description>
<wire x1="-2.286" y1="-1.016" x2="2.286" y2="-1.016" width="0.0254" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-2.159" y="1.143" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.286" y1="1.016" x2="2.286" y2="1.016" width="0.0254" layer="51"/>
<wire x1="2.286" y1="-1.016" x2="2.286" y2="1.016" width="0.0254" layer="51"/>
<wire x1="-2.286" y1="-1.016" x2="-2.286" y2="1.016" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="0.762" x2="1.524" y2="0.762" width="0.0508" layer="39"/>
<wire x1="1.524" y1="0.762" x2="1.524" y2="-0.762" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-0.762" x2="-1.524" y2="-0.762" width="0.0508" layer="39"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.0508" layer="39"/>
<wire x1="-0.508" y1="0.762" x2="0.508" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
</package>
<package name="1210">
<description>&lt;h3&gt;Common SMD Chip 1210&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 3.2 x 2.5 mm, Metric Code Size 3225&lt;/p&gt;
&lt;p&gt;power 0.500W&lt;p&gt;</description>
<wire x1="-2.286" y1="-1.4732" x2="2.286" y2="-1.4732" width="0.0254" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-2.286" y="1.651" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.286" y1="1.4732" x2="2.286" y2="1.4732" width="0.0254" layer="51"/>
<wire x1="2.286" y1="-1.4732" x2="2.286" y2="1.4732" width="0.0254" layer="51"/>
<wire x1="-2.286" y1="-1.4732" x2="-2.286" y2="1.4732" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.0508" layer="39"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.0508" layer="39"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.0508" layer="39"/>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.127" layer="21"/>
</package>
<package name="SMC_A">
<description>&lt;h3&gt;Chip Capacitor SMA&lt;/h3&gt;
&lt;p&gt;Polar tantalum capacitors with solid electrolyte&lt;/p&gt;
&lt;p&gt;Size: 3.2 x 1.6 x 1.6 mm, Metric code 3216-18&lt;/p&gt;
&lt;p&gt;(+) side marked&lt;/p&gt;</description>
<wire x1="-2.286" y1="0.8382" x2="2.286" y2="0.8382" width="0.0254" layer="51"/>
<wire x1="2.286" y1="0.8382" x2="2.286" y2="-0.8382" width="0.0254" layer="51"/>
<wire x1="2.286" y1="-0.8382" x2="-2.286" y2="-0.8382" width="0.0254" layer="51"/>
<wire x1="-2.286" y1="-0.8382" x2="-2.286" y2="0.8382" width="0.0254" layer="51"/>
<rectangle x1="-2.159" y1="-0.8382" x2="-1.778" y2="0.8382" layer="51"/>
<smd name="+" x="-1.5" y="0" dx="1.6" dy="1.4" layer="1"/>
<smd name="-" x="1.5" y="0" dx="1.6" dy="1.4" layer="1" rot="R180"/>
<text x="-2.159" y="0.889" size="0.8128" layer="25">&gt;NAME</text>
<text x="0.254" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.762" x2="-1.524" y2="-0.762" width="0.1524" layer="21"/>
<rectangle x1="-0.635" y1="-0.762" x2="0" y2="0.762" layer="21"/>
<wire x1="-1.651" y1="0.889" x2="1.651" y2="0.889" width="0.0508" layer="39"/>
<wire x1="1.651" y1="0.889" x2="1.651" y2="-0.889" width="0.0508" layer="39"/>
<wire x1="1.651" y1="-0.889" x2="-1.651" y2="-0.889" width="0.0508" layer="39"/>
<wire x1="-1.651" y1="-0.889" x2="-1.651" y2="0.889" width="0.0508" layer="39"/>
</package>
<package name="SMC_B">
<description>&lt;h3&gt;Chip Capacitor SMB&lt;/h3&gt;
&lt;p&gt;Polar tantalum capacitors with solid electrolyte&lt;/p&gt;
&lt;p&gt;Size: 3.5 x 2.8 x 1.9 mm, Metric code 3528-21&lt;/p&gt;
&lt;p&gt;(+) side marked&lt;/p&gt;</description>
<wire x1="-1.6764" y1="1.3208" x2="1.6764" y2="1.3208" width="0.1524" layer="21"/>
<wire x1="1.6764" y1="-1.3208" x2="-1.6764" y2="-1.3208" width="0.1524" layer="21"/>
<rectangle x1="-0.6096" y1="-1.3208" x2="-0.3048" y2="1.3208" layer="21"/>
<smd name="+" x="-1.5" y="0" dx="1.6" dy="2.4" layer="1"/>
<smd name="-" x="1.5" y="0" dx="1.6" dy="2.4" layer="1" rot="R180"/>
<text x="-2.159" y="1.524" size="0.8128" layer="25">&gt;NAME</text>
<text x="0.254" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.778" y1="1.397" x2="-1.778" y2="-1.397" width="0.0508" layer="39"/>
<wire x1="-1.778" y1="-1.397" x2="1.778" y2="-1.397" width="0.0508" layer="39"/>
<wire x1="1.778" y1="-1.397" x2="1.778" y2="1.397" width="0.0508" layer="39"/>
<wire x1="1.778" y1="1.397" x2="-1.778" y2="1.397" width="0.0508" layer="39"/>
<wire x1="-2.413" y1="1.397" x2="2.54" y2="1.397" width="0.0254" layer="51"/>
<wire x1="2.54" y1="1.397" x2="2.54" y2="-1.397" width="0.0254" layer="51"/>
<wire x1="2.54" y1="-1.397" x2="-2.413" y2="-1.397" width="0.0254" layer="51"/>
<wire x1="-2.413" y1="-1.397" x2="-2.413" y2="1.397" width="0.0254" layer="51"/>
<rectangle x1="-2.286" y1="-1.397" x2="-1.905" y2="1.397" layer="51"/>
</package>
<package name="SMC_C">
<description>&lt;h3&gt;Chip Capacitor SMC&lt;/h3&gt;
&lt;p&gt;Polar tantalum capacitors with solid electrolyte&lt;/p&gt;
&lt;p&gt;Size: 6.0 x 3.2 x 2.2 mm, Metric code 6032-28&lt;/p&gt;
&lt;p&gt;(+) side marked&lt;/p&gt;</description>
<wire x1="-2.9464" y1="1.524" x2="2.9464" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.9464" y1="-1.524" x2="-2.9464" y2="-1.524" width="0.1524" layer="21"/>
<rectangle x1="-1.143" y1="-1.524" x2="-0.381" y2="1.524" layer="21"/>
<smd name="+" x="-2.5" y="0" dx="2.4" dy="2.4" layer="1"/>
<smd name="-" x="2.5" y="0" dx="2.4" dy="2.4" layer="1" rot="R180"/>
<text x="-3.556" y="1.651" size="0.8128" layer="25">&gt;NAME</text>
<text x="0.381" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.048" y1="1.651" x2="3.048" y2="1.651" width="0.0508" layer="39"/>
<wire x1="3.048" y1="1.651" x2="3.048" y2="-1.651" width="0.0508" layer="39"/>
<wire x1="3.048" y1="-1.651" x2="-3.048" y2="-1.651" width="0.0508" layer="39"/>
<wire x1="-3.048" y1="-1.651" x2="-3.048" y2="1.651" width="0.0508" layer="39"/>
<wire x1="-3.683" y1="1.6002" x2="3.683" y2="1.6002" width="0.0254" layer="51"/>
<wire x1="3.683" y1="1.6002" x2="3.683" y2="-1.6002" width="0.0254" layer="51"/>
<wire x1="3.683" y1="-1.6002" x2="-3.683" y2="-1.6002" width="0.0254" layer="51"/>
<wire x1="-3.683" y1="-1.6002" x2="-3.683" y2="1.6002" width="0.0254" layer="51"/>
<rectangle x1="-3.556" y1="-1.6002" x2="-3.048" y2="1.6002" layer="51"/>
<wire x1="2.9464" y1="1.524" x2="2.9464" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.9464" y1="-1.397" x2="2.9464" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.9464" y1="-1.524" x2="-2.9464" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.9464" y1="1.397" x2="-2.9464" y2="1.524" width="0.1524" layer="21"/>
</package>
<package name="SMC_D">
<description>&lt;h3&gt;Chip Capacitor SMD&lt;/h3&gt;
&lt;p&gt;Polar tantalum capacitors with solid electrolyte&lt;/p&gt;
&lt;p&gt;Size: 7.3 x 4.3 x 2.4 mm, Metric code 7343-31&lt;/p&gt;
&lt;p&gt;(+) side marked&lt;/p&gt;</description>
<wire x1="-3.556" y1="2.0828" x2="3.556" y2="2.0828" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-2.0828" x2="-3.556" y2="-2.0828" width="0.1524" layer="21"/>
<rectangle x1="-3.556" y1="-2.032" x2="-1.778" y2="-1.524" layer="21"/>
<smd name="+" x="-3.15" y="0" dx="2.4" dy="2.8" layer="1"/>
<smd name="-" x="3.15" y="0" dx="2.4" dy="2.8" layer="1" rot="R180"/>
<text x="-3.556" y="2.286" size="0.8128" layer="25">&gt;NAME</text>
<text x="0.381" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.318" y1="2.159" x2="4.318" y2="2.159" width="0.0254" layer="51"/>
<wire x1="4.318" y1="2.159" x2="4.318" y2="-2.159" width="0.0254" layer="51"/>
<wire x1="4.318" y1="-2.159" x2="-4.318" y2="-2.159" width="0.0254" layer="51"/>
<wire x1="-4.318" y1="-2.159" x2="-4.318" y2="2.159" width="0.0254" layer="51"/>
<wire x1="-3.683" y1="2.159" x2="3.683" y2="2.159" width="0.0508" layer="39"/>
<wire x1="3.683" y1="2.159" x2="3.683" y2="-2.159" width="0.0508" layer="39"/>
<wire x1="3.683" y1="-2.159" x2="-3.683" y2="-2.159" width="0.0508" layer="39"/>
<wire x1="-3.683" y1="-2.159" x2="-3.683" y2="2.159" width="0.0508" layer="39"/>
<rectangle x1="-4.191" y1="-2.159" x2="-3.683" y2="2.159" layer="51"/>
<wire x1="3.556" y1="-1.6002" x2="3.556" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="3.556" y1="2.0828" x2="3.556" y2="1.6002" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.6002" x2="-3.556" y2="2.0828" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-2.0828" x2="-3.556" y2="-1.6002" width="0.1524" layer="21"/>
<rectangle x1="-1.778" y1="-2.032" x2="-0.381" y2="2.032" layer="21"/>
<rectangle x1="-3.556" y1="1.524" x2="-1.778" y2="2.032" layer="21"/>
</package>
<package name="SMC_E">
<description>&lt;h3&gt;Chip Capacitor SME&lt;/h3&gt;
&lt;p&gt;Polar tantalum capacitors with solid electrolyte&lt;/p&gt;
&lt;p&gt;Size: 7.3 x 4.3 x 4.1 mm, Metric code 7343-43&lt;/p&gt;
&lt;p&gt;(+) side marked&lt;/p&gt;</description>
<wire x1="-3.556" y1="2.0828" x2="3.556" y2="2.0828" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-2.0828" x2="-3.556" y2="-2.0828" width="0.1524" layer="21"/>
<rectangle x1="-3.556" y1="-2.0828" x2="-1.778" y2="-1.524" layer="21"/>
<smd name="+" x="-3.15" y="0" dx="2.4" dy="2.8" layer="1"/>
<smd name="-" x="3.15" y="0" dx="2.4" dy="2.8" layer="1" rot="R180"/>
<text x="-3.683" y="2.286" size="0.8128" layer="25">&gt;NAME</text>
<text x="0.508" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.318" y1="2.159" x2="4.318" y2="2.159" width="0.0254" layer="51"/>
<wire x1="4.318" y1="2.159" x2="4.318" y2="-2.159" width="0.0254" layer="51"/>
<wire x1="4.318" y1="-2.159" x2="-4.318" y2="-2.159" width="0.0254" layer="51"/>
<wire x1="-4.318" y1="-2.159" x2="-4.318" y2="2.159" width="0.0254" layer="51"/>
<wire x1="-3.683" y1="2.159" x2="3.683" y2="2.159" width="0.0508" layer="39"/>
<wire x1="3.683" y1="2.159" x2="3.683" y2="-2.159" width="0.0508" layer="39"/>
<wire x1="3.683" y1="-2.159" x2="-3.683" y2="-2.159" width="0.0508" layer="39"/>
<wire x1="-3.683" y1="-2.159" x2="-3.683" y2="2.159" width="0.0508" layer="39"/>
<rectangle x1="-4.191" y1="-2.159" x2="-3.683" y2="2.159" layer="51"/>
<wire x1="3.556" y1="-1.6002" x2="3.556" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="3.556" y1="2.0828" x2="3.556" y2="1.6002" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-2.0828" x2="-3.556" y2="-1.6002" width="0.1524" layer="21"/>
<rectangle x1="-1.778" y1="-2.0828" x2="-0.508" y2="2.0828" layer="21"/>
<wire x1="-3.556" y1="1.6002" x2="-3.556" y2="2.0828" width="0.1524" layer="21"/>
<rectangle x1="-3.556" y1="1.524" x2="-1.778" y2="2.0828" layer="21" rot="R180"/>
</package>
<package name="SO14">
<description>&lt;h3&gt;SO-14 Small outline integrated circuit&lt;/h3&gt;
&lt;p&gt;Alias: SO-14 Narrow, SO-14-N, SOIC-14, SOIC-14-N, JEDEC MS-012&lt;/p&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Pitch: 1.27 mm (50 mil)&lt;/li&gt;
&lt;li&gt;Body width, W&lt;sub&gt;B&lt;/sub&gt;: 4mm&lt;/li&gt;
&lt;li&gt;Lead-to-lead width, W&lt;sub&gt;L&lt;/sub&gt;: 6mm&lt;/li&gt;
&lt;li&gt;Pin count: 14&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="4.318" y1="-1.905" x2="-4.318" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-4.318" y1="-1.143" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.318" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="4.318" y1="-1.143" x2="4.318" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="1.905" x2="4.318" y2="1.905" width="0.1524" layer="51"/>
<rectangle x1="-4.055" y1="-3.1" x2="-3.565" y2="-2" layer="51"/>
<rectangle x1="-2.785" y1="-3.1" x2="-2.295" y2="-2" layer="51"/>
<rectangle x1="-1.515" y1="-3.1" x2="-1.025" y2="-2" layer="51"/>
<rectangle x1="-0.245" y1="-3.1" x2="0.245" y2="-2" layer="51"/>
<rectangle x1="-0.245" y1="2" x2="0.245" y2="3.1" layer="51"/>
<rectangle x1="-1.515" y1="2" x2="-1.025" y2="3.1" layer="51"/>
<rectangle x1="-2.785" y1="2" x2="-2.295" y2="3.1" layer="51"/>
<rectangle x1="-4.055" y1="2" x2="-3.565" y2="3.1" layer="51"/>
<rectangle x1="1.025" y1="-3.1" x2="1.515" y2="-2" layer="51"/>
<rectangle x1="2.295" y1="-3.1" x2="2.785" y2="-2" layer="51"/>
<rectangle x1="3.565" y1="-3.1" x2="4.055" y2="-2" layer="51"/>
<rectangle x1="3.565" y1="2" x2="4.055" y2="3.1" layer="51"/>
<rectangle x1="2.295" y1="2" x2="2.785" y2="3.1" layer="51"/>
<rectangle x1="1.025" y1="2" x2="1.515" y2="3.1" layer="51"/>
<smd name="1" x="-3.81" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2" x="-2.54" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.27" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="0" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.27" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="2.54" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.81" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="3.81" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="2.54" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="1.27" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="0" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="-1.27" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-2.54" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-3.81" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-4.445" y="-1.905" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.445" y1="2.032" x2="-4.445" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="-4.445" y1="-2.032" x2="4.445" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="4.445" y1="-2.032" x2="4.445" y2="2.032" width="0.0508" layer="39"/>
<wire x1="4.445" y1="2.032" x2="-4.445" y2="2.032" width="0.0508" layer="39"/>
<wire x1="-4.318" y1="-1.905" x2="-4.318" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-4.318" y1="-1.905" x2="-4.318" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.651" x2="-4.318" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.397" x2="-4.318" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.318" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.143" x2="-3.556" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.143" x2="-3.683" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="-1.143" x2="-3.81" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.143" x2="-4.064" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.143" x2="-4.318" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.905" x2="-3.556" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="-1.143" x2="-4.318" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.778" x2="-4.318" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.651" x2="-3.81" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.143" x2="-3.937" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.143" x2="-4.318" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.524" x2="-4.318" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.397" x2="-4.064" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-3.714" y="-1.301" radius="0.2499" width="0.254" layer="51"/>
</package>
<package name="TSSOP14">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 14&lt;/b&gt;</description>
<wire x1="-2.5146" y1="-2.0828" x2="-2.2606" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-2.2606" y1="-2.0828" x2="-2.0574" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-2.0828" x2="-1.8542" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-1.8542" y1="-2.0828" x2="-1.651" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="2.5146" y1="2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="2.5146" y1="2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="21"/>
<rectangle x1="1.8034" y1="2.1082" x2="2.0066" y2="2.9464" layer="51"/>
<rectangle x1="1.1684" y1="2.1082" x2="1.3716" y2="2.9464" layer="51"/>
<rectangle x1="0.5334" y1="2.1082" x2="0.7366" y2="2.9464" layer="51"/>
<rectangle x1="-0.1016" y1="2.1082" x2="0.1016" y2="2.9464" layer="51"/>
<rectangle x1="-0.7366" y1="2.1082" x2="-0.5334" y2="2.9464" layer="51"/>
<rectangle x1="-1.3716" y1="2.1082" x2="-1.1684" y2="2.9464" layer="51"/>
<rectangle x1="-2.0066" y1="2.1082" x2="-1.8034" y2="2.9464" layer="51"/>
<rectangle x1="-2.0066" y1="-2.921" x2="-1.8034" y2="-2.0828" layer="51"/>
<rectangle x1="-1.3716" y1="-2.921" x2="-1.1684" y2="-2.0828" layer="51"/>
<rectangle x1="-0.7366" y1="-2.921" x2="-0.5334" y2="-2.0828" layer="51"/>
<rectangle x1="-0.1016" y1="-2.921" x2="0.1016" y2="-2.0828" layer="51"/>
<rectangle x1="0.5334" y1="-2.921" x2="0.7366" y2="-2.0828" layer="51"/>
<rectangle x1="1.1684" y1="-2.921" x2="1.3716" y2="-2.0828" layer="51"/>
<rectangle x1="1.8034" y1="-2.921" x2="2.0066" y2="-2.0828" layer="51"/>
<smd name="1" x="-1.905" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-1.27" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-0.635" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="0" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="0.635" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="1.27" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="1.905" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.905" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="1.27" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="0.635" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="0" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="-0.635" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="-1.27" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="-1.905" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-2.6416" y="-2.0828" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<wire x1="-2.5146" y1="-2.0828" x2="-2.5146" y2="-1.8288" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.8288" x2="-2.5146" y2="-1.6256" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.6256" x2="-2.5146" y2="-1.4224" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.4224" x2="-2.5146" y2="-1.2192" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.2192" x2="-2.5146" y2="2.0828" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="2.159" x2="2.667" y2="2.159" width="0.0508" layer="39"/>
<wire x1="2.667" y1="2.159" x2="2.667" y2="-2.159" width="0.0508" layer="39"/>
<wire x1="2.667" y1="-2.159" x2="-2.667" y2="-2.159" width="0.0508" layer="39"/>
<wire x1="-2.667" y1="-2.159" x2="-2.667" y2="2.159" width="0.0508" layer="39"/>
<wire x1="2.5146" y1="2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-1.2192" x2="-1.651" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.8288" x2="-2.2606" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.6256" x2="-2.0574" y2="-2.0828" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-1.4224" x2="-1.8542" y2="-2.0828" width="0.1524" layer="21"/>
<circle x="-1.936" y="-1.555" radius="0.2499" width="0.254" layer="51"/>
</package>
<package name="MICRO_SD_SOCKET-PP">
<description>Micro SD socket</description>
<wire x1="-6.65" y1="13.2" x2="-6.65" y2="9.75" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="7.05" x2="-6.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="1.1" x2="-6.15" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="1.1" x2="6.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="5.4" y1="1.1" x2="6.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6.65" y1="1.1" x2="6.65" y2="7.05" width="0.2032" layer="21"/>
<wire x1="6.65" y1="9.75" x2="6.65" y2="13.2" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.5" x2="0" y2="14.25" width="0" layer="47" style="dashdot"/>
<wire x1="-6.65" y1="13.2" x2="6.65" y2="13.2" width="0.2032" layer="21"/>
<wire x1="0.25" y1="9.5" x2="0.25" y2="22.75" width="0" layer="47" style="dashdot"/>
<wire x1="-5.25" y1="13.35" x2="-5.25" y2="19.25" width="0.127" layer="51" style="shortdash"/>
<wire x1="-5.25" y1="19.25" x2="-4.5" y2="20" width="0.127" layer="51" style="shortdash" curve="-90"/>
<wire x1="-4.5" y1="20" x2="5" y2="20" width="0.127" layer="51" style="shortdash"/>
<wire x1="5" y1="20" x2="5.75" y2="19.25" width="0.127" layer="51" style="shortdash" curve="-90"/>
<wire x1="5.75" y1="19.25" x2="5.75" y2="13.35" width="0.127" layer="51" style="shortdash"/>
<smd name="SHIELD1" x="6.7" y="8.4" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="-2.95" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="-1.85" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-0.75" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="0.35" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="5" x="1.45" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="6" x="2.55" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="3.65" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="4.75" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="GND" x="-4.2" y="0" dx="2" dy="0.85" layer="1" rot="R90"/>
<smd name="CD" x="-5.45" y="0" dx="2" dy="0.85" layer="1" rot="R90"/>
<smd name="SHIELD2" x="-6.7" y="8.4" dx="2" dy="1.2" layer="1" rot="R90"/>
<text x="-7" y="1" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<hole x="5" y="3.6" drill="1.5"/>
<hole x="-5" y="3.6" drill="1.5"/>
<circle x="5" y="3.6" radius="0.9" width="0.127" layer="41"/>
<circle x="5" y="3.6" radius="0.9" width="0.127" layer="42"/>
<circle x="-5" y="3.6" radius="0.9" width="0.127" layer="42"/>
<circle x="-5" y="3.6" radius="0.9" width="0.127" layer="42"/>
<circle x="4.99" y="3.6" radius="0.9" width="0.127" layer="42"/>
<circle x="-5" y="3.6" radius="0.9" width="0.127" layer="41"/>
<rectangle x1="-4.5" y1="-0.25" x2="-3.9" y2="1" layer="51"/>
<rectangle x1="-5.75" y1="-0.25" x2="-5.15" y2="1" layer="51"/>
<rectangle x1="-3.2" y1="-0.25" x2="-2.7" y2="1" layer="51"/>
<rectangle x1="-2.1" y1="-0.25" x2="-1.6" y2="1" layer="51"/>
<rectangle x1="-1" y1="-0.25" x2="-0.5" y2="1" layer="51"/>
<rectangle x1="0.1" y1="-0.25" x2="0.6" y2="1" layer="51"/>
<rectangle x1="1.2" y1="-0.25" x2="1.7" y2="1" layer="51"/>
<rectangle x1="2.3" y1="-0.25" x2="2.8" y2="1" layer="51"/>
<rectangle x1="3.4" y1="-0.25" x2="3.9" y2="1" layer="51"/>
<rectangle x1="4.5" y1="-0.25" x2="5" y2="1" layer="51"/>
<rectangle x1="6.75" y1="7.8" x2="7.25" y2="9.05" layer="51"/>
<rectangle x1="-7.25" y1="7.8" x2="-6.75" y2="9.05" layer="51"/>
<wire x1="-6.65" y1="13.2" x2="-6.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="13.2" x2="6.65" y2="13.2" width="0.2032" layer="51"/>
<wire x1="6.65" y1="1.1" x2="6.65" y2="13.2" width="0.2032" layer="51"/>
</package>
<package name="TSSOP20">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 20&lt;/b&gt;&lt;p&gt;
MAX3223-MAX3243.pdf</description>
<wire x1="-2.286" y1="-2.286" x2="3.1496" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.1496" y1="2.286" x2="3.1496" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.1496" y1="2.286" x2="-3.1496" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-1.4224" x2="-3.1496" y2="2.286" width="0.1524" layer="21"/>
<smd name="1" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-3.302" y="-2.286" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-3.0266" y1="-3.121" x2="-2.8234" y2="-2.2828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="-3.121" x2="3.0266" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="2.2828" x2="3.0266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<rectangle x1="-3.0266" y1="2.2828" x2="-2.8234" y2="3.121" layer="51"/>
<wire x1="-3.1496" y1="-2.286" x2="-2.8956" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.8956" y1="-2.286" x2="-2.6924" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.6924" y1="-2.286" x2="-2.4892" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.4892" y1="-2.286" x2="-2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-2.286" x2="-3.1496" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-2.032" x2="-3.1496" y2="-1.8288" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-1.8288" x2="-3.1496" y2="-1.6256" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-1.6256" x2="-3.1496" y2="-1.4224" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-1.4224" x2="-2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-2.032" x2="-2.8956" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-1.8288" x2="-2.6924" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="-1.6256" x2="-2.4892" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.1496" y1="2.286" x2="-3.1496" y2="2.286" width="0.1524" layer="51"/>
<wire x1="3.1496" y1="2.286" x2="3.1496" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-3.1496" y1="-2.286" x2="-3.1496" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.1496" y1="-2.286" x2="3.1496" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="2.413" x2="3.302" y2="2.413" width="0.0508" layer="39"/>
<wire x1="3.302" y1="2.413" x2="3.302" y2="-2.413" width="0.0508" layer="39"/>
<wire x1="3.302" y1="-2.413" x2="-3.302" y2="-2.413" width="0.0508" layer="39"/>
<wire x1="-3.302" y1="-2.413" x2="-3.302" y2="2.413" width="0.0508" layer="39"/>
<circle x="-2.571" y="-1.682" radius="0.2499" width="0.254" layer="51"/>
</package>
<package name="TQFP64">
<description>64-Lead TQFP Plastic Thin Quad Flatpack - 10x10x1mm Body, 2mmFP</description>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-5.08" x2="-4.064" y2="-5.08" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="-5.08" x2="-5.08" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="-4.064" x2="-5.08" y2="5.08" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="4.064" x2="-5.08" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-4.064" y2="5.08" width="0.1524" layer="21"/>
<wire x1="4.064" y1="5.08" x2="5.08" y2="5.08" width="0.1524" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="4.064" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-4.064" width="0.1524" layer="21"/>
<smd name="8" x="-0.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="9" x="0.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="6" x="-1.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="4" x="-2.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="2" x="-3.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="7" x="-0.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="5" x="-1.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="3" x="-2.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="1" x="-3.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="11" x="1.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="13" x="2.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="15" x="3.25" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="16" x="3.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="14" x="2.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="12" x="1.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="10" x="0.75" y="-5.75" dx="0.22" dy="1" layer="1"/>
<smd name="24" x="5.75" y="-0.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="25" x="5.75" y="0.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="22" x="5.75" y="-1.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="20" x="5.75" y="-2.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="18" x="5.75" y="-3.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="23" x="5.75" y="-0.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="21" x="5.75" y="-1.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="19" x="5.75" y="-2.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="17" x="5.75" y="-3.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="27" x="5.75" y="1.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="29" x="5.75" y="2.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="31" x="5.75" y="3.25" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="32" x="5.75" y="3.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="30" x="5.75" y="2.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="28" x="5.75" y="1.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="26" x="5.75" y="0.75" dx="0.22" dy="1" layer="1" rot="R90"/>
<smd name="40" x="0.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="41" x="-0.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="38" x="1.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="36" x="2.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="34" x="3.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="39" x="0.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="37" x="1.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="35" x="2.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="33" x="3.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="43" x="-1.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="45" x="-2.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="47" x="-3.25" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="48" x="-3.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="46" x="-2.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="44" x="-1.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="42" x="-0.75" y="5.75" dx="0.22" dy="1" layer="1" rot="R180"/>
<smd name="56" x="-5.75" y="0.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="57" x="-5.75" y="-0.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="54" x="-5.75" y="1.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="52" x="-5.75" y="2.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="50" x="-5.75" y="3.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="55" x="-5.75" y="0.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="53" x="-5.75" y="1.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="51" x="-5.75" y="2.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="49" x="-5.75" y="3.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="59" x="-5.75" y="-1.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="61" x="-5.75" y="-2.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="63" x="-5.75" y="-3.25" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="64" x="-5.75" y="-3.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="62" x="-5.75" y="-2.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="60" x="-5.75" y="-1.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<smd name="58" x="-5.75" y="-0.75" dx="0.22" dy="1" layer="1" rot="R270"/>
<text x="-4.064" y="-5.334" size="0.8128" layer="25" rot="R180">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.08" y1="-4.191" x2="-4.191" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.318" x2="-4.318" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.445" x2="-4.445" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.572" x2="-4.572" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.699" x2="-4.699" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-4.826" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-5.08" x2="-4.699" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-5.08" x2="-4.572" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-5.08" x2="-4.445" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-5.08" x2="-4.318" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-5.08" x2="-4.191" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="-5.08" x2="-4.064" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.064" x2="-5.08" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.191" x2="-5.08" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.318" x2="-5.08" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.445" x2="-5.08" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.572" x2="-5.08" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.699" x2="-5.08" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.826" x2="-5.08" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-4.826" x2="-4.826" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="5.207" x2="5.207" y2="5.207" width="0.0508" layer="39"/>
<wire x1="5.207" y1="5.207" x2="5.207" y2="-5.207" width="0.0508" layer="39"/>
<wire x1="5.207" y1="-5.207" x2="-5.207" y2="-5.207" width="0.0508" layer="39"/>
<wire x1="-5.207" y1="-5.207" x2="-5.207" y2="5.207" width="0.0508" layer="39"/>
<circle x="-3.841" y="-4.476" radius="0.2499" width="0.254" layer="51"/>
</package>
<package name="TQFP100">
<description>&lt;b&gt;100-lead Thin Quad Flat Pack Package Outline&lt;/b&gt;</description>
<wire x1="-6.223" y1="-6.985" x2="-6.985" y2="-6.223" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-6.223" x2="-6.985" y2="6.731" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="6.731" x2="-6.731" y2="6.985" width="0.1524" layer="51"/>
<wire x1="-6.731" y1="6.985" x2="6.731" y2="6.985" width="0.1524" layer="51"/>
<wire x1="6.731" y1="6.985" x2="6.985" y2="6.731" width="0.1524" layer="51"/>
<wire x1="6.985" y1="6.731" x2="6.985" y2="-6.731" width="0.1524" layer="51"/>
<wire x1="6.985" y1="-6.731" x2="6.731" y2="-6.985" width="0.1524" layer="51"/>
<wire x1="6.731" y1="-6.985" x2="-6.223" y2="-6.985" width="0.1524" layer="51"/>
<circle x="-6" y="-6.381" radius="0.2499" width="0.254" layer="51"/>
<smd name="1" x="-6" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-5.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="-5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="-4.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-4" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-3.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-3" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-2.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-2" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="-1.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-1" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="-0.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="0" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="0.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="1" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="1.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="2" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="2.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="3" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="3.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="4" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="4.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="5.5" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="6" y="-8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="26" x="8" y="-6" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="27" x="8" y="-5.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="28" x="8" y="-5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="29" x="8" y="-4.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="30" x="8" y="-4" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="31" x="8" y="-3.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="32" x="8" y="-3" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="33" x="8" y="-2.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="34" x="8" y="-2" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="35" x="8" y="-1.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="36" x="8" y="-1" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="37" x="8" y="-0.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="38" x="8" y="0" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="39" x="8" y="0.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="40" x="8" y="1" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="41" x="8" y="1.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="42" x="8" y="2" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="43" x="8" y="2.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="44" x="8" y="3" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="45" x="8" y="3.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="46" x="8" y="4" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="47" x="8" y="4.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="48" x="8" y="5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="49" x="8" y="5.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="50" x="8" y="6" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="51" x="6" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="52" x="5.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="53" x="5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="54" x="4.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="55" x="4" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="56" x="3.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="57" x="3" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="58" x="2.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="59" x="2" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="60" x="1.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="61" x="1" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="62" x="0.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="63" x="0" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="64" x="-0.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="65" x="-1" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="66" x="-1.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="67" x="-2" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="68" x="-2.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="69" x="-3" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="70" x="-3.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="71" x="-4" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="72" x="-4.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="73" x="-5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="74" x="-5.5" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="75" x="-6" y="8" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="76" x="-8" y="6" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="77" x="-8" y="5.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="78" x="-8" y="5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="79" x="-8" y="4.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="80" x="-8" y="4" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="81" x="-8" y="3.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="82" x="-8" y="3" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="83" x="-8" y="2.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="84" x="-8" y="2" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="85" x="-8" y="1.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="86" x="-8" y="1" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="87" x="-8" y="0.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="88" x="-8" y="0" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="89" x="-8" y="-0.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="90" x="-8" y="-1" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="91" x="-8" y="-1.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="92" x="-8" y="-2" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="93" x="-8" y="-2.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="94" x="-8" y="-3" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="95" x="-8" y="-3.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="96" x="-8" y="-4" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="97" x="-8" y="-4.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="98" x="-8" y="-5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="99" x="-8" y="-5.5" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<smd name="100" x="-8" y="-6" dx="0.3" dy="1.5" layer="1" rot="R90"/>
<text x="-6.35" y="-7.62" size="0.8128" layer="25" rot="R180">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<rectangle x1="-6.525" y1="-7.8251" x2="-5.4751" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-6.025" y1="-7.825" x2="-4.9751" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="-5.525" y1="-7.8251" x2="-4.4751" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-5.025" y1="-7.825" x2="-3.9751" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="-4.525" y1="-7.8251" x2="-3.4751" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-4.025" y1="-7.825" x2="-2.9751" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="-3.525" y1="-7.8251" x2="-2.4751" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="-7.825" x2="-1.9751" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="-2.525" y1="-7.8251" x2="-1.4751" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-2.025" y1="-7.825" x2="-0.9751" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="-1.525" y1="-7.8251" x2="-0.4751" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-1.025" y1="-7.825" x2="0.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="-0.525" y1="-7.8251" x2="0.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="-0.025" y1="-7.825" x2="1.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="0.475" y1="-7.8251" x2="1.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="0.975" y1="-7.825" x2="2.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="1.475" y1="-7.8251" x2="2.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="1.975" y1="-7.825" x2="3.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="2.475" y1="-7.8251" x2="3.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="2.975" y1="-7.825" x2="4.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="3.475" y1="-7.8251" x2="4.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="3.975" y1="-7.825" x2="5.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="4.475" y1="-7.8251" x2="5.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="4.975" y1="-7.825" x2="6.0249" y2="-7.5249" layer="51" rot="R90"/>
<rectangle x1="5.475" y1="-7.8251" x2="6.5249" y2="-7.5248" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-6.5249" x2="7.8251" y2="-5.475" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-6.0249" x2="7.825" y2="-4.975" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-5.5249" x2="7.8251" y2="-4.475" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-5.0249" x2="7.825" y2="-3.975" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-4.5249" x2="7.8251" y2="-3.475" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-4.0249" x2="7.825" y2="-2.975" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-3.5249" x2="7.8251" y2="-2.475" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-3.0249" x2="7.825" y2="-1.975" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-2.5249" x2="7.8251" y2="-1.475" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-2.0249" x2="7.825" y2="-0.975" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-1.5249" x2="7.8251" y2="-0.475" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-1.0249" x2="7.825" y2="0.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="-0.5249" x2="7.8251" y2="0.525" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="-0.0249" x2="7.825" y2="1.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="0.4751" x2="7.8251" y2="1.525" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="0.9751" x2="7.825" y2="2.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="1.4751" x2="7.8251" y2="2.525" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="1.9751" x2="7.825" y2="3.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="2.4751" x2="7.8251" y2="3.525" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="2.9751" x2="7.825" y2="4.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="3.4751" x2="7.8251" y2="4.525" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="3.9751" x2="7.825" y2="5.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="4.4751" x2="7.8251" y2="5.525" layer="51" rot="R90"/>
<rectangle x1="7.5249" y1="4.9751" x2="7.825" y2="6.025" layer="51" rot="R90"/>
<rectangle x1="7.5248" y1="5.4751" x2="7.8251" y2="6.525" layer="51" rot="R90"/>
<rectangle x1="5.475" y1="7.5249" x2="6.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="4.975" y1="7.525" x2="6.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="4.475" y1="7.5249" x2="5.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="3.975" y1="7.525" x2="5.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="3.475" y1="7.5249" x2="4.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="2.975" y1="7.525" x2="4.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="2.475" y1="7.5249" x2="3.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="1.975" y1="7.525" x2="3.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="1.475" y1="7.5249" x2="2.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="0.975" y1="7.525" x2="2.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="0.475" y1="7.5249" x2="1.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-0.025" y1="7.525" x2="1.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-0.525" y1="7.5249" x2="0.5249" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-1.025" y1="7.525" x2="0.0249" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-1.525" y1="7.5249" x2="-0.4751" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-2.025" y1="7.525" x2="-0.9751" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-2.525" y1="7.5249" x2="-1.4751" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="7.525" x2="-1.9751" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-3.525" y1="7.5249" x2="-2.4751" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-4.025" y1="7.525" x2="-2.9751" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-4.525" y1="7.5249" x2="-3.4751" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-5.025" y1="7.525" x2="-3.9751" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-5.525" y1="7.5249" x2="-4.4751" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-6.025" y1="7.525" x2="-4.9751" y2="7.8251" layer="51" rot="R90"/>
<rectangle x1="-6.525" y1="7.5249" x2="-5.4751" y2="7.8252" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="5.4751" x2="-7.5249" y2="6.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="4.9751" x2="-7.525" y2="6.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="4.4751" x2="-7.5249" y2="5.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="3.9751" x2="-7.525" y2="5.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="3.4751" x2="-7.5249" y2="4.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="2.9751" x2="-7.525" y2="4.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="2.4751" x2="-7.5249" y2="3.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="1.9751" x2="-7.525" y2="3.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="1.4751" x2="-7.5249" y2="2.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="0.9751" x2="-7.525" y2="2.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="0.4751" x2="-7.5249" y2="1.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-0.0249" x2="-7.525" y2="1.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-0.5249" x2="-7.5249" y2="0.525" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-1.0249" x2="-7.525" y2="0.025" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-1.5249" x2="-7.5249" y2="-0.475" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-2.0249" x2="-7.525" y2="-0.975" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-2.5249" x2="-7.5249" y2="-1.475" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-3.0249" x2="-7.525" y2="-1.975" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-3.5249" x2="-7.5249" y2="-2.475" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-4.0249" x2="-7.525" y2="-2.975" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-4.5249" x2="-7.5249" y2="-3.475" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-5.0249" x2="-7.525" y2="-3.975" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-5.5249" x2="-7.5249" y2="-4.475" layer="51" rot="R90"/>
<rectangle x1="-7.8251" y1="-6.0249" x2="-7.525" y2="-4.975" layer="51" rot="R90"/>
<rectangle x1="-7.8252" y1="-6.5249" x2="-7.5249" y2="-5.475" layer="51" rot="R90"/>
<wire x1="-6.223" y1="-6.985" x2="-6.985" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-6.223" x2="-6.985" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.731" x2="-6.731" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="6.985" x2="6.731" y2="6.985" width="0.1524" layer="21"/>
<wire x1="6.731" y1="6.985" x2="6.985" y2="6.731" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.731" x2="6.985" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-6.731" x2="6.731" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-6.985" x2="-6.223" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-6.223" x2="-6.985" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-6.35" x2="-6.985" y2="-6.477" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-6.477" x2="-6.985" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-6.604" x2="-6.985" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-6.731" x2="-6.985" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-6.985" x2="-6.35" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-6.985" x2="-6.477" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-6.985" x2="-6.604" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-6.985" x2="-6.731" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-6.985" x2="-6.985" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-6.985" x2="-6.985" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-6.985" x2="-6.985" y2="-6.477" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-6.985" x2="-6.985" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-6.985" x2="-6.985" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="-7.112" x2="-7.112" y2="7.112" width="0.0508" layer="39"/>
<wire x1="-7.112" y1="7.112" x2="7.112" y2="7.112" width="0.0508" layer="39"/>
<wire x1="7.112" y1="7.112" x2="7.112" y2="-7.112" width="0.0508" layer="39"/>
<wire x1="7.112" y1="-7.112" x2="-7.112" y2="-7.112" width="0.0508" layer="39"/>
</package>
<package name="L1212">
<description>&lt;b&gt;Shielded Inductors&lt;/b&gt;&lt;p&gt;

12.3 x 12.3 mm&lt;p&gt;

Compatible: CDRH125, CDRH127</description>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.1524" layer="21"/>
<wire x1="6.5" y1="6.5" x2="6.5" y2="3" width="0.1524" layer="21"/>
<wire x1="6.5" y1="6.5" x2="6.5" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="6.5" y1="-3" x2="6.5" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="-3" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="6.5" width="0.1524" layer="51"/>
<wire x1="-6.5" y1="3" x2="-6.5" y2="6.5" width="0.1524" layer="21"/>
<wire x1="-4.5254" y1="3.783" x2="-3.8006" y2="4.5431" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="-3.783" y1="-4.5254" x2="-4.5431" y2="-3.8006" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="4.5254" y1="-3.783" x2="3.8006" y2="-4.5431" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="3.783" y1="4.5254" x2="4.5431" y2="3.8006" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="5.9" x2="4.9" y2="3.275" width="0.1524" layer="21" curve="-56.209779"/>
<wire x1="0" y1="5.9" x2="-4.95" y2="3.225" width="0.1524" layer="21" curve="56.95663"/>
<wire x1="0" y1="-5.9" x2="-4.9" y2="-3.275" width="0.1524" layer="21" curve="-56.209779"/>
<wire x1="0" y1="-5.9" x2="4.95" y2="-3.225" width="0.1524" layer="21" curve="56.95663"/>
<circle x="0" y="0" radius="5.9" width="0.1524" layer="51"/>
<smd name="1" x="-5.3" y="0" dx="5.6" dy="3.6" layer="1" rot="R90"/>
<smd name="2" x="5.3" y="0" dx="5.6" dy="3.6" layer="1" rot="R90"/>
<text x="-6.564" y="6.81" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.1524" layer="51"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="6.604" x2="6.604" y2="6.604" width="0.0508" layer="39"/>
<wire x1="6.604" y1="6.604" x2="6.604" y2="-6.604" width="0.0508" layer="39"/>
<wire x1="6.604" y1="-6.604" x2="-6.604" y2="-6.604" width="0.0508" layer="39"/>
<wire x1="-6.604" y1="-6.604" x2="-6.604" y2="6.604" width="0.0508" layer="39"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.762" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="square"/>
<text x="-2.54" y="1.397" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.905" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="51"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="51" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="51" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="51" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="51" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.1524" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.1524" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="square"/>
<text x="2.032" y="1.905" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<text x="0.381" y="-1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.1524" layer="21" curve="-61.926949"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.1524" layer="21" curve="-50.193108"/>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.1524" layer="21" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.1524" layer="21" curve="31.60822"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="square"/>
<text x="2.794" y="1.905" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<text x="3.937" y="1.905" size="0.8128" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="51" curve="-286.260205"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="51"/>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="51" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="51" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="51" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="51" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="51" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="51" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="51"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="51"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-1.27" y="1.27" size="0.8128" layer="25">&gt;NAME</text>
<text x="1.27" y="-1.27" size="0.8128" layer="27" rot="R180">&gt;VALUE</text>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.1524" layer="21" curve="-306.869898"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.1524" layer="21"/>
</package>
<package name="SO16">
<description>&lt;h3&gt;SO-16 Small outline integrated circuit&lt;/h3&gt;
&lt;p&gt;Alias: SO-16 Narrow, SO-16-N, SOIC-16, SOIC-16-N, JEDEC MS-012&lt;/p&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Pitch: 1.27 mm (50 mil)&lt;/li&gt;
&lt;li&gt;Body width, W&lt;sub&gt;B&lt;/sub&gt;: 4mm&lt;/li&gt;
&lt;li&gt;Lead-to-lead width, W&lt;sub&gt;L&lt;/sub&gt;: 6mm&lt;/li&gt;
&lt;li&gt;Pin count: 16&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="0" y="0" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="0.8128" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
<wire x1="-5.207" y1="2.032" x2="5.207" y2="2.032" width="0.0508" layer="39"/>
<wire x1="5.207" y1="2.032" x2="5.207" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="5.207" y1="-2.032" x2="-5.207" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="-5.207" y1="-2.032" x2="-5.207" y2="2.032" width="0.0508" layer="39"/>
<wire x1="-5.08" y1="1.905" x2="5.08" y2="1.905" width="0.1524" layer="51"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.143" x2="-5.08" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.778" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.318" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.143" x2="-4.826" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.143" x2="-4.699" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-1.143" x2="-4.445" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.143" x2="5.08" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.143" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.778" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.651" x2="-4.572" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-1.143" x2="-4.699" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-1.143" x2="-5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-5.08" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.397" x2="-4.826" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.476" y="-1.301" radius="0.254" width="0.254" layer="51"/>
</package>
<package name="FODM30*">
<description>&lt;h2&gt;Compact 4-pin mini-flat package&lt;/h2&gt;
The lead pitch is 2.54mm.&lt;br&gt;
2.4 mm maximum standoff height&lt;br&gt;

source: Fairchild FODM3011, FODM3012, FODM3022, FODM3023, 
FODM3052, FODM3053 datasheet</description>
<smd name="1" x="-1.27" y="-3.302" dx="0.8" dy="1" layer="1"/>
<smd name="2" x="1.27" y="-3.302" dx="0.8" dy="1" layer="1"/>
<smd name="3" x="1.27" y="3.302" dx="0.8" dy="1" layer="1"/>
<smd name="4" x="-1.27" y="3.302" dx="0.8" dy="1" layer="1"/>
<rectangle x1="-1.27" y1="-1.27" x2="1.27" y2="1.27" layer="35"/>
<wire x1="-1.905" y1="2.286" x2="1.905" y2="2.286" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.286" x2="1.905" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="-1.016" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-2.286" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-2.286" x2="-1.524" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-2.286" x2="-1.651" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-2.286" x2="-1.905" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-1.905" y2="-2.032" width="0.1524" layer="21"/>
<rectangle x1="-1.4732" y1="2.286" x2="-1.0668" y2="3.429" layer="51"/>
<rectangle x1="1.0668" y1="2.286" x2="1.4732" y2="3.429" layer="51"/>
<rectangle x1="1.0668" y1="-3.429" x2="1.4732" y2="-2.286" layer="51" rot="R180"/>
<rectangle x1="-1.4732" y1="-3.429" x2="-1.0668" y2="-2.286" layer="51" rot="R180"/>
<circle x="-1.27" y="-1.651" radius="0.381" width="0.1778" layer="51"/>
<text x="0" y="0" size="0.8128" layer="27" rot="R270" align="center">&gt;VALUE</text>
<text x="-2.032" y="-2.286" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<wire x1="-1.905" y1="-2.032" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-1.905" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.524" x2="-1.905" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.397" x2="-1.905" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="2.413" x2="2.032" y2="2.413" width="0.0508" layer="39"/>
<wire x1="2.032" y1="2.413" x2="2.032" y2="-2.413" width="0.0508" layer="39"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.0508" layer="39"/>
<wire x1="-2.032" y1="-2.413" x2="-2.032" y2="2.413" width="0.0508" layer="39"/>
<wire x1="-1.905" y1="2.286" x2="1.905" y2="2.286" width="0.1524" layer="51"/>
<wire x1="1.905" y1="2.286" x2="1.905" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-2.286" x2="-1.905" y2="2.286" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-2.286" x2="-1.905" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-2.032" x2="-1.651" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.524" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-1.397" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-2.286" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.286" x2="-1.905" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.651" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.524" x2="-1.143" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-2.286" x2="-1.016" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.286" x2="-1.905" y2="-1.397" width="0.1524" layer="21"/>
</package>
<package name="D0603">
<description>&lt;h3&gt;Diode SMD Chip 0603&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 1.6 x 0.8 mm, Metric Code Size 1608&lt;/p&gt;
&lt;p&gt;power 0.100W&lt;p&gt;</description>
<smd name="A" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="K" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-1.397" y="0.635" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-1.4224" y1="0.5588" x2="1.4224" y2="0.5588" width="0.0254" layer="51"/>
<wire x1="1.4224" y1="0.5588" x2="1.4224" y2="-0.5588" width="0.0254" layer="51"/>
<wire x1="1.4224" y1="-0.5588" x2="-1.4224" y2="-0.5588" width="0.0254" layer="51"/>
<wire x1="-1.4224" y1="-0.5588" x2="-1.4224" y2="0.5588" width="0.0254" layer="51"/>
<rectangle x1="0" y1="-0.381" x2="0.254" y2="0.381" layer="21"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0" x2="0.508" y2="0" width="0.0254" layer="51"/>
<wire x1="0.508" y1="0" x2="1.27" y2="0" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="0.508" x2="-0.508" y2="-0.508" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="-0.508" x2="0.508" y2="0" width="0.0254" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="0.508" width="0.0254" layer="51"/>
<wire x1="0.508" y1="0.508" x2="0.508" y2="-0.508" width="0.0254" layer="51"/>
</package>
<package name="D0805">
<description>&lt;h3&gt;Diode SMD Chip 0805&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 2.0 x 1.25 mm, Metric Code Size 2012&lt;/p&gt;
&lt;p&gt;power 0.125W&lt;p&gt;</description>
<smd name="A" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="K" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.651" y="0.889" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.651" y="-1.778" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-1.651" y1="0.8128" x2="1.651" y2="0.8128" width="0.0254" layer="51"/>
<wire x1="1.651" y1="0.8128" x2="1.651" y2="-0.8128" width="0.0254" layer="51"/>
<wire x1="1.651" y1="-0.8128" x2="-1.651" y2="-0.8128" width="0.0254" layer="51"/>
<wire x1="-1.651" y1="-0.8128" x2="-1.651" y2="0.8128" width="0.0254" layer="51"/>
<rectangle x1="0" y1="-0.635" x2="0.254" y2="0.635" layer="21"/>
<wire x1="-1.016" y1="0.635" x2="1.016" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.016" y1="-0.635" x2="-1.016" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="-0.635" x2="-1.016" y2="0.635" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0" x2="0.508" y2="0" width="0.0254" layer="51"/>
<wire x1="0.508" y1="0" x2="1.27" y2="0" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="0.508" x2="-0.508" y2="-0.508" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="-0.508" x2="0.508" y2="0" width="0.0254" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="0.508" width="0.0254" layer="51"/>
<wire x1="0.508" y1="0.508" x2="0.508" y2="-0.508" width="0.0254" layer="51"/>
<wire x1="-0.127" y1="0.635" x2="0.127" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.127" y1="-0.635" x2="-0.127" y2="-0.635" width="0.127" layer="21"/>
</package>
<package name="D1206">
<description>&lt;h3&gt;Diode SMD Chip 1206&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 3.2 x 1.6 mm, Metric Code Size 3216&lt;/p&gt;
&lt;p&gt;power 0.250W&lt;p&gt;</description>
<wire x1="-2.286" y1="-1.016" x2="2.286" y2="-1.016" width="0.0254" layer="51"/>
<smd name="A" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="K" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-2.159" y="1.143" size="0.8128" layer="25">&gt;NAME</text>
<text x="-2.159" y="-1.905" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="0" y1="-0.762" x2="0.635" y2="0.762" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="2.286" y2="1.016" width="0.0254" layer="51"/>
<wire x1="2.286" y1="-1.016" x2="2.286" y2="1.016" width="0.0254" layer="51"/>
<wire x1="-2.286" y1="-1.016" x2="-2.286" y2="1.016" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="0.762" x2="1.524" y2="0.762" width="0.0508" layer="39"/>
<wire x1="1.524" y1="0.762" x2="1.524" y2="-0.762" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-0.762" x2="-1.524" y2="-0.762" width="0.0508" layer="39"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.0508" layer="39"/>
<wire x1="-1.778" y1="0" x2="0.762" y2="0" width="0.0254" layer="51"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.0254" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.0254" layer="51"/>
<wire x1="-0.762" y1="-0.762" x2="0.762" y2="0" width="0.0254" layer="51"/>
<wire x1="0.762" y1="0" x2="-0.762" y2="0.762" width="0.0254" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.0254" layer="51"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="0.762" x2="0.508" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
</package>
<package name="D1210">
<description>&lt;h3&gt;Diode SMD Chip 1210&lt;/h3&gt;
&lt;p&gt;&lt;i&gt;*Reflow solder&lt;/i&gt;&lt;p&gt;
&lt;p&gt;Size: 3.2 x 2.5 mm, Metric Code Size 3225&lt;/p&gt;
&lt;p&gt;power 0.500W&lt;p&gt;</description>
<wire x1="-2.286" y1="-1.4732" x2="2.286" y2="-1.4732" width="0.0254" layer="51"/>
<smd name="A" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="K" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-2.286" y="1.524" size="0.8128" layer="25">&gt;NAME</text>
<text x="-2.286" y="-2.413" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="0" y1="-1.27" x2="0.635" y2="1.27" layer="21"/>
<wire x1="-2.286" y1="1.4732" x2="2.286" y2="1.4732" width="0.0254" layer="51"/>
<wire x1="2.286" y1="-1.4732" x2="2.286" y2="1.4732" width="0.0254" layer="51"/>
<wire x1="-2.286" y1="-1.4732" x2="-2.286" y2="1.4732" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.0508" layer="39"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.0508" layer="39"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.0508" layer="39"/>
<wire x1="-1.651" y1="0" x2="0.889" y2="0" width="0.0254" layer="51"/>
<wire x1="0.889" y1="0" x2="1.651" y2="0" width="0.0254" layer="51"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="-1.143" width="0.0254" layer="51"/>
<wire x1="-0.889" y1="-1.143" x2="0.889" y2="0" width="0.0254" layer="51"/>
<wire x1="0.889" y1="0" x2="-0.889" y2="1.143" width="0.0254" layer="51"/>
<wire x1="0.889" y1="1.143" x2="0.889" y2="0" width="0.0254" layer="51"/>
<wire x1="0.889" y1="0" x2="0.889" y2="-1.143" width="0.0254" layer="51"/>
<wire x1="-0.508" y1="1.27" x2="0.508" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.127" layer="21"/>
</package>
<package name="SOD-323">
<description>&lt;h3&gt;SMD Diode SOD-323&lt;/h3&gt;
&lt;p&gt;Size: 2.6(1.7) x 1.3 x 1.0 mm&lt;/p&gt;
&lt;p&gt;Approx size: 0805&lt;/p&gt;
&lt;p&gt;(-) cathode side marked&lt;/p&gt;</description>
<wire x1="0.762" y1="-0.635" x2="-0.762" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.635" x2="-0.762" y2="0.635" width="0.1524" layer="21"/>
<smd name="K" x="1.15" y="0" dx="0.63" dy="0.83" layer="1" rot="R180"/>
<smd name="A" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1" rot="R180"/>
<text x="-1.524" y="0.762" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.254" y="0" size="0.6096" layer="27" rot="R180" align="center">&gt;VALUE</text>
<wire x1="0.889" y1="-0.762" x2="-0.889" y2="-0.762" width="0.0508" layer="39"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.0508" layer="39"/>
<wire x1="-0.889" y1="0.762" x2="0.889" y2="0.762" width="0.0508" layer="39"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.762" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-0.635" x2="-1.524" y2="-0.635" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="-0.635" x2="-1.524" y2="0.635" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="0.635" x2="1.524" y2="0.635" width="0.0254" layer="51"/>
<wire x1="1.524" y1="0.635" x2="1.524" y2="-0.635" width="0.0254" layer="51"/>
<rectangle x1="0.127" y1="-0.635" x2="0.762" y2="0.635" layer="21"/>
<rectangle x1="1.143" y1="-0.635" x2="1.397" y2="0.635" layer="51" rot="R180"/>
</package>
<package name="SOT23-3">
<description>&lt;h3&gt;SOT23-3&lt;/h3&gt;
&lt;p&gt;Alias: SOT23, TO236AB, SOT346, SC59A, TO236AA, SMT3&lt;/p&gt;
&lt;p&gt;pitch 1.9mm (2x0.95)&lt;/p&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="0.95" y="-1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="1" x="-0.95" y="-1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<text x="-1.397" y="-1.778" size="0.8128" layer="25" align="top-left">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<wire x1="-0.2794" y1="-0.6604" x2="0.2794" y2="-0.6604" width="0.1524" layer="21"/>
</package>
<package name="SMA">
<description>&lt;h3&gt;SMA Diode (DO-214AC)&lt;/h3&gt;
&lt;p&gt;Size: 5.0 x 2.7 x 2.3 mm (300 x 105 mil)
&lt;p&gt;Approx size: 2010&lt;/p&gt;
&lt;p&gt;(-) cathode side marked&lt;/p&gt;</description>
<wire x1="2.286" y1="-1.778" x2="-2.286" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.778" x2="2.286" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.778" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.778" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-1.904406"/>
<wire x1="-2.286" y1="-1.778" x2="-2.286" y2="-1.27" width="0.1524" layer="21"/>
<smd name="K" x="2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="A" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<text x="-2.286" y="1.905" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="0.762" size="0.8128" layer="27" ratio="10" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="1.524" y1="-1.778" x2="2.032" y2="1.778" layer="51" rot="R180"/>
<wire x1="2.413" y1="-1.905" x2="-2.413" y2="-1.905" width="0.0508" layer="39"/>
<wire x1="-2.413" y1="-1.905" x2="-2.413" y2="1.905" width="0.0508" layer="39"/>
<wire x1="-2.413" y1="1.905" x2="2.413" y2="1.905" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.905" x2="2.413" y2="-1.905" width="0.0508" layer="39"/>
<wire x1="2.286" y1="-1.778" x2="-2.286" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-1.778" x2="-2.286" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.778" x2="2.286" y2="1.778" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.524" x2="-0.889" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0.381" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.762" x2="-0.889" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="0.381" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.762" x2="1.016" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.381" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.762" x2="0.381" y2="-1.524" width="0.1524" layer="51"/>
<rectangle x1="0.508" y1="-1.778" x2="0.889" y2="1.778" layer="21"/>
<rectangle x1="0.889" y1="1.143" x2="1.524" y2="1.778" layer="21"/>
<rectangle x1="0.889" y1="-1.778" x2="1.524" y2="-1.143" layer="21"/>
</package>
<package name="SMB">
<description>&lt;h3&gt;SMB Diode (DO-214AA)&lt;/h3&gt;
&lt;p&gt;Size: 5.5 x 3.6 x 2.1 mm (215 x 140 mil)
&lt;p&gt;Approx size: 2114&lt;/p&gt;
&lt;p&gt;(-) cathode side marked&lt;/p&gt;</description>
<wire x1="2.286" y1="-1.397" x2="2.286" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.905" x2="-2.286" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.905" x2="-2.286" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.397" x2="2.286" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.905" x2="-2.286" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.905" x2="-2.286" y2="1.397" width="0.1524" layer="21"/>
<smd name="K" x="2.2" y="0" dx="2.4" dy="2.4" layer="1" rot="R180"/>
<smd name="A" x="-2.2" y="0" dx="2.4" dy="2.4" layer="1" rot="R180"/>
<text x="-2.286" y="2.159" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.381" y="1.016" size="0.8128" layer="27" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="1.524" y1="-1.905" x2="2.032" y2="1.905" layer="51" rot="R180"/>
<rectangle x1="0" y1="-1.905" x2="0.889" y2="1.905" layer="21" rot="R180"/>
<wire x1="2.286" y1="-1.905" x2="-2.286" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-1.905" x2="-2.286" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.905" x2="2.286" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.905" x2="2.286" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-2.032" x2="-2.413" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="-2.413" y1="-2.032" x2="-2.413" y2="2.032" width="0.0508" layer="39"/>
<wire x1="-2.413" y1="2.032" x2="2.413" y2="2.032" width="0.0508" layer="39"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="-1.651" x2="-1.016" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0.127" x2="0.381" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.762" x2="-1.016" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-0.762" x2="0.381" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.762" x2="1.016" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0.127" x2="0.381" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.762" x2="0.381" y2="-1.651" width="0.1524" layer="51"/>
<rectangle x1="0.889" y1="1.27" x2="2.286" y2="1.905" layer="21"/>
<rectangle x1="0.889" y1="-1.905" x2="2.286" y2="-1.27" layer="21"/>
</package>
<package name="SMC">
<description>&lt;h3&gt;SMC Diode (DO-214AB)&lt;/h3&gt;
&lt;p&gt;Size: 8.0 x 5.9 x 2.3 mm (315 x 235 mil)
&lt;p&gt;Approx size: 3220&lt;/p&gt;
&lt;p&gt;(-) cathode side marked&lt;/p&gt;</description>
<wire x1="3.556" y1="-2.159" x2="3.556" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-3.048" x2="-3.556" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-3.048" x2="-3.556" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="3.556" y1="2.159" x2="3.556" y2="3.048" width="0.1524" layer="21"/>
<wire x1="3.556" y1="3.048" x2="-3.556" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="3.048" x2="-3.556" y2="2.159" width="0.1524" layer="21"/>
<smd name="K" x="3.7" y="0" dx="2.8" dy="3.8" layer="1" rot="R180"/>
<smd name="A" x="-3.7" y="0" dx="2.8" dy="3.8" layer="1" rot="R180"/>
<text x="-3.556" y="3.302" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.508" y="1.016" size="0.8128" layer="27" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="2.54" y1="-3.048" x2="3.175" y2="3.048" layer="51" rot="R180"/>
<wire x1="-1.016" y1="-1.905" x2="-1.016" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-0.127" x2="0.381" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-1.016" x2="-1.016" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-1.016" x2="0.381" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-1.016" x2="1.016" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.127" x2="0.381" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-1.016" x2="0.381" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="3.683" y1="-3.175" x2="-3.683" y2="-3.175" width="0.0508" layer="39"/>
<wire x1="-3.683" y1="-3.175" x2="-3.683" y2="3.175" width="0.0508" layer="39"/>
<wire x1="-3.683" y1="3.175" x2="3.683" y2="3.175" width="0.0508" layer="39"/>
<wire x1="3.683" y1="3.175" x2="3.683" y2="-3.175" width="0.0508" layer="39"/>
<wire x1="3.556" y1="-3.048" x2="-3.556" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-3.048" x2="-3.556" y2="3.048" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="3.048" x2="3.556" y2="3.048" width="0.1524" layer="51"/>
<wire x1="3.556" y1="3.048" x2="3.556" y2="-3.048" width="0.1524" layer="51"/>
<rectangle x1="1.27" y1="-3.048" x2="2.159" y2="3.048" layer="21"/>
<rectangle x1="2.159" y1="-3.048" x2="2.794" y2="-2.032" layer="21"/>
<rectangle x1="2.159" y1="2.032" x2="2.794" y2="3.048" layer="21"/>
</package>
<package name="SOD-123">
<description>&lt;h3&gt;SMD Diode SOD-123&lt;/h3&gt;
&lt;p&gt;Size: 3.7(2.7) x 1.6 x 1.2 mm&lt;/p&gt;
&lt;p&gt;Approx size: 1206&lt;/p&gt;
&lt;p&gt;(-) cathode side marked&lt;/p&gt;</description>
<wire x1="0.9906" y1="-0.762" x2="-0.9906" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.9906" y1="0.762" x2="0.9906" y2="0.762" width="0.1524" layer="21"/>
<smd name="K" x="1.9" y="0" dx="1.4" dy="1.4" layer="1" rot="R180"/>
<smd name="A" x="-1.9" y="0" dx="1.4" dy="1.4" layer="1" rot="R180"/>
<text x="-2.54" y="0.889" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.381" y="0" size="0.8128" layer="27" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="0.381" y1="-0.762" x2="1.0668" y2="0.762" layer="21"/>
<wire x1="1.397" y1="-0.889" x2="-1.397" y2="-0.889" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="-0.889" x2="-1.397" y2="0.889" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="0.889" x2="1.397" y2="0.889" width="0.0508" layer="39"/>
<wire x1="1.397" y1="0.889" x2="1.397" y2="-0.889" width="0.0508" layer="39"/>
<rectangle x1="1.905" y1="-0.762" x2="2.413" y2="0.762" layer="51" rot="R180"/>
<wire x1="2.667" y1="-0.762" x2="-2.667" y2="-0.762" width="0.0254" layer="51"/>
<wire x1="-2.667" y1="-0.762" x2="-2.667" y2="0.762" width="0.0254" layer="51"/>
<wire x1="-2.667" y1="0.762" x2="2.667" y2="0.762" width="0.0254" layer="51"/>
<wire x1="2.667" y1="0.762" x2="2.667" y2="-0.762" width="0.0254" layer="51"/>
</package>
<package name="L0707">
<description>&lt;b&gt;Shielded Inductors&lt;/b&gt;&lt;p&gt;

7.5 x 7.5 mm&lt;p&gt;

Compatible: CDRH74</description>
<wire x1="-3.65" y1="3.65" x2="3.65" y2="3.65" width="0.1524" layer="21"/>
<wire x1="3.65" y1="3.65" x2="3.65" y2="1.4" width="0.1524" layer="21"/>
<wire x1="3.65" y1="3.65" x2="3.65" y2="-3.65" width="0.1524" layer="51"/>
<wire x1="3.65" y1="-1.4" x2="3.65" y2="-3.65" width="0.1524" layer="21"/>
<wire x1="3.65" y1="-3.65" x2="-3.65" y2="-3.65" width="0.1524" layer="21"/>
<wire x1="-3.65" y1="-3.65" x2="-3.65" y2="-1.4" width="0.1524" layer="21"/>
<wire x1="-3.65" y1="-3.65" x2="-3.65" y2="3.65" width="0.1524" layer="51"/>
<wire x1="-3.65" y1="1.4" x2="-3.65" y2="3.65" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="1.8" x2="-1.8" y2="2.1" width="0.5" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="2.7" x2="-2.35" y2="1.325" width="0.1524" layer="21" curve="60"/>
<circle x="0" y="0" radius="2.7" width="0.1524" layer="51"/>
<smd name="1" x="-3.3" y="0" dx="2.4" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="3.3" y="0" dx="2.4" dy="1.8" layer="1" rot="R90"/>
<text x="-3.643" y="3.889" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.65" y1="3.65" x2="3.65" y2="3.65" width="0.1524" layer="51"/>
<wire x1="3.65" y1="-3.65" x2="-3.65" y2="-3.65" width="0.1524" layer="51"/>
<wire x1="1.8" y1="2.1" x2="2.1" y2="1.8" width="0.5" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="2.1" y1="-1.8" x2="1.8" y2="-2.1" width="0.5" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="-1.8" y1="-2.1" x2="-2.1" y2="-1.8" width="0.5" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="2.7" x2="2.35" y2="1.325" width="0.1524" layer="21" curve="-60"/>
<wire x1="0" y1="-2.7" x2="-2.35" y2="-1.325" width="0.1524" layer="21" curve="-60"/>
<wire x1="0" y1="-2.7" x2="2.35" y2="-1.325" width="0.1524" layer="21" curve="60"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.0508" layer="39"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.0508" layer="39"/>
<wire x1="3.81" y1="-3.81" x2="-3.81" y2="-3.81" width="0.0508" layer="39"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.0508" layer="39"/>
</package>
<package name="L1010">
<description>&lt;b&gt;Shielded Inductors&lt;/b&gt;&lt;p&gt;

10.1 x 10.1 mm&lt;p&gt;

Compatible: CDRH104</description>
<wire x1="-5" y1="5" x2="5" y2="5" width="0.1524" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="-5" width="0.1524" layer="51"/>
<wire x1="5" y1="-2.5" x2="5" y2="-5" width="0.1524" layer="21"/>
<wire x1="5" y1="-5" x2="-5" y2="-5" width="0.1524" layer="21"/>
<wire x1="-5" y1="-5" x2="-5" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="-5" y1="-5" x2="-5" y2="5" width="0.1524" layer="51"/>
<wire x1="-5" y1="2.5" x2="-5" y2="5" width="0.1524" layer="21"/>
<wire x1="2.5" y1="3.1" x2="3.1" y2="2.5" width="0.8" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="4" x2="3.2" y2="2.4" width="0.1524" layer="21" curve="-55"/>
<wire x1="0" y1="4" x2="-3.2" y2="2.4" width="0.1524" layer="21" curve="55"/>
<wire x1="0" y1="-4" x2="-3.2" y2="-2.4" width="0.1524" layer="21" curve="-55"/>
<wire x1="0" y1="-4" x2="3.2" y2="-2.4" width="0.1524" layer="21" curve="55"/>
<circle x="0" y="0" radius="4" width="0.1524" layer="51"/>
<smd name="1" x="-4.6" y="0" dx="3.4" dy="2" layer="1" rot="R90"/>
<smd name="2" x="4.6" y="0" dx="3.4" dy="2" layer="1" rot="R90"/>
<text x="-4.913" y="5.286" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5" y1="5" x2="5" y2="5" width="0.1524" layer="51"/>
<wire x1="5" y1="-5" x2="-5" y2="-5" width="0.1524" layer="51"/>
<wire x1="-3.1" y1="2.5" x2="-2.5" y2="3.1" width="0.8" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="-2.5" y1="-3.1" x2="-3.1" y2="-2.5" width="0.8" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="3.1" y1="-2.5" x2="2.5" y2="-3.1" width="0.8" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.0508" layer="39"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.0508" layer="39"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.0508" layer="39"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.0508" layer="39"/>
</package>
<package name="L03">
<description>&lt;b&gt;Inductors&lt;/b&gt;&lt;p&gt;

3.5 x 3.0 mm&lt;p&gt;

Compatible: CD3x, SND030x</description>
<smd name="1" x="-1.4" y="0" dx="3.5" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="1.4" y="0" dx="3.5" dy="1.6" layer="1" rot="R90"/>
<text x="-2.032" y="1.905" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-0.8" y1="1.5" x2="0.8" y2="1.5" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-1.5" x2="0.8" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.1" y2="0" width="0.127" layer="51" curve="-90"/>
<wire x1="-1.7" y1="-0.6" x2="-1.1" y2="0" width="0.127" layer="51" curve="90"/>
<wire x1="1.7" y1="0.6" x2="1.1" y2="0" width="0.127" layer="51" curve="90"/>
<wire x1="1.7" y1="-0.6" x2="1.1" y2="0" width="0.127" layer="51" curve="-90"/>
<wire x1="0.8" y1="-1.5" x2="1.7" y2="-0.6" width="0.127" layer="51" curve="90"/>
<wire x1="0.8" y1="1.5" x2="1.7" y2="0.6" width="0.127" layer="51" curve="-90"/>
<wire x1="-0.8" y1="1.5" x2="-1.7" y2="0.6" width="0.127" layer="51" curve="90"/>
<wire x1="-0.8" y1="-1.5" x2="-1.7" y2="-0.6" width="0.127" layer="51" curve="-90"/>
<wire x1="-0.8" y1="1.5" x2="0.8" y2="1.5" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-1.5" x2="0.8" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.524" x2="1.778" y2="1.524" width="0.0508" layer="39"/>
<wire x1="1.778" y1="1.524" x2="1.778" y2="-1.524" width="0.0508" layer="39"/>
<wire x1="1.778" y1="-1.524" x2="-1.778" y2="-1.524" width="0.0508" layer="39"/>
<wire x1="-1.778" y1="-1.524" x2="-1.778" y2="1.524" width="0.0508" layer="39"/>
</package>
<package name="L05">
<description>&lt;b&gt;Inductors&lt;/b&gt;&lt;p&gt;

5.8 x 5.2 mm&lt;p&gt;

Compatible: CD5x, SND050x</description>
<smd name="1" x="-2.1" y="0" dx="5.5" dy="2.5" layer="1" rot="R90"/>
<smd name="2" x="2.1" y="0" dx="5.5" dy="2.5" layer="1" rot="R90"/>
<text x="-2.921" y="2.921" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.048" y1="2.667" x2="3.048" y2="2.667" width="0.0508" layer="39"/>
<wire x1="3.048" y1="2.667" x2="3.048" y2="-2.667" width="0.0508" layer="39"/>
<wire x1="3.048" y1="-2.667" x2="-3.048" y2="-2.667" width="0.0508" layer="39"/>
<wire x1="-3.048" y1="-2.667" x2="-3.048" y2="2.667" width="0.0508" layer="39"/>
<wire x1="-1" y1="-2.6" x2="1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-1" y1="2.6" x2="1" y2="2.6" width="0.127" layer="21"/>
<wire x1="1" y1="2.6" x2="2.9" y2="0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="1" y1="-2.6" x2="2.9" y2="-0.9" width="0.127" layer="51" curve="90"/>
<wire x1="-1" y1="-2.6" x2="-2.9" y2="-0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-1" y1="2.6" x2="-2.9" y2="0.9" width="0.127" layer="51" curve="90"/>
<wire x1="2.9" y1="-0.9" x2="2" y2="0" width="0.127" layer="51" curve="-90"/>
<wire x1="2" y1="0" x2="2.9" y2="0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-2.9" y1="0.9" x2="-2" y2="0" width="0.127" layer="51" curve="-90"/>
<wire x1="-2" y1="0" x2="-2.9" y2="-0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-1" y1="2.6" x2="1" y2="2.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-2.6" x2="1" y2="-2.6" width="0.127" layer="51"/>
</package>
<package name="L07">
<description>&lt;b&gt;Inductors&lt;/b&gt;&lt;p&gt;

7.8 x 7.0 mm&lt;p&gt;

Compatible: CD7x, SND070x</description>
<smd name="1" x="-2.7" y="0" dx="7.5" dy="3.4" layer="1" rot="R90"/>
<smd name="2" x="2.7" y="0" dx="7.5" dy="3.4" layer="1" rot="R90"/>
<text x="-3.937" y="3.937" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.937" y1="3.556" x2="3.937" y2="3.556" width="0.0508" layer="39"/>
<wire x1="3.937" y1="3.556" x2="3.937" y2="-3.556" width="0.0508" layer="39"/>
<wire x1="3.937" y1="-3.556" x2="-3.937" y2="-3.556" width="0.0508" layer="39"/>
<wire x1="-3.937" y1="-3.556" x2="-3.937" y2="3.556" width="0.0508" layer="39"/>
<wire x1="-1" y1="-3.5" x2="1" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-1" y1="3.5" x2="1" y2="3.5" width="0.127" layer="21"/>
<wire x1="1" y1="3.5" x2="3.9" y2="0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="1" y1="-3.5" x2="3.9" y2="-0.9" width="0.127" layer="51" curve="90"/>
<wire x1="-1" y1="-3.5" x2="-3.9" y2="-0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-1" y1="3.5" x2="-3.9" y2="0.9" width="0.127" layer="51" curve="90"/>
<wire x1="3.9" y1="-0.9" x2="3" y2="0" width="0.127" layer="51" curve="-90"/>
<wire x1="3" y1="0" x2="3.9" y2="0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.9" y1="0.9" x2="-3" y2="0" width="0.127" layer="51" curve="-90"/>
<wire x1="-3" y1="0" x2="-3.9" y2="-0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-1" y1="3.5" x2="1" y2="3.5" width="0.127" layer="51"/>
<wire x1="-1" y1="-3.5" x2="1" y2="-3.5" width="0.127" layer="51"/>
</package>
<package name="SJ">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.27" y1="-1.016" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="0.762" x2="-1.27" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="-0.762" x2="-1.27" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-2.032" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SJ-M">
<description>&lt;b&gt;Solder jumper Mini&lt;/b&gt;</description>
<wire x1="0.381" y1="-0.635" x2="-0.381" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.635" x2="0.635" y2="0.381" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.635" y1="0.381" x2="-0.381" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.635" y1="-0.381" x2="-0.381" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="-0.635" x2="0.635" y2="-0.381" width="0.1524" layer="21" curve="90"/>
<wire x1="0.635" y1="-0.381" x2="0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-0.381" x2="-0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.635" x2="0.381" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.127" y1="0.127" x2="0.127" y2="-0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.127" y1="-0.127" x2="-0.127" y2="0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.254" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0.254" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<text x="-1.016" y="0.762" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.651" size="0.8128" layer="27">&gt;VALUE</text>
</package>
<package name="SJ-M-12">
<description>&lt;b&gt;Solder jumper Mini, CLOSED&lt;/b&gt; by default</description>
<wire x1="0.381" y1="-0.635" x2="-0.381" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.635" x2="0.635" y2="0.381" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.635" y1="0.381" x2="-0.381" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.635" y1="-0.381" x2="-0.381" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="-0.635" x2="0.635" y2="-0.381" width="0.1524" layer="21" curve="90"/>
<wire x1="0.635" y1="-0.381" x2="0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-0.381" x2="-0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.635" x2="0.381" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.127" y1="0.127" x2="0.127" y2="-0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.127" y1="-0.127" x2="-0.127" y2="0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.254" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0.254" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<text x="-1.016" y="0.762" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.651" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.1778" layer="1"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="SJ2">
<description>&lt;b&gt;Solder jumper 2-sided&lt;/b&gt;</description>
<wire x1="1.143" y1="-0.889" x2="-1.143" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.889" x2="1.397" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="0.635" x2="-1.143" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="-0.635" x2="-1.143" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.143" y1="-0.889" x2="1.397" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-0.635" x2="1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.889" x2="1.143" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.127" x2="0.635" y2="-0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.635" y1="-0.127" x2="-0.635" y2="0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.381" y1="-0.635" x2="0.381" y2="0.635" layer="51"/>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-1.397" y="1.016" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.905" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-1.143" y1="0.635" x2="1.143" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="-0.635" x2="-1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="0.635" width="0.0508" layer="39"/>
</package>
<package name="SJ2-M">
<description>&lt;b&gt;Solder jumper 2-sided, Mini&lt;/b&gt;</description>
<wire x1="0.508" y1="-0.508" x2="-0.508" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.508" x2="0.762" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0.508" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="-0.254" x2="-0.508" y2="-0.508" width="0.1524" layer="21" curve="90"/>
<wire x1="0.508" y1="-0.508" x2="0.762" y2="-0.254" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="-0.254" x2="0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.254" x2="-0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.508" x2="0.508" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.127" y1="-0.381" x2="0.127" y2="0.381" layer="51"/>
<smd name="1" x="-0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="3" x="0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<text x="-0.889" y="0.635" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.524" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
</package>
<package name="SJ2-12">
<description>&lt;b&gt;Solder jumper 2-sided, CLOSED 1-2&lt;/b&gt; by default</description>
<wire x1="1.143" y1="-0.889" x2="-1.143" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.889" x2="1.397" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="0.635" x2="-1.143" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="-0.635" x2="-1.143" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.143" y1="-0.889" x2="1.397" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-0.635" x2="1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.889" x2="1.143" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.127" x2="0.635" y2="-0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.635" y1="-0.127" x2="-0.635" y2="0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-1.4478" y1="-1.016" x2="0.2794" y2="0.9398" layer="31"/>
<rectangle x1="-0.381" y1="-0.635" x2="0.381" y2="0.635" layer="51"/>
<rectangle x1="-0.762" y1="-0.381" x2="-0.254" y2="0.381" layer="51"/>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-1.397" y="1.016" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.905" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.635" y1="0" x2="-0.254" y2="0" width="0.1778" layer="1"/>
<wire x1="-1.143" y1="0.635" x2="1.143" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="-0.635" x2="-1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="0.635" width="0.0508" layer="39"/>
</package>
<package name="SJ2-23">
<description>&lt;b&gt;Solder jumper 2-sided, CLOSED 2-3&lt;/b&gt; by default</description>
<wire x1="1.143" y1="-0.889" x2="-1.143" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.889" x2="1.397" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="0.635" x2="-1.143" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="-0.635" x2="-1.143" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.143" y1="-0.889" x2="1.397" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-0.635" x2="1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.889" x2="1.143" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-0.127" x2="-0.635" y2="0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<wire x1="0.635" y1="0.127" x2="0.635" y2="-0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.2794" y1="-1.016" x2="1.4224" y2="0.9906" layer="31"/>
<rectangle x1="-0.381" y1="-0.635" x2="0.381" y2="0.635" layer="51" rot="R180"/>
<rectangle x1="0.254" y1="-0.381" x2="0.762" y2="0.381" layer="51" rot="R180"/>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-1.397" y="1.016" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.905" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="0.254" y1="0" x2="0.635" y2="0" width="0.1778" layer="1"/>
<wire x1="-1.143" y1="0.635" x2="1.143" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="-0.635" x2="-1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="0.635" width="0.0508" layer="39"/>
</package>
<package name="SJ2-123">
<description>&lt;b&gt;Solder jumper 2-sided, both CLOSED&lt;/b&gt; by default</description>
<wire x1="1.143" y1="-0.889" x2="-1.143" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.889" x2="1.397" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="0.635" x2="-1.143" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.397" y1="-0.635" x2="-1.143" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.143" y1="-0.889" x2="1.397" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-0.635" x2="1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.889" x2="1.143" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.127" x2="0.635" y2="-0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.635" y1="-0.127" x2="-0.635" y2="0.127" width="1.016" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-1.4478" y1="-1.016" x2="0.2794" y2="0.9398" layer="31"/>
<rectangle x1="-0.381" y1="-0.635" x2="0.381" y2="0.635" layer="51"/>
<rectangle x1="-0.762" y1="-0.381" x2="-0.254" y2="0.381" layer="51"/>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-1.397" y="1.016" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.905" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.635" y1="0" x2="-0.254" y2="0" width="0.1778" layer="1"/>
<wire x1="0.635" y1="0" x2="0.254" y2="0" width="0.1778" layer="1"/>
<rectangle x1="0.254" y1="-0.381" x2="0.762" y2="0.381" layer="51"/>
<wire x1="-1.143" y1="0.635" x2="1.143" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.143" y1="-0.635" x2="-1.143" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="0.635" width="0.0508" layer="39"/>
</package>
<package name="SHORT_SMD6">
<description>&lt;b&gt;Short Connection&lt;/b&gt;&lt;br&gt;
Force connect two different signals, &lt;i&gt;for evample: GND and AGND&lt;/i&gt;</description>
<smd name="1" x="0" y="0" dx="0.1778" dy="0.1778" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0" y="0" dx="0.1778" dy="0.1778" layer="1" stop="no" thermals="no" cream="no"/>
</package>
<package name="SHORT_SMD16">
<description>&lt;b&gt;Short Connection&lt;/b&gt;&lt;br&gt;
Force connect two different signals, &lt;i&gt;for evample: GND and AGND&lt;/i&gt;</description>
<smd name="1" x="0" y="0" dx="0.4064" dy="0.4064" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0" y="0" dx="0.4064" dy="0.4064" layer="1" stop="no" thermals="no"/>
</package>
<package name="SJ2-M-12">
<description>&lt;b&gt;Solder jumper 2-sided, Mini, CLOSED 1-2&lt;/b&gt;</description>
<wire x1="0.508" y1="-0.508" x2="-0.508" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.508" x2="0.762" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0.508" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="-0.254" x2="-0.508" y2="-0.508" width="0.1524" layer="21" curve="90"/>
<wire x1="0.508" y1="-0.508" x2="0.762" y2="-0.254" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="-0.254" x2="0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.254" x2="-0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.508" x2="0.508" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.127" y1="-0.381" x2="0.127" y2="0.381" layer="51"/>
<smd name="1" x="-0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="3" x="0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<text x="-0.889" y="0.635" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.524" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0" x2="-0.127" y2="0" width="0.1778" layer="1"/>
<rectangle x1="-0.381" y1="-0.254" x2="0" y2="0.254" layer="51"/>
</package>
<package name="SJ2-M-23">
<description>&lt;b&gt;Solder jumper 2-sided, Mini, CLOSED 1-2&lt;/b&gt;</description>
<wire x1="0.508" y1="-0.508" x2="-0.508" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.508" x2="0.762" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0.508" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="-0.254" x2="-0.508" y2="-0.508" width="0.1524" layer="21" curve="90"/>
<wire x1="0.508" y1="-0.508" x2="0.762" y2="-0.254" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="-0.254" x2="0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.254" x2="-0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.508" x2="0.508" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.127" y1="-0.381" x2="0.127" y2="0.381" layer="51"/>
<smd name="1" x="-0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="3" x="0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<text x="-0.889" y="0.635" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.524" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.127" y1="0" x2="0.381" y2="0" width="0.1778" layer="1"/>
<rectangle x1="0" y1="-0.254" x2="0.381" y2="0.254" layer="51"/>
</package>
<package name="SJ2-M-123">
<description>&lt;b&gt;Solder jumper 2-sided, Mini, CLOSED 1-2&lt;/b&gt;</description>
<wire x1="0.508" y1="-0.508" x2="-0.508" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.508" x2="0.762" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0.508" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="-0.254" x2="-0.508" y2="-0.508" width="0.1524" layer="21" curve="90"/>
<wire x1="0.508" y1="-0.508" x2="0.762" y2="-0.254" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="-0.254" x2="0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.254" x2="-0.762" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.508" x2="0.508" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="0.508" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.127" y1="-0.381" x2="0.127" y2="0.381" layer="51"/>
<smd name="1" x="-0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<smd name="3" x="0.508" y="0" dx="0.6096" dy="0.3048" layer="1" rot="R90" cream="no"/>
<text x="-0.889" y="0.635" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.524" size="0.8128" layer="27">&gt;VALUE</text>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0" x2="-0.127" y2="0" width="0.1778" layer="1"/>
<wire x1="0.127" y1="0" x2="0.381" y2="0" width="0.1778" layer="1"/>
<rectangle x1="-0.381" y1="-0.254" x2="0" y2="0.254" layer="51"/>
<rectangle x1="0" y1="-0.254" x2="0.381" y2="0.254" layer="51"/>
</package>
<package name="SOT23-5">
<description>&lt;h3&gt;SOT23-5&lt;/h3&gt;
&lt;p&gt;Alias: SOT25, SC74A, SOT753, MO-178AA, SMT5&lt;/p&gt;
&lt;p&gt;Pitch: 0.95mm&lt;/p&gt;</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0.81" x2="0.5" y2="0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.397" y="-2.159" size="0.8128" layer="25" align="top-left">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="51"/>
</package>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.1524" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="51" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="51" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-3.683" y="2.413" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="51"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="51" curve="180"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="51"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="51" curve="180"/>
<wire x1="-5.334" y1="2.286" x2="5.334" y2="2.286" width="0.0508" layer="39"/>
<wire x1="5.334" y1="2.286" x2="5.334" y2="-2.286" width="0.0508" layer="39"/>
<wire x1="5.334" y1="-2.286" x2="-5.334" y2="-2.286" width="0.0508" layer="39"/>
<wire x1="-5.334" y1="-2.286" x2="-5.334" y2="2.286" width="0.0508" layer="39"/>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="51" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="51"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="51" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="51" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="51" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="51" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="51" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="51"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-4.191" y="2.667" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="51" curve="-90"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="51" curve="90"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="51" curve="90"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="2.54" x2="5.842" y2="2.54" width="0.0508" layer="39"/>
<wire x1="5.842" y1="2.54" x2="5.842" y2="-2.54" width="0.0508" layer="39"/>
<wire x1="5.842" y1="-2.54" x2="-5.842" y2="-2.54" width="0.0508" layer="39"/>
<wire x1="-5.842" y1="-2.54" x2="-5.842" y2="2.54" width="0.0508" layer="39"/>
</package>
<package name="QSMD3225">
<description>&lt;b&gt;Geyer Model KX-7 3.2x2.5mm Low Cost Surface Mount Crystal&lt;/b&gt;&lt;p&gt;
Source: GEYER-KX-7.pdf&lt;p&gt;
solder pads expanded by 0.2mm for manual soldering</description>
<wire x1="-1.125" y1="1.25" x2="-0.3" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="1.25" x2="0.3" y2="1.25" width="0.2032" layer="21"/>
<wire x1="0.3" y1="1.25" x2="1.125" y2="1.25" width="0.2032" layer="51"/>
<wire x1="1.55" y1="0.825" x2="1.55" y2="0.15" width="0.2032" layer="51"/>
<wire x1="1.55" y1="0.15" x2="1.55" y2="-0.15" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.15" x2="1.55" y2="-0.825" width="0.2032" layer="51"/>
<wire x1="1.125" y1="-1.25" x2="0.3" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="0.3" y1="-1.25" x2="-0.3" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-1.25" x2="-1.125" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="-0.825" x2="-1.55" y2="-0.15" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="-0.15" x2="-1.55" y2="0.15" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="0.15" x2="-1.55" y2="0.825" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="0.825" x2="-1.125" y2="1.25" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="1.125" y1="1.25" x2="1.55" y2="0.825" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="1.55" y1="-0.825" x2="1.125" y2="-1.25" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-1.125" y1="-1.25" x2="-1.55" y2="-0.825" width="0.2032" layer="51" curve="89.516721"/>
<smd name="1" x="-1.25" y="-1" dx="1.5" dy="1.3" layer="1"/>
<smd name="2" x="1.25" y="-1" dx="1.5" dy="1.3" layer="1"/>
<smd name="3" x="1.25" y="1" dx="1.5" dy="1.3" layer="1"/>
<smd name="4" x="-1.25" y="1" dx="1.5" dy="1.3" layer="1"/>
<text x="0" y="1.905" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SO8">
<description>&lt;h3&gt;SO-8 Small outline integrated circuit&lt;/h3&gt;
&lt;p&gt;Alias: SO-8 Narrow, SO-8-N, SOIC-8, SOIC-8-N, JEDEC MS-012&lt;/p&gt;
&lt;p&gt;&lt;ul&gt;&lt;li&gt;Pitch: 1.27 mm (50 mil)&lt;/li&gt;
&lt;li&gt;Body width, W&lt;sub&gt;B&lt;/sub&gt;: 4mm&lt;/li&gt;
&lt;li&gt;Lead-to-lead width, W&lt;sub&gt;L&lt;/sub&gt;: 6mm&lt;/li&gt;
&lt;li&gt;Pin count: 8&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.413" y1="1.905" x2="2.413" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.905" x2="-2.413" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-1.905" x2="-2.413" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="1.905" x2="2.413" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-2.667" y="-1.905" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
<wire x1="-1.651" y1="-1.143" x2="-1.778" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-1.143" x2="-1.905" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.143" x2="-2.159" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.143" x2="-2.413" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.905" x2="2.413" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.143" x2="2.413" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.143" x2="-2.413" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.905" x2="-2.413" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.651" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.524" x2="-2.413" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.397" x2="-2.413" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.905" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-1.143" x2="-2.413" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.778" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.651" x2="-1.905" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.143" x2="-2.032" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.143" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.397" x2="-2.159" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-1.809" y="-1.301" radius="0.2499" width="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" layer="35"/>
</package>
<package name="UFQFPN28">
<description>&lt;b&gt;UFQFPN28&lt;/b&gt;&lt;p&gt;
4 x 4 mm, 28-pin ultra thin fine pitch quad flat no-lead package</description>
<wire x1="2" y1="-2" x2="-1.75" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-2" x2="-2" y2="-2" width="0.127" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-2" y1="-1.75" x2="-2" y2="2" width="0.127" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.127" layer="51"/>
<smd name="1" x="-1.5" y="-1.975" dx="0.35" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="2" x="-1" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<rectangle x1="-1.875" y1="-2.125" x2="-1.125" y2="-1.625" layer="29" rot="R90"/>
<smd name="3" x="-0.5" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.5" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="1" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="1.5" y="-1.975" dx="0.35" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="8" x="1.975" y="-1.5" dx="0.35" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="9" x="1.875" y="-1" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="10" x="1.875" y="-0.5" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="11" x="1.875" y="0" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="12" x="1.875" y="0.5" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="13" x="1.875" y="1" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="14" x="1.975" y="1.5" dx="0.35" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="15" x="1.5" y="1.975" dx="0.35" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="16" x="1" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R270"/>
<smd name="17" x="0.5" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R270"/>
<smd name="18" x="0" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R270"/>
<smd name="19" x="-0.5" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R270"/>
<smd name="20" x="-1" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R270"/>
<smd name="21" x="-1.5" y="1.975" dx="0.35" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="22" x="-1.975" y="1.5" dx="0.35" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="23" x="-1.875" y="1" dx="0.55" dy="0.3" layer="1"/>
<smd name="24" x="-1.875" y="0.5" dx="0.55" dy="0.3" layer="1"/>
<smd name="25" x="-1.875" y="0" dx="0.55" dy="0.3" layer="1"/>
<smd name="26" x="-1.875" y="-0.5" dx="0.55" dy="0.3" layer="1"/>
<smd name="27" x="-1.875" y="-1" dx="0.55" dy="0.3" layer="1"/>
<smd name="28" x="-1.975" y="-1.5" dx="0.35" dy="0.3" layer="1" stop="no" cream="no"/>
<rectangle x1="1.125" y1="-2.125" x2="1.875" y2="-1.625" layer="29" rot="R90"/>
<rectangle x1="1.625" y1="-1.875" x2="2.125" y2="-1.125" layer="29" rot="R90"/>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="-1.625" y="2.125"/>
<vertex x="-1.625" y="1.8125"/>
<vertex x="-1.4875" y="1.675"/>
<vertex x="-1.375" y="1.675"/>
<vertex x="-1.375" y="2.125"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="-2.125" y="1.625"/>
<vertex x="-1.8125" y="1.625"/>
<vertex x="-1.675" y="1.4875"/>
<vertex x="-1.675" y="1.375"/>
<vertex x="-2.125" y="1.375"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="-1.625" y="1.8"/>
<vertex x="-1.5" y="1.675"/>
<vertex x="-1.375" y="1.675"/>
<vertex x="-1.375" y="2.125"/>
<vertex x="-1.625" y="2.125"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="-2.125" y="1.375"/>
<vertex x="-2.125" y="1.625"/>
<vertex x="-1.8" y="1.625"/>
<vertex x="-1.675" y="1.5"/>
<vertex x="-1.675" y="1.375"/>
</polygon>
<rectangle x1="1.625" y1="1.125" x2="2.125" y2="1.875" layer="29" rot="R90"/>
<rectangle x1="1.125" y1="1.625" x2="1.875" y2="2.125" layer="29" rot="R90"/>
<rectangle x1="-1.875" y1="1.625" x2="-1.125" y2="2.125" layer="29" rot="R90"/>
<rectangle x1="-2.125" y1="1.125" x2="-1.625" y2="1.875" layer="29" rot="R90"/>
<rectangle x1="-2.125" y1="-1.875" x2="-1.625" y2="-1.125" layer="29" rot="R90"/>
<circle x="-1.143" y="-1.143" radius="0.254" width="0.254" layer="21"/>
<text x="-1.905" y="-2.54" size="0.8128" layer="25" align="top-left">&gt;Name</text>
<text x="1.016" y="0" size="0.8128" layer="27" rot="R90" align="center">&gt;Value</text>
<rectangle x1="-1.675" y1="-2.015" x2="-1.325" y2="-1.765" layer="51" rot="R90"/>
<rectangle x1="-2.065" y1="-1.625" x2="-1.715" y2="-1.375" layer="51" rot="R180"/>
<rectangle x1="1.325" y1="-2.015" x2="1.675" y2="-1.765" layer="51" rot="R270"/>
<rectangle x1="1.715" y1="-1.625" x2="2.065" y2="-1.375" layer="51"/>
<rectangle x1="1.715" y1="1.375" x2="2.065" y2="1.625" layer="51"/>
<rectangle x1="1.325" y1="1.765" x2="1.675" y2="2.015" layer="51" rot="R90"/>
<rectangle x1="-1.675" y1="1.765" x2="-1.325" y2="2.015" layer="51" rot="R90"/>
<rectangle x1="-2.065" y1="1.375" x2="-1.715" y2="1.625" layer="51" rot="R180"/>
<rectangle x1="-1.2" y1="-1.99" x2="-0.8" y2="-1.74" layer="51" rot="R270"/>
<rectangle x1="-0.7" y1="-1.99" x2="-0.3" y2="-1.74" layer="51" rot="R270"/>
<rectangle x1="-0.2" y1="-1.99" x2="0.2" y2="-1.74" layer="51" rot="R270"/>
<rectangle x1="0.3" y1="-1.99" x2="0.7" y2="-1.74" layer="51" rot="R270"/>
<rectangle x1="0.8" y1="-1.99" x2="1.2" y2="-1.74" layer="51" rot="R270"/>
<rectangle x1="1.665" y1="-1.125" x2="2.065" y2="-0.875" layer="51"/>
<rectangle x1="1.665" y1="-0.625" x2="2.065" y2="-0.375" layer="51"/>
<rectangle x1="1.665" y1="-0.125" x2="2.065" y2="0.125" layer="51"/>
<rectangle x1="1.665" y1="0.375" x2="2.065" y2="0.625" layer="51"/>
<rectangle x1="1.665" y1="0.875" x2="2.065" y2="1.125" layer="51"/>
<rectangle x1="0.8" y1="1.74" x2="1.2" y2="1.99" layer="51" rot="R90"/>
<rectangle x1="0.3" y1="1.74" x2="0.7" y2="1.99" layer="51" rot="R90"/>
<rectangle x1="-0.2" y1="1.74" x2="0.2" y2="1.99" layer="51" rot="R90"/>
<rectangle x1="-0.7" y1="1.74" x2="-0.3" y2="1.99" layer="51" rot="R90"/>
<rectangle x1="-1.2" y1="1.74" x2="-0.8" y2="1.99" layer="51" rot="R90"/>
<rectangle x1="-2.065" y1="0.875" x2="-1.665" y2="1.125" layer="51" rot="R180"/>
<rectangle x1="-2.065" y1="0.375" x2="-1.665" y2="0.625" layer="51" rot="R180"/>
<rectangle x1="-2.065" y1="-0.125" x2="-1.665" y2="0.125" layer="51" rot="R180"/>
<rectangle x1="-2.065" y1="-0.625" x2="-1.665" y2="-0.375" layer="51" rot="R180"/>
<rectangle x1="-2.065" y1="-1.125" x2="-1.665" y2="-0.875" layer="51" rot="R180"/>
<wire x1="-2" y1="1.75" x2="-2" y2="2" width="0.127" layer="21"/>
<wire x1="-1.75" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<wire x1="1.75" y1="2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="1.75" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="-1.75" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.75" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="1.75" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.75" x2="-1.75" y2="-2" width="0.127" layer="21"/>
<circle x="-1.143" y="-1.143" radius="0.254" width="0.254" layer="51"/>
<wire x1="-2" y1="-1.75" x2="-1.75" y2="-2" width="0.127" layer="51"/>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="1.375" y="2.125"/>
<vertex x="1.625" y="2.125"/>
<vertex x="1.625" y="1.8"/>
<vertex x="1.5" y="1.675"/>
<vertex x="1.375" y="1.675"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="1.8" y="1.625"/>
<vertex x="1.675" y="1.5"/>
<vertex x="1.675" y="1.375"/>
<vertex x="2.125" y="1.375"/>
<vertex x="2.125" y="1.625"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="2.125" y="-1.375"/>
<vertex x="2.125" y="-1.625"/>
<vertex x="1.8" y="-1.625"/>
<vertex x="1.675" y="-1.5"/>
<vertex x="1.675" y="-1.375"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="1.625" y="-1.8"/>
<vertex x="1.5" y="-1.675"/>
<vertex x="1.375" y="-1.675"/>
<vertex x="1.375" y="-2.125"/>
<vertex x="1.625" y="-2.125"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="-1.375" y="-2.125"/>
<vertex x="-1.625" y="-2.125"/>
<vertex x="-1.625" y="-1.8"/>
<vertex x="-1.5" y="-1.675"/>
<vertex x="-1.375" y="-1.675"/>
</polygon>
<polygon width="0.05" layer="31" spacing="0.01">
<vertex x="-1.8" y="-1.625"/>
<vertex x="-1.675" y="-1.5"/>
<vertex x="-1.675" y="-1.375"/>
<vertex x="-2.125" y="-1.375"/>
<vertex x="-2.125" y="-1.625"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="2.125" y="1.625"/>
<vertex x="1.8125" y="1.625"/>
<vertex x="1.675" y="1.4875"/>
<vertex x="1.675" y="1.375"/>
<vertex x="2.125" y="1.375"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="1.625" y="2.125"/>
<vertex x="1.625" y="1.8125"/>
<vertex x="1.4875" y="1.675"/>
<vertex x="1.375" y="1.675"/>
<vertex x="1.375" y="2.125"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="-1.625" y="-2.125"/>
<vertex x="-1.625" y="-1.8125"/>
<vertex x="-1.4875" y="-1.675"/>
<vertex x="-1.375" y="-1.675"/>
<vertex x="-1.375" y="-2.125"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="-2.125" y="-1.625"/>
<vertex x="-1.8125" y="-1.625"/>
<vertex x="-1.675" y="-1.4875"/>
<vertex x="-1.675" y="-1.375"/>
<vertex x="-2.125" y="-1.375"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="1.625" y="-2.125"/>
<vertex x="1.625" y="-1.8125"/>
<vertex x="1.4875" y="-1.675"/>
<vertex x="1.375" y="-1.675"/>
<vertex x="1.375" y="-2.125"/>
</polygon>
<polygon width="0.05" layer="1" spacing="0.01">
<vertex x="2.125" y="-1.625"/>
<vertex x="1.8125" y="-1.625"/>
<vertex x="1.675" y="-1.4875"/>
<vertex x="1.675" y="-1.375"/>
<vertex x="2.125" y="-1.375"/>
</polygon>
</package>
<package name="RGBBTN-19">
<description>&lt;h3&gt;RGB LED Light Push Button - 19mm Metal &lt;/h3&gt;</description>
<text x="-6" y="0" size="0.8128" layer="21" rot="R90" align="bottom-center">C</text>
<circle x="0" y="0" radius="9" width="0.0254" layer="21"/>
<smd name="C$TOP" x="-5.05" y="0" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R90"/>
<wire x1="-5.55" y1="1.3" x2="-5.55" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="-5.55" y1="-1.3" x2="-4.55" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="-4.55" y1="-1.3" x2="-4.55" y2="1.3" width="0.0254" layer="44"/>
<wire x1="-4.55" y1="1.3" x2="-5.55" y2="1.3" width="0.0254" layer="44"/>
<smd name="C$BOT" x="-5.05" y="0" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R90"/>
<smd name="B$TOP" x="-3.55" y="-3.55" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R135"/>
<wire x1="-4.822790625" y1="-2.984315625" x2="-2.984315625" y2="-4.822790625" width="0.0254" layer="44"/>
<wire x1="-2.984315625" y1="-4.822790625" x2="-2.277209375" y2="-4.115684375" width="0.0254" layer="44"/>
<wire x1="-2.277209375" y1="-4.115684375" x2="-4.115684375" y2="-2.277209375" width="0.0254" layer="44"/>
<wire x1="-4.115684375" y1="-2.277209375" x2="-4.822790625" y2="-2.984315625" width="0.0254" layer="44"/>
<smd name="B$BOT" x="-3.55" y="-3.55" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R135"/>
<smd name="NO1$TOP" x="0" y="-5.05" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R180"/>
<wire x1="-1.3" y1="-5.55" x2="1.3" y2="-5.55" width="0.0254" layer="44"/>
<wire x1="1.3" y1="-5.55" x2="1.3" y2="-4.55" width="0.0254" layer="44"/>
<wire x1="1.3" y1="-4.55" x2="-1.3" y2="-4.55" width="0.0254" layer="44"/>
<wire x1="-1.3" y1="-4.55" x2="-1.3" y2="-5.55" width="0.0254" layer="44"/>
<smd name="NO1$BOT" x="0" y="-5.05" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R180"/>
<smd name="R$TOP" x="5.05" y="0" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R90"/>
<wire x1="4.55" y1="1.3" x2="4.55" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="4.55" y1="-1.3" x2="5.55" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="5.55" y1="-1.3" x2="5.55" y2="1.3" width="0.0254" layer="44"/>
<wire x1="5.55" y1="1.3" x2="4.55" y2="1.3" width="0.0254" layer="44"/>
<smd name="R$BOT" x="5.05" y="0" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R90"/>
<smd name="G$TOP" x="3.55" y="3.55" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R135"/>
<wire x1="2.277209375" y1="4.115684375" x2="4.115684375" y2="2.277209375" width="0.0254" layer="44"/>
<wire x1="4.115684375" y1="2.277209375" x2="4.822790625" y2="2.984315625" width="0.0254" layer="44"/>
<wire x1="4.822790625" y1="2.984315625" x2="2.984315625" y2="4.822790625" width="0.0254" layer="44"/>
<wire x1="2.984315625" y1="4.822790625" x2="2.277209375" y2="4.115684375" width="0.0254" layer="44"/>
<smd name="G$BOT" x="3.55" y="3.55" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R135"/>
<smd name="NO2$TOP" x="0" y="5.05" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R180"/>
<wire x1="-1.3" y1="4.55" x2="1.3" y2="4.55" width="0.0254" layer="44"/>
<wire x1="1.3" y1="4.55" x2="1.3" y2="5.55" width="0.0254" layer="44"/>
<wire x1="1.3" y1="5.55" x2="-1.3" y2="5.55" width="0.0254" layer="44"/>
<wire x1="-1.3" y1="5.55" x2="-1.3" y2="4.55" width="0.0254" layer="44"/>
<smd name="NO2$BOT" x="0" y="5.05" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R180"/>
<text x="6" y="0" size="0.8128" layer="21" rot="R270" align="bottom-center">R</text>
<text x="0" y="-8.5" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.5" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<text x="0" y="6" size="0.8128" layer="21" align="bottom-center">NO</text>
<text x="0" y="-6" size="0.8128" layer="21" rot="R180" align="bottom-center">NO</text>
<text x="-4.2" y="-4.2" size="0.8128" layer="21" rot="R135" align="bottom-center">B</text>
<text x="4.2" y="4.2" size="0.8128" layer="21" rot="R315" align="bottom-center">G</text>
<circle x="0" y="0" radius="9" width="0.0254" layer="51"/>
<text x="-7.62" y="0" size="1.27" layer="51" rot="R90" align="center">C</text>
</package>
<package name="SOT323">
<description>&lt;h3&gt;SOT323&lt;/h3&gt;
&lt;p&gt;Alias: TO323, SC70-3, UMT3&lt;/p&gt;
&lt;p&gt;pitch 1.3mm (2x0.65)&lt;/p&gt;</description>
<wire x1="0.9224" y1="0.4604" x2="0.9224" y2="-0.4604" width="0.127" layer="51"/>
<wire x1="0.9224" y1="-0.4604" x2="-0.9224" y2="-0.4604" width="0.127" layer="51"/>
<wire x1="-0.9224" y1="-0.4604" x2="-0.9224" y2="0.4604" width="0.127" layer="51"/>
<wire x1="-0.9224" y1="0.4604" x2="0.9224" y2="0.4604" width="0.127" layer="51"/>
<smd name="3" x="0" y="0.95" dx="0.9" dy="0.7" layer="1" rot="R90"/>
<smd name="1" x="-0.65" y="-0.95" dx="0.9" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="0.65" y="-0.95" dx="0.9" dy="0.7" layer="1" rot="R90"/>
<text x="-1.143" y="-1.67" size="0.8128" layer="25" align="top-left">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.1" x2="-0.5" y2="-0.5" layer="51"/>
<rectangle x1="0.5" y1="-1.1" x2="0.8" y2="-0.5" layer="51"/>
<rectangle x1="-0.15" y1="0.5" x2="0.15" y2="1.1" layer="51"/>
<wire x1="0.9224" y1="0.4604" x2="0.9224" y2="-0.2604" width="0.127" layer="21"/>
<wire x1="-0.9224" y1="-0.2604" x2="-0.9224" y2="0.4604" width="0.127" layer="21"/>
<wire x1="-0.9224" y1="0.4604" x2="-0.5776" y2="0.4604" width="0.127" layer="21"/>
<wire x1="0.5776" y1="0.4604" x2="0.9224" y2="0.4604" width="0.127" layer="21"/>
</package>
<package name="TO252-DPAK">
<description>&lt;h3&gt;TO-252 (DPACK)&lt;/h3&gt;
&lt;p&gt;Alias: TO-252, DPACK, Decawatt Package&lt;/p&gt;
&lt;p&gt;Like TO-251 for surface mount&lt;/p&gt;</description>
<wire x1="3.275" y1="-2.15" x2="-3.275" y2="-2.15" width="0.2032" layer="51"/>
<wire x1="-3.275" y1="-2.15" x2="-3.275" y2="3.325" width="0.2032" layer="51"/>
<wire x1="-3.275" y1="3.325" x2="-2.775" y2="3.825" width="0.2032" layer="51"/>
<wire x1="-2.775" y1="3.825" x2="2.775" y2="3.825" width="0.2032" layer="51"/>
<wire x1="2.775" y1="3.825" x2="3.275" y2="3.325" width="0.2032" layer="51"/>
<wire x1="3.275" y1="3.325" x2="3.275" y2="-2.15" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="4" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="3" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<text x="0" y="-3.175" size="0.8128" layer="25" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.8128" layer="27" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
<wire x1="3.275" y1="-2.15" x2="-3.275" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-3.275" y1="-2.15" x2="-3.275" y2="3.325" width="0.2032" layer="21"/>
<wire x1="-3.275" y1="3.325" x2="-3.029" y2="3.571" width="0.2032" layer="21"/>
<wire x1="3.029" y1="3.571" x2="3.275" y2="3.325" width="0.2032" layer="21"/>
<wire x1="3.275" y1="3.325" x2="3.275" y2="-2.15" width="0.2032" layer="21"/>
<rectangle x1="-2.7178" y1="-3.8862" x2="-1.8542" y2="-2.2606" layer="21"/>
<rectangle x1="1.8542" y1="-3.8862" x2="2.7178" y2="-2.2606" layer="21"/>
<rectangle x1="-0.4318" y1="-2.6162" x2="0.4318" y2="-2.2606" layer="21"/>
</package>
<package name="TO263-D2PACK">
<description>&lt;h3&gt;TO-263 (D2PACK)&lt;/h3&gt;
&lt;p&gt;Alias: TO-263, D2PACK, D&lt;sup&gt;2&lt;/sup&gt;PACK, DDPACK&lt;/p&gt;
&lt;p&gt;Like TO-220 for surface mount&lt;/p&gt;</description>
<wire x1="-5.1308" y1="-4.0894" x2="5.1308" y2="-4.0894" width="0.254" layer="51"/>
<wire x1="5.1308" y1="-4.0894" x2="5.1308" y2="4.445" width="0.254" layer="51"/>
<wire x1="5.1308" y1="4.445" x2="3.1242" y2="5.8166" width="0.254" layer="51"/>
<wire x1="3.1242" y1="5.8166" x2="-3.3782" y2="5.8166" width="0.254" layer="51"/>
<wire x1="-3.3782" y1="5.8166" x2="-5.1308" y2="4.699" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="4.699" x2="-5.1308" y2="4.445" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="4.445" x2="-5.1308" y2="-4.0894" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="4.445" x2="5.1308" y2="4.445" width="0.254" layer="51"/>
<smd name="1" x="-2.54" y="-8.23" dx="3.5" dy="1.6" layer="1" rot="R90" thermals="no"/>
<smd name="3" x="2.54" y="-8.23" dx="3.5" dy="1.6" layer="1" rot="R90" thermals="no"/>
<smd name="4" x="0" y="2" dx="12.2" dy="9.75" layer="1" thermals="no"/>
<text x="0" y="-5.588" size="0.8128" layer="25" align="center">&gt;NAME</text>
<text x="0" y="-3.175" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<rectangle x1="-3.0988" y1="-9.525" x2="-1.9812" y2="-8.1026" layer="51"/>
<rectangle x1="-3.2512" y1="-8.1534" x2="-1.8288" y2="-4.2418" layer="51"/>
<rectangle x1="1.9812" y1="-9.525" x2="3.0988" y2="-8.1026" layer="51"/>
<rectangle x1="1.8288" y1="-8.1534" x2="3.2512" y2="-4.2418" layer="51"/>
<rectangle x1="-0.7112" y1="-4.826" x2="0.7112" y2="-4.2418" layer="51"/>
<wire x1="-5.1308" y1="-4.0894" x2="5.1308" y2="-4.0894" width="0.254" layer="21"/>
<wire x1="-5.1308" y1="-4.0894" x2="-5.1308" y2="-3.2004" width="0.254" layer="21"/>
<wire x1="5.1308" y1="-4.0894" x2="5.1308" y2="-3.2004" width="0.254" layer="21"/>
<rectangle x1="-3.2512" y1="-6.223" x2="-1.8288" y2="-4.2418" layer="21"/>
<rectangle x1="1.8288" y1="-6.223" x2="3.2512" y2="-4.2418" layer="21"/>
<rectangle x1="-0.7112" y1="-4.826" x2="0.7112" y2="-4.2418" layer="21"/>
</package>
<package name="MODULE-DCDC-MINI">
<description>&lt;h3&gt;Module Mini DC Buck Converter&lt;/h3&gt;
&lt;ul&gt;&lt;li&gt;Input voltage: 7V - &lt;b&gt;28V&lt;/b&gt; DC&lt;/li&gt;
&lt;li&gt;Output voltage: 0.8V - 20V DC&lt;/li&gt;
&lt;li&gt;Output current: &lt;b&gt;1.5A&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;Peak output current: 3A&lt;/li&gt;
&lt;li&gt;Convertion freq: 1 MHz&lt;/li&gt;
&lt;/ul&gt;
based on MP1584, provided by Aliexpress</description>
<wire x1="-11.049" y1="-8.382" x2="-11.049" y2="8.382" width="0.127" layer="21"/>
<wire x1="-11.049" y1="8.382" x2="11.049" y2="8.382" width="0.127" layer="21"/>
<wire x1="11.049" y1="8.382" x2="11.049" y2="-8.382" width="0.127" layer="21"/>
<wire x1="11.049" y1="-8.382" x2="-11.049" y2="-8.382" width="0.127" layer="21"/>
<pad name="IN+" x="-9.271" y="-6.604" drill="0.8" shape="square"/>
<pad name="IN+1" x="-9.271" y="-4.064" drill="0.8" shape="square"/>
<pad name="IN-1" x="-9.271" y="4.064" drill="0.8"/>
<pad name="IN-" x="-9.271" y="6.604" drill="0.8"/>
<pad name="OUT-" x="9.271" y="6.604" drill="0.8"/>
<pad name="OUT-1" x="9.271" y="4.064" drill="0.8"/>
<pad name="OUT+1" x="9.271" y="-4.064" drill="0.8" shape="square"/>
<pad name="OUT+" x="9.271" y="-6.604" drill="0.8" shape="square"/>
<text x="-8.128" y="-4.826" size="1.016" layer="51" rot="R180">IN+</text>
<text x="6.858" y="-5.842" size="1.016" layer="51">OUT+</text>
<circle x="7.112" y="7.366" radius="0.762" width="0.127" layer="21"/>
<circle x="6.858" y="-7.112" radius="0.762" width="0.127" layer="21"/>
<wire x1="6.35" y1="-7.112" x2="7.366" y2="-7.112" width="0.127" layer="21"/>
<wire x1="6.858" y1="-6.604" x2="6.858" y2="-7.62" width="0.127" layer="21"/>
<wire x1="6.604" y1="7.366" x2="7.62" y2="7.366" width="0.127" layer="21"/>
<text x="-11.43" y="-8.89" size="0.8128" layer="25" rot="R180" align="bottom-right">&gt;NAME</text>
<text x="0" y="-5.08" size="0.8128" layer="27" rot="R180" align="center">&gt;VALUE</text>
<wire x1="-11.049" y1="-8.382" x2="-11.049" y2="8.382" width="0.127" layer="51"/>
<wire x1="-11.049" y1="8.382" x2="11.049" y2="8.382" width="0.127" layer="51"/>
<wire x1="11.049" y1="8.382" x2="11.049" y2="-8.382" width="0.127" layer="51"/>
<wire x1="11.049" y1="-8.382" x2="-11.049" y2="-8.382" width="0.127" layer="51"/>
<wire x1="-0.762" y1="6.35" x2="-0.762" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-2.54" x2="8.128" y2="-2.54" width="0.127" layer="51"/>
<wire x1="8.128" y1="-2.54" x2="8.128" y2="6.35" width="0.127" layer="51"/>
<wire x1="8.128" y1="6.35" x2="-0.762" y2="6.35" width="0.127" layer="51"/>
<circle x="7.112" y="7.366" radius="0.762" width="0.127" layer="51"/>
<circle x="6.858" y="-7.112" radius="0.762" width="0.127" layer="51"/>
<wire x1="6.35" y1="-7.112" x2="7.366" y2="-7.112" width="0.127" layer="51"/>
<wire x1="6.858" y1="-6.604" x2="6.858" y2="-7.62" width="0.127" layer="51"/>
<wire x1="6.604" y1="7.366" x2="7.62" y2="7.366" width="0.127" layer="51"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="3.048" y2="0.508" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="3.048" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="MODULE-DCDC-UNI">
<wire x1="-21.59" y1="-8.89" x2="-21.59" y2="8.89" width="0.127" layer="21"/>
<wire x1="-21.59" y1="8.89" x2="26.67" y2="8.89" width="0.127" layer="21"/>
<wire x1="26.67" y1="8.89" x2="26.67" y2="-8.89" width="0.127" layer="21"/>
<wire x1="26.67" y1="-8.89" x2="-21.59" y2="-8.89" width="0.127" layer="21"/>
<pad name="IN+" x="-25.4" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+1" x="-25.4" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-1" x="-25.4" y="8.89" drill="0.8"/>
<pad name="IN-" x="-25.4" y="11.43" drill="0.8"/>
<pad name="OUT-" x="25.4" y="11.43" drill="0.8"/>
<pad name="OUT-1" x="25.4" y="8.89" drill="0.8"/>
<pad name="OUT+1" x="25.4" y="-8.89" drill="0.8" shape="square"/>
<pad name="OUT+" x="25.4" y="-11.43" drill="0.8" shape="square"/>
<text x="-20.828" y="-12.446" size="1.016" layer="25" rot="R180">IN+</text>
<text x="19.558" y="-13.462" size="1.016" layer="25">OUT+</text>
<circle x="24.13" y="1.27" radius="0.762" width="0.127" layer="21"/>
<circle x="24.13" y="-1.27" radius="0.762" width="0.127" layer="21"/>
<wire x1="23.622" y1="-1.27" x2="24.638" y2="-1.27" width="0.127" layer="21"/>
<wire x1="24.13" y1="-0.762" x2="24.13" y2="-1.778" width="0.127" layer="21"/>
<wire x1="23.622" y1="1.27" x2="24.638" y2="1.27" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-13.97" x2="27.94" y2="-13.97" width="0.127" layer="21"/>
<wire x1="27.94" y1="-13.97" x2="27.94" y2="13.97" width="0.127" layer="21"/>
<wire x1="27.94" y1="13.97" x2="-27.94" y2="13.97" width="0.127" layer="21"/>
<wire x1="-27.94" y1="13.97" x2="-27.94" y2="-13.97" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="3.81" y1="0" x2="1.27" y2="2.54" width="0.254" layer="21"/>
<wire x1="3.81" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="21"/>
<circle x="-24.13" y="1.27" radius="0.762" width="0.127" layer="21"/>
<circle x="-24.13" y="-1.27" radius="0.762" width="0.127" layer="21"/>
<wire x1="-24.638" y1="-1.27" x2="-23.622" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-0.762" x2="-24.13" y2="-1.778" width="0.127" layer="21"/>
<wire x1="-24.638" y1="1.27" x2="-23.622" y2="1.27" width="0.127" layer="21"/>
<pad name="IN-2" x="-25.4" y="6.35" drill="0.8"/>
<pad name="IN+2" x="-25.4" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-3" x="-25.4" y="3.81" drill="0.8"/>
<pad name="IN+3" x="-25.4" y="-3.81" drill="0.8" shape="square"/>
<pad name="OUT-2" x="25.4" y="6.35" drill="0.8"/>
<pad name="OUT-3" x="25.4" y="3.81" drill="0.8"/>
<pad name="OUT+2" x="25.4" y="-6.35" drill="0.8" shape="square"/>
<pad name="OUT+3" x="25.4" y="-3.81" drill="0.8" shape="square"/>
<pad name="OUT-4" x="22.86" y="3.81" drill="0.8"/>
<pad name="OUT-5" x="22.86" y="6.35" drill="0.8"/>
<pad name="OUT-6" x="22.86" y="8.89" drill="0.8"/>
<pad name="OUT-7" x="22.86" y="11.43" drill="0.8"/>
<pad name="OUT-8" x="20.32" y="11.43" drill="0.8"/>
<pad name="OUT-9" x="20.32" y="8.89" drill="0.8"/>
<pad name="OUT-10" x="20.32" y="6.35" drill="0.8"/>
<pad name="OUT-11" x="20.32" y="3.81" drill="0.8"/>
<pad name="OUT-12" x="17.78" y="3.81" drill="0.8"/>
<pad name="OUT-13" x="17.78" y="6.35" drill="0.8"/>
<pad name="OUT-14" x="17.78" y="8.89" drill="0.8"/>
<pad name="OUT-15" x="17.78" y="11.43" drill="0.8"/>
<pad name="OUT+4" x="22.86" y="-3.81" drill="0.8" shape="square"/>
<pad name="OUT+5" x="22.86" y="-6.35" drill="0.8" shape="square"/>
<pad name="OUT+6" x="22.86" y="-8.89" drill="0.8" shape="square"/>
<pad name="OUT+7" x="22.86" y="-11.43" drill="0.8" shape="square"/>
<pad name="OUT+8" x="20.32" y="-11.43" drill="0.8" shape="square"/>
<pad name="OUT+9" x="20.32" y="-8.89" drill="0.8" shape="square"/>
<pad name="OUT+10" x="20.32" y="-6.35" drill="0.8" shape="square"/>
<pad name="OUT+11" x="20.32" y="-3.81" drill="0.8" shape="square"/>
<pad name="OUT+12" x="17.78" y="-3.81" drill="0.8" shape="square"/>
<pad name="OUT+13" x="17.78" y="-6.35" drill="0.8" shape="square"/>
<pad name="OUT+14" x="17.78" y="-8.89" drill="0.8" shape="square"/>
<pad name="OUT+15" x="17.78" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+4" x="-22.86" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+5" x="-22.86" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-4" x="-22.86" y="8.89" drill="0.8"/>
<pad name="IN-5" x="-22.86" y="11.43" drill="0.8"/>
<pad name="IN-6" x="-22.86" y="6.35" drill="0.8"/>
<pad name="IN+6" x="-22.86" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-7" x="-22.86" y="3.81" drill="0.8"/>
<pad name="IN+7" x="-22.86" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+8" x="-20.32" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+9" x="-20.32" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-8" x="-20.32" y="8.89" drill="0.8"/>
<pad name="IN-9" x="-20.32" y="11.43" drill="0.8"/>
<pad name="IN-10" x="-20.32" y="6.35" drill="0.8"/>
<pad name="IN+10" x="-20.32" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-11" x="-20.32" y="3.81" drill="0.8"/>
<pad name="IN+11" x="-20.32" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+12" x="-17.78" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+13" x="-17.78" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-12" x="-17.78" y="8.89" drill="0.8"/>
<pad name="IN-13" x="-17.78" y="11.43" drill="0.8"/>
<pad name="IN-14" x="-17.78" y="6.35" drill="0.8"/>
<pad name="IN+14" x="-17.78" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-15" x="-17.78" y="3.81" drill="0.8"/>
<pad name="IN+15" x="-17.78" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+16" x="-15.24" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+17" x="-15.24" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-16" x="-15.24" y="8.89" drill="0.8"/>
<pad name="IN-17" x="-15.24" y="11.43" drill="0.8"/>
<pad name="IN-18" x="-15.24" y="6.35" drill="0.8"/>
<pad name="IN+18" x="-15.24" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-19" x="-15.24" y="3.81" drill="0.8"/>
<pad name="IN+19" x="-15.24" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+20" x="-12.7" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+21" x="-12.7" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-20" x="-12.7" y="8.89" drill="0.8"/>
<pad name="IN-21" x="-12.7" y="11.43" drill="0.8"/>
<pad name="IN-22" x="-12.7" y="6.35" drill="0.8"/>
<pad name="IN+22" x="-12.7" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-23" x="-12.7" y="3.81" drill="0.8"/>
<pad name="IN+23" x="-12.7" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+24" x="-10.16" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+25" x="-10.16" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-24" x="-10.16" y="8.89" drill="0.8"/>
<pad name="IN-25" x="-10.16" y="11.43" drill="0.8"/>
<pad name="IN-26" x="-10.16" y="6.35" drill="0.8"/>
<pad name="IN+26" x="-10.16" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-27" x="-10.16" y="3.81" drill="0.8"/>
<pad name="IN+27" x="-10.16" y="-3.81" drill="0.8" shape="square"/>
<text x="-20.828" y="13.462" size="1.016" layer="25" rot="R180">IN-</text>
<text x="23.622" y="13.462" size="1.016" layer="25" rot="R180">OUT-</text>
<pad name="IN+28" x="-7.62" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+29" x="-7.62" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-28" x="-7.62" y="8.89" drill="0.8"/>
<pad name="IN-29" x="-7.62" y="11.43" drill="0.8"/>
<pad name="IN-30" x="-7.62" y="6.35" drill="0.8"/>
<pad name="IN+30" x="-7.62" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-31" x="-7.62" y="3.81" drill="0.8"/>
<pad name="IN+31" x="-7.62" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+32" x="-5.08" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+33" x="-5.08" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-32" x="-5.08" y="8.89" drill="0.8"/>
<pad name="IN-33" x="-5.08" y="11.43" drill="0.8"/>
<pad name="IN-34" x="-5.08" y="6.35" drill="0.8"/>
<pad name="IN+34" x="-5.08" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-35" x="-5.08" y="3.81" drill="0.8"/>
<pad name="IN+35" x="-5.08" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+36" x="-2.54" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+37" x="-2.54" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-36" x="-2.54" y="8.89" drill="0.8"/>
<pad name="IN-37" x="-2.54" y="11.43" drill="0.8"/>
<pad name="IN-38" x="-2.54" y="6.35" drill="0.8"/>
<pad name="IN+38" x="-2.54" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-39" x="-2.54" y="3.81" drill="0.8"/>
<pad name="IN+39" x="-2.54" y="-3.81" drill="0.8" shape="square"/>
<pad name="IN+40" x="0" y="-11.43" drill="0.8" shape="square"/>
<pad name="IN+41" x="0" y="-8.89" drill="0.8" shape="square"/>
<pad name="IN-40" x="0" y="8.89" drill="0.8"/>
<pad name="IN-41" x="0" y="11.43" drill="0.8"/>
<pad name="IN-42" x="0" y="6.35" drill="0.8"/>
<pad name="IN+42" x="0" y="-6.35" drill="0.8" shape="square"/>
<pad name="IN-43" x="0" y="3.81" drill="0.8"/>
<pad name="IN+43" x="0" y="-3.81" drill="0.8" shape="square"/>
</package>
<package name="MODULE-DCDC-MINI360">
<description>&lt;h3&gt;Module Mini360 DC Buck Converter&lt;/h3&gt;
&lt;ul&gt;&lt;li&gt;Input voltage: 4.75V - &lt;b&gt;23V&lt;/b&gt; DC&lt;/li&gt;
&lt;li&gt;Output voltage: 1.0V - 17V DC&lt;/li&gt;
&lt;li&gt;Output current: &lt;b&gt;1.8A&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;Peak output current: 3A&lt;/li&gt;
&lt;li&gt;Convertion freq: 340 kHz&lt;/li&gt;
&lt;/ul&gt;
based on MP2307, provided by Aliexpress</description>
<wire x1="-8.636" y1="-5.588" x2="-8.636" y2="5.588" width="0.127" layer="21"/>
<wire x1="-8.636" y1="5.588" x2="8.636" y2="5.588" width="0.127" layer="21"/>
<wire x1="8.636" y1="5.588" x2="8.636" y2="-5.588" width="0.127" layer="21"/>
<wire x1="8.636" y1="-5.588" x2="-8.636" y2="-5.588" width="0.127" layer="21"/>
<pad name="IN+" x="-7.62" y="-4.572" drill="0.8" shape="square"/>
<pad name="IN-" x="-7.62" y="4.572" drill="0.8" shape="square"/>
<pad name="OUT-" x="7.62" y="4.572" drill="0.8" shape="square"/>
<pad name="OUT+" x="7.62" y="-4.572" drill="0.8" shape="square"/>
<text x="-6.35" y="-4.318" size="1.016" layer="51" align="center-left">IN+</text>
<text x="6.35" y="-4.318" size="1.016" layer="51" align="center-right">OUT+</text>
<text x="-6.35" y="4.318" size="1.016" layer="51" align="center-left">IN-</text>
<text x="6.096" y="4.318" size="1.016" layer="51" align="center-right">OUT-</text>
<wire x1="3.048" y1="0" x2="-3.302" y2="0" width="0.127" layer="21"/>
<wire x1="3.048" y1="0" x2="2.032" y2="0.508" width="0.127" layer="21"/>
<wire x1="3.048" y1="0" x2="2.032" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-8.636" y1="-5.588" x2="-8.636" y2="5.588" width="0.127" layer="51"/>
<wire x1="-8.636" y1="5.588" x2="8.636" y2="5.588" width="0.127" layer="51"/>
<wire x1="8.636" y1="5.588" x2="8.636" y2="-5.588" width="0.127" layer="51"/>
<wire x1="8.636" y1="-5.588" x2="-8.636" y2="-5.588" width="0.127" layer="51"/>
<wire x1="0" y1="1.27" x2="0" y2="-5.08" width="0.127" layer="51"/>
<wire x1="0" y1="-5.08" x2="6.35" y2="-5.08" width="0.127" layer="51"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="1.27" width="0.127" layer="51"/>
<wire x1="6.35" y1="1.27" x2="0" y2="1.27" width="0.127" layer="51"/>
<text x="-8.89" y="-6.35" size="0.8128" layer="25" rot="R180" align="bottom-right">&gt;NAME</text>
<text x="0" y="2.54" size="0.8128" layer="27" align="center">&gt;VALUE</text>
</package>
<package name="RGBBTN-19-1">
<description>&lt;h3&gt;RGB LED Light Push Button - 19mm Metal &lt;/h3&gt;
Single side, half soldered</description>
<text x="-7.3" y="0" size="0.8128" layer="21" rot="R90" align="bottom-center">C</text>
<circle x="0" y="0" radius="9" width="0.0254" layer="21"/>
<smd name="C$TOP" x="-6.35" y="0" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R90"/>
<wire x1="-5.55" y1="1.3" x2="-5.55" y2="-1.3" width="0.0254" layer="46"/>
<wire x1="-5.55" y1="-1.3" x2="-4.55" y2="-1.3" width="0.0254" layer="46"/>
<wire x1="-4.55" y1="-1.3" x2="-4.55" y2="1.3" width="0.0254" layer="46"/>
<wire x1="-4.55" y1="1.3" x2="-5.55" y2="1.3" width="0.0254" layer="46"/>
<smd name="B$TOP" x="-4.45" y="-4.45" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R135"/>
<wire x1="-4.822790625" y1="-2.984315625" x2="-2.984315625" y2="-4.822790625" width="0.0254" layer="46"/>
<wire x1="-2.984315625" y1="-4.822790625" x2="-2.277209375" y2="-4.115684375" width="0.0254" layer="46"/>
<wire x1="-2.277209375" y1="-4.115684375" x2="-4.115684375" y2="-2.277209375" width="0.0254" layer="46"/>
<wire x1="-4.115684375" y1="-2.277209375" x2="-4.822790625" y2="-2.984315625" width="0.0254" layer="46"/>
<smd name="NO1$TOP" x="0" y="-6.35" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R180"/>
<wire x1="-1.3" y1="-5.55" x2="1.3" y2="-5.55" width="0.0254" layer="46"/>
<wire x1="1.3" y1="-5.55" x2="1.3" y2="-4.55" width="0.0254" layer="46"/>
<wire x1="1.3" y1="-4.55" x2="-1.3" y2="-4.55" width="0.0254" layer="46"/>
<wire x1="-1.3" y1="-4.55" x2="-1.3" y2="-5.55" width="0.0254" layer="46"/>
<smd name="R$TOP" x="6.35" y="0" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R90"/>
<wire x1="4.55" y1="1.3" x2="4.55" y2="-1.3" width="0.0254" layer="46"/>
<wire x1="4.55" y1="-1.3" x2="5.55" y2="-1.3" width="0.0254" layer="46"/>
<wire x1="5.55" y1="-1.3" x2="5.55" y2="1.3" width="0.0254" layer="46"/>
<wire x1="5.55" y1="1.3" x2="4.55" y2="1.3" width="0.0254" layer="46"/>
<smd name="G$TOP" x="4.45" y="4.45" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R135"/>
<wire x1="2.277209375" y1="4.115684375" x2="4.115684375" y2="2.277209375" width="0.0254" layer="46"/>
<wire x1="4.115684375" y1="2.277209375" x2="4.822790625" y2="2.984315625" width="0.0254" layer="46"/>
<wire x1="4.822790625" y1="2.984315625" x2="2.984315625" y2="4.822790625" width="0.0254" layer="46"/>
<wire x1="2.984315625" y1="4.822790625" x2="2.277209375" y2="4.115684375" width="0.0254" layer="46"/>
<smd name="NO2$TOP" x="0" y="6.35" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R180"/>
<wire x1="-1.3" y1="4.55" x2="1.3" y2="4.55" width="0.0254" layer="46"/>
<wire x1="1.3" y1="4.55" x2="1.3" y2="5.55" width="0.0254" layer="46"/>
<wire x1="1.3" y1="5.55" x2="-1.3" y2="5.55" width="0.0254" layer="46"/>
<wire x1="-1.3" y1="5.55" x2="-1.3" y2="4.55" width="0.0254" layer="46"/>
<text x="7.3" y="0" size="0.8128" layer="21" rot="R270" align="bottom-center">R</text>
<text x="0" y="-8.5" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="8.5" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<text x="0" y="7.3" size="0.8128" layer="21" align="bottom-center">NO</text>
<text x="-5.1" y="-5.1" size="0.8128" layer="21" rot="R135" align="bottom-center">B</text>
<text x="5.1" y="5.1" size="0.8128" layer="21" rot="R315" align="bottom-center">G</text>
</package>
<package name="SIP4">
<description>&lt;h3&gt;SIP-4 plastic case&lt;/h3&gt;
&lt;p&gt;often used in 1W DC/DC converters&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 2.54mm&lt;/li&gt;
&lt;li&gt;Case: 12mm x 6mm&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-5.842" y1="-1.27" x2="-5.588" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.588" y1="-1.27" x2="-5.334" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.334" y1="-1.27" x2="-5.08" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-4.826" y1="-1.27" x2="5.334" y2="-1.27" width="0.2" layer="21"/>
<wire x1="5.334" y1="-1.27" x2="5.842" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.842" y2="-1.016" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.7" shape="square" rot="R90" first="yes"/>
<pad name="2" x="-1.27" y="0" drill="0.7" shape="octagon" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.7" shape="octagon" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="0.7" shape="octagon" rot="R90"/>
<text x="-5.334" y="-1.524" size="0.8128" layer="25" align="top-left">&gt;NAME</text>
<text x="0" y="2.794" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.842" y1="-1.016" x2="-5.842" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-5.842" y1="-0.762" x2="-5.842" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-5.842" y1="-0.508" x2="-5.842" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="-5.842" y1="-0.254" x2="-5.842" y2="4.826" width="0.2032" layer="21"/>
<wire x1="5.842" y1="-1.27" x2="5.842" y2="4.826" width="0.2032" layer="21"/>
<wire x1="-5.842" y1="4.826" x2="-5.334" y2="4.826" width="0.2" layer="21"/>
<wire x1="-5.334" y1="4.826" x2="5.334" y2="4.826" width="0.2" layer="21"/>
<wire x1="5.334" y1="4.826" x2="5.842" y2="4.826" width="0.2" layer="21"/>
<wire x1="-5.334" y1="-1.27" x2="-5.334" y2="4.826" width="0.2032" layer="21"/>
<wire x1="5.334" y1="-1.27" x2="5.334" y2="4.826" width="0.2032" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.334" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-5.334" y1="-1.27" x2="5.334" y2="-1.27" width="0.2" layer="51"/>
<wire x1="5.334" y1="-1.27" x2="5.842" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-5.842" y2="4.826" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-1.27" x2="5.842" y2="4.826" width="0.2032" layer="51"/>
<wire x1="-5.842" y1="4.826" x2="-5.334" y2="4.826" width="0.2" layer="51"/>
<wire x1="-5.334" y1="4.826" x2="5.334" y2="4.826" width="0.2" layer="51"/>
<wire x1="5.334" y1="4.826" x2="5.842" y2="4.826" width="0.2" layer="51"/>
<wire x1="-5.334" y1="-1.27" x2="-5.334" y2="4.826" width="0.2032" layer="51"/>
<wire x1="5.334" y1="-1.27" x2="5.334" y2="4.826" width="0.2032" layer="51"/>
<circle x="-3.81" y="-0.254" radius="0.381" width="0.381" layer="51"/>
<wire x1="-6.096" y1="-1.524" x2="6.096" y2="-1.524" width="0.127" layer="39"/>
<wire x1="6.096" y1="-1.524" x2="6.096" y2="5.08" width="0.127" layer="39"/>
<wire x1="6.096" y1="5.08" x2="-6.096" y2="5.08" width="0.127" layer="39"/>
<wire x1="-6.096" y1="5.08" x2="-6.096" y2="-1.524" width="0.127" layer="39"/>
<wire x1="-5.842" y1="-0.254" x2="-4.826" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.842" y1="-0.508" x2="-5.08" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.842" y1="-0.762" x2="-5.334" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-5.842" y1="-1.016" x2="-5.588" y2="-1.27" width="0.2" layer="21"/>
</package>
<package name="SIP7-4">
<description>&lt;h3&gt;SIP-7 plastic case&lt;/h3&gt;
&lt;p&gt;often used in Dual outputs 2W or Dual Outputs DC/DC converters&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 2.54mm&lt;/li&gt;
&lt;li&gt;Case: 20mm x 6mm&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-9.652" y1="-4.826" x2="-9.398" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.398" y1="-4.826" x2="-9.144" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.144" y1="-4.826" x2="-8.89" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-8.89" y1="-4.826" x2="-8.636" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-8.636" y1="-4.826" x2="9.144" y2="-4.826" width="0.2" layer="21"/>
<wire x1="9.144" y1="-4.826" x2="9.652" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.826" x2="-9.652" y2="-4.572" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.7" shape="square" rot="R90" first="yes"/>
<pad name="2" x="-5.08" y="0" drill="0.7" shape="octagon" rot="R90"/>
<pad name="4" x="0" y="0" drill="0.7" shape="octagon" rot="R90"/>
<text x="-9.144" y="-5.08" size="0.8128" layer="25" align="top-left">&gt;NAME</text>
<text x="0" y="-2.286" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-9.652" y1="-4.572" x2="-9.652" y2="-4.318" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-4.318" x2="-9.652" y2="-4.064" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-4.064" x2="-9.652" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-3.81" x2="-9.652" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.652" y1="-4.826" x2="9.652" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="1.27" x2="-9.144" y2="1.27" width="0.2" layer="21"/>
<wire x1="-9.144" y1="1.27" x2="9.144" y2="1.27" width="0.2" layer="21"/>
<wire x1="9.144" y1="1.27" x2="9.652" y2="1.27" width="0.2" layer="21"/>
<wire x1="-9.144" y1="-4.826" x2="-9.144" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-4.826" x2="9.144" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-4.826" x2="-9.144" y2="-4.826" width="0.2" layer="51"/>
<wire x1="-9.144" y1="-4.826" x2="9.144" y2="-4.826" width="0.2" layer="51"/>
<wire x1="9.144" y1="-4.826" x2="9.652" y2="-4.826" width="0.2" layer="51"/>
<wire x1="-9.652" y1="-4.826" x2="-9.652" y2="1.27" width="0.2032" layer="51"/>
<wire x1="9.652" y1="-4.826" x2="9.652" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-9.652" y1="1.27" x2="-9.144" y2="1.27" width="0.2" layer="51"/>
<wire x1="-9.144" y1="1.27" x2="9.144" y2="1.27" width="0.2" layer="51"/>
<wire x1="9.144" y1="1.27" x2="9.652" y2="1.27" width="0.2" layer="51"/>
<wire x1="-9.144" y1="-4.826" x2="-9.144" y2="1.27" width="0.2032" layer="51"/>
<wire x1="9.144" y1="-4.826" x2="9.144" y2="1.27" width="0.2032" layer="51"/>
<circle x="-7.62" y="-4.064" radius="0.381" width="0.381" layer="51"/>
<wire x1="-9.906" y1="-5.08" x2="9.906" y2="-5.08" width="0.127" layer="39"/>
<wire x1="9.906" y1="-5.08" x2="9.906" y2="1.524" width="0.127" layer="39"/>
<wire x1="9.906" y1="1.524" x2="-9.906" y2="1.524" width="0.127" layer="39"/>
<wire x1="-9.906" y1="1.524" x2="-9.906" y2="-5.08" width="0.127" layer="39"/>
<wire x1="-9.652" y1="-3.81" x2="-8.636" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.064" x2="-8.89" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.318" x2="-9.144" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.572" x2="-9.398" y2="-4.826" width="0.2" layer="21"/>
<pad name="6" x="5.08" y="0" drill="0.7" shape="octagon" rot="R90"/>
</package>
<package name="SIP7-5">
<description>&lt;h3&gt;SIP-7 plastic case&lt;/h3&gt;
&lt;p&gt;often used in Dual outputs 2W or Dual Outputs DC/DC converters&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 2.54mm&lt;/li&gt;
&lt;li&gt;Case: 20mm x 6mm&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-9.652" y1="-4.826" x2="-9.398" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.398" y1="-4.826" x2="-9.144" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.144" y1="-4.826" x2="-8.89" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-8.89" y1="-4.826" x2="-8.636" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-8.636" y1="-4.826" x2="9.144" y2="-4.826" width="0.2" layer="21"/>
<wire x1="9.144" y1="-4.826" x2="9.652" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.826" x2="-9.652" y2="-4.572" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.7" shape="square" rot="R90" first="yes"/>
<pad name="2" x="-5.08" y="0" drill="0.7" shape="octagon" rot="R90"/>
<pad name="4" x="0" y="0" drill="0.7" shape="octagon" rot="R90"/>
<text x="-9.144" y="-5.08" size="0.8128" layer="25" align="top-left">&gt;NAME</text>
<text x="0" y="-2.286" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<wire x1="-9.652" y1="-4.572" x2="-9.652" y2="-4.318" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-4.318" x2="-9.652" y2="-4.064" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-4.064" x2="-9.652" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-3.81" x2="-9.652" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.652" y1="-4.826" x2="9.652" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="1.27" x2="-9.144" y2="1.27" width="0.2" layer="21"/>
<wire x1="-9.144" y1="1.27" x2="9.144" y2="1.27" width="0.2" layer="21"/>
<wire x1="9.144" y1="1.27" x2="9.652" y2="1.27" width="0.2" layer="21"/>
<wire x1="-9.144" y1="-4.826" x2="-9.144" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-4.826" x2="9.144" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-9.652" y1="-4.826" x2="-9.144" y2="-4.826" width="0.2" layer="51"/>
<wire x1="-9.144" y1="-4.826" x2="9.144" y2="-4.826" width="0.2" layer="51"/>
<wire x1="9.144" y1="-4.826" x2="9.652" y2="-4.826" width="0.2" layer="51"/>
<wire x1="-9.652" y1="-4.826" x2="-9.652" y2="1.27" width="0.2032" layer="51"/>
<wire x1="9.652" y1="-4.826" x2="9.652" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-9.652" y1="1.27" x2="-9.144" y2="1.27" width="0.2" layer="51"/>
<wire x1="-9.144" y1="1.27" x2="9.144" y2="1.27" width="0.2" layer="51"/>
<wire x1="9.144" y1="1.27" x2="9.652" y2="1.27" width="0.2" layer="51"/>
<wire x1="-9.144" y1="-4.826" x2="-9.144" y2="1.27" width="0.2032" layer="51"/>
<wire x1="9.144" y1="-4.826" x2="9.144" y2="1.27" width="0.2032" layer="51"/>
<circle x="-7.62" y="-4.064" radius="0.381" width="0.381" layer="51"/>
<wire x1="-9.906" y1="-5.08" x2="9.906" y2="-5.08" width="0.127" layer="39"/>
<wire x1="9.906" y1="-5.08" x2="9.906" y2="1.524" width="0.127" layer="39"/>
<wire x1="9.906" y1="1.524" x2="-9.906" y2="1.524" width="0.127" layer="39"/>
<wire x1="-9.906" y1="1.524" x2="-9.906" y2="-5.08" width="0.127" layer="39"/>
<wire x1="-9.652" y1="-3.81" x2="-8.636" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.064" x2="-8.89" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.318" x2="-9.144" y2="-4.826" width="0.2" layer="21"/>
<wire x1="-9.652" y1="-4.572" x2="-9.398" y2="-4.826" width="0.2" layer="21"/>
<pad name="5" x="2.54" y="0" drill="0.7" shape="octagon" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="0.7" shape="octagon" rot="R90"/>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="51"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="51"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="51" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="51" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="51" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="51"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-2.54" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
</package>
<package name="C075_105-063X106_133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="51"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="51"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="51" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="51" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="51" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="51"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-2.54" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="2@1" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<wire x1="7.493" y1="3.048" x2="5.842" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.842" y1="-3.048" x2="7.493" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="7.747" y1="-2.794" x2="7.747" y2="2.794" width="0.1524" layer="51"/>
<wire x1="7.493" y1="3.048" x2="7.747" y2="2.794" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.493" y1="-3.048" x2="7.747" y2="-2.794" width="0.1524" layer="51" curve="90"/>
<wire x1="7.493" y1="3.048" x2="5.842" y2="3.048" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-3.048" x2="7.493" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.747" y1="-2.794" x2="7.747" y2="2.794" width="0.1524" layer="21"/>
<wire x1="7.493" y1="3.048" x2="7.747" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.493" y1="-3.048" x2="7.747" y2="-2.794" width="0.1524" layer="21" curve="90"/>
</package>
<package name="MA01-1-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.27" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="MA01-2-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="2.54" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.27" y="4.191" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="1.905" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
</package>
<package name="MF01-2-H">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 4 (2 x 2)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="2" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.7" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-0.1" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="2.7" y1="1.1" x2="2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="2.7" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="2.7" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.6" y1="1.1" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.8" y1="1.1" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="1.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.6" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="2.7" y1="1.1" x2="2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="2.7" y1="13.9" x2="1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="2.7" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3.2" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-3.2" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-3.2" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-3.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-3.2" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-3.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.8" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="21"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MF01-2-HP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 4 (2 x 2)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="2" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.7" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-0.1" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="2.7" y1="1.1" x2="2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="2.7" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="2.7" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.6" y1="1.1" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.8" y1="1.1" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="1.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.6" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="2.7" y1="1.1" x2="2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="2.7" y1="13.9" x2="1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="2.7" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="51"/>
<hole x="0" y="7.3" drill="3"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3.2" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-3.2" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-3.2" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-3.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-3.2" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-3.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.8" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="21"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MF01-2-V">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 4 (2 x 2)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="2" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.5" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="0.9" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.3" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.5" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.7" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.9" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="2.1" x2="-2.7" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="2.3" x2="-2.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="-3.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-1" y1="-2.8" x2="-2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="1" y1="-2.8" x2="-1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.7" y1="-7.5" x2="2.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="2.7" y1="2.5" x2="2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="2" y1="2.5" x2="2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="2" y1="2.1" x2="-1.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-1.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-1.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="2.1" x2="-2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.1" x2="-2" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.2" x2="-2" y2="2.4" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.4" x2="-2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.5" x2="-2.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="2.5" x2="-2.3" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.5" x2="-2.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="2.7" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="2.5" width="0.1524" layer="51"/>
<wire x1="2.7" y1="-7.5" x2="2.7" y2="2.5" width="0.1524" layer="51"/>
<wire x1="2.7" y1="2.5" x2="2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="2" y1="2.1" x2="-2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="-2" y1="2.1" x2="-2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-2" y1="2.5" x2="-2.7" y2="2.5" width="0.1524" layer="51"/>
<wire x1="2.7" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-2.5" y1="2.5" x2="-2.7" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.5" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="2.5" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.4" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.2" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="2.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
</package>
<package name="MF01-2-VP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-6.5" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="3" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.5" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<hole x="-4.7" y="-5.04" drill="3"/>
<hole x="4.7" y="-5.04" drill="3"/>
<wire x1="-2.7" y1="-6.5" x2="-2.7" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-3.6" x2="-2.7" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="0.9" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.3" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.5" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.7" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.9" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="-3.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-1" y1="-2.8" x2="-2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="1" y1="-2.8" x2="-1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.7" y1="-7.5" x2="2.7" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="2.7" y1="-6.5" x2="2.7" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="2.7" y1="-3.6" x2="2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="2.7" y1="2.1" x2="-1.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-1.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-1.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="2.1" x2="-2.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="2.1" x2="-2.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.1" x2="-2.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="2.1" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="2.7" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-6.5" x2="-2.7" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-3.6" x2="-2.7" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="2.1" width="0.1524" layer="51"/>
<wire x1="2.7" y1="-7.5" x2="2.7" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="2.7" y1="-6.5" x2="2.7" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="2.7" y1="-3.6" x2="2.7" y2="2.1" width="0.1524" layer="51"/>
<wire x1="2.7" y1="2.1" x2="-2.7" y2="2.1" width="0.1524" layer="51"/>
<wire x1="2.7" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-1.9" y1="2.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-3.6" x2="-3.8" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-6.5" x2="-3.8" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-3.6" x2="2.7" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-6.5" x2="2.7" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-6.5" x2="2.7" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-3.6" x2="-3.8" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-6.5" x2="-3.8" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="3.8" y1="-3.6" x2="2.7" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-3.8" y1="-6.5" x2="-3.8" y2="-3.6" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="3.8" y1="-3.6" x2="3.8" y2="-6.5" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="-2.1" y1="2.1" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.1" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="2.1" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
</package>
<package name="MA02-1-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.27" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="JST-PH-02-V">
<description>&lt;h3&gt;Connector 2mm 2 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.95" y1="1.72" x2="3.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-2.78" x2="-1.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.72" x2="-1.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="1.72" x2="-1.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.72" x2="-1.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="1.72" x2="-1" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.72" x2="-0.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="1.72" x2="-0.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.72" x2="-0.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-0.4" y1="1.72" x2="-0.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-0.2" y1="1.72" x2="0.27" y2="1.72" width="0.2032" layer="51"/>
<wire x1="1.73" y1="1.72" x2="3.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="1.73" y1="1.22" x2="0.27" y2="1.22" width="0.2032" layer="51"/>
<wire x1="0.27" y1="1.22" x2="0.27" y2="1.72" width="0.2032" layer="51"/>
<wire x1="1.73" y1="1.22" x2="1.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="-2.78" x2="-1.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="1" y="2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.95" y1="0.12" x2="-1.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.32" x2="-1.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.52" x2="-1.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.72" x2="-1.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.92" x2="-1.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.12" x2="-1.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.32" x2="-1.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.52" x2="-1.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.12" x2="-0.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.32" x2="-0.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.52" x2="-0.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.72" x2="-0.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="0.92" x2="-1" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.12" x2="-1.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.32" x2="-1.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.52" x2="-1.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="3.95" y1="1.72" x2="3.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.78" x2="-1.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.72" x2="-1.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="1.72" x2="-1.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="1.72" x2="-1.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="1.72" x2="-1" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1" y1="1.72" x2="-0.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="1.72" x2="-0.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="1.72" x2="-0.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-0.4" y1="1.72" x2="-0.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-0.2" y1="1.72" x2="0.27" y2="1.72" width="0.2032" layer="21"/>
<wire x1="1.73" y1="1.72" x2="3.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="1.73" y1="1.22" x2="0.27" y2="1.22" width="0.2032" layer="21"/>
<wire x1="0.27" y1="1.22" x2="0.27" y2="1.72" width="0.2032" layer="21"/>
<wire x1="1.73" y1="1.22" x2="1.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="-2.78" x2="-1.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.12" x2="-1.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.32" x2="-1.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.52" x2="-1.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.72" x2="-1.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.92" x2="-1.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.12" x2="-1.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.32" x2="-1.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.52" x2="-1.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.12" x2="-0.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.32" x2="-0.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.52" x2="-0.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.72" x2="-0.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="0.92" x2="-1" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.12" x2="-1.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.32" x2="-1.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.52" x2="-1.6" y2="1.72" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-02-H">
<description>&lt;h3&gt;Connector 2mm 2 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="2" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<wire x1="3.95" y1="1.6" x2="3.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-6" x2="1.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-6" x2="0.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-6" x2="-1.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="-6" x2="-1.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="1.6" x2="3.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.6" x2="-1.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="3.3" y1="1.6" x2="3.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.3" y1="1.6" x2="-1.3" y2="0" width="0.2032" layer="51"/>
<text x="1" y="1.032" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5" y1="-3.6" x2="1.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-3.6" x2="0.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-3.6" x2="0.5" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-5.5" x2="0" y2="-3.5" width="0.127" layer="51"/>
<wire x1="0" y1="-3.5" x2="-1" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-1" y1="-3.5" x2="-0.5" y2="-5.5" width="0.127" layer="51"/>
<wire x1="-1.3" y1="0" x2="3.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<wire x1="3.95" y1="1.6" x2="3.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-6" x2="1.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-6" x2="0.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-6" x2="-1.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="-6" x2="-1.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="1.6" x2="3.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.6" x2="-1.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="1.6" x2="-1.3" y2="0" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-3.6" x2="1.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-3.6" x2="0.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-3.6" x2="0.5" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-5.5" x2="0" y2="-3.5" width="0.127" layer="21"/>
<wire x1="0" y1="-3.5" x2="-1" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-3.5" x2="-0.5" y2="-5.5" width="0.127" layer="21"/>
</package>
<package name="JST-SR-02-HS">
<description>&lt;h3&gt;Connector 1mm 2 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM02B-SRSS-TB, JOINT TECH A1001-WR-S-02P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-2.3" x2="-1.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.05" x2="-1.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.25" x2="-1.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.45" x2="-1.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.65" x2="-1.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.95" x2="-1.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="1.95" x2="-1" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.95" x2="-0.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="1.95" x2="-0.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.95" x2="2.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="2.2" y1="1.95" x2="2.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-2.3" x2="0.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-2.3" x2="-0.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0.5" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="2.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-1.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="2.5" y1="1.95" x2="2.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="2.5" y1="0.75" x2="2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="1.95" x2="-1.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="0.75" x2="-1.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="2.2" y1="1.95" x2="2.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="2.2" y1="0.75" x2="2.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="0" y1="-1" x2="-0.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-2.3" x2="0" y2="-1" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="1.95" x2="-1.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.95" x2="-1.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="1.95" x2="-1.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.95" x2="-1.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-0.45" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="1.05" x2="-1.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="1.25" x2="-1.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="1.45" x2="-1.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="1.65" x2="-1.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="1.95" x2="-1.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="1.95" x2="-1" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-1" y1="1.95" x2="-0.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="1.95" x2="-0.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="1.6" y1="1.95" x2="2.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.95" x2="2.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-2.3" x2="0.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-2.3" x2="-0.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="2.5" y1="1.95" x2="2.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.75" x2="2.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="1.95" x2="-1.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="-1.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.95" x2="2.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.75" x2="2.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="0" y1="-1" x2="-0.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-2.3" x2="0" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="1.95" x2="-1.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-1" y1="1.95" x2="-1.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="1.95" x2="-1.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="1.95" x2="-1.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-02-VS">
<description>&lt;h3&gt;Connector 1mm 2 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM02B-SRSS-RM, JOINT TECH A1001-WV-S-02P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.5" y1="0.95" x2="2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.85" x2="2.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-1.95" x2="2.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-1.95" x2="-0.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="-1.95" x2="-0.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-1.95" x2="-1" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.95" x2="-1.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="-1.95" x2="-1.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="0.95" x2="2.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-1.95" x2="-1.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0.5" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<smd name="1" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-1.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="2.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-1.5" y1="-1.65" x2="-1.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-1.45" x2="-1.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-1.05" x2="-1.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-0.85" x2="-1.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-1.95" x2="2.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-1.95" x2="-1.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.95" x2="-1.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="-1.95" x2="-1.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="-1.95" x2="-1.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.85" x2="2.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="-1.95" x2="-1.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="-0.85" x2="-1.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="0" y1="0.65" x2="-0.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="0" y1="0.65" x2="0.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-0.2" y1="1.15" x2="0.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.85" x2="2.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-1.95" x2="2.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.95" x2="1.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-1.95" x2="-0.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-1.95" x2="-0.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.95" x2="-1" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.95" x2="-1.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-1.95" x2="-1.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.95" x2="1.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.95" x2="-1.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.65" x2="-1.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.45" x2="-1.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.05" x2="-1.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.95" x2="2.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.95" x2="-1.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.95" x2="-1.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-1.95" x2="-1.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-1.95" x2="-1.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.85" x2="2.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-1.95" x2="-1.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-0.85" x2="-1.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="0" y1="0.65" x2="-0.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="0" y1="0.65" x2="0.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-0.2" y1="1.15" x2="0.2" y2="1.15" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-02-HS">
<description>&lt;h3&gt;Connector 2mm 2 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S2B-PH-SM4-TB, NXW-02SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.9" y1="-4" x2="-2.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="2.4" x2="-2.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="2.7" x2="-2.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="2.9" x2="-2.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="3.1" x2="-2.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="3.3" x2="-2.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="3.6" x2="-2.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="3.6" x2="-2.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="3.6" x2="-2.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="3.6" x2="-2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-2" y1="3.6" x2="4.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="4.6" y1="3.6" x2="4.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-4" x2="0.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-4" x2="-0.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-4" x2="-2.9" y2="-4" width="0.2032" layer="51"/>
<text x="1" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="4.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-2.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="4.9" y1="3.6" x2="4.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="4.9" y1="2.4" x2="4.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="3.6" x2="-2.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="2.4" x2="-2.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="4.6" y1="3.6" x2="4.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="4.6" y1="2.4" x2="4.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="0" y1="-2.7" x2="-0.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-4" x2="0" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="3.6" x2="-2.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="3.6" x2="-2.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="3.6" x2="-2.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-2" y1="3.6" x2="-2.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-0.5" x2="-2.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="2.4" x2="-2.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="2.7" x2="-2.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="2.9" x2="-2.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="3.1" x2="-2.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="3.3" x2="-2.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="3.6" x2="-2.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="3.6" x2="-2.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="3.6" x2="-2.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="3.6" x2="-2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="2.8" y1="3.6" x2="4.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="3.6" x2="4.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-4" x2="0.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-4" x2="-0.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.9" y1="3.6" x2="4.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="4.9" y1="2.4" x2="4.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="3.6" x2="-2.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="2.4" x2="-2.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="4.6" y1="3.6" x2="4.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="4.6" y1="2.4" x2="4.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="0" y1="-2.7" x2="-0.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-4" x2="0" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="3.6" x2="-2.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="3.6" x2="-2.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="3.6" x2="-2.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-2" y1="3.6" x2="-2.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<smd name="2" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<wire x1="-0.5" y1="-4" x2="-1.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="3.6" x2="-2" y2="3.6" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-02-VS">
<description>&lt;h3&gt;Connector 2mm 4 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B4B-PH-SM4-TB, NXW-04SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="4.95" y1="1.9" x2="4.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-2" x2="4.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-3.1" x2="4.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.1" x2="-2.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-2.05" y1="-3.1" x2="-2.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-3.1" x2="-2.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-3.1" x2="-2.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="-3.1" x2="-2.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-2.95" y1="1.9" x2="4.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.95" y1="-3.1" x2="-2.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="1" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<smd name="1" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-2.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="4.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-2.95" y1="-2.8" x2="-2.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-2.95" y1="-2.6" x2="-2.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-2.95" y1="-2.4" x2="-2.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-2.95" y1="-2.2" x2="-2.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.95" y1="-2" x2="-2.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.1" x2="4.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-3.1" x2="-2.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-3.1" x2="-2.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="-3.1" x2="-2.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-2.05" y1="-3.1" x2="-2.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-2" x2="4.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="-3.1" x2="-2.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="-2" x2="-2.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="0" y1="1.6" x2="-0.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="0" y1="1.6" x2="0.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-0.2" y1="2.1" x2="0.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-2.4" x2="4.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-3.1" x2="4.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-3.1" x2="2.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-2.05" y1="-3.1" x2="-2.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="-3.1" x2="-2.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-3.1" x2="-2.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-3.1" x2="-2.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="1.9" x2="4.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-3.1" x2="-2.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-2.8" x2="-2.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-2.6" x2="-2.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-3.1" x2="4.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="-3.1" x2="-2.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-3.1" x2="-2.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-3.1" x2="-2.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.05" y1="-3.1" x2="-2.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.4" x2="4.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-3.1" x2="-2.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-2.4" x2="-2.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2.75" y1="-2.4" x2="-2.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="0" y1="1.6" x2="-0.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="0" y1="1.6" x2="0.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-0.2" y1="2.1" x2="0.2" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<text x="1" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<wire x1="-2.95" y1="1.9" x2="-2.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="4.95" y1="1.9" x2="4.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-3.1" x2="-2.05" y2="-3.1" width="0.2032" layer="21"/>
</package>
<package name="MF02-2-V">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 4 (2 x 2)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="3" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.5" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<pad name="2" x="4.2" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<pad name="4" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="0.9" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.3" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.5" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.7" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.9" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="2.1" x2="-2.7" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="2.3" x2="-2.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.8" x2="2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-2.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-6.8" x2="6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.4" x2="2.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="-3.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-1" y1="-2.8" x2="-2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="1" y1="-2.8" x2="-1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="6.9" y1="2.5" x2="6.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="6.2" y1="2.5" x2="6.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="6.2" y1="2.1" x2="-1.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-1.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-1.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="2.1" x2="-2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.1" x2="-2" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.2" x2="-2" y2="2.4" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.4" x2="-2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.5" x2="-2.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="2.5" x2="-2.3" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.5" x2="-2.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="2.5" x2="6.2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.2" y1="2.5" x2="6.2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.2" y1="2.1" x2="-2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="-2" y1="2.1" x2="-2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-2" y1="2.5" x2="-2.7" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-2.5" y1="2.5" x2="-2.7" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.5" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="2.5" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.4" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.2" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="2.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
</package>
<package name="MF02-2-HP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 4 (2 x 2)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="3" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.7" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="2" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.6" y1="1.1" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.8" y1="1.1" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="1.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.6" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="13.9" x2="3.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="0.6" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="51"/>
<hole x="2.1" y="7.3" drill="3"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3.2" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-3.2" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-3.2" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-3.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-3.2" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-3.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.8" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MF02-2-VP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-6.5" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="3" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.5" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<pad name="2" x="4.2" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<pad name="4" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<hole x="-4.7" y="-5.04" drill="3"/>
<hole x="8.9" y="-5.04" drill="3"/>
<wire x1="-2.7" y1="-6.5" x2="-2.7" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-3.6" x2="-2.7" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="0.9" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.3" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.5" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.7" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.9" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.8" x2="2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-2.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-6.8" x2="6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="51"/>
<wire x1="2" y1="1.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.4" x2="2.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="-3.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-1" y1="-2.8" x2="-2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="1" y1="-2.8" x2="-1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="1" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2" y1="-3.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-6.5" x2="6.9" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-3.6" x2="6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="6.9" y1="2.1" x2="-1.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-1.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-1.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="2.1" x2="-2.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.1" y1="2.1" x2="-2.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.1" x2="-2.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="2.1" x2="-2.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7" y1="-7.5" x2="-2.7" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-6.5" x2="-2.7" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-3.6" x2="-2.7" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-0.1" x2="-2.7" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-6.5" x2="6.9" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-3.6" x2="6.9" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.9" y1="2.1" x2="-2.7" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-7.5" x2="-2.7" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-1.1" x2="-2.7" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-1.9" y1="2.1" x2="-2.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.7" y1="2.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-2.7" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-3.6" x2="-3.8" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="-6.5" x2="-3.8" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="8" y1="-3.6" x2="6.9" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="8" y1="-6.5" x2="6.9" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="8" y1="-6.5" x2="6.9" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-3.6" x2="-3.8" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="-6.5" x2="-3.8" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="8" y1="-3.6" x2="6.9" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-3.8" y1="-6.5" x2="-3.8" y2="-3.6" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="8" y1="-3.6" x2="8" y2="-6.5" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="-2.1" y1="2.1" x2="-2.7" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-2.3" y1="2.1" x2="-2.7" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="2.1" x2="-2.7" y2="1.9" width="0.1524" layer="21"/>
</package>
<package name="MF02-2-H">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 4 (2 x 2)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="0" y="0" drill="1.8" shape="square"/>
<pad name="3" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-1.7" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="2" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.6" y1="1.1" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.8" y1="1.1" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2" y1="1.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.6" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="13.9" x2="3.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="0.6" y1="13.9" x2="-2.7" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="-2.7" y2="1.1" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-2.7" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-3.2" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3.2" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-3.2" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-3.2" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-3.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-3.2" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-3.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="1.1" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.8" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-2.7" y1="13.9" x2="-3.2" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.3" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.5" x2="-2.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.7" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="1.9" x2="-2.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.1" x2="-2.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.3" x2="-2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.5" x2="-1.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MA02-2-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<text x="-1.27" y="4.191" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.889" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
</package>
<package name="MA03-1-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-3.81" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="JST-PH-03-H">
<description>&lt;h3&gt;Connector 2mm 3 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-2" y="0" drill="0.8" shape="octagon"/>
<pad name="2" x="0" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="2" y="0" drill="0.8" shape="octagon"/>
<wire x1="3.95" y1="1.6" x2="3.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-6" x2="1" y2="-6" width="0.2032" layer="51"/>
<wire x1="1" y1="-6" x2="-1" y2="-6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-6" x2="-3.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-6" x2="-3.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="1.6" x2="3.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.6" x2="-3.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="3.3" y1="1.6" x2="3.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.6" x2="-3.3" y2="0" width="0.2032" layer="51"/>
<text x="0" y="1" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1" y1="-3.6" x2="1" y2="-6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-3.6" x2="-1" y2="-6" width="0.2032" layer="51"/>
<wire x1="1" y1="-3.6" x2="-1" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-2" y1="-5.5" x2="-1.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-3.5" x2="-2.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3.5" x2="-2" y2="-5.5" width="0.127" layer="51"/>
<wire x1="-3.3" y1="0" x2="3.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-2.3" y1="0" x2="-1.7" y2="0.3" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<wire x1="3.95" y1="1.6" x2="3.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-6" x2="1" y2="-6" width="0.2032" layer="21"/>
<wire x1="1" y1="-6" x2="-1" y2="-6" width="0.2032" layer="21"/>
<wire x1="-1" y1="-6" x2="-3.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-6" x2="-3.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="1.6" x2="3.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.6" x2="-3.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="1" y1="-3.6" x2="1" y2="-6" width="0.2032" layer="21"/>
<wire x1="-1" y1="-3.6" x2="-1" y2="-6" width="0.2032" layer="21"/>
<wire x1="1" y1="-3.6" x2="-1" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2" y1="-5.5" x2="-1.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.5" x2="-2.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3.5" x2="-2" y2="-5.5" width="0.127" layer="21"/>
</package>
<package name="JST-PH-03-V">
<description>&lt;h3&gt;Connector 2mm 3 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.95" y1="1.72" x2="3.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-2.78" x2="-3.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.72" x2="-3.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="1.72" x2="-3.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="1.72" x2="-3.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.72" x2="-3" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3" y1="1.72" x2="-2.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1.72" x2="-2.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.72" x2="-2.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.72" x2="-2.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.72" x2="-1.23" y2="1.72" width="0.2032" layer="51"/>
<wire x1="1.23" y1="1.72" x2="3.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="1.23" y1="1.22" x2="-1.23" y2="1.22" width="0.2032" layer="51"/>
<wire x1="-1.23" y1="1.22" x2="-1.23" y2="1.72" width="0.2032" layer="51"/>
<wire x1="1.23" y1="1.22" x2="1.23" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-2.78" x2="-3.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="-2" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="0" y="2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-3.95" y1="0.12" x2="-3.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.32" x2="-3.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.52" x2="-3.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.72" x2="-3.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.92" x2="-3.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.12" x2="-3.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.32" x2="-3.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.52" x2="-3.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.12" x2="-2.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.32" x2="-2.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.52" x2="-2.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.72" x2="-2.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.92" x2="-3" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.12" x2="-3.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.32" x2="-3.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.52" x2="-3.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<wire x1="3.95" y1="1.72" x2="3.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.78" x2="-3.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.72" x2="-3.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="1.72" x2="-3.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="1.72" x2="-3.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.72" x2="-3" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.72" x2="-2.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="1.72" x2="-2.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.72" x2="-2.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1.72" x2="-2.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.72" x2="-1.23" y2="1.72" width="0.2032" layer="21"/>
<wire x1="1.23" y1="1.72" x2="3.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="1.23" y1="1.22" x2="-1.23" y2="1.22" width="0.2032" layer="21"/>
<wire x1="-1.23" y1="1.22" x2="-1.23" y2="1.72" width="0.2032" layer="21"/>
<wire x1="1.23" y1="1.22" x2="1.23" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-2.78" x2="-3.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.12" x2="-3.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.32" x2="-3.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.52" x2="-3.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.72" x2="-3.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.92" x2="-3.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.12" x2="-3.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.32" x2="-3.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.52" x2="-3.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.12" x2="-2.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.32" x2="-2.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.52" x2="-2.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.72" x2="-2.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.92" x2="-3" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.12" x2="-3.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.32" x2="-3.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.52" x2="-3.6" y2="1.72" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-03-HS">
<description>&lt;h3&gt;Connector 2mm 3 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S3B-PH-SM4-TB, NXW-03SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.9" y1="-4" x2="-4.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.4" x2="-4.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.7" x2="-4.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.9" x2="-4.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="3.1" x2="-4.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="3.3" x2="-4.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="3.6" x2="-4.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="3.6" x2="-4.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="3.6" x2="-4.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="3.6" x2="-4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4" y1="3.6" x2="4.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="4.6" y1="3.6" x2="4.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-4" x2="-1.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-4" x2="-2.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-4" x2="-4.9" y2="-4" width="0.2032" layer="51"/>
<text x="0" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="4.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-4.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="4.9" y1="3.6" x2="4.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="4.9" y1="2.4" x2="4.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="3.6" x2="-4.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.4" x2="-4.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="4.6" y1="3.6" x2="4.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="4.6" y1="2.4" x2="4.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-2" y1="-2.7" x2="-2.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-4" x2="-2" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="3.6" x2="-4.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="3.6" x2="-4.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="3.6" x2="-4.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-4" y1="3.6" x2="-4.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-0.5" x2="-4.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.4" x2="-4.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.7" x2="-4.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.9" x2="-4.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="3.1" x2="-4.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="3.3" x2="-4.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="3.6" x2="-4.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="3.6" x2="-4.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="3.6" x2="-4.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="3.6" x2="-4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="2.8" y1="3.6" x2="4.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="3.6" x2="4.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-4" x2="-1.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-2.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.9" y1="3.6" x2="4.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="4.9" y1="2.4" x2="4.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="3.6" x2="-4.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.4" x2="-4.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="4.6" y1="3.6" x2="4.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="4.6" y1="2.4" x2="4.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.7" x2="-2.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-2" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="3.6" x2="-4.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="3.6" x2="-4.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="3.6" x2="-4.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-4" y1="3.6" x2="-4.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-2.2" y1="3.6" x2="-1.8" y2="4.6" layer="51"/>
<smd name="2" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="3" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<wire x1="-2.5" y1="-4" x2="-3.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="3.6" x2="-4" y2="3.6" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-03-VS">
<description>&lt;h3&gt;Connector 2mm 3 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B3B-PH-SM4-TB, NXW-03SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="4.95" y1="1.9" x2="4.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-2" x2="4.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-3.1" x2="4.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.1" x2="-4.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.05" y1="-3.1" x2="-4.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.25" y1="-3.1" x2="-4.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.45" y1="-3.1" x2="-4.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="1.9" x2="4.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="0" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<smd name="1" x="-2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-4.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="4.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-4.95" y1="-2.8" x2="-4.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2.6" x2="-4.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2.4" x2="-4.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2.2" x2="-4.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2" x2="-4.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.65" y1="-3.1" x2="4.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.25" y1="-3.1" x2="-4.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-4.45" y1="-3.1" x2="-4.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-4.05" y1="-3.1" x2="-4.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-2" x2="4.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.1" x2="-4.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-2" x2="-4.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.6" x2="-2.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.6" x2="-1.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="2.1" x2="-1.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="4.95" y1="-2.4" x2="4.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-3.1" x2="4.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-3.1" x2="2.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.05" y1="-3.1" x2="-4.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3.1" x2="-4.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-3.1" x2="-4.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="1.9" x2="4.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-2.8" x2="-4.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-2.6" x2="-4.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-3.1" x2="4.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3.1" x2="-4.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-3.1" x2="-4.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-4.05" y1="-3.1" x2="-4.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.4" x2="4.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.1" x2="-4.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-2.4" x2="-4.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.75" y1="-2.4" x2="-4.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.6" x2="-2.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.6" x2="-1.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="2.1" x2="-1.8" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<text x="0" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-4.1" x2="-1.75" y2="-3.1" layer="51"/>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<wire x1="-4.95" y1="1.9" x2="-4.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="4.95" y1="1.9" x2="4.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-3.1" x2="-4.05" y2="-3.1" width="0.2032" layer="21"/>
</package>
<package name="JST-SR-03-HS">
<description>&lt;h3&gt;Connector 1mm 3 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM03B-SRSS-TB, JOINT TECH A1001-WR-S-03P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.5" y1="-2.3" x2="-2.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="0.75" x2="-2.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.05" x2="-2.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.25" x2="-2.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.45" x2="-2.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.65" x2="-2.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.95" x2="-2.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.95" x2="-2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.95" x2="-1.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="1.95" x2="-1.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="1.95" x2="2.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="2.2" y1="1.95" x2="2.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-2.3" x2="-0.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="-1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="2.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-2.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="2.5" y1="1.95" x2="2.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="2.5" y1="0.75" x2="2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.95" x2="-2.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="0.75" x2="-2.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="2.2" y1="1.95" x2="2.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="2.2" y1="0.75" x2="2.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-2.3" x2="-1" y2="-1" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.95" x2="-2.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.95" x2="-2.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="1.95" x2="-2.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="1.95" x2="-2.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-0.45" x2="-2.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.75" x2="-2.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.05" x2="-2.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.25" x2="-2.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.45" x2="-2.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.65" x2="-2.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.95" x2="-2.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.95" x2="-2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.95" x2="-1.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.95" x2="-1.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="1.6" y1="1.95" x2="2.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.95" x2="2.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-2.3" x2="-0.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="2.5" y1="1.95" x2="2.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.75" x2="2.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.95" x2="-2.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.75" x2="-2.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.95" x2="2.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.75" x2="2.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1" x2="-1.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-2.3" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.95" x2="-2.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.95" x2="-2.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.95" x2="-2.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="1.95" x2="-2.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-1.2" y1="2" x2="-0.8" y2="2.6" layer="51"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-03-VS">
<description>&lt;h3&gt;Connector 1mm 3 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM03B-SRSS-RM, JOINT TECH A1001-WV-S-03P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.5" y1="0.95" x2="2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.85" x2="2.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-1.95" x2="2.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-1.95" x2="-1.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.95" x2="-1.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="-1.95" x2="-2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.95" x2="-2.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="0.95" x2="2.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<smd name="1" x="-1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-2.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="2.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-2.5" y1="-1.65" x2="-2.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.45" x2="-2.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.05" x2="-2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-0.85" x2="-2.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-1.95" x2="2.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="-1.95" x2="-2.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.95" x2="-2.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.95" x2="-2.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.85" x2="2.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-1.95" x2="-2.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-0.85" x2="-2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.65" x2="-1.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.65" x2="-0.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="1.15" x2="-0.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.85" x2="2.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-1.95" x2="2.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.95" x2="1.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.95" x2="-1.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.95" x2="-1.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.95" x2="-2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.95" x2="-2.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.95" x2="1.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.65" x2="-2.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.45" x2="-2.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.05" x2="-2.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.95" x2="2.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.95" x2="-2.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.95" x2="-2.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.95" x2="-2.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.85" x2="2.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-1.95" x2="-2.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-0.85" x2="-2.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1" y1="0.65" x2="-1.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-1" y1="0.65" x2="-0.8" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="1.15" x2="-0.8" y2="1.15" width="0.2032" layer="21"/>
</package>
<package name="MF03-2-V">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 6 (2 x 3)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="4" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.7" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="5" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<pad name="6" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="0.9" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.3" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.5" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.7" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.9" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="2.1" x2="-6.9" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="2.3" x2="-6.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="1.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-6.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-5.2" y1="-2.8" x2="-6.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.2" y1="-2.8" x2="-5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="6.2" y1="-2.8" x2="2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-2.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-6.8" x2="6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.4" x2="2.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="6.9" y1="2.5" x2="6.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="6.2" y1="2.5" x2="6.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="6.2" y1="2.1" x2="-5.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-5.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.1" y1="2.1" x2="-6.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.1" x2="-6.2" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.2" x2="-6.2" y2="2.4" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.4" x2="-6.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.5" x2="-6.3" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.5" x2="-6.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.7" y1="2.5" x2="-6.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="2.5" x2="6.2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.2" y1="2.5" x2="6.2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.2" y1="2.1" x2="-6.2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="-6.2" y1="2.1" x2="-6.2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-6.2" y1="2.5" x2="-6.9" y2="2.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-6.7" y1="2.5" x2="-6.9" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.5" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.4" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.2" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.1" y1="2.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
</package>
<package name="MF03-2-VP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-6.5" width="0.1524" layer="21"/>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="4" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.7" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="5" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<pad name="6" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<hole x="-8.9" y="-5.04" drill="3"/>
<hole x="8.9" y="-5.04" drill="3"/>
<wire x1="-6.9" y1="-6.5" x2="-6.9" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-3.6" x2="-6.9" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="0.9" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.3" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.5" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.7" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.9" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="1.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-6.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-5.2" y1="-2.8" x2="-6.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.2" y1="-2.8" x2="-5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="6.2" y1="-2.8" x2="2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-2.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-6.8" x2="6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.4" x2="2.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-6.5" x2="6.9" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-3.6" x2="6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="6.9" y1="2.1" x2="-5.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-5.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.1" y1="2.1" x2="-6.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.3" y1="2.1" x2="-6.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.1" x2="-6.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.7" y1="2.1" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="6.9" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-6.5" x2="-6.9" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-3.6" x2="-6.9" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="6.9" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-6.5" x2="6.9" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-3.6" x2="6.9" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.9" y1="2.1" x2="-6.9" y2="2.1" width="0.1524" layer="51"/>
<wire x1="6.9" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-6.1" y1="2.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-3.6" x2="-8" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-6.5" x2="-8" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="8" y1="-3.6" x2="6.9" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="8" y1="-6.5" x2="6.9" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="8" y1="-6.5" x2="6.9" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-3.6" x2="-8" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-6.5" x2="-8" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="8" y1="-3.6" x2="6.9" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-8" y1="-6.5" x2="-8" y2="-3.6" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="8" y1="-3.6" x2="8" y2="-6.5" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="-6.3" y1="2.1" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.1" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.7" y1="2.1" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
</package>
<package name="MF03-2-H">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="4" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.9" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-0.1" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="5" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="6" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.8" y1="1.1" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6" y1="1.1" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="1.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.4" y1="1.1" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.6" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.8" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="13.9" x2="1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="51"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.4" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7.4" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-7.4" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-7.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-7.4" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-7.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7" y1="1.1" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.2" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MF03-2-HP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="4" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.9" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-0.1" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="5" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="6" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="6.9" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.8" y1="1.1" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6" y1="1.1" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="1.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.4" y1="1.1" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.6" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.8" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="13.9" x2="1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="6.9" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="51"/>
<hole x="-4.2" y="7.3" drill="3"/>
<hole x="4.2" y="7.3" drill="3"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.4" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7.4" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-7.4" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-7.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-7.4" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-7.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7" y1="1.1" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.2" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MA03-2-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<text x="-3.81" y="4.191" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="2.286" x2="-2.286" y2="2.794" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
</package>
<package name="MA04-1-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-3.81" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="JST-PH-04-H">
<description>&lt;h3&gt;Connector 2mm 4 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="4" x="4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<wire x1="5.95" y1="1.6" x2="5.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="5.95" y1="-6" x2="2.8" y2="-6" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-6" x2="-0.8" y2="-6" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-6" x2="-3.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-6" x2="-3.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="5.95" y1="1.6" x2="5.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.6" x2="-3.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="5.3" y1="1.6" x2="5.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.6" x2="-3.3" y2="0" width="0.2032" layer="51"/>
<text x="1" y="1" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.8" y1="-3.6" x2="2.8" y2="-6" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-3.6" x2="-0.8" y2="-6" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-3.55" x2="-0.8" y2="-3.55" width="0.2032" layer="51"/>
<wire x1="-2" y1="-5.5" x2="-1.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-3.5" x2="-2.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3.5" x2="-2" y2="-5.5" width="0.127" layer="51"/>
<wire x1="-3.3" y1="0" x2="5.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-2.3" y1="0" x2="-1.7" y2="0.3" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<rectangle x1="3.7" y1="0" x2="4.3" y2="0.3" layer="51"/>
<wire x1="5.95" y1="1.6" x2="5.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="5.95" y1="-6" x2="2.8" y2="-6" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-6" x2="-0.8" y2="-6" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-6" x2="-3.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-6" x2="-3.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="5.95" y1="1.6" x2="5.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.6" x2="-3.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="5.3" y1="1.6" x2="5.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-3.6" x2="2.8" y2="-6" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-3.6" x2="-0.8" y2="-6" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-3.55" x2="-0.8" y2="-3.55" width="0.2032" layer="21"/>
<wire x1="-2" y1="-5.5" x2="-1.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.5" x2="-2.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3.5" x2="-2" y2="-5.5" width="0.127" layer="21"/>
</package>
<package name="JST-PH-04-V">
<description>&lt;h3&gt;Connector 2mm 4 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.95" y1="1.72" x2="5.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="5.95" y1="-2.78" x2="-3.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.72" x2="-3.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="1.72" x2="-3.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="1.72" x2="-3.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.72" x2="-3" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3" y1="1.72" x2="-2.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1.72" x2="-2.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.72" x2="-2.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.72" x2="-2.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.72" x2="-0.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="2.73" y1="1.72" x2="5.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="2.73" y1="1.22" x2="-0.73" y2="1.22" width="0.2032" layer="51"/>
<wire x1="-0.73" y1="1.22" x2="-0.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="2.73" y1="1.22" x2="2.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-2.78" x2="-3.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="-2" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="4" x="4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="1" y="2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-3.95" y1="0.12" x2="-3.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.32" x2="-3.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.52" x2="-3.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.72" x2="-3.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.92" x2="-3.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.12" x2="-3.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.32" x2="-3.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.52" x2="-3.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.12" x2="-2.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.32" x2="-2.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.52" x2="-2.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.72" x2="-2.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="0.92" x2="-3" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.12" x2="-3.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.32" x2="-3.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="1.52" x2="-3.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<wire x1="5.95" y1="1.72" x2="5.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="5.95" y1="-2.78" x2="-3.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.72" x2="-3.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="1.72" x2="-3.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="1.72" x2="-3.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.72" x2="-3" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.72" x2="-2.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="1.72" x2="-2.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.72" x2="-2.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1.72" x2="-2.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.72" x2="-0.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="2.73" y1="1.72" x2="5.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="2.73" y1="1.22" x2="-0.73" y2="1.22" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1.22" x2="-0.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="2.73" y1="1.22" x2="2.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-2.78" x2="-3.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.12" x2="-3.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.32" x2="-3.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.52" x2="-3.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.72" x2="-3.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.92" x2="-3.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.12" x2="-3.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.32" x2="-3.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.52" x2="-3.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.12" x2="-2.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.32" x2="-2.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.52" x2="-2.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.72" x2="-2.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="0.92" x2="-3" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.12" x2="-3.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.32" x2="-3.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="1.52" x2="-3.6" y2="1.72" width="0.2032" layer="21"/>
</package>
<package name="JST-SR-04-HS">
<description>&lt;h3&gt;Connector 1mm 4 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM04B-SRSS-TB, JOINT TECH A1001-WR-S-04P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.5" y1="-2.3" x2="-2.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="0.75" x2="-2.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.05" x2="-2.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.25" x2="-2.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.45" x2="-2.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.65" x2="-2.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.95" x2="-2.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.95" x2="-2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.95" x2="-1.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="1.95" x2="-1.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="1.95" x2="3.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="3.2" y1="1.95" x2="3.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-2.3" x2="-0.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0.5" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="-1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="3.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-2.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="3.5" y1="1.95" x2="3.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="3.5" y1="0.75" x2="3.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.95" x2="-2.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="0.75" x2="-2.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="3.2" y1="1.95" x2="3.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="3.2" y1="0.75" x2="3.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-2.3" x2="-1" y2="-1" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.95" x2="-2.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.95" x2="-2.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="1.95" x2="-2.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="1.95" x2="-2.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-0.45" x2="-2.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.75" x2="-2.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.05" x2="-2.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.25" x2="-2.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.45" x2="-2.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.65" x2="-2.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.95" x2="-2.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.95" x2="-2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.95" x2="-1.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.95" x2="-1.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.95" x2="3.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.95" x2="3.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-2.3" x2="-0.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="3.5" y1="1.95" x2="3.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="3.5" y1="0.75" x2="3.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.95" x2="-2.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.75" x2="-2.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.95" x2="3.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="3.2" y1="0.75" x2="3.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1" x2="-1.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-2.3" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.95" x2="-2.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.95" x2="-2.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.95" x2="-2.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="1.95" x2="-2.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-1.2" y1="2" x2="-0.8" y2="2.6" layer="51"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
<rectangle x1="1.8" y1="2" x2="2.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-04-VS">
<description>&lt;h3&gt;Connector 1mm 4 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM04B-SRSS-RM, JOINT TECH A1001-WV-S-04P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.5" y1="0.95" x2="3.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-0.85" x2="3.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-1.95" x2="3.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-1.95" x2="-1.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.95" x2="-1.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="-1.95" x2="-2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.95" x2="-2.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="0.95" x2="3.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0.5" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<smd name="1" x="-1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-2.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="3.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-2.5" y1="-1.65" x2="-2.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.45" x2="-2.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.05" x2="-2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-0.85" x2="-2.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-1.95" x2="3.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="-1.95" x2="-2.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.95" x2="-2.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.95" x2="-2.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-0.85" x2="3.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-1.95" x2="-2.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="-0.85" x2="-2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.65" x2="-1.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.65" x2="-0.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="1.15" x2="-0.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-0.85" x2="3.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-1.95" x2="3.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.95" x2="2.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.95" x2="-1.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.95" x2="-1.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.95" x2="-2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.95" x2="-2.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.95" x2="2.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.65" x2="-2.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.45" x2="-2.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.05" x2="-2.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.95" x2="3.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.95" x2="-2.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.95" x2="-2.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-1.95" x2="-2.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.95" x2="-2.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-0.85" x2="3.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-1.95" x2="-2.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-0.85" x2="-2.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1" y1="0.65" x2="-1.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-1" y1="0.65" x2="-0.8" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="1.15" x2="-0.8" y2="1.15" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-04-HS">
<description>&lt;h3&gt;Connector 2mm 4 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S4B-PH-SM4-TB, NXW-04SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.9" y1="-4" x2="-4.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.4" x2="-4.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.7" x2="-4.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.9" x2="-4.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="3.1" x2="-4.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="3.3" x2="-4.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="3.6" x2="-4.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="3.6" x2="-4.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="3.6" x2="-4.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="3.6" x2="-4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-4" y1="3.6" x2="6.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="6.6" y1="3.6" x2="6.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="6.9" y1="-4" x2="-1.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-4" x2="-2.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-4" x2="-4.9" y2="-4" width="0.2032" layer="51"/>
<text x="1" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="6.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-4.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="6.9" y1="3.6" x2="6.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="6.9" y1="2.4" x2="6.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="3.6" x2="-4.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="2.4" x2="-4.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="6.6" y1="3.6" x2="6.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="6.6" y1="2.4" x2="6.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-2" y1="-2.7" x2="-2.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-4" x2="-2" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="3.6" x2="-4.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="3.6" x2="-4.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="3.6" x2="-4.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-4" y1="3.6" x2="-4.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-0.5" x2="-4.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.4" x2="-4.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.7" x2="-4.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.9" x2="-4.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="3.1" x2="-4.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="3.3" x2="-4.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="3.6" x2="-4.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="3.6" x2="-4.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="3.6" x2="-4.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="3.6" x2="-4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="4.8" y1="3.6" x2="6.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="6.6" y1="3.6" x2="6.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-4" x2="-1.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-2.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="6.9" y1="3.6" x2="6.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="6.9" y1="2.4" x2="6.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="3.6" x2="-4.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.4" x2="-4.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="6.6" y1="3.6" x2="6.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="6.6" y1="2.4" x2="6.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.7" x2="-2.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-2" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="3.6" x2="-4.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="3.6" x2="-4.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="3.6" x2="-4.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-4" y1="3.6" x2="-4.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-2.2" y1="3.6" x2="-1.8" y2="4.6" layer="51"/>
<smd name="2" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="3" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="4" x="4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<rectangle x1="3.8" y1="3.6" x2="4.2" y2="4.6" layer="51"/>
<wire x1="-2.5" y1="-4" x2="-3.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="3.6" x2="-4" y2="3.6" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-04-VS">
<description>&lt;h3&gt;Connector 2mm 4 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B4B-PH-SM4-TB, NXW-04SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.95" y1="1.9" x2="6.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-2" x2="6.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-3.1" x2="6.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="6.65" y1="-3.1" x2="-4.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.05" y1="-3.1" x2="-4.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.25" y1="-3.1" x2="-4.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.45" y1="-3.1" x2="-4.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="1.9" x2="6.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="1" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<smd name="1" x="-2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-4.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="6.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-4.95" y1="-2.8" x2="-4.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2.6" x2="-4.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2.4" x2="-4.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2.2" x2="-4.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.95" y1="-2" x2="-4.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="6.65" y1="-3.1" x2="6.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.25" y1="-3.1" x2="-4.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-4.45" y1="-3.1" x2="-4.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-4.05" y1="-3.1" x2="-4.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-2" x2="6.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-3.1" x2="-4.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.65" y1="-2" x2="-4.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.6" x2="-2.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.6" x2="-1.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="2.1" x2="-1.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-2.4" x2="6.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="6.95" y1="-3.1" x2="6.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="6.65" y1="-3.1" x2="4.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.05" y1="-3.1" x2="-4.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3.1" x2="-4.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-3.1" x2="-4.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="1.9" x2="6.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-2.8" x2="-4.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-2.6" x2="-4.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="6.65" y1="-3.1" x2="6.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3.1" x2="-4.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.45" y1="-3.1" x2="-4.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.1" x2="-4.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-4.05" y1="-3.1" x2="-4.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="6.95" y1="-2.4" x2="6.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.1" x2="-4.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-2.4" x2="-4.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4.75" y1="-2.4" x2="-4.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.6" x2="-2.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.6" x2="-1.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="2.1" x2="-1.8" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<text x="1" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-4.1" x2="-1.75" y2="-3.1" layer="51"/>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<rectangle x1="3.75" y1="-4.1" x2="4.25" y2="-3.1" layer="51"/>
<wire x1="-4.95" y1="1.9" x2="-4.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="6.95" y1="1.9" x2="6.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-3.1" x2="-4.05" y2="-3.1" width="0.2032" layer="21"/>
</package>
<package name="MA04-2-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="7" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="8" x="5.08" y="2.54" drill="1.016" shape="octagon"/>
<text x="-3.81" y="4.191" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="2.286" x2="-2.286" y2="2.794" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="51"/>
<wire x1="4.445" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="51"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.302" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.429" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-1.27" x2="-3.81" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-1.27" x2="-3.81" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="MF04-2-V">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="5" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.7" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="6" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<pad name="7" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="8" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="0.9" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.3" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.5" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.7" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.9" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="2.1" x2="-6.9" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="2.3" x2="-6.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="1.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-6.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-5.2" y1="-2.8" x2="-6.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.2" y1="-2.8" x2="-5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="6.2" y1="-2.8" x2="2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-2.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-6.8" x2="6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.4" x2="2.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="8.146" y1="-0.854" x2="8.654" y2="-0.346" layer="51"/>
<rectangle x1="8.146" y1="-5.054" x2="8.654" y2="-4.546" layer="51"/>
<wire x1="6.4" y1="-6.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="1.4" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="-2.6" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="1.4" x2="6.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="10.4" y1="1.4" x2="10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.4" y1="-3.8" x2="6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="7.4" y1="-2.8" x2="6.4" y2="-3.8" width="0.127" layer="51"/>
<wire x1="9.4" y1="-2.8" x2="7.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-3.8" x2="9.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-3.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="11.1" y1="2.5" x2="10.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="10.4" y1="2.5" x2="10.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="10.4" y1="2.1" x2="-5.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-5.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.1" y1="2.1" x2="-6.2" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.1" x2="-6.2" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.2" x2="-6.2" y2="2.4" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.4" x2="-6.2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.5" x2="-6.3" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.5" x2="-6.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-6.7" y1="2.5" x2="-6.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="2.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="2.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="2.5" x2="10.4" y2="2.5" width="0.1524" layer="51"/>
<wire x1="10.4" y1="2.5" x2="10.4" y2="2.1" width="0.1524" layer="51"/>
<wire x1="10.4" y1="2.1" x2="-6.2" y2="2.1" width="0.1524" layer="51"/>
<wire x1="-6.2" y1="2.1" x2="-6.2" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-6.2" y1="2.5" x2="-6.9" y2="2.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-6.7" y1="2.5" x2="-6.9" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.5" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.4" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="2.2" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.1" y1="2.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
</package>
<package name="MF04-2-H">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="5" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.9" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="2" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="6" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="7" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="8" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="9.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="7.2" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.8" y1="1.1" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6" y1="1.1" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="1.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.4" y1="1.1" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.6" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.8" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="13.9" x2="3.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="0.6" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="51"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<rectangle x1="7.8" y1="-6.1" x2="9" y2="1.1" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.4" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7.4" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-7.4" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-7.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-7.4" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-7.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7" y1="1.1" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.2" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8" y1="-1.6" x2="8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8.8" y1="-1.6" x2="8.8" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MF04-2-VP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-6.5" width="0.1524" layer="21"/>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="5" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.7" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="6" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<pad name="7" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="8" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<hole x="-8.9" y="-5.04" drill="3"/>
<hole x="13.1" y="-5.04" drill="3"/>
<wire x1="-6.9" y1="-6.5" x2="-6.9" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-3.6" x2="-6.9" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="0.9" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.3" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.5" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.7" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.9" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="1.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-6.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-5.2" y1="-2.8" x2="-6.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.2" y1="-2.8" x2="-5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-3.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="6.2" y1="-2.8" x2="2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-2.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-6.8" x2="6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="5.2" y1="1.4" x2="3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.4" x2="2.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="0.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="8.146" y1="-0.854" x2="8.654" y2="-0.346" layer="51"/>
<rectangle x1="8.146" y1="-5.054" x2="8.654" y2="-4.546" layer="51"/>
<wire x1="6.4" y1="-6.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="1.4" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="-2.6" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="1.4" x2="6.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="10.4" y1="1.4" x2="10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.4" y1="-3.8" x2="6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="7.4" y1="-2.8" x2="6.4" y2="-3.8" width="0.127" layer="51"/>
<wire x1="9.4" y1="-2.8" x2="7.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-3.8" x2="9.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-3.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-6.5" x2="11.1" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-3.6" x2="11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="11.1" y1="2.1" x2="-5.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-5.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.1" y1="2.1" x2="-6.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.3" y1="2.1" x2="-6.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.1" x2="-6.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-6.7" y1="2.1" x2="-6.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-6.9" y1="-7.5" x2="-6.9" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-6.5" x2="-6.9" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-3.6" x2="-6.9" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-0.1" x2="-6.9" y2="2.1" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-6.5" x2="11.1" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-3.6" x2="11.1" y2="2.1" width="0.1524" layer="51"/>
<wire x1="11.1" y1="2.1" x2="-6.9" y2="2.1" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-7.5" x2="-6.9" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-1.1" x2="-6.9" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-6.1" y1="2.1" x2="-6.9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-5.9" y1="2.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.7" y1="2.1" x2="-6.9" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-3.6" x2="-8" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="-6.5" x2="-8" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-3.6" x2="11.1" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-6.5" x2="11.1" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-6.5" x2="11.1" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-3.6" x2="-8" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="-6.5" x2="-8" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="12.2" y1="-3.6" x2="11.1" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-8" y1="-6.5" x2="-8" y2="-3.6" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="12.2" y1="-3.6" x2="12.2" y2="-6.5" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="-6.3" y1="2.1" x2="-6.9" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="2.1" x2="-6.9" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-6.7" y1="2.1" x2="-6.9" y2="1.9" width="0.1524" layer="21"/>
</package>
<package name="MF04-2-HP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-4.2" y="0" drill="1.8" shape="square"/>
<pad name="5" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-5.9" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="2" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="6" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="7" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="8" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="9.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="7.2" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-2.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.8" y1="1.1" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6" y1="1.1" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.2" y1="1.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.4" y1="1.1" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.6" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.8" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="13.9" x2="3.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="0.6" y1="13.9" x2="-6.9" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="-6.9" y2="1.1" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-6.9" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="51"/>
<hole x="-4.2" y="7.3" drill="3"/>
<hole x="8.4" y="7.3" drill="3"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<rectangle x1="7.8" y1="-6.1" x2="9" y2="1.1" layer="51"/>
<wire x1="-7.4" y1="1.1" x2="-7.4" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.4" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7.4" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-7.4" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-7.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-7.4" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-7.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="1.1" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7" y1="1.1" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.2" y1="1.1" x2="-7.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-6.9" y1="13.9" x2="-7.4" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.3" x2="-7.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.5" x2="-7" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.7" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="1.9" x2="-6.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.1" x2="-6.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.3" x2="-6.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8" y1="-1.6" x2="8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8.8" y1="-1.6" x2="8.8" y2="-3.9" width="0.1524" layer="21"/>
</package>
<package name="MA05-1-V">
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-6.35" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="JST-PH-05-H">
<description>&lt;h3&gt;Connector 2mm 5 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-4" y="0" drill="0.8" shape="octagon"/>
<pad name="2" x="-2" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="0" y="0" drill="0.8" shape="octagon"/>
<pad name="4" x="2" y="0" drill="0.8" shape="octagon"/>
<pad name="5" x="4" y="0" drill="0.8" shape="octagon"/>
<wire x1="5.95" y1="1.6" x2="5.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="5.95" y1="-6" x2="2.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-6" x2="-2.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-6" x2="-5.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="-6" x2="-5.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="5.95" y1="1.6" x2="5.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.6" x2="-5.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="5.3" y1="1.6" x2="5.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.3" y1="1.6" x2="-5.3" y2="0" width="0.2032" layer="51"/>
<text x="0.063" y="1.032" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.5" y1="-3.6" x2="2.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-3.6" x2="-2.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-3.6" x2="-2.5" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-4" y1="-5.5" x2="-3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-4.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-3.5" x2="-4" y2="-5.5" width="0.127" layer="51"/>
<wire x1="-5.3" y1="0" x2="5.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-4.3" y1="0" x2="-3.7" y2="0.3" layer="51"/>
<rectangle x1="-2.3" y1="0" x2="-1.7" y2="0.3" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<rectangle x1="3.7" y1="0" x2="4.3" y2="0.3" layer="51"/>
<wire x1="5.95" y1="1.6" x2="5.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="5.95" y1="-6" x2="2.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-6" x2="-2.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-6" x2="-5.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="-6" x2="-5.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="5.95" y1="1.6" x2="5.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.6" x2="-5.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="5.3" y1="1.6" x2="5.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="1.6" x2="-5.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-3.6" x2="2.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.6" x2="-2.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-3.6" x2="-2.5" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-4" y1="-5.5" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-4.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-3.5" x2="-4" y2="-5.5" width="0.127" layer="21"/>
</package>
<package name="JST-PH-05-V">
<description>&lt;h3&gt;Connector 2mm 5 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.95" y1="1.72" x2="5.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="5.95" y1="-2.78" x2="-5.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.72" x2="-5.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.6" y1="1.72" x2="-5.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.4" y1="1.72" x2="-5.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.72" x2="-5" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5" y1="1.72" x2="-4.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="1.72" x2="-4.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="1.72" x2="-4.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="1.72" x2="-4.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.72" x2="-2.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="2.73" y1="1.72" x2="5.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="2.73" y1="1.22" x2="-2.73" y2="1.22" width="0.2032" layer="51"/>
<wire x1="-2.73" y1="1.22" x2="-2.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="2.73" y1="1.22" x2="2.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="-2.78" x2="-5.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="-4" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="-2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="4" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="5" x="4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="0" y="1.5" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5.95" y1="0.12" x2="-5.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.32" x2="-5.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.52" x2="-5.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.72" x2="-5.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.92" x2="-5.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.12" x2="-5.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.32" x2="-5.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.52" x2="-5.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.12" x2="-4.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.32" x2="-4.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.52" x2="-4.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.72" x2="-4.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.92" x2="-5" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.12" x2="-5.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.32" x2="-5.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.52" x2="-5.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<wire x1="5.95" y1="1.72" x2="5.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="5.95" y1="-2.78" x2="-5.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.72" x2="-5.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="1.72" x2="-5.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="1.72" x2="-5.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="1.72" x2="-5" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5" y1="1.72" x2="-4.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="1.72" x2="-4.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="1.72" x2="-4.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="1.72" x2="-4.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.72" x2="-2.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="2.73" y1="1.72" x2="5.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="2.73" y1="1.22" x2="-2.73" y2="1.22" width="0.2032" layer="21"/>
<wire x1="-2.73" y1="1.22" x2="-2.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="2.73" y1="1.22" x2="2.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="-2.78" x2="-5.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.12" x2="-5.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.32" x2="-5.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.52" x2="-5.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.72" x2="-5.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.92" x2="-5.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.12" x2="-5.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.32" x2="-5.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.52" x2="-5.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.12" x2="-4.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.32" x2="-4.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.52" x2="-4.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.72" x2="-4.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.92" x2="-5" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.12" x2="-5.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.32" x2="-5.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.52" x2="-5.6" y2="1.72" width="0.2032" layer="21"/>
</package>
<package name="JST-SR-05-HS">
<description>&lt;h3&gt;Connector 1mm 5 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM05B-SRSS-TB, JOINT TECH A1001-WR-S-05P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.5" y1="-2.3" x2="-3.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="0.75" x2="-3.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.05" x2="-3.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.25" x2="-3.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.45" x2="-3.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.65" x2="-3.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.95" x2="-3.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.95" x2="-3" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3" y1="1.95" x2="-2.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1.95" x2="-2.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.95" x2="3.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="3.2" y1="1.95" x2="3.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-2.3" x2="-3.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="-2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="-1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="5" x="2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="3.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-3.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="3.5" y1="1.95" x2="3.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="3.5" y1="0.75" x2="3.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.95" x2="-3.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="0.75" x2="-3.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="3.2" y1="1.95" x2="3.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="3.2" y1="0.75" x2="3.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2.3" x2="-2" y2="-1" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.95" x2="-3.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-3" y1="1.95" x2="-3.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1.95" x2="-3.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.95" x2="-3.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-0.45" x2="-3.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="0.75" x2="-3.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.05" x2="-3.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.25" x2="-3.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.45" x2="-3.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.65" x2="-3.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.95" x2="-3.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.95" x2="-3" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.95" x2="-2.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="1.95" x2="-2.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.95" x2="3.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.95" x2="3.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="3.5" y1="1.95" x2="3.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="3.5" y1="0.75" x2="3.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.95" x2="-3.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="0.75" x2="-3.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.95" x2="3.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="3.2" y1="0.75" x2="3.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1" x2="-2.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-2.3" x2="-2" y2="-1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.95" x2="-3.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.95" x2="-3.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="1.95" x2="-3.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.95" x2="-3.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-2.2" y1="2" x2="-1.8" y2="2.6" layer="51"/>
<rectangle x1="-1.2" y1="2" x2="-0.8" y2="2.6" layer="51"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
<rectangle x1="1.8" y1="2" x2="2.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-05-VS">
<description>&lt;h3&gt;Connector 1mm 5 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM05B-SRSS-RM, JOINT TECH A1001-WV-S-05P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.5" y1="0.95" x2="3.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-0.85" x2="3.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-1.95" x2="3.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-1.95" x2="-2.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-1.95" x2="-2.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="-1.95" x2="-3" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.95" x2="-3.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="0.95" x2="3.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<smd name="1" x="-2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-3.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="3.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-3.5" y1="-1.65" x2="-3.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.45" x2="-3.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.05" x2="-3.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-0.85" x2="-3.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-1.95" x2="3.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="-1.95" x2="-3.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.95" x2="-3.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-1.95" x2="-3.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-0.85" x2="3.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.95" x2="-3.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-0.85" x2="-3.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2" y1="0.65" x2="-2.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-2" y1="0.65" x2="-1.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.15" x2="-1.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-0.85" x2="3.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-1.95" x2="3.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.95" x2="2.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.95" x2="-2.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.95" x2="-2.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-1.95" x2="-3" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.95" x2="-3.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.95" x2="2.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.65" x2="-3.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.45" x2="-3.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.05" x2="-3.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.95" x2="3.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-1.95" x2="-3.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.95" x2="-3.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.95" x2="-3.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-0.85" x2="3.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.95" x2="-3.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-0.85" x2="-3.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2" y1="0.65" x2="-2.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-2" y1="0.65" x2="-1.8" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.15" x2="-1.8" y2="1.15" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-05-HS">
<description>&lt;h3&gt;Connector 2mm 5 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S5B-PH-SM4-TB, NXW-05SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-6.9" y1="-4" x2="-6.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.4" x2="-6.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.7" x2="-6.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.9" x2="-6.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="3.1" x2="-6.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="3.3" x2="-6.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="3.6" x2="-6.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="3.6" x2="-6.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="3.6" x2="-6.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.2" y1="3.6" x2="-6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6" y1="3.6" x2="6.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="6.6" y1="3.6" x2="6.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="6.9" y1="-4" x2="-3.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4" x2="-4.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-4" x2="-6.9" y2="-4" width="0.2032" layer="51"/>
<text x="0" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="6.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-6.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="6.9" y1="3.6" x2="6.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="6.9" y1="2.4" x2="6.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="3.6" x2="-6.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.4" x2="-6.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="6.6" y1="3.6" x2="6.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="6.6" y1="2.4" x2="6.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-4" y1="-2.7" x2="-4.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4" x2="-4" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="3.6" x2="-6.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="3.6" x2="-6.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-6.2" y1="3.6" x2="-6.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-6" y1="3.6" x2="-6.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="-0.5" x2="-6.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.4" x2="-6.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.7" x2="-6.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.9" x2="-6.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="3.1" x2="-6.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="3.3" x2="-6.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="3.6" x2="-6.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="3.6" x2="-6.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.6" x2="-6.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.2" y1="3.6" x2="-6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="4.8" y1="3.6" x2="6.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="6.6" y1="3.6" x2="6.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-4" x2="-3.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4" x2="-4.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="6.9" y1="3.6" x2="6.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="6.9" y1="2.4" x2="6.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="3.6" x2="-6.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.4" x2="-6.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="6.6" y1="3.6" x2="6.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="6.6" y1="2.4" x2="6.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-4" y1="-2.7" x2="-4.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4" x2="-4" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="3.6" x2="-6.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.6" x2="-6.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-6.2" y1="3.6" x2="-6.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-6" y1="3.6" x2="-6.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-4.2" y1="3.6" x2="-3.8" y2="4.6" layer="51"/>
<smd name="2" x="-2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="3" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="4" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-2.2" y1="3.6" x2="-1.8" y2="4.6" layer="51"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<wire x1="-4.5" y1="-4" x2="-5.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="3.6" x2="-6" y2="3.6" width="0.2032" layer="21"/>
<smd name="5" x="4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="3.8" y1="3.6" x2="4.2" y2="4.6" layer="51"/>
</package>
<package name="JST-PH-05-VS">
<description>&lt;h3&gt;Connector 2mm 5 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B5B-PH-SM4-TB, NXW-05SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.95" y1="1.9" x2="6.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-2" x2="6.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-3.1" x2="6.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="6.65" y1="-3.1" x2="-6.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.05" y1="-3.1" x2="-6.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.25" y1="-3.1" x2="-6.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.45" y1="-3.1" x2="-6.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="1.9" x2="6.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="0" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-4.25" y1="-0.25" x2="-3.75" y2="0.25" layer="51"/>
<smd name="1" x="-4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-6.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="6.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-6.95" y1="-2.8" x2="-6.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2.6" x2="-6.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2.4" x2="-6.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2.2" x2="-6.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2" x2="-6.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="6.65" y1="-3.1" x2="6.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.25" y1="-3.1" x2="-6.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-6.45" y1="-3.1" x2="-6.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-6.05" y1="-3.1" x2="-6.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-2" x2="6.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-3.1" x2="-6.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-2" x2="-6.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4" y1="1.6" x2="-4.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-4" y1="1.6" x2="-3.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="2.1" x2="-3.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="6.95" y1="-2.4" x2="6.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="6.95" y1="-3.1" x2="6.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="6.65" y1="-3.1" x2="4.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-3.1" x2="-6.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="-3.1" x2="-6.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.45" y1="-3.1" x2="-6.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="1.9" x2="6.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="-2.8" x2="-6.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="-2.6" x2="-6.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="6.65" y1="-3.1" x2="6.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="-3.1" x2="-6.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.45" y1="-3.1" x2="-6.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-3.1" x2="-6.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="6.95" y1="-2.4" x2="6.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-3.1" x2="-6.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-2.4" x2="-6.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="-2.4" x2="-6.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.6" x2="-4.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.6" x2="-3.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="2.1" x2="-3.8" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="-2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<text x="0" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.25" y1="-4.1" x2="-3.75" y2="-3.1" layer="51"/>
<rectangle x1="-2.25" y1="-4.1" x2="-1.75" y2="-3.1" layer="51"/>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<wire x1="-6.95" y1="1.9" x2="-6.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="6.95" y1="1.9" x2="6.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-3.1" x2="-6.05" y2="-3.1" width="0.2032" layer="21"/>
<smd name="5" x="4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="-4.1" x2="4.25" y2="-3.1" layer="51"/>
</package>
<package name="MA05-2-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="3.81" x2="-4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="3.81" x2="-6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="2.54" drill="1.016" shape="octagon"/>
<text x="-6.35" y="4.191" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="2.286" x2="-4.826" y2="2.794" layer="51"/>
<rectangle x1="-2.794" y1="2.286" x2="-2.286" y2="2.794" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="3.81" x2="-4.445" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="3.175" x2="-3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="3.81" x2="-6.35" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="51"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="51"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="MF05-2-H">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="6" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-10.1" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-0.1" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="7" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="8" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="9" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="9.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10" y1="1.1" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.2" y1="1.1" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="1.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.6" y1="1.1" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.8" y1="1.1" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="13.9" x2="1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="51"/>
<rectangle x1="-9" y1="-6.1" x2="-7.8" y2="1.1" layer="51"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.6" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.6" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11.6" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-11.6" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-11.6" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-11.6" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.2" y1="1.1" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.4" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-8.8" y1="-1.6" x2="-8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-8" y1="-1.6" x2="-8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="10" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="7.8" y1="-6.1" x2="9" y2="1.1" layer="51"/>
<wire x1="8" y1="-1.6" x2="8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8.8" y1="-1.6" x2="8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="7.2" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
</package>
<package name="MF05-2-HP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="6" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-10.1" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-0.1" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="7" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="8" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="9" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="11.1" y1="1.1" x2="9.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10" y1="1.1" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.2" y1="1.1" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="1.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.6" y1="1.1" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.8" y1="1.1" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="13.9" x2="1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="11.1" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="51"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="51"/>
<hole x="-8.4" y="7.3" drill="3"/>
<hole x="8.4" y="7.3" drill="3"/>
<rectangle x1="-9" y1="-6.1" x2="-7.8" y2="1.1" layer="51"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.6" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.6" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11.6" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-11.6" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-11.6" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-11.6" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.2" y1="1.1" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.4" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="51"/>
<wire x1="1.5" y1="11.3" x2="-1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="11.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="-1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="12.3" x2="1.5" y2="11.3" width="0.1524" layer="21"/>
<wire x1="1.5" y1="13.9" x2="1.5" y2="12.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="12.3" x2="-1.5" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-8.8" y1="-1.6" x2="-8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-8" y1="-1.6" x2="-8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="10" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="7.8" y1="-6.1" x2="9" y2="1.1" layer="51"/>
<wire x1="8" y1="-1.6" x2="8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8.8" y1="-1.6" x2="8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="7.2" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
</package>
<package name="MF05-2-V">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="6" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-9.9" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-8.654" y1="-0.854" x2="-8.146" y2="-0.346" layer="51"/>
<rectangle x1="-8.654" y1="-5.054" x2="-8.146" y2="-4.546" layer="51"/>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="7" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="8" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="9" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="0.9" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.3" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.5" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.7" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.9" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="2.1" x2="-11.1" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="2.3" x2="-11.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="-6.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.8" x2="-6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-2.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-6.8" x2="-2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-10.4" y1="1.4" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-2.6" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-10.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="0.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.4" x2="-5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-5.2" y1="1.4" x2="-6.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-10.4" y1="-3.8" x2="-10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-9.4" y1="-2.8" x2="-10.4" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-7.4" y1="-2.8" x2="-9.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-7.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="2.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.2" y1="-3.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="3.2" y1="-2.8" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="5.2" y1="-2.8" x2="3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="11.1" y1="2.5" x2="10.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="10.4" y1="2.5" x2="10.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="10.4" y1="2.1" x2="-9.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-10.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-10.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.3" y1="2.1" x2="-10.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.1" x2="-10.4" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.2" x2="-10.4" y2="2.4" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.4" x2="-10.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.5" x2="-10.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="2.5" x2="-10.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.5" x2="-10.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.9" y1="2.5" x2="-11.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="2.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="2.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="2.5" x2="10.4" y2="2.5" width="0.1524" layer="51"/>
<wire x1="10.4" y1="2.5" x2="10.4" y2="2.1" width="0.1524" layer="51"/>
<wire x1="10.4" y1="2.1" x2="-10.4" y2="2.1" width="0.1524" layer="51"/>
<wire x1="-10.4" y1="2.1" x2="-10.4" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-10.4" y1="2.5" x2="-11.1" y2="2.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-10.9" y1="2.5" x2="-11.1" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.5" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="2.5" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.4" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.2" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-10.3" y1="2.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-0.854" x2="8.654" y2="-0.346" layer="51"/>
<pad name="10" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-5.054" x2="8.654" y2="-4.546" layer="51"/>
<wire x1="10.4" y1="-2.8" x2="6.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-2.8" x2="6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-6.8" x2="10.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-6.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="0.4" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="-2.6" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="9.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="9.4" y1="1.4" x2="7.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="7.4" y1="1.4" x2="6.4" y2="0.4" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="10.4" y2="-2.6" width="0.127" layer="51"/>
</package>
<package name="MF05-2-VP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-6.5" width="0.1524" layer="21"/>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="6" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-9.9" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-8.654" y1="-0.854" x2="-8.146" y2="-0.346" layer="51"/>
<rectangle x1="-8.654" y1="-5.054" x2="-8.146" y2="-4.546" layer="51"/>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="7" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="8" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="9" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<hole x="-13.1" y="-5.04" drill="3"/>
<hole x="13.1" y="-5.04" drill="3"/>
<wire x1="-11.1" y1="-6.5" x2="-11.1" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-3.6" x2="-11.1" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="0.9" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.3" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.5" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.7" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.9" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="-6.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.8" x2="-6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-2.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-6.8" x2="-2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-10.4" y1="1.4" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-2.6" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-10.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="0.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.4" x2="-5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-5.2" y1="1.4" x2="-6.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-10.4" y1="-3.8" x2="-10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-9.4" y1="-2.8" x2="-10.4" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-7.4" y1="-2.8" x2="-9.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-7.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="2.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.2" y1="-3.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="3.2" y1="-2.8" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="5.2" y1="-2.8" x2="3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-6.5" x2="11.1" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-3.6" x2="11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="11.1" y1="2.1" x2="-9.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-10.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-10.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.3" y1="2.1" x2="-10.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="2.1" x2="-10.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.1" x2="-10.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.9" y1="2.1" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="11.1" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-6.5" x2="-11.1" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-3.6" x2="-11.1" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="2.1" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="11.1" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-6.5" x2="11.1" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-3.6" x2="11.1" y2="2.1" width="0.1524" layer="51"/>
<wire x1="11.1" y1="2.1" x2="-11.1" y2="2.1" width="0.1524" layer="51"/>
<wire x1="11.1" y1="-7.5" x2="1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-7.5" x2="1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="1.5" y1="-8.5" x2="-1.5" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-8.5" x2="-1.5" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-10.3" y1="2.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-3.6" x2="-12.2" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-6.5" x2="-12.2" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-3.6" x2="11.1" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-6.5" x2="11.1" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-6.5" x2="11.1" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-3.6" x2="-12.2" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-6.5" x2="-12.2" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="12.2" y1="-3.6" x2="11.1" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-12.2" y1="-6.5" x2="-12.2" y2="-3.6" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="12.2" y1="-3.6" x2="12.2" y2="-6.5" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="-10.5" y1="2.1" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.1" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-10.9" y1="2.1" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-0.854" x2="8.654" y2="-0.346" layer="51"/>
<pad name="10" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-5.054" x2="8.654" y2="-4.546" layer="51"/>
<wire x1="10.4" y1="-2.8" x2="6.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-2.8" x2="6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-6.8" x2="10.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-6.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="0.4" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="-2.6" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="9.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="9.4" y1="1.4" x2="7.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="7.4" y1="1.4" x2="6.4" y2="0.4" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="10.4" y2="-2.6" width="0.127" layer="51"/>
</package>
<package name="MA06-1-V">
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="6" x="7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-6.35" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="0.635" x2="6.985" y2="1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="51"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="JST-PH-06-H">
<description>&lt;h3&gt;Connector 2mm 6 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-4" y="0" drill="0.8" shape="octagon"/>
<pad name="2" x="-2" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="0" y="0" drill="0.8" shape="octagon"/>
<pad name="4" x="2" y="0" drill="0.8" shape="octagon"/>
<pad name="5" x="4" y="0" drill="0.8" shape="octagon"/>
<wire x1="7.95" y1="1.6" x2="7.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="7.95" y1="-6" x2="4.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-6" x2="-2.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-6" x2="-5.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="-6" x2="-5.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="7.95" y1="1.6" x2="7.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.6" x2="-5.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="7.3" y1="1.6" x2="7.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.3" y1="1.6" x2="-5.3" y2="0" width="0.2032" layer="51"/>
<text x="1" y="1" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.5" y1="-3.6" x2="4.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-3.6" x2="-2.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-3.6" x2="-2.5" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-4" y1="-5.5" x2="-3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-4.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-3.5" x2="-4" y2="-5.5" width="0.127" layer="51"/>
<pad name="6" x="6" y="0" drill="0.8" shape="octagon"/>
<wire x1="-5.3" y1="0" x2="7.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-4.3" y1="0" x2="-3.7" y2="0.3" layer="51"/>
<rectangle x1="-2.3" y1="0" x2="-1.7" y2="0.3" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<rectangle x1="3.7" y1="0" x2="4.3" y2="0.3" layer="51"/>
<rectangle x1="5.7" y1="0" x2="6.3" y2="0.3" layer="51"/>
<wire x1="7.95" y1="1.6" x2="7.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-6" x2="4.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-6" x2="-2.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-6" x2="-5.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="-6" x2="-5.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="7.95" y1="1.6" x2="7.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.6" x2="-5.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="7.3" y1="1.6" x2="7.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="1.6" x2="-5.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-3.6" x2="4.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.6" x2="-2.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-3.6" x2="-2.5" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-4" y1="-5.5" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-4.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-3.5" x2="-4" y2="-5.5" width="0.127" layer="21"/>
</package>
<package name="JST-PH-06-V">
<description>&lt;h3&gt;Connector 2mm 6 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.95" y1="1.72" x2="7.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="7.95" y1="-2.78" x2="-5.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.72" x2="-5.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.6" y1="1.72" x2="-5.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.4" y1="1.72" x2="-5.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.72" x2="-5" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5" y1="1.72" x2="-4.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="1.72" x2="-4.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="1.72" x2="-4.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="1.72" x2="-4.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.72" x2="-2.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="4.73" y1="1.72" x2="7.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="4.73" y1="1.22" x2="-2.73" y2="1.22" width="0.2032" layer="51"/>
<wire x1="-2.73" y1="1.22" x2="-2.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="4.73" y1="1.22" x2="4.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="-2.78" x2="-5.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="-4" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="-2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="4" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="5" x="4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="1" y="1.5" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5.95" y1="0.12" x2="-5.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.32" x2="-5.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.52" x2="-5.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.72" x2="-5.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.92" x2="-5.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.12" x2="-5.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.32" x2="-5.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.52" x2="-5.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.12" x2="-4.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.32" x2="-4.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.52" x2="-4.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.72" x2="-4.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="0.92" x2="-5" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.12" x2="-5.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.32" x2="-5.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="1.52" x2="-5.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<pad name="6" x="6" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="5.746" y1="-0.254" x2="6.254" y2="0.254" layer="51"/>
<wire x1="7.95" y1="1.72" x2="7.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-2.78" x2="-5.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.72" x2="-5.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="1.72" x2="-5.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="1.72" x2="-5.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="1.72" x2="-5" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5" y1="1.72" x2="-4.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="1.72" x2="-4.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="1.72" x2="-4.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="1.72" x2="-4.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.72" x2="-2.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="4.73" y1="1.72" x2="7.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="4.73" y1="1.22" x2="-2.73" y2="1.22" width="0.2032" layer="21"/>
<wire x1="-2.73" y1="1.22" x2="-2.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="4.73" y1="1.22" x2="4.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="-2.78" x2="-5.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.12" x2="-5.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.32" x2="-5.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.52" x2="-5.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.72" x2="-5.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.92" x2="-5.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.12" x2="-5.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.32" x2="-5.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.52" x2="-5.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.12" x2="-4.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.32" x2="-4.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.52" x2="-4.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.72" x2="-4.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="0.92" x2="-5" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.12" x2="-5.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.32" x2="-5.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="1.52" x2="-5.6" y2="1.72" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-06-HS">
<description>&lt;h3&gt;Connector 2mm 6 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S6B-PH-SM4-TB, NXW-06SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-6.9" y1="-4" x2="-6.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.4" x2="-6.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.7" x2="-6.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.9" x2="-6.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="3.1" x2="-6.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="3.3" x2="-6.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="3.6" x2="-6.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="3.6" x2="-6.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="3.6" x2="-6.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6.2" y1="3.6" x2="-6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-6" y1="3.6" x2="8.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="8.6" y1="3.6" x2="8.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="8.9" y1="-4" x2="-3.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4" x2="-4.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-4" x2="-6.9" y2="-4" width="0.2032" layer="51"/>
<text x="1" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="8.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-6.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="8.9" y1="3.6" x2="8.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="8.9" y1="2.4" x2="8.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="3.6" x2="-6.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="2.4" x2="-6.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="8.6" y1="3.6" x2="8.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="8.6" y1="2.4" x2="8.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-4" y1="-2.7" x2="-4.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4" x2="-4" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="3.6" x2="-6.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="3.6" x2="-6.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-6.2" y1="3.6" x2="-6.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-6" y1="3.6" x2="-6.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="-0.5" x2="-6.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.4" x2="-6.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.7" x2="-6.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.9" x2="-6.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="3.1" x2="-6.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="3.3" x2="-6.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="3.6" x2="-6.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="3.6" x2="-6.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.6" x2="-6.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.2" y1="3.6" x2="-6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="6.8" y1="3.6" x2="8.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="8.6" y1="3.6" x2="8.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="7.3" y1="-4" x2="-3.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4" x2="-4.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="3.6" x2="8.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="8.9" y1="2.4" x2="8.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="3.6" x2="-6.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-6.9" y1="2.4" x2="-6.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="8.6" y1="3.6" x2="8.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="8.6" y1="2.4" x2="8.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-4" y1="-2.7" x2="-4.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4" x2="-4" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="3.6" x2="-6.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.6" x2="-6.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-6.2" y1="3.6" x2="-6.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-6" y1="3.6" x2="-6.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-4.2" y1="3.6" x2="-3.8" y2="4.6" layer="51"/>
<smd name="2" x="-2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="3" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="4" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-2.2" y1="3.6" x2="-1.8" y2="4.6" layer="51"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<wire x1="-4.5" y1="-4" x2="-5.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="3.6" x2="-6" y2="3.6" width="0.2032" layer="21"/>
<smd name="5" x="4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="3.8" y1="3.6" x2="4.2" y2="4.6" layer="51"/>
<smd name="6" x="6" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="5.8" y1="3.6" x2="6.2" y2="4.6" layer="51"/>
</package>
<package name="JST-PH-06-VS">
<description>&lt;h3&gt;Connector 2mm 6 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B6B-PH-SM4-TB, NXW-06SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="8.95" y1="1.9" x2="8.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="8.95" y1="-2" x2="8.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="8.95" y1="-3.1" x2="8.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="8.65" y1="-3.1" x2="-6.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.05" y1="-3.1" x2="-6.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.25" y1="-3.1" x2="-6.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.45" y1="-3.1" x2="-6.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="1.9" x2="8.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="1" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-4.25" y1="-0.25" x2="-3.75" y2="0.25" layer="51"/>
<smd name="1" x="-4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-6.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="8.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-6.95" y1="-2.8" x2="-6.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2.6" x2="-6.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2.4" x2="-6.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2.2" x2="-6.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.95" y1="-2" x2="-6.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="8.65" y1="-3.1" x2="8.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.25" y1="-3.1" x2="-6.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-6.45" y1="-3.1" x2="-6.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-6.05" y1="-3.1" x2="-6.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="8.95" y1="-2" x2="8.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-3.1" x2="-6.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.65" y1="-2" x2="-6.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4" y1="1.6" x2="-4.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-4" y1="1.6" x2="-3.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="2.1" x2="-3.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="8.95" y1="-2.4" x2="8.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="8.95" y1="-3.1" x2="8.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.1" x2="6.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-3.1" x2="-6.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="-3.1" x2="-6.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.45" y1="-3.1" x2="-6.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="1.9" x2="8.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="-2.8" x2="-6.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-6.95" y1="-2.6" x2="-6.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.1" x2="8.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="-3.1" x2="-6.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.45" y1="-3.1" x2="-6.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-3.1" x2="-6.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-3.1" x2="-6.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="8.95" y1="-2.4" x2="8.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-3.1" x2="-6.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.65" y1="-2.4" x2="-6.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="-2.4" x2="-6.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.6" x2="-4.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.6" x2="-3.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="2.1" x2="-3.8" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="-2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<text x="1" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.25" y1="-4.1" x2="-3.75" y2="-3.1" layer="51"/>
<rectangle x1="-2.25" y1="-4.1" x2="-1.75" y2="-3.1" layer="51"/>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<wire x1="-6.95" y1="1.9" x2="-6.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="8.95" y1="1.9" x2="8.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-3.1" x2="-6.05" y2="-3.1" width="0.2032" layer="21"/>
<smd name="5" x="4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="-4.1" x2="4.25" y2="-3.1" layer="51"/>
<smd name="6" x="6" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51"/>
<rectangle x1="5.75" y1="-4.1" x2="6.25" y2="-3.1" layer="51"/>
</package>
<package name="JST-SR-06-HS">
<description>&lt;h3&gt;Connector 1mm 6 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM6B-SRSS-TB, JOINT TECH A1001-WR-S-06P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.5" y1="-2.3" x2="-3.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="0.75" x2="-3.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.05" x2="-3.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.25" x2="-3.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.45" x2="-3.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.65" x2="-3.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="1.95" x2="-3.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.95" x2="-3" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3" y1="1.95" x2="-2.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1.95" x2="-2.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.95" x2="4.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="4.2" y1="1.95" x2="4.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-2.3" x2="-3.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0.5" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="-2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="-1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="5" x="2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="4.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-3.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="4.5" y1="1.95" x2="4.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0.75" x2="4.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.95" x2="-3.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="0.75" x2="-3.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="4.2" y1="1.95" x2="4.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="4.2" y1="0.75" x2="4.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2.3" x2="-2" y2="-1" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.95" x2="-3.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-3" y1="1.95" x2="-3.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1.95" x2="-3.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.95" x2="-3.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-0.45" x2="-3.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="0.75" x2="-3.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.05" x2="-3.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.25" x2="-3.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.45" x2="-3.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.65" x2="-3.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.95" x2="-3.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.95" x2="-3" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.95" x2="-2.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="1.95" x2="-2.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="3.6" y1="1.95" x2="4.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.95" x2="4.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-2.3" x2="-1.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.95" x2="4.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="4.5" y1="0.75" x2="4.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.95" x2="-3.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="0.75" x2="-3.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.95" x2="4.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="4.2" y1="0.75" x2="4.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1" x2="-2.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-2.3" x2="-2" y2="-1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.95" x2="-3.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.95" x2="-3.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="1.95" x2="-3.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.95" x2="-3.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-2.2" y1="2" x2="-1.8" y2="2.6" layer="51"/>
<rectangle x1="-1.2" y1="2" x2="-0.8" y2="2.6" layer="51"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
<rectangle x1="1.8" y1="2" x2="2.2" y2="2.6" layer="51"/>
<smd name="6" x="3" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="2.8" y1="2" x2="3.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-06-VS">
<description>&lt;h3&gt;Connector 1mm 6 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM6B-SRSS-RM, JOINT TECH A1001-WV-S-06P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="4.5" y1="0.95" x2="4.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-0.85" x2="4.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-1.95" x2="4.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.95" x2="-2.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-1.95" x2="-2.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="-1.95" x2="-3" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.95" x2="-3.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="0.95" x2="4.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0.5" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<smd name="1" x="-2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-3.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="4.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-3.5" y1="-1.65" x2="-3.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.45" x2="-3.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.05" x2="-3.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-0.85" x2="-3.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.95" x2="4.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="-1.95" x2="-3.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.95" x2="-3.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-1.95" x2="-3.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-0.85" x2="4.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.95" x2="-3.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-0.85" x2="-3.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2" y1="0.65" x2="-2.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-2" y1="0.65" x2="-1.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-2.2" y1="1.15" x2="-1.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-0.85" x2="4.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.95" x2="4.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.95" x2="3.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.95" x2="-2.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.95" x2="-2.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-1.95" x2="-3" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.95" x2="-3.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.95" x2="3.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.65" x2="-3.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.45" x2="-3.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.05" x2="-3.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.95" x2="4.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-1.95" x2="-3.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.95" x2="-3.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.95" x2="-3.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.95" x2="-3.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-0.85" x2="4.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.95" x2="-3.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-0.85" x2="-3.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-2" y1="0.65" x2="-2.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-2" y1="0.65" x2="-1.8" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.15" x2="-1.8" y2="1.15" width="0.2032" layer="21"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
<smd name="6" x="3" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
</package>
<package name="MA06-2-V">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="3.81" x2="-4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="3.81" x2="-6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="11" x="7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="2.54" drill="1.016" shape="octagon"/>
<pad name="12" x="7.62" y="2.54" drill="1.016" shape="octagon"/>
<text x="-6.35" y="4.191" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="2.286" x2="-4.826" y2="2.794" layer="51"/>
<rectangle x1="-2.794" y1="2.286" x2="-2.286" y2="2.794" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="3.81" x2="-4.445" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="3.81" x2="-3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="3.175" x2="-3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="3.81" x2="-1.905" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="3.81" x2="-6.35" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="51"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="51"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="51"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="51"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="51"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="51"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="51"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="51"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="51"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="51"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-1.016" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.889" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.762" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-1.27" x2="-5.842" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-5.969" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.096" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.842" y1="-1.27" x2="-6.35" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-1.27" x2="-6.35" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-1.27" x2="-6.35" y2="-1.016" width="0.1524" layer="51"/>
</package>
<package name="MF06-2-V">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="7" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-9.9" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-8.654" y1="-0.854" x2="-8.146" y2="-0.346" layer="51"/>
<rectangle x1="-8.654" y1="-5.054" x2="-8.146" y2="-4.546" layer="51"/>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="8" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="9" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="10" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="0.9" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.3" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.5" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.7" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.9" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="2.1" x2="-11.1" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="2.3" x2="-11.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="-6.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.8" x2="-6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-2.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-6.8" x2="-2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-10.4" y1="1.4" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-2.6" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-10.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="0.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.4" x2="-5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-5.2" y1="1.4" x2="-6.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-10.4" y1="-3.8" x2="-10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-9.4" y1="-2.8" x2="-10.4" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-7.4" y1="-2.8" x2="-9.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-7.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="2.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.2" y1="-3.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="3.2" y1="-2.8" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="5.2" y1="-2.8" x2="3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="15.3" y1="-7.5" x2="15.3" y2="2.5" width="0.1524" layer="21"/>
<wire x1="15.3" y1="2.5" x2="14.6" y2="2.5" width="0.1524" layer="21"/>
<wire x1="14.6" y1="2.5" x2="14.6" y2="2.1" width="0.1524" layer="21"/>
<wire x1="14.6" y1="2.1" x2="-9.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-10.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-10.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.3" y1="2.1" x2="-10.4" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.1" x2="-10.4" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.2" x2="-10.4" y2="2.4" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.4" x2="-10.4" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.5" x2="-10.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="2.5" x2="-10.7" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.5" x2="-10.9" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-10.9" y1="2.5" x2="-11.1" y2="2.5" width="0.1524" layer="21"/>
<wire x1="15.3" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="2.5" width="0.1524" layer="51"/>
<wire x1="15.3" y1="-7.5" x2="15.3" y2="2.5" width="0.1524" layer="51"/>
<wire x1="15.3" y1="2.5" x2="14.6" y2="2.5" width="0.1524" layer="51"/>
<wire x1="14.6" y1="2.5" x2="14.6" y2="2.1" width="0.1524" layer="51"/>
<wire x1="14.6" y1="2.1" x2="-10.4" y2="2.1" width="0.1524" layer="51"/>
<wire x1="-10.4" y1="2.1" x2="-10.4" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-10.4" y1="2.5" x2="-11.1" y2="2.5" width="0.1524" layer="51"/>
<wire x1="15.3" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-10.9" y1="2.5" x2="-11.1" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.5" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="2.5" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.4" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="2.2" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-10.3" y1="2.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-0.854" x2="8.654" y2="-0.346" layer="51"/>
<pad name="6" x="12.6" y="0" drill="1.8" shape="octagon"/>
<pad name="11" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-5.054" x2="8.654" y2="-4.546" layer="51"/>
<pad name="12" x="12.6" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="10.4" y1="-2.8" x2="6.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-2.8" x2="6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-6.8" x2="10.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-6.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="0.4" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="-2.6" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="9.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="9.4" y1="1.4" x2="7.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="7.4" y1="1.4" x2="6.4" y2="0.4" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="10.4" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="12.346" y1="-0.854" x2="12.854" y2="-0.346" layer="51"/>
<rectangle x1="12.346" y1="-5.054" x2="12.854" y2="-4.546" layer="51"/>
<wire x1="14.6" y1="-2.8" x2="10.6" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.6" y1="-2.8" x2="10.6" y2="-6.8" width="0.127" layer="51"/>
<wire x1="14.6" y1="-6.8" x2="14.6" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.6" y1="-6.8" x2="14.6" y2="-6.8" width="0.127" layer="51"/>
<wire x1="10.6" y1="0.4" x2="10.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="14.6" y1="-2.6" x2="10.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="14.6" y1="0.4" x2="13.6" y2="1.4" width="0.127" layer="51"/>
<wire x1="13.6" y1="1.4" x2="11.6" y2="1.4" width="0.127" layer="51"/>
<wire x1="11.6" y1="1.4" x2="10.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="14.6" y1="0.4" x2="14.6" y2="-2.6" width="0.127" layer="51"/>
</package>
<package name="MF06-2-H">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="7" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-10.1" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="2" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="8" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="9" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="10" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="15.3" y1="1.1" x2="15.3" y2="13.9" width="0.1524" layer="21"/>
<wire x1="15.3" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="15.3" y1="1.1" x2="13.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10" y1="1.1" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.2" y1="1.1" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="1.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.6" y1="1.1" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.8" y1="1.1" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="15.3" y1="1.1" x2="15.3" y2="13.9" width="0.1524" layer="51"/>
<wire x1="15.3" y1="13.9" x2="3.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="0.6" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="15.3" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="51"/>
<rectangle x1="-9" y1="-6.1" x2="-7.8" y2="1.1" layer="51"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.6" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.6" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11.6" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-11.6" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-11.6" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-11.6" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.2" y1="1.1" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.4" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-8.8" y1="-1.6" x2="-8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-8" y1="-1.6" x2="-8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="6" x="12.6" y="0" drill="1.8" shape="octagon"/>
<pad name="11" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="12" x="12.6" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="7.8" y1="-6.1" x2="9" y2="1.1" layer="51"/>
<rectangle x1="12" y1="-6.1" x2="13.2" y2="1.1" layer="51"/>
<wire x1="8" y1="-1.6" x2="8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8.8" y1="-1.6" x2="8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-1.6" x2="12.2" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="13" y1="-1.6" x2="13" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="7.2" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="11.4" y1="1.1" x2="9.6" y2="1.1" width="0.1524" layer="21"/>
</package>
<package name="MF06-2-VP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Straight pin with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-6.5" width="0.1524" layer="21"/>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="7" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-9.9" y="2.4" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1" y="-8.9" size="0.8128" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-8.654" y1="-0.854" x2="-8.146" y2="-0.346" layer="51"/>
<rectangle x1="-8.654" y1="-5.054" x2="-8.146" y2="-4.546" layer="51"/>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-0.854" x2="-3.946" y2="-0.346" layer="51"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="8" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="-4.454" y1="-5.054" x2="-3.946" y2="-4.546" layer="51"/>
<pad name="9" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="10" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<hole x="-13.1" y="-5.04" drill="3"/>
<hole x="17.3" y="-5.04" drill="3"/>
<wire x1="-11.1" y1="-6.5" x2="-11.1" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-3.6" x2="-11.1" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="0.9" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.3" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.5" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.7" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.9" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="-6.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.8" x2="-6.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-2.8" x2="-6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-6.8" x2="-2.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-6.8" x2="-2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-10.4" y1="1.4" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-2.6" x2="-10.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-10.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="-6.4" y1="1.4" x2="-6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="0.4" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-2.6" x2="-6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-3.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.4" x2="-5.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="-5.2" y1="1.4" x2="-6.2" y2="0.4" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-10.4" y1="-3.8" x2="-10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-9.4" y1="-2.8" x2="-10.4" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-7.4" y1="-2.8" x2="-9.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-7.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-6.4" y1="-3.8" x2="-6.4" y2="-6.8" width="0.127" layer="51"/>
<rectangle x1="-0.254" y1="-0.854" x2="0.254" y2="-0.346" layer="51"/>
<rectangle x1="-0.254" y1="-5.054" x2="0.254" y2="-4.546" layer="51"/>
<wire x1="2" y1="-2.8" x2="-2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.8" x2="-2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2" y1="-6.8" x2="2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.8" x2="2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="-2" y1="0.4" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="-2.6" x2="-2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="1" y2="1.4" width="0.127" layer="51"/>
<wire x1="1" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1" y1="1.4" x2="-2" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="0.4" x2="2" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="3.946" y1="-0.854" x2="4.454" y2="-0.346" layer="51"/>
<rectangle x1="3.946" y1="-5.054" x2="4.454" y2="-4.546" layer="51"/>
<wire x1="2.2" y1="-6.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.4" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-2.6" x2="2.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="2.2" y2="1.4" width="0.127" layer="51"/>
<wire x1="6.2" y1="1.4" x2="6.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="2.2" y1="-3.8" x2="2.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="3.2" y1="-2.8" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="5.2" y1="-2.8" x2="3.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="5.2" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.8" x2="6.2" y2="-6.8" width="0.127" layer="51"/>
<wire x1="15.3" y1="-7.5" x2="15.3" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="15.3" y1="-6.5" x2="15.3" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="15.3" y1="-3.6" x2="15.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="15.3" y1="2.1" x2="-9.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-10.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-10.3" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.3" y1="2.1" x2="-10.5" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="2.1" x2="-10.7" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.1" x2="-10.9" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-10.9" y1="2.1" x2="-11.1" y2="2.1" width="0.1524" layer="21"/>
<wire x1="15.3" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="0.6" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="21" curve="-180"/>
<wire x1="-11.1" y1="-7.5" x2="-11.1" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-6.5" x2="-11.1" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-3.6" x2="-11.1" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-0.1" x2="-11.1" y2="2.1" width="0.1524" layer="51"/>
<wire x1="15.3" y1="-7.5" x2="15.3" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="15.3" y1="-6.5" x2="15.3" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="15.3" y1="-3.6" x2="15.3" y2="2.1" width="0.1524" layer="51"/>
<wire x1="15.3" y1="2.1" x2="-11.1" y2="2.1" width="0.1524" layer="51"/>
<wire x1="15.3" y1="-7.5" x2="3.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-7.5" x2="3.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="3.6" y1="-8.5" x2="0.6" y2="-8.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-8.5" x2="0.6" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="0.6" y1="-7.5" x2="-11.1" y2="-7.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-1.1" x2="-11.1" y2="-0.1" width="0.1524" layer="51" curve="-180"/>
<wire x1="-10.3" y1="2.1" x2="-11.1" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-10.1" y1="2.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-9.9" y1="2.1" x2="-11.1" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-3.6" x2="-12.2" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="-6.5" x2="-12.2" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="16.4" y1="-3.6" x2="15.3" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="16.4" y1="-6.5" x2="15.3" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="16.4" y1="-6.5" x2="15.3" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-3.6" x2="-12.2" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="-6.5" x2="-12.2" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="16.4" y1="-3.6" x2="15.3" y2="-3.6" width="0.1524" layer="51"/>
<wire x1="-12.2" y1="-6.5" x2="-12.2" y2="-3.6" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="16.4" y1="-3.6" x2="16.4" y2="-6.5" width="0.1524" layer="51" curve="-243.654893"/>
<wire x1="-10.5" y1="2.1" x2="-11.1" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-10.7" y1="2.1" x2="-11.1" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-10.9" y1="2.1" x2="-11.1" y2="1.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-0.854" x2="8.654" y2="-0.346" layer="51"/>
<pad name="6" x="12.6" y="0" drill="1.8" shape="octagon"/>
<pad name="11" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="8.146" y1="-5.054" x2="8.654" y2="-4.546" layer="51"/>
<pad name="12" x="12.6" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="10.4" y1="-2.8" x2="6.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-2.8" x2="6.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="10.4" y1="-6.8" x2="10.4" y2="-2.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="-6.8" x2="10.4" y2="-6.8" width="0.127" layer="51"/>
<wire x1="6.4" y1="0.4" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="-2.6" x2="6.4" y2="-2.6" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="9.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="9.4" y1="1.4" x2="7.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="7.4" y1="1.4" x2="6.4" y2="0.4" width="0.127" layer="51"/>
<wire x1="10.4" y1="0.4" x2="10.4" y2="-2.6" width="0.127" layer="51"/>
<rectangle x1="12.346" y1="-0.854" x2="12.854" y2="-0.346" layer="51"/>
<rectangle x1="12.346" y1="-5.054" x2="12.854" y2="-4.546" layer="51"/>
<wire x1="14.6" y1="-2.8" x2="10.6" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.6" y1="-2.8" x2="10.6" y2="-6.8" width="0.127" layer="51"/>
<wire x1="14.6" y1="-6.8" x2="14.6" y2="-2.8" width="0.127" layer="51"/>
<wire x1="10.6" y1="-6.8" x2="14.6" y2="-6.8" width="0.127" layer="51"/>
<wire x1="10.6" y1="0.4" x2="10.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="14.6" y1="-2.6" x2="10.6" y2="-2.6" width="0.127" layer="51"/>
<wire x1="14.6" y1="0.4" x2="13.6" y2="1.4" width="0.127" layer="51"/>
<wire x1="13.6" y1="1.4" x2="11.6" y2="1.4" width="0.127" layer="51"/>
<wire x1="11.6" y1="1.4" x2="10.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="14.6" y1="0.4" x2="14.6" y2="-2.6" width="0.127" layer="51"/>
</package>
<package name="MF06-2-HP">
<description>&lt;h3&gt;Mini Fit 4.2mm Double layer Right pin (Horizontal) with Peg&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Pitch: 4.2mm&lt;/li&gt;
&lt;li&gt;Pins: 8 (2 x 4)&lt;/li&gt;
&lt;li&gt;Rated Voltage: 300V AC.DC&lt;/li&gt;
&lt;li&gt;Rated Current: 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="1" x="-8.4" y="0" drill="1.8" shape="square"/>
<pad name="7" x="-8.4" y="-5.5" drill="1.8" shape="octagon"/>
<text x="-10.1" y="0.8" size="0.8128" layer="25" ratio="10" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="2" y="2" size="0.8128" layer="27" ratio="10" align="bottom-center">&gt;VALUE</text>
<pad name="2" x="-4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="3" x="0" y="0" drill="1.8" shape="octagon"/>
<pad name="4" x="4.2" y="0" drill="1.8" shape="octagon"/>
<pad name="8" x="-4.2" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="9" x="0" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="10" x="4.2" y="-5.5" drill="1.8" shape="octagon"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="15.3" y1="1.1" x2="15.3" y2="13.9" width="0.1524" layer="21"/>
<wire x1="15.3" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="21"/>
<wire x1="15.3" y1="1.1" x2="13.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3" y1="1.1" x2="1.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-1.2" y1="1.1" x2="-3" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-5.4" y1="1.1" x2="-6.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10" y1="1.1" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.2" y1="1.1" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.4" y1="1.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.6" y1="1.1" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-10.8" y1="1.1" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="15.3" y1="1.1" x2="15.3" y2="13.9" width="0.1524" layer="51"/>
<wire x1="15.3" y1="13.9" x2="3.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="0.6" y1="13.9" x2="-11.1" y2="13.9" width="0.1524" layer="51"/>
<wire x1="15.3" y1="1.1" x2="-11.1" y2="1.1" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="51"/>
<wire x1="-11.1" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="51"/>
<hole x="-8.4" y="7.3" drill="3"/>
<hole x="12.6" y="7.3" drill="3"/>
<rectangle x1="-9" y1="-6.1" x2="-7.8" y2="1.1" layer="51"/>
<rectangle x1="-4.8" y1="-6.1" x2="-3.6" y2="1.1" layer="51"/>
<rectangle x1="-0.6" y1="-6.1" x2="0.6" y2="1.1" layer="51"/>
<rectangle x1="3.6" y1="-6.1" x2="4.8" y2="1.1" layer="51"/>
<wire x1="-11.6" y1="1.1" x2="-11.6" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.6" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.6" y2="1.7" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11.6" y2="1.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-11.6" y2="2.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-11.6" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-11.6" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="1.1" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.2" y1="1.1" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.4" y1="1.1" x2="-11.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.1" y1="13.9" x2="-11.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.3" x2="-11.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.5" x2="-11.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.7" x2="-11" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="1.9" x2="-10.8" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.1" x2="-10.6" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.3" x2="-10.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="2.5" x2="-10.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="51"/>
<wire x1="3.6" y1="11.3" x2="0.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="11.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="0.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="12.3" x2="3.6" y2="11.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="13.9" x2="3.6" y2="12.3" width="0.1524" layer="21"/>
<wire x1="0.6" y1="12.3" x2="0.6" y2="13.9" width="0.1524" layer="21"/>
<wire x1="-8.8" y1="-1.6" x2="-8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-8" y1="-1.6" x2="-8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-1.6" x2="-4.6" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-3.8" y1="-1.6" x2="-3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="0.4" y1="-1.6" x2="0.4" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-1.6" x2="3.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-1.6" x2="4.6" y2="-3.9" width="0.1524" layer="21"/>
<pad name="5" x="8.4" y="0" drill="1.8" shape="octagon"/>
<pad name="6" x="12.6" y="0" drill="1.8" shape="octagon"/>
<pad name="11" x="8.4" y="-5.5" drill="1.8" shape="octagon"/>
<pad name="12" x="12.6" y="-5.5" drill="1.8" shape="octagon"/>
<rectangle x1="7.8" y1="-6.1" x2="9" y2="1.1" layer="51"/>
<rectangle x1="12" y1="-6.1" x2="13.2" y2="1.1" layer="51"/>
<wire x1="8" y1="-1.6" x2="8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="8.8" y1="-1.6" x2="8.8" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="12.2" y1="-1.6" x2="12.2" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="13" y1="-1.6" x2="13" y2="-3.9" width="0.1524" layer="21"/>
<wire x1="7.2" y1="1.1" x2="5.4" y2="1.1" width="0.1524" layer="21"/>
<wire x1="11.4" y1="1.1" x2="9.6" y2="1.1" width="0.1524" layer="21"/>
</package>
<package name="MA08-1-V">
<wire x1="-8.255" y1="1.27" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-8.89" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-8.89" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<wire x1="-8.89" y1="-1.27" x2="-8.89" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-1.016" x2="-8.89" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.889" x2="-8.89" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.762" x2="-8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-8.382" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-1.27" x2="-8.509" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="-1.27" x2="-8.636" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-1.27" x2="-8.89" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-1.27" x2="-8.89" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="-1.27" x2="-8.89" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-1.27" x2="-8.89" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<pad name="7" x="7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0.635" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<pad name="8" x="10.16" y="0" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<wire x1="-8.255" y1="1.27" x2="-6.985" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-1.27" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.27" x2="-8.89" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-1.27" x2="-8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="-1.27" x2="-8.89" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="-1.016" x2="-8.89" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="-0.889" x2="-8.89" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="-0.762" x2="-8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="-1.27" x2="-8.382" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.382" y1="-1.27" x2="-8.509" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.509" y1="-1.27" x2="-8.636" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.636" y1="-1.27" x2="-8.89" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.382" y1="-1.27" x2="-8.89" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-8.509" y1="-1.27" x2="-8.89" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-8.636" y1="-1.27" x2="-8.89" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="0.635" x2="6.985" y2="1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="51"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="51"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.1524" layer="51"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.1524" layer="51"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.1524" layer="51"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="11.43" y1="0.635" x2="11.43" y2="-0.635" width="0.1524" layer="51"/>
</package>
<package name="JST-PH-08-H">
<description>&lt;h3&gt;Connector 2mm 8 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-6" y="0" drill="0.8" shape="octagon"/>
<pad name="2" x="-4" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="-2" y="0" drill="0.8" shape="octagon"/>
<pad name="4" x="0" y="0" drill="0.8" shape="octagon"/>
<pad name="5" x="2" y="0" drill="0.8" shape="octagon"/>
<wire x1="9.95" y1="1.6" x2="9.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="9.95" y1="-6" x2="6.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-6" x2="-4.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-6" x2="-7.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="-6" x2="-7.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="9.95" y1="1.6" x2="9.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.6" x2="-7.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="9.3" y1="1.6" x2="9.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-7.3" y1="1.6" x2="-7.3" y2="0" width="0.2032" layer="51"/>
<text x="1" y="1" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="6.5" y1="-3.6" x2="6.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-3.6" x2="-4.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-3.6" x2="-4.5" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-6" y1="-5.5" x2="-5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-3.5" x2="-6.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-6.5" y1="-3.5" x2="-6" y2="-5.5" width="0.127" layer="51"/>
<pad name="6" x="4" y="0" drill="0.8" shape="octagon"/>
<pad name="7" x="6" y="0" drill="0.8" shape="octagon"/>
<pad name="8" x="8" y="0" drill="0.8" shape="octagon"/>
<wire x1="-7.3" y1="0" x2="9.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-6.3" y1="0" x2="-5.7" y2="0.3" layer="51"/>
<rectangle x1="-4.3" y1="0" x2="-3.7" y2="0.3" layer="51"/>
<rectangle x1="-2.3" y1="0" x2="-1.7" y2="0.3" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<rectangle x1="3.7" y1="0" x2="4.3" y2="0.3" layer="51"/>
<rectangle x1="5.7" y1="0" x2="6.3" y2="0.3" layer="51"/>
<rectangle x1="7.7" y1="0" x2="8.3" y2="0.3" layer="51"/>
<wire x1="9.95" y1="1.6" x2="9.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="9.95" y1="-6" x2="6.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-6" x2="-4.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-6" x2="-7.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-6" x2="-7.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="9.95" y1="1.6" x2="9.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.6" x2="-7.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="1.6" x2="9.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-7.3" y1="1.6" x2="-7.3" y2="0" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-3.6" x2="6.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-3.6" x2="-4.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-3.6" x2="-4.5" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5.5" x2="-5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3.5" x2="-6.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-3.5" x2="-6" y2="-5.5" width="0.127" layer="21"/>
</package>
<package name="JST-PH-08-V">
<description>&lt;h3&gt;Connector 2mm 8 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="9.95" y1="1.72" x2="9.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="9.95" y1="-2.78" x2="-7.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.72" x2="-7.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.6" y1="1.72" x2="-7.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.4" y1="1.72" x2="-7.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.2" y1="1.72" x2="-7" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7" y1="1.72" x2="-6.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-6.8" y1="1.72" x2="-6.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-6.6" y1="1.72" x2="-6.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="1.72" x2="-6.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-6.2" y1="1.72" x2="-4.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="6.73" y1="1.72" x2="9.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="6.73" y1="1.22" x2="-4.73" y2="1.22" width="0.2032" layer="51"/>
<wire x1="-4.73" y1="1.22" x2="-4.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="6.73" y1="1.22" x2="6.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="-2.78" x2="-7.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="-6" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="-4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="-2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="4" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="5" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="1" y="1.5" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-7.95" y1="0.12" x2="-7.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.32" x2="-7.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.52" x2="-7.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.72" x2="-7.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.92" x2="-7.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.12" x2="-7.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.32" x2="-7.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.52" x2="-7.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.12" x2="-6.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.32" x2="-6.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.52" x2="-6.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.72" x2="-6.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="0.92" x2="-7" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.12" x2="-7.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.32" x2="-7.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-7.95" y1="1.52" x2="-7.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<rectangle x1="-6.254" y1="-0.254" x2="-5.746" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<pad name="6" x="4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<pad name="7" x="6" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="5.746" y1="-0.254" x2="6.254" y2="0.254" layer="51"/>
<pad name="8" x="8" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="7.746" y1="-0.254" x2="8.254" y2="0.254" layer="51"/>
<wire x1="9.95" y1="1.72" x2="9.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="9.95" y1="-2.78" x2="-7.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.72" x2="-7.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="1.72" x2="-7.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.4" y1="1.72" x2="-7.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.2" y1="1.72" x2="-7" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7" y1="1.72" x2="-6.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-6.8" y1="1.72" x2="-6.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-6.6" y1="1.72" x2="-6.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="1.72" x2="-6.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-6.2" y1="1.72" x2="-4.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="6.73" y1="1.72" x2="9.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="6.73" y1="1.22" x2="-4.73" y2="1.22" width="0.2032" layer="21"/>
<wire x1="-4.73" y1="1.22" x2="-4.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="6.73" y1="1.22" x2="6.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-2.78" x2="-7.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.12" x2="-7.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.32" x2="-7.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.52" x2="-7.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.72" x2="-7.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.92" x2="-7.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.12" x2="-7.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.32" x2="-7.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.52" x2="-7.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.12" x2="-6.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.32" x2="-6.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.52" x2="-6.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.72" x2="-6.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="0.92" x2="-7" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.12" x2="-7.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.32" x2="-7.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="1.52" x2="-7.6" y2="1.72" width="0.2032" layer="21"/>
</package>
<package name="JST-PH-08-HS">
<description>&lt;h3&gt;Connector 2mm 8 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S8B-PH-SM4-TB, NXW-08SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-8.9" y1="-4" x2="-8.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="2.4" x2="-8.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="2.7" x2="-8.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="2.9" x2="-8.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="3.1" x2="-8.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="3.3" x2="-8.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="3.6" x2="-8.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="3.6" x2="-8.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-8.4" y1="3.6" x2="-8.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-8.2" y1="3.6" x2="-8" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-8" y1="3.6" x2="10.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="10.6" y1="3.6" x2="10.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="10.9" y1="-4" x2="-5.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-4" x2="-6.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-4" x2="-8.9" y2="-4" width="0.2032" layer="51"/>
<text x="1" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-6" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="10.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-8.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="10.9" y1="3.6" x2="10.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="10.9" y1="2.4" x2="10.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="3.6" x2="-8.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="2.4" x2="-8.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="10.6" y1="3.6" x2="10.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="10.6" y1="2.4" x2="10.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-6" y1="-2.7" x2="-6.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-4" x2="-6" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="3.6" x2="-8.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-8.4" y1="3.6" x2="-8.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-8.2" y1="3.6" x2="-8.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-8" y1="3.6" x2="-8.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-8.9" y1="-0.5" x2="-8.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="2.4" x2="-8.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="2.7" x2="-8.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="2.9" x2="-8.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="3.1" x2="-8.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="3.3" x2="-8.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="3.6" x2="-8.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-8.6" y1="3.6" x2="-8.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-8.4" y1="3.6" x2="-8.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="3.6" x2="-8" y2="3.6" width="0.2032" layer="21"/>
<wire x1="8.8" y1="3.6" x2="10.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="10.6" y1="3.6" x2="10.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-4" x2="-5.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-4" x2="-6.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="10.9" y1="3.6" x2="10.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="10.9" y1="2.4" x2="10.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-8.6" y1="3.6" x2="-8.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-8.9" y1="2.4" x2="-8.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="10.6" y1="3.6" x2="10.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="10.6" y1="2.4" x2="10.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-2.7" x2="-6.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-4" x2="-6" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-8.6" y1="3.6" x2="-8.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-8.4" y1="3.6" x2="-8.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="3.6" x2="-8.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-8" y1="3.6" x2="-8.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-6.2" y1="3.6" x2="-5.8" y2="4.6" layer="51"/>
<smd name="2" x="-4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="4" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-4.2" y1="3.6" x2="-3.8" y2="4.6" layer="51"/>
<rectangle x1="-2.2" y1="3.6" x2="-1.8" y2="4.6" layer="51"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<wire x1="-6.5" y1="-4" x2="-7.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6.8" y1="3.6" x2="-8" y2="3.6" width="0.2032" layer="21"/>
<smd name="5" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<smd name="6" x="4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="3.8" y1="3.6" x2="4.2" y2="4.6" layer="51"/>
<smd name="7" x="6" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="5.8" y1="3.6" x2="6.2" y2="4.6" layer="51"/>
<smd name="8" x="8" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="7.8" y1="3.6" x2="8.2" y2="4.6" layer="51"/>
</package>
<package name="JST-PH-08-VS">
<description>&lt;h3&gt;Connector 2mm 8 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B8B-PH-SM4-TB, NXW-08SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="10.95" y1="1.9" x2="10.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="10.95" y1="-2" x2="10.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="10.95" y1="-3.1" x2="10.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="10.65" y1="-3.1" x2="-8.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-8.05" y1="-3.1" x2="-8.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-8.25" y1="-3.1" x2="-8.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-8.45" y1="-3.1" x2="-8.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.1" x2="-8.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-8.95" y1="1.9" x2="10.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-8.95" y1="-3.1" x2="-8.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="1" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-6.25" y1="-0.25" x2="-5.75" y2="0.25" layer="51"/>
<smd name="1" x="-6" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-8.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="10.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-8.95" y1="-2.8" x2="-8.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-8.95" y1="-2.6" x2="-8.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-8.95" y1="-2.4" x2="-8.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-8.95" y1="-2.2" x2="-8.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-8.95" y1="-2" x2="-8.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="10.65" y1="-3.1" x2="10.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-8.25" y1="-3.1" x2="-8.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-8.45" y1="-3.1" x2="-8.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.1" x2="-8.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-8.05" y1="-3.1" x2="-8.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="10.95" y1="-2" x2="10.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.1" x2="-8.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-2" x2="-8.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6" y1="1.6" x2="-6.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-6" y1="1.6" x2="-5.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-6.2" y1="2.1" x2="-5.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="10.95" y1="-2.4" x2="10.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="10.95" y1="-3.1" x2="10.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="10.65" y1="-3.1" x2="8.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-8.05" y1="-3.1" x2="-8.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-3.1" x2="-8.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-8.45" y1="-3.1" x2="-8.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-8.65" y1="-3.1" x2="-8.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-8.95" y1="1.9" x2="10.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-8.95" y1="-3.1" x2="-8.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-8.95" y1="-2.8" x2="-8.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-8.95" y1="-2.6" x2="-8.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="10.65" y1="-3.1" x2="10.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-8.25" y1="-3.1" x2="-8.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-8.45" y1="-3.1" x2="-8.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-8.65" y1="-3.1" x2="-8.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-8.05" y1="-3.1" x2="-8.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="10.95" y1="-2.4" x2="10.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-8.65" y1="-3.1" x2="-8.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-8.65" y1="-2.4" x2="-8.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-8.75" y1="-2.4" x2="-8.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.6" x2="-6.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.6" x2="-5.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-6.2" y1="2.1" x2="-5.8" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="-4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-4.25" y1="-0.25" x2="-3.75" y2="0.25" layer="51"/>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<text x="1" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-6.25" y1="-4.1" x2="-5.75" y2="-3.1" layer="51"/>
<rectangle x1="-4.25" y1="-4.1" x2="-3.75" y2="-3.1" layer="51"/>
<rectangle x1="-2.25" y1="-4.1" x2="-1.75" y2="-3.1" layer="51"/>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<wire x1="-8.95" y1="1.9" x2="-8.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="10.95" y1="1.9" x2="10.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-6.8" y1="-3.1" x2="-8.05" y2="-3.1" width="0.2032" layer="21"/>
<smd name="5" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<smd name="6" x="4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="-4.1" x2="4.25" y2="-3.1" layer="51"/>
<smd name="7" x="6" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51"/>
<rectangle x1="5.75" y1="-4.1" x2="6.25" y2="-3.1" layer="51"/>
<smd name="8" x="8" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="7.75" y1="-0.25" x2="8.25" y2="0.25" layer="51"/>
<rectangle x1="7.75" y1="-4.1" x2="8.25" y2="-3.1" layer="51"/>
</package>
<package name="JST-SR-08-HS">
<description>&lt;h3&gt;Connector 1mm 8 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM8B-SRSS-TB, JOINT TECH A1001-WR-S-08P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.5" y1="-2.3" x2="-4.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="0.75" x2="-4.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="1.05" x2="-4.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="1.25" x2="-4.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="1.45" x2="-4.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="1.65" x2="-4.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="1.95" x2="-4.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.95" x2="-4" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-4" y1="1.95" x2="-3.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3.8" y1="1.95" x2="-3.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="1.95" x2="5.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="5.2" y1="1.95" x2="5.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="5.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-2.3" x2="-3.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-2.3" x2="-4.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0.5" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="-3" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="-2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="-1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="5" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="5.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-4.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="5.5" y1="1.95" x2="5.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="5.5" y1="0.75" x2="5.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.95" x2="-4.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="0.75" x2="-4.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="5.2" y1="1.95" x2="5.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="5.2" y1="0.75" x2="5.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1" x2="-3.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-2.3" x2="-3" y2="-1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.95" x2="-4.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-4" y1="1.95" x2="-4.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-3.8" y1="1.95" x2="-4.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="1.95" x2="-4.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-0.45" x2="-4.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="0.75" x2="-4.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.05" x2="-4.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.25" x2="-4.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.45" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="-4.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.95" x2="-4.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.95" x2="-4" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.95" x2="-3.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3.8" y1="1.95" x2="-3.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="4.6" y1="1.95" x2="5.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.95" x2="5.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-2.3" x2="-2.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-2.3" x2="-3.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="5.5" y1="1.95" x2="5.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="5.5" y1="0.75" x2="5.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.95" x2="-4.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="0.75" x2="-4.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.95" x2="5.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="5.2" y1="0.75" x2="5.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1" x2="-3.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-2.3" x2="-3" y2="-1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.95" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.95" x2="-4.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-3.8" y1="1.95" x2="-4.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="1.95" x2="-4.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-3.2" y1="2" x2="-2.8" y2="2.6" layer="51"/>
<rectangle x1="-2.2" y1="2" x2="-1.8" y2="2.6" layer="51"/>
<rectangle x1="-1.2" y1="2" x2="-0.8" y2="2.6" layer="51"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
<smd name="6" x="2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="1.8" y1="2" x2="2.2" y2="2.6" layer="51"/>
<smd name="7" x="3" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="2.8" y1="2" x2="3.2" y2="2.6" layer="51"/>
<smd name="8" x="4" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="3.8" y1="2" x2="4.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-08-VS">
<description>&lt;h3&gt;Connector 1mm 8 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM8B-SRSS-RM, JOINT TECH A1001-WV-S-8P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.5" y1="0.95" x2="5.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="5.5" y1="-0.85" x2="5.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="5.5" y1="-1.95" x2="5.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.95" x2="-3.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="-1.95" x2="-3.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-3.8" y1="-1.95" x2="-4" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-4" y1="-1.95" x2="-4.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="-1.95" x2="-4.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="0.95" x2="5.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-1.95" x2="-4.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0.5" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-3.254" y1="-0.254" x2="-2.746" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<smd name="1" x="-3" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-4.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="5.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-1.45" x2="-4.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-1.25" x2="-4.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-1.05" x2="-4.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-0.85" x2="-4.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.95" x2="5.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3.8" y1="-1.95" x2="-4.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-4" y1="-1.95" x2="-4.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="-1.95" x2="-4.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="-1.95" x2="-4.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="5.5" y1="-0.85" x2="5.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="-1.95" x2="-4.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="-0.85" x2="-4.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-3" y1="0.65" x2="-3.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-3" y1="0.65" x2="-2.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="1.15" x2="-2.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="5.5" y1="-0.85" x2="5.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-1.95" x2="5.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-1.95" x2="4.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.95" x2="-3.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.95" x2="-3.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3.8" y1="-1.95" x2="-4" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4" y1="-1.95" x2="-4.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.95" x2="-4.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="0.95" x2="4.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.95" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.45" x2="-4.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.25" x2="-4.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.05" x2="-4.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-1.95" x2="5.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-3.8" y1="-1.95" x2="-4.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-4" y1="-1.95" x2="-4.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.95" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.95" x2="-4.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-0.85" x2="5.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.95" x2="-4.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-0.85" x2="-4.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-3" y1="0.65" x2="-3.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-3" y1="0.65" x2="-2.8" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="1.15" x2="-2.8" y2="1.15" width="0.2032" layer="21"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<smd name="6" x="2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
<smd name="7" x="3" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<smd name="8" x="4" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
</package>
<package name="MA10-1-V">
<wire x1="-10.795" y1="1.27" x2="-9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="0.635" x2="-11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-11.43" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-10.16" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="-7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="-5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="6" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-11.43" y="1.651" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-11.43" y="-1.651" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="-10.414" y1="-0.254" x2="-9.906" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="-11.43" y1="-1.27" x2="-11.43" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-1.016" x2="-11.43" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-0.889" x2="-11.43" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-0.762" x2="-11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-10.922" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.922" y1="-1.27" x2="-11.049" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-11.049" y1="-1.27" x2="-11.176" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-1.27" x2="-11.43" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.922" y1="-1.27" x2="-11.43" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-11.049" y1="-1.27" x2="-11.43" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-1.27" x2="-11.43" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="7" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<pad name="8" x="7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<wire x1="-10.795" y1="1.27" x2="-9.525" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-9.525" y1="1.27" x2="-8.89" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="-0.635" x2="-9.525" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.89" y1="0.635" x2="-8.255" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.27" x2="-6.985" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-1.27" x2="-8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="-1.27" x2="-8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="0.635" x2="-11.43" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-10.795" y1="1.27" x2="-11.43" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-9.525" y1="-1.27" x2="-10.795" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-1.27" x2="-6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-1.27" x2="-11.43" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-1.016" x2="-11.43" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-0.889" x2="-11.43" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-0.762" x2="-11.43" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-10.795" y1="-1.27" x2="-10.922" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-10.922" y1="-1.27" x2="-11.049" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.049" y1="-1.27" x2="-11.176" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-1.27" x2="-11.43" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-10.922" y1="-1.27" x2="-11.43" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-11.049" y1="-1.27" x2="-11.43" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-1.27" x2="-11.43" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.35" y1="0.635" x2="6.985" y2="1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="51"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="6.985" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<pad name="9" x="10.16" y="0" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.1524" layer="51"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.1524" layer="51"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.1524" layer="51"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.1524" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.1524" layer="21"/>
<pad name="10" x="12.7" y="0" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.1524" layer="51"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.1524" layer="51"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.1524" layer="51"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.1524" layer="51"/>
</package>
<package name="JST-SR-10-HS">
<description>&lt;h3&gt;Connector 1mm 10 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST SM10B-SRSS-TB, JOINT TECH A1001-WR-S-10P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 10&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.5" y1="-2.3" x2="-5.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="0.75" x2="-5.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.05" x2="-5.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.25" x2="-5.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.45" x2="-5.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.65" x2="-5.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.95" x2="-5.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.95" x2="-5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-5" y1="1.95" x2="-4.8" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="1.95" x2="-4.6" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="1.95" x2="6.2" y2="1.95" width="0.2032" layer="51"/>
<wire x1="6.2" y1="1.95" x2="6.5" y2="1.95" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-2.3" x2="-3.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-2.3" x2="-4.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.3" x2="-5.5" y2="-2.3" width="0.2032" layer="51"/>
<text x="0.5" y="-2.6" size="0.8128" layer="25" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="0.5" size="0.8128" layer="27" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<smd name="1" x="-4" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="-3" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="-2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="-1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="5" x="0" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<smd name="P$2" x="6.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<smd name="P$1" x="-5.3" y="-1.6" dx="1.8" dy="1.2" layer="1" rot="R270"/>
<wire x1="6.5" y1="1.95" x2="6.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="6.5" y1="0.75" x2="6.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.95" x2="-5.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="0.75" x2="-5.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="6.2" y1="1.95" x2="6.2" y2="0.75" width="0.2032" layer="51"/>
<wire x1="6.2" y1="0.75" x2="6.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-4" y1="-1" x2="-4.5" y2="-2.3" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-2.3" x2="-4" y2="-1" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.95" x2="-5.5" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-5" y1="1.95" x2="-5.5" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="1.95" x2="-5.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="1.95" x2="-5.5" y2="1.05" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-0.45" x2="-5.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0.75" x2="-5.5" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="1.05" x2="-5.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="1.25" x2="-5.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="1.45" x2="-5.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="1.65" x2="-5.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="1.95" x2="-5.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="1.95" x2="-5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-5" y1="1.95" x2="-4.8" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="1.95" x2="-4.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="5.6" y1="1.95" x2="6.2" y2="1.95" width="0.2032" layer="21"/>
<wire x1="6.2" y1="1.95" x2="6.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2.3" x2="-3.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.3" x2="-4.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="6.5" y1="1.95" x2="6.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="6.5" y1="0.75" x2="6.5" y2="-0.45" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="1.95" x2="-5.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0.75" x2="-5.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="6.2" y1="1.95" x2="6.2" y2="0.75" width="0.2032" layer="21"/>
<wire x1="6.2" y1="0.75" x2="6.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-4" y1="-1" x2="-4.5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-2.3" x2="-4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="1.95" x2="-5.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-5" y1="1.95" x2="-5.5" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="1.95" x2="-5.5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="1.95" x2="-5.5" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-4.2" y1="2" x2="-3.8" y2="2.6" layer="51"/>
<rectangle x1="-3.2" y1="2" x2="-2.8" y2="2.6" layer="51"/>
<rectangle x1="-2.2" y1="2" x2="-1.8" y2="2.6" layer="51"/>
<rectangle x1="-1.2" y1="2" x2="-0.8" y2="2.6" layer="51"/>
<rectangle x1="-0.2" y1="2" x2="0.2" y2="2.6" layer="51"/>
<smd name="6" x="1" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="0.8" y1="2" x2="1.2" y2="2.6" layer="51"/>
<smd name="7" x="2" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="1.8" y1="2" x2="2.2" y2="2.6" layer="51"/>
<smd name="8" x="3" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="2.8" y1="2" x2="3.2" y2="2.6" layer="51"/>
<smd name="9" x="4" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="3.8" y1="2" x2="4.2" y2="2.6" layer="51"/>
<smd name="10" x="5" y="2.275" dx="1.55" dy="0.6" layer="1" rot="R270"/>
<rectangle x1="4.8" y1="2" x2="5.2" y2="2.6" layer="51"/>
</package>
<package name="JST-SR-10-VS">
<description>&lt;h3&gt;Connector 1mm 10 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST BM10B-SRSS-RM, JOINT TECH A1001-WV-S-10P, NX1001&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 10&lt;/li&gt;
&lt;li&gt;Pin pitch: 1mm&lt;/li&gt;
&lt;li&gt;Rated current: 0.7A&lt;/li&gt;
&lt;li&gt;Rated voltage: 50V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.5" y1="0.95" x2="6.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-0.85" x2="6.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-1.95" x2="6.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="6.2" y1="-1.95" x2="-4.6" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="-1.95" x2="-4.8" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="-1.95" x2="-5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-5" y1="-1.95" x2="-5.2" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="-1.95" x2="-5.5" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="0.95" x2="6.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-1.95" x2="-5.5" y2="-1.65" width="0.2032" layer="51"/>
<text x="0.5" y="1.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0.5" y="-0.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-3.254" y1="-0.254" x2="-2.746" y2="0.254" layer="51"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<smd name="1" x="-4" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-3" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="0" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="P$1" x="-5.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$2" x="6.3" y="0.25" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-5.5" y1="-1.65" x2="-5.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-1.45" x2="-5.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-1.25" x2="-5.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-1.05" x2="-5.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-0.85" x2="-5.5" y2="0.95" width="0.2032" layer="51"/>
<wire x1="6.2" y1="-1.95" x2="6.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="-1.95" x2="-5.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-5" y1="-1.95" x2="-5.5" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="-1.95" x2="-5.5" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="-1.95" x2="-5.5" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-0.85" x2="6.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="-1.95" x2="-5.2" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="-0.85" x2="-5.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-4" y1="0.65" x2="-4.2" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-4" y1="0.65" x2="-3.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.15" x2="-3.8" y2="1.15" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-0.85" x2="6.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-1.95" x2="6.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="6.2" y1="-1.95" x2="5.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.95" x2="-4.6" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-1.95" x2="-4.8" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-1.95" x2="-5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.95" x2="-5.2" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.95" x2="-5.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="0.95" x2="5.5" y2="0.95" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-1.95" x2="-5.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-1.65" x2="-5.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-1.45" x2="-5.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-1.25" x2="-5.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-1.05" x2="-5.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="6.2" y1="-1.95" x2="6.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-1.95" x2="-5.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.95" x2="-5.5" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.95" x2="-5.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-1.95" x2="-5.5" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-0.85" x2="6.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.95" x2="-5.2" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-0.85" x2="-5.5" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-4" y1="0.65" x2="-4.2" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-4" y1="0.65" x2="-3.8" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.15" x2="-3.8" y2="1.15" width="0.2032" layer="21"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<smd name="6" x="1" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<smd name="7" x="2" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
<smd name="8" x="3" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<smd name="9" x="4" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<rectangle x1="4.746" y1="-0.254" x2="5.254" y2="0.254" layer="51"/>
<smd name="10" x="5" y="-2.275" dx="1.55" dy="0.6" layer="1" rot="R90"/>
</package>
<package name="JST-PH-10-H">
<description>&lt;h3&gt;Connector 2mm 8 Pin Horizontal THT&lt;/h3&gt;
&lt;p&gt;
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-8" y="0" drill="0.8" shape="octagon"/>
<pad name="2" x="-6" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="-4" y="0" drill="0.8" shape="octagon"/>
<pad name="4" x="-2" y="0" drill="0.8" shape="octagon"/>
<pad name="5" x="0" y="0" drill="0.8" shape="octagon"/>
<wire x1="11.95" y1="1.6" x2="11.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="11.95" y1="-6" x2="8.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="8.5" y1="-6" x2="-6.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-6" x2="-9.95" y2="-6" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="-6" x2="-9.95" y2="1.6" width="0.2032" layer="51"/>
<wire x1="11.95" y1="1.6" x2="11.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.6" x2="-9.3" y2="1.6" width="0.2032" layer="51"/>
<wire x1="11.3" y1="1.6" x2="11.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-9.3" y1="1.6" x2="-9.3" y2="0" width="0.2032" layer="51"/>
<text x="1" y="1" size="0.8128" layer="25" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1.5" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="8.5" y1="-3.6" x2="8.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-3.6" x2="-6.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="8.5" y1="-3.6" x2="-6.5" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-8" y1="-5.5" x2="-7.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-7.5" y1="-3.5" x2="-8.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-8.5" y1="-3.5" x2="-8" y2="-5.5" width="0.127" layer="51"/>
<pad name="6" x="2" y="0" drill="0.8" shape="octagon"/>
<pad name="7" x="4" y="0" drill="0.8" shape="octagon"/>
<pad name="8" x="6" y="0" drill="0.8" shape="octagon"/>
<wire x1="-9.3" y1="0" x2="11.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="-8.3" y1="0" x2="-7.7" y2="0.3" layer="51"/>
<rectangle x1="-6.3" y1="0" x2="-5.7" y2="0.3" layer="51"/>
<rectangle x1="-4.3" y1="0" x2="-3.7" y2="0.3" layer="51"/>
<rectangle x1="-2.3" y1="0" x2="-1.7" y2="0.3" layer="51"/>
<rectangle x1="-0.3" y1="0" x2="0.3" y2="0.3" layer="51"/>
<rectangle x1="1.7" y1="0" x2="2.3" y2="0.3" layer="51"/>
<rectangle x1="3.7" y1="0" x2="4.3" y2="0.3" layer="51"/>
<rectangle x1="5.7" y1="0" x2="6.3" y2="0.3" layer="51"/>
<wire x1="11.95" y1="1.6" x2="11.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="11.95" y1="-6" x2="8.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="8.5" y1="-6" x2="-6.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="-6" x2="-9.95" y2="-6" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="-6" x2="-9.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="11.95" y1="1.6" x2="11.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.6" x2="-9.3" y2="1.6" width="0.2032" layer="21"/>
<wire x1="11.3" y1="1.6" x2="11.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-9.3" y1="1.6" x2="-9.3" y2="0" width="0.2032" layer="21"/>
<wire x1="8.5" y1="-3.6" x2="8.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="-3.6" x2="-6.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="8.5" y1="-3.6" x2="-6.5" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-8" y1="-5.5" x2="-7.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-3.5" x2="-8.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.5" x2="-8" y2="-5.5" width="0.127" layer="21"/>
<rectangle x1="7.7" y1="0" x2="8.3" y2="0.3" layer="51"/>
<rectangle x1="9.7" y1="0" x2="10.3" y2="0.3" layer="51"/>
<pad name="9" x="8" y="0" drill="0.8" shape="octagon"/>
<pad name="10" x="10" y="0" drill="0.8" shape="octagon"/>
</package>
<package name="JST-PH-10-V">
<description>&lt;h3&gt;Connector 2mm 8 Pin Vertical THT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 8&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="11.95" y1="1.72" x2="11.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="11.95" y1="-2.78" x2="-9.95" y2="-2.78" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.72" x2="-9.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.6" y1="1.72" x2="-9.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.4" y1="1.72" x2="-9.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.2" y1="1.72" x2="-9" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9" y1="1.72" x2="-8.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-8.8" y1="1.72" x2="-8.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="1.72" x2="-8.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-8.4" y1="1.72" x2="-8.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-8.2" y1="1.72" x2="-6.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="8.73" y1="1.72" x2="11.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="8.73" y1="1.22" x2="-6.73" y2="1.22" width="0.2032" layer="51"/>
<wire x1="-6.73" y1="1.22" x2="-6.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="8.73" y1="1.22" x2="8.73" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="-2.78" x2="-9.95" y2="0.12" width="0.2032" layer="51"/>
<pad name="1" x="-8" y="0" drill="0.8" shape="octagon" rot="R270"/>
<pad name="2" x="-6" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="3" x="-4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="4" x="-2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<pad name="5" x="0" y="0" drill="0.8" shape="octagon" rot="R180"/>
<text x="1" y="1.5" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="-1" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-9.95" y1="0.12" x2="-9.95" y2="0.32" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.32" x2="-9.95" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.52" x2="-9.95" y2="0.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.72" x2="-9.95" y2="0.92" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.92" x2="-9.95" y2="1.12" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.12" x2="-9.95" y2="1.32" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.32" x2="-9.95" y2="1.52" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.52" x2="-9.95" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.12" x2="-8.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.32" x2="-8.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.52" x2="-8.6" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.72" x2="-8.8" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="0.92" x2="-9" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.12" x2="-9.2" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.32" x2="-9.4" y2="1.72" width="0.2032" layer="51"/>
<wire x1="-9.95" y1="1.52" x2="-9.6" y2="1.72" width="0.2032" layer="51"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<rectangle x1="-6.254" y1="-0.254" x2="-5.746" y2="0.254" layer="51"/>
<rectangle x1="-8.254" y1="-0.254" x2="-7.746" y2="0.254" layer="51"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<pad name="6" x="2" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<pad name="7" x="4" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
<pad name="8" x="6" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="5.746" y1="-0.254" x2="6.254" y2="0.254" layer="51"/>
<wire x1="11.95" y1="1.72" x2="11.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="11.95" y1="-2.78" x2="-9.95" y2="-2.78" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.72" x2="-9.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.6" y1="1.72" x2="-9.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.4" y1="1.72" x2="-9.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.2" y1="1.72" x2="-9" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9" y1="1.72" x2="-8.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-8.8" y1="1.72" x2="-8.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-8.6" y1="1.72" x2="-8.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-8.4" y1="1.72" x2="-8.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="1.72" x2="-6.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="8.73" y1="1.72" x2="11.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="8.73" y1="1.22" x2="-6.73" y2="1.22" width="0.2032" layer="21"/>
<wire x1="-6.73" y1="1.22" x2="-6.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="8.73" y1="1.22" x2="8.73" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="-2.78" x2="-9.95" y2="0.12" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.12" x2="-9.95" y2="0.32" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.32" x2="-9.95" y2="0.52" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.52" x2="-9.95" y2="0.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.72" x2="-9.95" y2="0.92" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.92" x2="-9.95" y2="1.12" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.12" x2="-9.95" y2="1.32" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.32" x2="-9.95" y2="1.52" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.52" x2="-9.95" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.12" x2="-8.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.32" x2="-8.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.52" x2="-8.6" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.72" x2="-8.8" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="0.92" x2="-9" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.12" x2="-9.2" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.32" x2="-9.4" y2="1.72" width="0.2032" layer="21"/>
<wire x1="-9.95" y1="1.52" x2="-9.6" y2="1.72" width="0.2032" layer="21"/>
<pad name="9" x="8" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="7.746" y1="-0.254" x2="8.254" y2="0.254" layer="51"/>
<pad name="10" x="10" y="0" drill="0.8" shape="octagon" rot="R180"/>
<rectangle x1="9.746" y1="-0.254" x2="10.254" y2="0.254" layer="51"/>
</package>
<package name="JST-PH-10-VS">
<description>&lt;h3&gt;Connector 2mm 10 Pin Vertical SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST B10B-PH-SM4-TB, NXW-10SMD&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 10&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="12.95" y1="1.9" x2="12.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="12.95" y1="-2" x2="12.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="12.95" y1="-3.1" x2="12.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="12.65" y1="-3.1" x2="-10.05" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-10.05" y1="-3.1" x2="-10.25" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-10.25" y1="-3.1" x2="-10.45" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-10.45" y1="-3.1" x2="-10.65" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-10.65" y1="-3.1" x2="-10.95" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-10.95" y1="1.9" x2="12.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-10.95" y1="-3.1" x2="-10.95" y2="-2.8" width="0.2032" layer="51"/>
<text x="1" y="2.15" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<rectangle x1="-8.25" y1="-0.25" x2="-7.75" y2="0.25" layer="51"/>
<smd name="1" x="-8" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$1" x="-10.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="12.4" y="-0.6" dx="3" dy="1.6" layer="1" rot="R90"/>
<wire x1="-10.95" y1="-2.8" x2="-10.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-10.95" y1="-2.6" x2="-10.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-10.95" y1="-2.4" x2="-10.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-10.95" y1="-2.2" x2="-10.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-10.95" y1="-2" x2="-10.95" y2="1.9" width="0.2032" layer="51"/>
<wire x1="12.65" y1="-3.1" x2="12.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-10.25" y1="-3.1" x2="-10.95" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-10.45" y1="-3.1" x2="-10.95" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="-10.65" y1="-3.1" x2="-10.95" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-10.05" y1="-3.1" x2="-10.95" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="12.95" y1="-2" x2="12.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-10.65" y1="-3.1" x2="-10.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-10.65" y1="-2" x2="-10.95" y2="-2" width="0.2032" layer="51"/>
<wire x1="-8" y1="1.6" x2="-8.2" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-8" y1="1.6" x2="-7.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-8.2" y1="2.1" x2="-7.8" y2="2.1" width="0.2032" layer="51"/>
<wire x1="12.95" y1="-2.4" x2="12.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="12.95" y1="-3.1" x2="12.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="12.65" y1="-3.1" x2="10.8" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-10.05" y1="-3.1" x2="-10.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-10.25" y1="-3.1" x2="-10.45" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-10.45" y1="-3.1" x2="-10.65" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-10.65" y1="-3.1" x2="-10.95" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-10.95" y1="1.9" x2="12.95" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-10.95" y1="-3.1" x2="-10.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-10.95" y1="-2.8" x2="-10.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-10.95" y1="-2.6" x2="-10.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="12.65" y1="-3.1" x2="12.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-10.25" y1="-3.1" x2="-10.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-10.45" y1="-3.1" x2="-10.95" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="-10.65" y1="-3.1" x2="-10.95" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-10.05" y1="-3.1" x2="-10.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="12.95" y1="-2.4" x2="12.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-10.65" y1="-3.1" x2="-10.65" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-10.65" y1="-2.4" x2="-10.75" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-10.75" y1="-2.4" x2="-10.95" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-8" y1="1.6" x2="-8.2" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-8" y1="1.6" x2="-7.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="2.1" x2="-7.8" y2="2.1" width="0.2032" layer="21"/>
<smd name="2" x="-6" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-6.25" y1="-0.25" x2="-5.75" y2="0.25" layer="51"/>
<rectangle x1="-4.25" y1="-0.25" x2="-3.75" y2="0.25" layer="51"/>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<text x="1" y="-0.55" size="0.8128" layer="27" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-8.25" y1="-4.1" x2="-7.75" y2="-3.1" layer="51"/>
<rectangle x1="-6.25" y1="-4.1" x2="-5.75" y2="-3.1" layer="51"/>
<rectangle x1="-4.25" y1="-4.1" x2="-3.75" y2="-3.1" layer="51"/>
<rectangle x1="-2.25" y1="-4.1" x2="-1.75" y2="-3.1" layer="51"/>
<wire x1="-10.95" y1="1.9" x2="-10.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="12.95" y1="1.9" x2="12.95" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-8.8" y1="-3.1" x2="-10.05" y2="-3.1" width="0.2032" layer="21"/>
<smd name="5" x="0" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="-0.25" y1="-4.1" x2="0.25" y2="-3.1" layer="51"/>
<smd name="6" x="2" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-4.1" x2="2.25" y2="-3.1" layer="51"/>
<smd name="7" x="4" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="-4.1" x2="4.25" y2="-3.1" layer="51"/>
<smd name="8" x="6" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51"/>
<rectangle x1="5.75" y1="-4.1" x2="6.25" y2="-3.1" layer="51"/>
<smd name="9" x="8" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="7.75" y1="-0.25" x2="8.25" y2="0.25" layer="51"/>
<rectangle x1="7.75" y1="-4.1" x2="8.25" y2="-3.1" layer="51"/>
<smd name="10" x="10" y="-2.85" dx="5.5" dy="1" layer="1" rot="R90"/>
<rectangle x1="9.75" y1="-0.25" x2="10.25" y2="0.25" layer="51"/>
<rectangle x1="9.75" y1="-4.1" x2="10.25" y2="-3.1" layer="51"/>
</package>
<package name="JST-PH-10-HS">
<description>&lt;h3&gt;Connector 2mm 10 Pin Horizontal SMT&lt;/h3&gt;
&lt;p&gt; Alias: JST S10B-PH-SM4-TB, NXW-10SMDK&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 10&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;li&gt;Rated current: 1A&lt;/li&gt;
&lt;li&gt;Rated voltage: 100V&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-10.9" y1="-4" x2="-10.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="2.4" x2="-10.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="2.7" x2="-10.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="2.9" x2="-10.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="3.1" x2="-10.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="3.3" x2="-10.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="3.6" x2="-10.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-10.6" y1="3.6" x2="-10.4" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-10.4" y1="3.6" x2="-10.2" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-10.2" y1="3.6" x2="-10" y2="3.6" width="0.2032" layer="51"/>
<wire x1="-10" y1="3.6" x2="12.6" y2="3.6" width="0.2032" layer="51"/>
<wire x1="12.6" y1="3.6" x2="12.9" y2="3.6" width="0.2032" layer="51"/>
<wire x1="12.9" y1="-4" x2="-7.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-7.5" y1="-4" x2="-8.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-8.5" y1="-4" x2="-10.9" y2="-4" width="0.2032" layer="51"/>
<text x="1" y="5.3" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="1" y="0" size="0.8128" layer="27" ratio="20" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-8" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="P$2" x="12.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<smd name="P$1" x="-10.35" y="-2.5" dx="3.4" dy="1.5" layer="1" rot="R270"/>
<wire x1="12.9" y1="3.6" x2="12.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="12.9" y1="2.4" x2="12.9" y2="-4" width="0.2032" layer="51"/>
<wire x1="-10.6" y1="3.6" x2="-10.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="2.4" x2="-10.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="12.6" y1="3.6" x2="12.6" y2="2.4" width="0.2032" layer="51"/>
<wire x1="12.6" y1="2.4" x2="12.9" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-8" y1="-2.7" x2="-8.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="-7.5" y1="-4" x2="-8" y2="-2.7" width="0.2032" layer="51"/>
<wire x1="-10.6" y1="3.6" x2="-10.9" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-10.4" y1="3.6" x2="-10.9" y2="3.1" width="0.2032" layer="51"/>
<wire x1="-10.2" y1="3.6" x2="-10.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-10" y1="3.6" x2="-10.9" y2="2.7" width="0.2032" layer="51"/>
<wire x1="-10.9" y1="-0.5" x2="-10.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="2.4" x2="-10.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="2.7" x2="-10.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="2.9" x2="-10.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="3.1" x2="-10.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="3.3" x2="-10.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="3.6" x2="-10.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-10.6" y1="3.6" x2="-10.4" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-10.4" y1="3.6" x2="-10.2" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-10.2" y1="3.6" x2="-10" y2="3.6" width="0.2032" layer="21"/>
<wire x1="10.8" y1="3.6" x2="12.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="12.6" y1="3.6" x2="12.9" y2="3.6" width="0.2032" layer="21"/>
<wire x1="11.3" y1="-4" x2="-7.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="-4" x2="-8.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="12.9" y1="3.6" x2="12.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="12.9" y1="2.4" x2="12.9" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-10.6" y1="3.6" x2="-10.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-10.9" y1="2.4" x2="-10.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="12.6" y1="3.6" x2="12.6" y2="2.4" width="0.2032" layer="21"/>
<wire x1="12.6" y1="2.4" x2="12.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-8" y1="-2.7" x2="-8.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="-4" x2="-8" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-10.6" y1="3.6" x2="-10.9" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-10.4" y1="3.6" x2="-10.9" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-10.2" y1="3.6" x2="-10.9" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-10" y1="3.6" x2="-10.9" y2="2.7" width="0.2032" layer="21"/>
<rectangle x1="-8.2" y1="3.6" x2="-7.8" y2="4.6" layer="51"/>
<smd name="2" x="-6" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-6.2" y1="3.6" x2="-5.8" y2="4.6" layer="51"/>
<rectangle x1="-4.2" y1="3.6" x2="-3.8" y2="4.6" layer="51"/>
<rectangle x1="-2.2" y1="3.6" x2="-1.8" y2="4.6" layer="51"/>
<wire x1="-8.5" y1="-4" x2="-9.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-8.8" y1="3.6" x2="-10" y2="3.6" width="0.2032" layer="21"/>
<smd name="5" x="0" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="-0.2" y1="3.6" x2="0.2" y2="4.6" layer="51"/>
<smd name="6" x="2" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="1.8" y1="3.6" x2="2.2" y2="4.6" layer="51"/>
<smd name="7" x="4" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="3.8" y1="3.6" x2="4.2" y2="4.6" layer="51"/>
<smd name="8" x="6" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="5.8" y1="3.6" x2="6.2" y2="4.6" layer="51"/>
<smd name="9" x="8" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="7.8" y1="3.6" x2="8.2" y2="4.6" layer="51"/>
<smd name="10" x="10" y="3.25" dx="3.5" dy="1" layer="1" rot="R270"/>
<rectangle x1="9.8" y1="3.6" x2="10.2" y2="4.6" layer="51"/>
</package>
<package name="SOD-523">
<description>&lt;h3&gt;SMD Diode SOD-523&lt;/h3&gt;
&lt;p&gt;Size: 1.6(1.2) x 0.8 x 0.65 mm&lt;/p&gt;
&lt;p&gt;Approx size: 0603&lt;/p&gt;
&lt;p&gt;(-) cathode side marked&lt;/p&gt;</description>
<smd name="K" x="0.95" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="A" x="-0.95" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-1.524" y="0.762" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.254" y="0" size="0.6096" layer="27" rot="R180" align="center">&gt;VALUE</text>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.0508" layer="39"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-0.635" x2="-1.524" y2="-0.635" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="-0.635" x2="-1.524" y2="0.635" width="0.0254" layer="51"/>
<wire x1="-1.524" y1="0.635" x2="1.524" y2="0.635" width="0.0254" layer="51"/>
<wire x1="1.524" y1="0.635" x2="1.524" y2="-0.635" width="0.0254" layer="51"/>
<rectangle x1="0" y1="-0.381" x2="0.381" y2="0.381" layer="21"/>
<rectangle x1="1.143" y1="-0.635" x2="1.397" y2="0.635" layer="51" rot="R180"/>
</package>
<package name="RGBBTN-22">
<description>&lt;h3&gt;RGB LED Light Push Button - 22mm Metal &lt;/h3&gt;</description>
<text x="-8.15" y="0" size="0.8128" layer="21" rot="R90" align="bottom-center">C</text>
<circle x="0" y="0" radius="11" width="0.0254" layer="21"/>
<smd name="C$TOP" x="-7.2" y="0" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R90"/>
<wire x1="-7.7" y1="1.3" x2="-7.7" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="-7.7" y1="-1.3" x2="-6.7" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="-6.7" y1="-1.3" x2="-6.7" y2="1.3" width="0.0254" layer="44"/>
<wire x1="-6.7" y1="1.3" x2="-7.7" y2="1.3" width="0.0254" layer="44"/>
<smd name="C$BOT" x="-7.2" y="0" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R90"/>
<smd name="B$TOP" x="-5.1" y="-5.1" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R135"/>
<wire x1="-6.372790625" y1="-4.534315625" x2="-4.534315625" y2="-6.372790625" width="0.0254" layer="44"/>
<wire x1="-4.534315625" y1="-6.372790625" x2="-3.827209375" y2="-5.665684375" width="0.0254" layer="44"/>
<wire x1="-3.827209375" y1="-5.665684375" x2="-5.665684375" y2="-3.827209375" width="0.0254" layer="44"/>
<wire x1="-5.665684375" y1="-3.827209375" x2="-6.372790625" y2="-4.534315625" width="0.0254" layer="44"/>
<smd name="B$BOT" x="-5.1" y="-5.1" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R135"/>
<smd name="NO1$TOP" x="0" y="-7.2" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R180"/>
<wire x1="-1.3" y1="-7.7" x2="1.3" y2="-7.7" width="0.0254" layer="44"/>
<wire x1="1.3" y1="-7.7" x2="1.3" y2="-6.7" width="0.0254" layer="44"/>
<wire x1="1.3" y1="-6.7" x2="-1.3" y2="-6.7" width="0.0254" layer="44"/>
<wire x1="-1.3" y1="-6.7" x2="-1.3" y2="-7.7" width="0.0254" layer="44"/>
<smd name="NO1$BOT" x="0" y="-7.2" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R180"/>
<smd name="R$TOP" x="7.2" y="0" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R90"/>
<wire x1="6.7" y1="1.3" x2="6.7" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="6.7" y1="-1.3" x2="7.7" y2="-1.3" width="0.0254" layer="44"/>
<wire x1="7.7" y1="-1.3" x2="7.7" y2="1.3" width="0.0254" layer="44"/>
<wire x1="7.7" y1="1.3" x2="6.7" y2="1.3" width="0.0254" layer="44"/>
<smd name="R$BOT" x="7.2" y="0" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R90"/>
<smd name="G$TOP" x="5.1" y="5.1" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R135"/>
<wire x1="3.827209375" y1="5.665684375" x2="5.665684375" y2="3.827209375" width="0.0254" layer="44"/>
<wire x1="5.665684375" y1="3.827209375" x2="6.372790625" y2="4.534315625" width="0.0254" layer="44"/>
<wire x1="6.372790625" y1="4.534315625" x2="4.534315625" y2="6.372790625" width="0.0254" layer="44"/>
<wire x1="4.534315625" y1="6.372790625" x2="3.827209375" y2="5.665684375" width="0.0254" layer="44"/>
<smd name="G$BOT" x="5.1" y="5.1" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R135"/>
<smd name="NO2$TOP" x="0" y="7.2" dx="3.2" dy="1.6" layer="1" roundness="20" rot="R180"/>
<wire x1="-1.3" y1="6.7" x2="1.3" y2="6.7" width="0.0254" layer="44"/>
<wire x1="1.3" y1="6.7" x2="1.3" y2="7.7" width="0.0254" layer="44"/>
<wire x1="1.3" y1="7.7" x2="-1.3" y2="7.7" width="0.0254" layer="44"/>
<wire x1="-1.3" y1="7.7" x2="-1.3" y2="6.7" width="0.0254" layer="44"/>
<smd name="NO2$BOT" x="0" y="7.2" dx="3.2" dy="1.6" layer="16" roundness="20" rot="R180"/>
<text x="8.15" y="0" size="0.8128" layer="21" rot="R270" align="bottom-center">R</text>
<text x="0" y="-10" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="10" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<text x="0" y="8.15" size="0.8128" layer="21" align="bottom-center">NO</text>
<text x="0" y="-8.15" size="0.8128" layer="21" rot="R180" align="bottom-center">NO</text>
<text x="-5.75" y="-5.75" size="0.8128" layer="21" rot="R135" align="bottom-center">B</text>
<text x="5.75" y="5.75" size="0.8128" layer="21" rot="R315" align="bottom-center">G</text>
<circle x="0" y="0" radius="11" width="0.0254" layer="51"/>
<text x="-9.12" y="0" size="1.27" layer="51" rot="R90" align="center">C</text>
</package>
<package name="LGA-16">
<description>&lt;h3&gt;16 Lead LGA&lt;/h3&gt;
Dimentions: 3 x 3 x 0.75 mm</description>
<wire x1="-1.5" y1="1.5" x2="-1" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.3" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.3" x2="-1.5" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.4" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<smd name="5" x="-1.425" y="-1" dx="0.8" dy="0.3" layer="1"/>
<smd name="4" x="-1.425" y="-0.5" dx="0.8" dy="0.3" layer="1"/>
<smd name="3" x="-1.425" y="0" dx="0.8" dy="0.3" layer="1"/>
<smd name="2" x="-1.425" y="0.5" dx="0.8" dy="0.3" layer="1"/>
<smd name="1" x="-1.425" y="1" dx="0.8" dy="0.3" layer="1"/>
<smd name="16" x="-0.5" y="1.425" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="15" x="0" y="1.425" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="14" x="0.5" y="1.425" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="13" x="1.425" y="1" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="12" x="1.425" y="0.5" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="11" x="1.425" y="0" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="10" x="1.425" y="-0.5" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="9" x="1.425" y="-1" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="8" x="0.5" y="-1.425" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="0" y="-1.425" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-0.5" y="-1.425" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<wire x1="-1" y1="1.3" x2="-1" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1" y1="1.4" x2="-1" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.3" x2="-1" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.4" x2="-1" y2="1.4" width="0.127" layer="21"/>
<circle x="-2" y="2" radius="0.2" width="0.127" layer="51"/>
<text x="0" y="2.54" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LQFP32-7X7">
<description>&lt;b&gt;LQFP-32 package&lt;/b&gt; 7x7 mm case, 0.8 mm lead pitch&lt;/b&gt;&lt;p&gt;
Sourced: &lt;a href="http://www.st.com/stonline/products/literature/ds/14771/stm8s105c4.pdf"&gt; Data sheet &lt;/a&gt;</description>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3" x2="-3" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-3" y1="-3.5" x2="3.5" y2="-3.5" width="0.127" layer="51"/>
<smd name="5" x="0.4" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="6" x="1.2" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="7" x="2" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="8" x="2.8" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="4" x="-0.4" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="3" x="-1.2" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="2" x="-2" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="1" x="-2.8" y="-4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="12" x="4.3" y="-0.4" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="11" x="4.3" y="-1.2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="10" x="4.3" y="-2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="9" x="4.3" y="-2.8" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="13" x="4.3" y="0.4" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="14" x="4.3" y="1.2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="15" x="4.3" y="2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="16" x="4.3" y="2.8" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="17" x="2.8" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="18" x="2" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="19" x="1.2" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="20" x="0.4" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="21" x="-0.4" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="22" x="-1.2" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="23" x="-2" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="24" x="-2.8" y="4.3" dx="0.4" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="25" x="-4.3" y="2.8" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="26" x="-4.3" y="2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="27" x="-4.3" y="1.2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="28" x="-4.3" y="0.4" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="29" x="-4.3" y="-0.4" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="30" x="-4.3" y="-1.2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="31" x="-4.3" y="-2" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<smd name="32" x="-4.3" y="-2.8" dx="1.2" dy="0.4" layer="1" stop="no" cream="no"/>
<text x="0" y="0" size="0.8128" layer="27" align="center">&gt;VALUE</text>
<rectangle x1="-3.2875" y1="-4.1625" x2="-2.3125" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="-3.375" y1="-4.475" x2="-2.225" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="-3.45" y1="-4.55" x2="-2.15" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="-2.4875" y1="-4.1625" x2="-1.5125" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="-2.575" y1="-4.475" x2="-1.425" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="-2.65" y1="-4.55" x2="-1.35" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="-1.6875" y1="-4.1625" x2="-0.7125" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="-1.775" y1="-4.475" x2="-0.625" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="-1.85" y1="-4.55" x2="-0.55" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="-0.8875" y1="-4.1625" x2="0.0875" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="-0.975" y1="-4.475" x2="0.175" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="-1.05" y1="-4.55" x2="0.25" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="-0.0875" y1="-4.1625" x2="0.8875" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="-0.175" y1="-4.475" x2="0.975" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="-0.25" y1="-4.55" x2="1.05" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="0.7125" y1="-4.1625" x2="1.6875" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="0.625" y1="-4.475" x2="1.775" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="0.55" y1="-4.55" x2="1.85" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="1.5125" y1="-4.1625" x2="2.4875" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="1.425" y1="-4.475" x2="2.575" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="1.35" y1="-4.55" x2="2.65" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="2.3125" y1="-4.1625" x2="3.2875" y2="-3.8625" layer="51" rot="R90"/>
<rectangle x1="2.225" y1="-4.475" x2="3.375" y2="-4.125" layer="31" rot="R90"/>
<rectangle x1="2.15" y1="-4.55" x2="3.45" y2="-4.05" layer="29" rot="R90"/>
<rectangle x1="3.525" y1="-2.95" x2="4.5" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="-2.975" x2="4.875" y2="-2.625" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="-3.05" x2="4.95" y2="-2.55" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-2.15" x2="4.5" y2="-1.85" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="-2.175" x2="4.875" y2="-1.825" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="-2.25" x2="4.95" y2="-1.75" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-1.35" x2="4.5" y2="-1.05" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="-1.375" x2="4.875" y2="-1.025" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="-1.45" x2="4.95" y2="-0.95" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="-0.55" x2="4.5" y2="-0.25" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="-0.575" x2="4.875" y2="-0.225" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="-0.65" x2="4.95" y2="-0.15" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="0.25" x2="4.5" y2="0.55" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="0.225" x2="4.875" y2="0.575" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="0.15" x2="4.95" y2="0.65" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="1.05" x2="4.5" y2="1.35" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="1.025" x2="4.875" y2="1.375" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="0.95" x2="4.95" y2="1.45" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="1.85" x2="4.5" y2="2.15" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="1.825" x2="4.875" y2="2.175" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="1.75" x2="4.95" y2="2.25" layer="29" rot="R180"/>
<rectangle x1="3.525" y1="2.65" x2="4.5" y2="2.95" layer="51" rot="R180"/>
<rectangle x1="3.725" y1="2.625" x2="4.875" y2="2.975" layer="31" rot="R180"/>
<rectangle x1="3.65" y1="2.55" x2="4.95" y2="3.05" layer="29" rot="R180"/>
<rectangle x1="2.3125" y1="3.8625" x2="3.2875" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="2.225" y1="4.125" x2="3.375" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="2.15" y1="4.05" x2="3.45" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="1.5125" y1="3.8625" x2="2.4875" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="1.425" y1="4.125" x2="2.575" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="1.35" y1="4.05" x2="2.65" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="0.7125" y1="3.8625" x2="1.6875" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="0.625" y1="4.125" x2="1.775" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="0.55" y1="4.05" x2="1.85" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="-0.0875" y1="3.8625" x2="0.8875" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="-0.175" y1="4.125" x2="0.975" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="-0.25" y1="4.05" x2="1.05" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="-0.8875" y1="3.8625" x2="0.0875" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="-0.975" y1="4.125" x2="0.175" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="-1.05" y1="4.05" x2="0.25" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="-1.6875" y1="3.8625" x2="-0.7125" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="-1.775" y1="4.125" x2="-0.625" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="-1.85" y1="4.05" x2="-0.55" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="-2.4875" y1="3.8625" x2="-1.5125" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="-2.575" y1="4.125" x2="-1.425" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="-2.65" y1="4.05" x2="-1.35" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="-3.2875" y1="3.8625" x2="-2.3125" y2="4.1625" layer="51" rot="R270"/>
<rectangle x1="-3.375" y1="4.125" x2="-2.225" y2="4.475" layer="31" rot="R270"/>
<rectangle x1="-3.45" y1="4.05" x2="-2.15" y2="4.55" layer="29" rot="R270"/>
<rectangle x1="-4.5" y1="2.65" x2="-3.525" y2="2.95" layer="51"/>
<rectangle x1="-4.875" y1="2.625" x2="-3.725" y2="2.975" layer="31"/>
<rectangle x1="-4.95" y1="2.55" x2="-3.65" y2="3.05" layer="29"/>
<rectangle x1="-4.5" y1="1.85" x2="-3.525" y2="2.15" layer="51"/>
<rectangle x1="-4.875" y1="1.825" x2="-3.725" y2="2.175" layer="31"/>
<rectangle x1="-4.95" y1="1.75" x2="-3.65" y2="2.25" layer="29"/>
<rectangle x1="-4.5" y1="1.05" x2="-3.525" y2="1.35" layer="51"/>
<rectangle x1="-4.875" y1="1.025" x2="-3.725" y2="1.375" layer="31"/>
<rectangle x1="-4.95" y1="0.95" x2="-3.65" y2="1.45" layer="29"/>
<rectangle x1="-4.5" y1="0.25" x2="-3.525" y2="0.55" layer="51"/>
<rectangle x1="-4.875" y1="0.225" x2="-3.725" y2="0.575" layer="31"/>
<rectangle x1="-4.95" y1="0.15" x2="-3.65" y2="0.65" layer="29"/>
<rectangle x1="-4.5" y1="-0.55" x2="-3.525" y2="-0.25" layer="51"/>
<rectangle x1="-4.875" y1="-0.575" x2="-3.725" y2="-0.225" layer="31"/>
<rectangle x1="-4.95" y1="-0.65" x2="-3.65" y2="-0.15" layer="29"/>
<rectangle x1="-4.5" y1="-1.35" x2="-3.525" y2="-1.05" layer="51"/>
<rectangle x1="-4.875" y1="-1.375" x2="-3.725" y2="-1.025" layer="31"/>
<rectangle x1="-4.95" y1="-1.45" x2="-3.65" y2="-0.95" layer="29"/>
<rectangle x1="-4.5" y1="-2.15" x2="-3.525" y2="-1.85" layer="51"/>
<rectangle x1="-4.875" y1="-2.175" x2="-3.725" y2="-1.825" layer="31"/>
<rectangle x1="-4.95" y1="-2.25" x2="-3.65" y2="-1.75" layer="29"/>
<rectangle x1="-4.5" y1="-2.95" x2="-3.525" y2="-2.65" layer="51"/>
<rectangle x1="-4.875" y1="-2.975" x2="-3.725" y2="-2.625" layer="31"/>
<rectangle x1="-4.95" y1="-3.05" x2="-3.65" y2="-2.55" layer="29"/>
<text x="0" y="-5.207" size="0.8128" layer="25" rot="R180">&gt;NAME</text>
<wire x1="3.6" y1="-3.6" x2="-3.6" y2="-3.6" width="0.0508" layer="39"/>
<wire x1="-3.6" y1="-3.6" x2="-3.6" y2="3.6" width="0.0508" layer="39"/>
<circle x="-2.571" y="-2.825" radius="0.2499" width="0.254" layer="51"/>
<wire x1="-3.5" y1="-3.2" x2="-3.2" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3" x2="-3.5" y2="-3.1" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.1" x2="-3.5" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.2" x2="-3.5" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.3" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.3" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3.5" x2="-3.2" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.2" y1="-3.5" x2="-3.1" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-3.5" x2="-3" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3" y1="-3.5" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3.5" x2="3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.3" x2="-3.3" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.1" x2="-3.1" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="3.6" width="0.0508" layer="39"/>
<wire x1="3.6" y1="3.6" x2="-3.6" y2="3.6" width="0.0508" layer="39"/>
</package>
<package name="RELAY-ISO-MINI-5">
<description>&lt;h3&gt;HIGH POWER AUTOMOTIVE RELAY&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Socket: 5 pins, H=14mm&lt;/li&gt;
&lt;li&gt;Case: 27mm x 27mm x 25mm&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-14" y1="-12" x2="-5" y2="-12" width="0.2" layer="21"/>
<pad name="86" x="-8.4" y="0" drill="2.3" shape="square" rot="R90" first="yes"/>
<pad name="85" x="8.4" y="0" drill="2.3" shape="square" rot="R90"/>
<text x="-13.208" y="-11.43" size="0.8128" layer="25">&gt;NAME</text>
<text x="13.208" y="-11.43" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
<wire x1="-5" y1="-12" x2="5" y2="-12" width="0.2" layer="21"/>
<wire x1="5" y1="-12" x2="14" y2="-12" width="0.2" layer="21"/>
<wire x1="-14" y1="-12" x2="-14" y2="-5" width="0.2032" layer="21"/>
<wire x1="-14" y1="-5" x2="-14" y2="5" width="0.2032" layer="21"/>
<wire x1="-14" y1="5" x2="-14" y2="16" width="0.2032" layer="21"/>
<wire x1="14" y1="-12" x2="14" y2="-5" width="0.2032" layer="21"/>
<wire x1="14" y1="-5" x2="14" y2="5" width="0.2032" layer="21"/>
<wire x1="14" y1="5" x2="14" y2="16" width="0.2032" layer="21"/>
<wire x1="-14" y1="16" x2="14" y2="16" width="0.2" layer="21"/>
<wire x1="-14" y1="-12" x2="14" y2="-12" width="0.2" layer="51"/>
<wire x1="-14" y1="-12" x2="-14" y2="16" width="0.2032" layer="51"/>
<wire x1="14" y1="-12" x2="14" y2="16" width="0.2032" layer="51"/>
<wire x1="-14" y1="16" x2="14" y2="16" width="0.2" layer="51"/>
<wire x1="-14.2" y1="-12.2" x2="14.2" y2="-12.2" width="0.127" layer="39"/>
<wire x1="14.2" y1="-12.2" x2="14.2" y2="16.2" width="0.127" layer="39"/>
<wire x1="14.2" y1="16.2" x2="-14.2" y2="16.2" width="0.127" layer="39"/>
<wire x1="-14.2" y1="16.2" x2="-14.2" y2="-12.2" width="0.127" layer="39"/>
<pad name="87A" x="0" y="-0.4" drill="2.3" shape="square" rot="R90"/>
<pad name="87" x="0" y="-8.4" drill="2.3" shape="square" rot="R90"/>
<pad name="30" x="0" y="9.5" drill="2.3" shape="square" rot="R90"/>
<wire x1="5" y1="-12" x2="5" y2="-5" width="0.2" layer="21"/>
<wire x1="-5" y1="-12" x2="-5" y2="-5" width="0.2" layer="21"/>
<wire x1="14" y1="5" x2="5" y2="5" width="0.2" layer="21"/>
<wire x1="14" y1="-5" x2="5" y2="-5" width="0.2" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="14" width="0.2" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="14" width="0.2" layer="21"/>
<wire x1="-5" y1="-5" x2="-14" y2="-5" width="0.2" layer="21"/>
<wire x1="-5" y1="5" x2="-14" y2="5" width="0.2" layer="21"/>
<wire x1="5" y1="14" x2="-5" y2="14" width="0.2" layer="21"/>
<wire x1="-14" y1="-5" x2="-5" y2="-12" width="0.2" layer="21"/>
<wire x1="5" y1="14" x2="14" y2="5" width="0.2" layer="21"/>
</package>
<package name="RELAY-ISO-MICRO-5">
<description>&lt;h3&gt;AUTOMOTIVE MICRO-ISO RELAY SOCKET&lt;/h3&gt;</description>
<wire x1="-12.5" y1="8" x2="12.5" y2="8" width="0.2032" layer="21"/>
<wire x1="12.5" y1="8" x2="12.5" y2="-8" width="0.2032" layer="21"/>
<wire x1="12.5" y1="-8" x2="-12.5" y2="-8" width="0.2032" layer="21"/>
<wire x1="-12.5" y1="-8" x2="-12.5" y2="8" width="0.2032" layer="21"/>
<pad name="5" x="0" y="0" drill="2.3" shape="square" rot="R180"/>
<pad name="3" x="-8" y="0" drill="2.3" shape="square" rot="R180"/>
<pad name="4" x="6" y="0.3" drill="2.3" shape="square" rot="R180"/>
<pad name="2" x="6" y="4.8" drill="2.3" shape="square" rot="R180"/>
<pad name="1" x="6" y="-4.2" drill="2.3" shape="square" rot="R180"/>
<text x="-12.7" y="7.62" size="0.8128" layer="25" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-12.7" y="-7.62" size="0.8128" layer="27" rot="R90">&gt;VALUE</text>
<wire x1="-12.5" y1="8" x2="12.5" y2="8" width="0.2032" layer="51"/>
<wire x1="-12.5" y1="-8" x2="12.5" y2="-8" width="0.2032" layer="51"/>
<wire x1="12.5" y1="8" x2="12.5" y2="-8" width="0.2032" layer="51"/>
<wire x1="-12.5" y1="8" x2="-12.5" y2="-8" width="0.2032" layer="51"/>
</package>
<package name="RELAY-ISO-MINI-4">
<description>&lt;h3&gt;HIGH POWER AUTOMOTIVE RELAY&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Socket: 5 pins, H=14mm&lt;/li&gt;
&lt;li&gt;Case: 27mm x 27mm x 25mm&lt;/li&gt;
&lt;/ul&gt;</description>
<wire x1="-14" y1="-12" x2="-5" y2="-12" width="0.2" layer="21"/>
<pad name="86" x="-8.4" y="0" drill="2.3" shape="square" rot="R90" first="yes"/>
<pad name="85" x="8.4" y="0" drill="2.3" shape="square" rot="R90"/>
<text x="-13.208" y="-11.43" size="0.8128" layer="25">&gt;NAME</text>
<text x="13.208" y="-11.43" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
<wire x1="-5" y1="-12" x2="5" y2="-12" width="0.2" layer="21"/>
<wire x1="5" y1="-12" x2="14" y2="-12" width="0.2" layer="21"/>
<wire x1="-14" y1="-12" x2="-14" y2="-5" width="0.2032" layer="21"/>
<wire x1="-14" y1="-5" x2="-14" y2="5" width="0.2032" layer="21"/>
<wire x1="-14" y1="5" x2="-14" y2="16" width="0.2032" layer="21"/>
<wire x1="14" y1="-12" x2="14" y2="-5" width="0.2032" layer="21"/>
<wire x1="14" y1="-5" x2="14" y2="5" width="0.2032" layer="21"/>
<wire x1="14" y1="5" x2="14" y2="16" width="0.2032" layer="21"/>
<wire x1="-14" y1="16" x2="14" y2="16" width="0.2" layer="21"/>
<wire x1="-14" y1="-12" x2="14" y2="-12" width="0.2" layer="51"/>
<wire x1="-14" y1="-12" x2="-14" y2="16" width="0.2032" layer="51"/>
<wire x1="14" y1="-12" x2="14" y2="16" width="0.2032" layer="51"/>
<wire x1="-14" y1="16" x2="14" y2="16" width="0.2" layer="51"/>
<wire x1="-14.2" y1="-12.2" x2="14.2" y2="-12.2" width="0.127" layer="39"/>
<wire x1="14.2" y1="-12.2" x2="14.2" y2="16.2" width="0.127" layer="39"/>
<wire x1="14.2" y1="16.2" x2="-14.2" y2="16.2" width="0.127" layer="39"/>
<wire x1="-14.2" y1="16.2" x2="-14.2" y2="-12.2" width="0.127" layer="39"/>
<pad name="87" x="0" y="-8.4" drill="2.3" shape="square" rot="R90"/>
<pad name="30" x="0" y="9.5" drill="2.3" shape="square" rot="R90"/>
<wire x1="5" y1="-12" x2="5" y2="-5" width="0.2" layer="21"/>
<wire x1="-5" y1="-12" x2="-5" y2="-5" width="0.2" layer="21"/>
<wire x1="14" y1="5" x2="5" y2="5" width="0.2" layer="21"/>
<wire x1="14" y1="-5" x2="5" y2="-5" width="0.2" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="14" width="0.2" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="14" width="0.2" layer="21"/>
<wire x1="-5" y1="-5" x2="-14" y2="-5" width="0.2" layer="21"/>
<wire x1="-5" y1="5" x2="-14" y2="5" width="0.2" layer="21"/>
<wire x1="5" y1="14" x2="-5" y2="14" width="0.2" layer="21"/>
<wire x1="-14" y1="-5" x2="-5" y2="-12" width="0.2" layer="21"/>
<wire x1="5" y1="14" x2="14" y2="5" width="0.2" layer="21"/>
</package>
<package name="RELAY-ISO-MICRO-4">
<description>&lt;h3&gt;AUTOMOTIVE MICRO-ISO RELAY SOCKET&lt;/h3&gt;</description>
<wire x1="-12.5" y1="8" x2="12.5" y2="8" width="0.2032" layer="21"/>
<wire x1="12.5" y1="8" x2="12.5" y2="-8" width="0.2032" layer="21"/>
<wire x1="12.5" y1="-8" x2="-12.5" y2="-8" width="0.2032" layer="21"/>
<wire x1="-12.5" y1="-8" x2="-12.5" y2="8" width="0.2032" layer="21"/>
<pad name="5" x="0" y="0" drill="2.3" shape="square" rot="R180"/>
<pad name="3" x="-8" y="0" drill="2.3" shape="square" rot="R180"/>
<pad name="2" x="6" y="4.8" drill="2.3" shape="square" rot="R180"/>
<pad name="1" x="6" y="-4.2" drill="2.3" shape="square" rot="R180"/>
<text x="-12.7" y="7.62" size="0.8128" layer="25" rot="R90" align="bottom-right">&gt;NAME</text>
<text x="-12.7" y="-7.62" size="0.8128" layer="27" rot="R90">&gt;VALUE</text>
<wire x1="-12.5" y1="8" x2="12.5" y2="8" width="0.2032" layer="51"/>
<wire x1="-12.5" y1="-8" x2="12.5" y2="-8" width="0.2032" layer="51"/>
<wire x1="12.5" y1="8" x2="12.5" y2="-8" width="0.2032" layer="51"/>
<wire x1="-12.5" y1="8" x2="-12.5" y2="-8" width="0.2032" layer="51"/>
</package>
<package name="FUSE-AUTO">
<description>&lt;h3&gt;FUSE HOLDER AUTOMOTIVE&lt;/h3&gt;&lt;p&gt;
Fuse size - 19mm (Regular, APR, ATC, ATO, ATS) blade-type</description>
<wire x1="-10" y1="-3" x2="10" y2="-3" width="0.1524" layer="21"/>
<wire x1="-6" y1="0" x2="6" y2="0" width="0.1524" layer="51"/>
<wire x1="-3" y1="1" x2="-3" y2="-1" width="0.1524" layer="51"/>
<wire x1="-3" y1="-1" x2="3" y2="-1" width="0.1524" layer="51"/>
<wire x1="3" y1="1" x2="3" y2="-1" width="0.1524" layer="51"/>
<wire x1="3" y1="1" x2="-3" y2="1" width="0.1524" layer="51"/>
<pad name="1A" x="-6.3" y="-1.3" drill="1.3" shape="square" rot="R90"/>
<text x="10.668" y="0" size="1.27" layer="25" ratio="10" rot="R90" align="top-center">&gt;NAME</text>
<text x="-10.668" y="0" size="1.27" layer="27" ratio="10" rot="R270" align="top-center">&gt;VALUE</text>
<wire x1="-10" y1="3" x2="10" y2="3" width="0.1524" layer="21"/>
<wire x1="-10" y1="3" x2="-10" y2="-3" width="0.1524" layer="21"/>
<wire x1="10" y1="3" x2="10" y2="-3" width="0.1524" layer="21"/>
<hole x="0" y="0" drill="2.8"/>
<pad name="1B" x="-2.8" y="-1.3" drill="1.3" shape="square" rot="R90"/>
<pad name="1C" x="-6.3" y="1.3" drill="1.3" shape="square" rot="R90"/>
<pad name="1D" x="-2.8" y="1.3" drill="1.3" shape="square" rot="R90"/>
<pad name="2A" x="6.3" y="-1.3" drill="1.3" shape="square" rot="R90"/>
<pad name="2B" x="2.8" y="-1.3" drill="1.3" shape="square" rot="R90"/>
<pad name="2D" x="2.8" y="1.3" drill="1.3" shape="square" rot="R90"/>
<pad name="2C" x="6.3" y="1.3" drill="1.3" shape="square" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="AGND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="-2.667" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="AVDD">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<pin name="AVDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="C">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CPOL">
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="1.143" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-4.5974" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="OPAMP">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PWR+-">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="1.27" y="3.175" size="0.8128" layer="93" rot="R90">V+</text>
<text x="1.27" y="-4.445" size="0.8128" layer="93" rot="R90">V-</text>
</symbol>
<symbol name="MICRO_SD">
<description>&lt;b&gt;MicroSD memory card socket&lt;/b&gt;</description>
<wire x1="-2.54" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="-22.86" x2="-2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-22.86" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<pin name="DTA2" x="-7.62" y="12.7" length="middle"/>
<pin name="CD/DAT3" x="-7.62" y="10.16" length="middle"/>
<pin name="CMD" x="-7.62" y="7.62" length="middle"/>
<pin name="VDD" x="-7.62" y="5.08" length="middle"/>
<pin name="CLK" x="-7.62" y="2.54" length="middle"/>
<pin name="VSS" x="-7.62" y="0" length="middle"/>
<pin name="DAT0" x="-7.62" y="-2.54" length="middle"/>
<pin name="DAT1" x="-7.62" y="-5.08" length="middle"/>
<pin name="CD" x="-7.62" y="-12.7" length="middle"/>
<pin name="SHIELD@" x="-7.62" y="-20.32" visible="pad" length="middle"/>
<pin name="SHIELD" x="-7.62" y="-17.78" length="middle"/>
<pin name="GND" x="-7.62" y="-10.16" length="middle"/>
<text x="-1.27" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.27" y="-24.13" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
</symbol>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="1.524" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="STM32F042F">
<description>&lt;h3&gt;STM32F032G - 32-bit MCU, TSSOP20 (6.5*4.4mm)&lt;/h3&gt;</description>
<wire x1="-25.4" y1="22.86" x2="27.94" y2="22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="-22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="-22.86" x2="-25.4" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-22.86" x2="-25.4" y2="22.86" width="0.254" layer="94"/>
<text x="-25.4" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<text x="-25.4" y="24.13" size="1.778" layer="95">&gt;NAME</text>
<pin name="AVCC" x="-30.48" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-30.48" y="-5.08" length="middle" direction="pwr"/>
<pin name="!NRST!" x="-30.48" y="20.32" length="middle" function="dot"/>
<pin name="VCC" x="-30.48" y="10.16" length="middle" direction="pwr"/>
<pin name="PF0/XTAL/SDA1" x="-30.48" y="-12.7" length="middle"/>
<pin name="PF1/XTAL/SCL1" x="-30.48" y="-15.24" length="middle"/>
<pin name="BOOT0/PB8/T16.1" x="-30.48" y="17.78" length="middle" function="dot"/>
<pin name="T3.4/T14.1/T1.3N/ADC9/PB1" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="WKUP/T2.E/ADC0/PA0" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="T2.2/ADC1/PA1" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="WKUP/TX2/T2.3/ADC2/PA2" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="RX2/T2.4/ADC3/PA3" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="USBNOE/NSS1/T14.1/ADC4/PA4" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="SCK1/T2.E/ADC5/PA5" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="TX1/SCL1/T1.2/PA9" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="RX1/SDA1/T1.3/PA10" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="USBNOE/SWDIO/PA13" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="SWCLK/TX2/PA14" x="33.02" y="20.32" length="middle" rot="R180"/>
<text x="25.4" y="15.24" size="1.524" layer="95" rot="R180">USBDP/SDA1/T1.E/PA12</text>
<text x="25.4" y="10.16" size="1.524" layer="95" rot="R180">USBDM/SCL1/T1.4/PA11</text>
</symbol>
<symbol name="STM32F405RG">
<wire x1="-25.4" y1="53.34" x2="27.94" y2="53.34" width="0.254" layer="94"/>
<wire x1="27.94" y1="53.34" x2="27.94" y2="-58.42" width="0.254" layer="94"/>
<wire x1="27.94" y1="-58.42" x2="-25.4" y2="-58.42" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-58.42" x2="-25.4" y2="53.34" width="0.254" layer="94"/>
<text x="-25.4" y="-60.96" size="1.778" layer="96">&gt;VALUE</text>
<text x="-25.4" y="54.61" size="1.778" layer="95">&gt;NAME</text>
<text x="-23.0187" y="4.6038" size="1.524" layer="95">GND</text>
<text x="-23.0187" y="16.9863" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="14.4463" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="11.9063" size="1.524" layer="95">VCC</text>
<pin name="(ADC8)PB0" x="33.02" y="-45.72" length="middle" rot="R180"/>
<pin name="AGND" x="-30.48" y="38.1" length="middle" direction="pwr"/>
<pin name="AVCC" x="-30.48" y="40.64" length="middle" direction="pwr"/>
<pin name="GND" x="-30.48" y="7.62" length="middle" direction="pwr"/>
<pin name="GND1" x="-30.48" y="5.08" visible="pad" length="middle" direction="pwr"/>
<pin name="PC0(ADC10)" x="-30.48" y="-48.26" length="middle"/>
<pin name="PC1(ADC11)" x="-30.48" y="-45.72" length="middle"/>
<pin name="PC2(ADC12/MISO2)" x="-30.48" y="-43.18" length="middle"/>
<pin name="PC3(ADC13/MOSI2/SD2)" x="-30.48" y="-40.64" length="middle"/>
<pin name="PC4(ADC14)" x="-30.48" y="-38.1" length="middle"/>
<pin name="PC5(ADC15)" x="-30.48" y="-35.56" length="middle"/>
<pin name="PC6(TX6)" x="-30.48" y="-33.02" length="middle"/>
<pin name="PC7(RX6)" x="-30.48" y="-30.48" length="middle"/>
<pin name="PC8" x="-30.48" y="-27.94" length="middle"/>
<pin name="PC9(SDA3)" x="-30.48" y="-25.4" length="middle"/>
<pin name="PC10(SCK3/TX4/TX3)" x="-30.48" y="-22.86" length="middle"/>
<pin name="PC11(MISO3/RX4/RX3)" x="-30.48" y="-20.32" length="middle"/>
<pin name="PC12(MOSI3/TX5)" x="-30.48" y="-17.78" length="middle"/>
<pin name="PC13(RTC_AF1)" x="-30.48" y="-15.24" length="middle"/>
<pin name="!NRST!" x="-30.48" y="48.26" length="middle" function="dot"/>
<pin name="VBAT" x="-30.48" y="25.4" length="middle" direction="pwr"/>
<pin name="VCC" x="-30.48" y="20.32" length="middle" direction="pwr"/>
<pin name="VCC1" x="-30.48" y="17.78" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC3" x="-30.48" y="15.24" visible="pad" length="middle" direction="pwr"/>
<pin name="XTAL/PH0" x="-30.48" y="0" length="middle"/>
<pin name="XTAL/PH1" x="-30.48" y="-2.54" length="middle"/>
<pin name="VCAP1" x="-30.48" y="33.02" length="middle" direction="pwr"/>
<pin name="VCAP2" x="-30.48" y="30.48" length="middle" direction="pwr"/>
<pin name="VCC2" x="-30.48" y="12.7" visible="pad" length="middle" direction="pwr"/>
<pin name="BOOT0" x="-30.48" y="45.72" length="middle" function="dot"/>
<pin name="XTAL32/PC15" x="-30.48" y="-7.62" length="middle"/>
<pin name="XTAL32/PC14" x="-30.48" y="-10.16" length="middle"/>
<pin name="(ADC9)PB1" x="33.02" y="-43.18" length="middle" rot="R180"/>
<pin name="(BOOT1)PB2" x="33.02" y="-40.64" length="middle" rot="R180"/>
<pin name="(TRACESWO/SCK1,3)PB3" x="33.02" y="-38.1" length="middle" rot="R180"/>
<pin name="(NJTRST/MISO1,3)PB4" x="33.02" y="-35.56" length="middle" rot="R180"/>
<pin name="(SMBA1/CANRX2/MOSI1,3)PB5" x="33.02" y="-33.02" length="middle" rot="R180"/>
<pin name="(SCL1/CAN2TC/TX1)PB6" x="33.02" y="-30.48" length="middle" rot="R180"/>
<pin name="(SDA1/RX1)PB7" x="33.02" y="-27.94" length="middle" rot="R180"/>
<pin name="(SCL1/CANRX1)PB8" x="33.02" y="-25.4" length="middle" rot="R180"/>
<pin name="(NSS2/SDA1/CANTX1)PB9" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="(SCK2/SCL2/TX3)PB10" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="(SDA2/RX3)PB11" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="(NSS2/SMBA2/CANRX2)PB12" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="(SCK2/CANTX2)PB13" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="(MISO2)PB14" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="(MOSI2)PB15" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="(WKUP/TX4/ADC0)PA0" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="(RX4/ADC1)PA1" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="(TX2/ADC2)PA2" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="(RX2/ADC3)PA3" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="(NSS1,3/WS3/ADC4)PA4" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="(SCK1/DAC2/ADC5)PA5" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="(MISO1/ADC6)PA6" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="(MOSI1/ADC7)PA7" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="(MCO1/SCL3)PA8" x="33.02" y="25.4" length="middle" rot="R180"/>
<pin name="(TX1/SMBA3)PA9" x="33.02" y="27.94" length="middle" rot="R180"/>
<pin name="(USBID/RX1)PA10" x="33.02" y="30.48" length="middle" rot="R180"/>
<pin name="(USBDM/CANRX1)PA11" x="33.02" y="33.02" length="middle" rot="R180"/>
<pin name="(USBDP/CANTX1)PA12" x="33.02" y="35.56" length="middle" rot="R180"/>
<pin name="(JT-SWD)PA13" x="33.02" y="38.1" length="middle" rot="R180"/>
<pin name="(SWCLK)PA14" x="33.02" y="40.64" length="middle" rot="R180"/>
<pin name="(NSS1,3/WS3)PA15" x="33.02" y="43.18" length="middle" rot="R180"/>
<pin name="PD2(RX5)" x="-30.48" y="-53.34" length="middle"/>
</symbol>
<symbol name="STM32F405VG">
<wire x1="-25.4" y1="81.28" x2="27.94" y2="81.28" width="0.254" layer="94"/>
<wire x1="27.94" y1="81.28" x2="27.94" y2="-71.12" width="0.254" layer="94"/>
<wire x1="27.94" y1="-71.12" x2="-25.4" y2="-71.12" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-71.12" x2="-25.4" y2="81.28" width="0.254" layer="94"/>
<text x="-22.86" y="-73.66" size="1.778" layer="96">&gt;VALUE</text>
<text x="-25.4" y="82.55" size="1.778" layer="95">&gt;NAME</text>
<text x="-23.0187" y="35.0838" size="1.524" layer="95">GND</text>
<text x="-23.0187" y="50.0063" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="47.4663" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="44.9263" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="42.3863" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="39.8463" size="1.524" layer="95">VCC</text>
<text x="-23.0187" y="32.5438" size="1.524" layer="95">GND</text>
<text x="-23.0187" y="30.0038" size="1.524" layer="95">GND</text>
<pin name="(T3.3/T8.2N/T1.2N/ADC8)PB0" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="AGND" x="-30.48" y="66.04" length="middle" direction="pwr"/>
<pin name="AVCC" x="-30.48" y="68.58" length="middle" direction="pwr"/>
<pin name="GND" x="-30.48" y="38.1" length="middle" direction="pwr"/>
<pin name="GND@1" x="-30.48" y="35.56" visible="pad" length="middle" direction="pwr"/>
<pin name="PC0(ADC10)" x="-30.48" y="-20.32" length="middle"/>
<pin name="PC1(ADC11)" x="-30.48" y="-17.78" length="middle"/>
<pin name="PC2(ADC12/MISO2)" x="-30.48" y="-15.24" length="middle"/>
<pin name="PC3(ADC13/MOSI2/SD2)" x="-30.48" y="-12.7" length="middle"/>
<pin name="PC4(ADC14)" x="-30.48" y="-10.16" length="middle"/>
<pin name="PC5(ADC15)" x="-30.48" y="-7.62" length="middle"/>
<pin name="PC6(TX6/T8.1/T3.1)" x="-30.48" y="-5.08" length="middle"/>
<pin name="PC7(RX6/T8.2/T3.2)" x="-30.48" y="-2.54" length="middle"/>
<pin name="PC8(T8.3/T3.3)" x="-30.48" y="0" length="middle"/>
<pin name="PC9(SDA3/T8.4/T3.4)" x="-30.48" y="2.54" length="middle"/>
<pin name="PC10(SCK3/TX4/TX3)" x="-30.48" y="5.08" length="middle"/>
<pin name="PC11(MISO3/RX4/RX3)" x="-30.48" y="7.62" length="middle"/>
<pin name="PC12(MOSI3/TX5)" x="-30.48" y="10.16" length="middle"/>
<pin name="PC13(RTC_AF1)" x="-30.48" y="12.7" length="middle"/>
<pin name="!NRST!" x="-30.48" y="76.2" length="middle" function="dot"/>
<pin name="VBAT" x="-30.48" y="55.88" length="middle" direction="pwr"/>
<pin name="VCC" x="-30.48" y="53.34" length="middle" direction="pwr"/>
<pin name="VCC@1" x="-30.48" y="50.8" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC@2" x="-30.48" y="48.26" visible="pad" length="middle" direction="pwr"/>
<pin name="XTAL/PH0" x="-30.48" y="27.94" length="middle"/>
<pin name="XTAL/PH1" x="-30.48" y="25.4" length="middle"/>
<pin name="VCAP1" x="-30.48" y="60.96" length="middle" direction="pwr"/>
<pin name="VCAP2" x="-30.48" y="58.42" length="middle" direction="pwr"/>
<pin name="VCC@3" x="-30.48" y="45.72" visible="pad" length="middle" direction="pwr"/>
<pin name="BOOT0" x="-30.48" y="73.66" length="middle" function="dot"/>
<pin name="XTAL32/PC15" x="-30.48" y="20.32" length="middle"/>
<pin name="XTAL32/PC14" x="-30.48" y="17.78" length="middle"/>
<pin name="(T3.4/T8.3N/T1.3N/ADC9)PB1" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="(BOOT1)PB2" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="(TRACESWO/SCK1,3/T2.2)PB3" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="(NJTRST/MISO1,3/T3.1)PB4" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="(SMBA1/CRX2/MOSI1,3/T3.2)PB5" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="(SCL1/CTX2/TX1/T4.1)PB6" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="(SDA1/RX1/T4.2)PB7" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="(SCL1/CRX1/T4.3/T10.1)PB8" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="(NSS2/SDA1/CTX1/T4.4/T11.1)PB9" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="(SCK2/SCL2/TX3/T2.3)PB10" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="(SDA2/RX3/T2.4)PB11" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="(NSS2/SMBA2/CRX2/T1.B)PB12" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="(SCK2/CTX2/T1.1N)PB13" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="(MISO2/T1.2N/T12.1/T8.2N)PB14" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="(MOSI2/T1.3N/T8.3N/T12.2)PB15" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="(WKUP/TX4/T2.1/T5.1/T8.E/ADC0)PA0" x="33.02" y="33.02" length="middle" rot="R180"/>
<pin name="(RX4/T5.2/T2.2/ADC1)PA1" x="33.02" y="35.56" length="middle" rot="R180"/>
<pin name="(TX2/T5.3/T9.1/T2.3/ADC2)PA2" x="33.02" y="38.1" length="middle" rot="R180"/>
<pin name="(RX2/T5.4/T9.2/T2.4/ADC3)PA3" x="33.02" y="40.64" length="middle" rot="R180"/>
<pin name="(NSS1,3/WS3/DAC1/ADC4)PA4" x="33.02" y="43.18" length="middle" rot="R180"/>
<pin name="(SCK1/DAC2/T2.E/T8.C/DAC2)ADC5)PA5" x="33.02" y="45.72" length="middle" rot="R180"/>
<pin name="(MISO1/T8.B/T13.1/T3.1/T1.B/ADC6)PA6" x="33.02" y="48.26" length="middle" rot="R180"/>
<pin name="(MOSI1/T8.1N/T14.1/T3.2/T1.1N/ADC7)PA7" x="33.02" y="50.8" length="middle" rot="R180"/>
<pin name="(MCO1/SCL3/T1.1)PA8" x="33.02" y="53.34" length="middle" rot="R180"/>
<pin name="(USBVBUS/TX1/SMBA3/T1.2)PA9" x="33.02" y="55.88" length="middle" rot="R180"/>
<pin name="(USBID/RX1/T1.3)PA10" x="33.02" y="58.42" length="middle" rot="R180"/>
<pin name="(USBDM/CRX1/T1.4)PA11" x="33.02" y="60.96" length="middle" rot="R180"/>
<pin name="(USBDP/CTX1/T1.E)PA12" x="33.02" y="63.5" length="middle" rot="R180"/>
<pin name="(JT-SWD)PA13" x="33.02" y="66.04" length="middle" rot="R180"/>
<pin name="(SWCLK)PA14" x="33.02" y="68.58" length="middle" rot="R180"/>
<pin name="(NSS1,3/WS3/T2.1E)PA15" x="33.02" y="71.12" length="middle" rot="R180"/>
<pin name="PD2(RX5/T3.E)" x="-30.48" y="-60.96" length="middle"/>
<pin name="PE1" x="33.02" y="-63.5" length="middle" rot="R180"/>
<pin name="PE2" x="33.02" y="-60.96" length="middle" rot="R180"/>
<pin name="PE3" x="33.02" y="-58.42" length="middle" rot="R180"/>
<pin name="PE4" x="33.02" y="-55.88" length="middle" rot="R180"/>
<pin name="(T9.1)PE5" x="33.02" y="-53.34" length="middle" rot="R180"/>
<pin name="(T9.2)PE6" x="33.02" y="-50.8" length="middle" rot="R180"/>
<pin name="(T1.E)PE7" x="33.02" y="-48.26" length="middle" rot="R180"/>
<pin name="VREF+" x="-30.48" y="63.5" length="middle" direction="pwr"/>
<pin name="VCC@4" x="-30.48" y="43.18" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC@5" x="-30.48" y="40.64" visible="pad" length="middle" direction="pwr"/>
<pin name="GND@2" x="-30.48" y="33.02" visible="pad" length="middle" direction="pwr"/>
<pin name="(T1.1N)PE8" x="33.02" y="-45.72" length="middle" rot="R180"/>
<pin name="(T1.1)PE9" x="33.02" y="-43.18" length="middle" rot="R180"/>
<pin name="(T1.2N)PE10" x="33.02" y="-40.64" length="middle" rot="R180"/>
<pin name="(T1.2)PE11" x="33.02" y="-38.1" length="middle" rot="R180"/>
<pin name="(T1.3N)PE12" x="33.02" y="-35.56" length="middle" rot="R180"/>
<pin name="(T1.3)PE13" x="33.02" y="-33.02" length="middle" rot="R180"/>
<pin name="(T1.4)PE14" x="33.02" y="-30.48" length="middle" rot="R180"/>
<pin name="(T1.B)PE15" x="33.02" y="-27.94" length="middle" rot="R180"/>
<pin name="PD1(CTX1)" x="-30.48" y="-63.5" length="middle"/>
<pin name="PD0(CRX1)" x="-30.48" y="-66.04" length="middle"/>
<pin name="PD3" x="-30.48" y="-58.42" length="middle"/>
<pin name="PD4" x="-30.48" y="-55.88" length="middle"/>
<pin name="PD5(TX2)" x="-30.48" y="-53.34" length="middle"/>
<pin name="PD6(RX2)" x="-30.48" y="-50.8" length="middle"/>
<pin name="PD7" x="-30.48" y="-48.26" length="middle"/>
<pin name="PD8(TX3)" x="-30.48" y="-45.72" length="middle"/>
<pin name="PD9(RX3)" x="-30.48" y="-43.18" length="middle"/>
<pin name="PD10" x="-30.48" y="-40.64" length="middle"/>
<pin name="PD11" x="-30.48" y="-38.1" length="middle"/>
<pin name="PD12(T4.1)" x="-30.48" y="-35.56" length="middle"/>
<pin name="PD13(T4.2)" x="-30.48" y="-33.02" length="middle"/>
<pin name="PD14(T4.3)" x="-30.48" y="-30.48" length="middle"/>
<pin name="PD15(T4.4)" x="-30.48" y="-27.94" length="middle"/>
<pin name="(T4.E)PE0" x="33.02" y="-66.04" length="middle" rot="R180"/>
<pin name="GND@3" x="-30.48" y="30.48" visible="pad" length="middle" direction="pwr"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="STM32F030F">
<wire x1="-25.4" y1="22.86" x2="27.94" y2="22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="-22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="-22.86" x2="-25.4" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-22.86" x2="-25.4" y2="22.86" width="0.254" layer="94"/>
<text x="-25.4" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<text x="-25.4" y="24.13" size="1.778" layer="95">&gt;NAME</text>
<pin name="AVCC" x="-30.48" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-30.48" y="-5.08" length="middle" direction="pwr"/>
<pin name="!NRST!" x="-30.48" y="20.32" length="middle" function="dot"/>
<pin name="VCC" x="-30.48" y="10.16" length="middle" direction="pwr"/>
<pin name="PF0/XTAL" x="-30.48" y="-12.7" length="middle"/>
<pin name="PF1/XTAL" x="-30.48" y="-15.24" length="middle"/>
<pin name="BOOT0" x="-30.48" y="17.78" length="middle" function="dot"/>
<pin name="T3.4/T14.1/T1.3N/ADC9/PB1" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="WKUP/ADC0/PA0" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="ADC1/PA1" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="TX1/ADC2/PA2" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="RX1/ADC3/PA3" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="NSS1/T14.1/ADC4/PA4" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="SCK1/ADC5/PA5" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="TX1/SCL1/T1.2/PA9" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="RX1/SDA1/T1.3/PA10" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="SWDIO/PA13" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="SWCLK/TX1/PA14" x="33.02" y="20.32" length="middle" rot="R180"/>
</symbol>
<symbol name="2001A">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I1" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="I2" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="I3" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="I4" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="I5" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="I6" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="I7" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="O1" x="12.7" y="7.62" length="middle" direction="oc" rot="R180"/>
<pin name="O2" x="12.7" y="5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O3" x="12.7" y="2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O4" x="12.7" y="0" length="middle" direction="oc" rot="R180"/>
<pin name="O5" x="12.7" y="-2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O6" x="12.7" y="-5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O7" x="12.7" y="-7.62" length="middle" direction="oc" rot="R180"/>
<pin name="CD+" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="-12.7" y="-10.16" length="middle" direction="pwr"/>
</symbol>
<symbol name="V+">
<wire x1="0.889" y1="-1.27" x2="0" y2="0.127" width="0.254" layer="94"/>
<wire x1="0" y1="0.127" x2="-0.889" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-1.27" x2="0.889" y2="-1.27" width="0.254" layer="94"/>
<pin name="V+" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="VCC/2">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="0" y2="1.778" width="0.254" layer="94"/>
<pin name="VCC/2" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="OPTOCOUPLER-TR">
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="-1.143" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.254" x2="-1.905" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-0.127" x2="-1.397" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.397" y1="-0.635" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="1.397" x2="-2.032" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.016" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-4.445" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.27" x2="1.397" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.889" y2="-0.508" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.016" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.889" y1="-0.508" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="6.985" y1="-5.08" x2="6.985" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="-5.08" x2="6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="6.985" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<text x="-7.0104" y="5.6896" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.0104" y="-7.5438" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="A1" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="A2" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="ST485">
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="10.16" y="2.54" length="short" direction="in" rot="R180"/>
<pin name="GND" x="10.16" y="-5.08" length="short" direction="in" rot="R180"/>
<pin name="RO" x="-10.16" y="2.54" length="short" direction="out"/>
<pin name="!RE" x="-10.16" y="0" length="short" direction="in"/>
<pin name="DE" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="DI" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="A" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="B" x="10.16" y="0" length="short" rot="R180"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="0" y="1.905" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE-SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.524" width="0.254" layer="94"/>
<text x="0" y="1.905" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE-ZENNER">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="0" y="1.905" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="0" y="0" size="2.54" layer="94" font="vector" ratio="25" distance="0" rot="MR0" align="center">&gt;12</text>
</symbol>
<symbol name="SJ_2">
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="2.54" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="1.016" size="2.54" layer="94" font="vector" ratio="25" rot="R270" align="center">&gt;12</text>
<text x="0" y="-1.016" size="2.54" layer="94" font="vector" ratio="25" rot="R270" align="center">&gt;23</text>
</symbol>
<symbol name="SHORT">
<description>&lt;b&gt;Short Connection&lt;/b&gt;&lt;br&gt;
Force connect two different signals, &lt;i&gt;for evample: GND and AGND&lt;/i&gt;</description>
<pin name="1" x="0" y="0" visible="off" length="short" direction="nc"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="nc" rot="R180"/>
</symbol>
<symbol name="TPS736XX">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="IN" x="-10.16" y="0" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="10.16" y="0" length="short" direction="sup" rot="R180"/>
<pin name="EN" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="NR" x="10.16" y="-2.54" length="short" direction="sup" rot="R180"/>
</symbol>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="DS2482-100">
<description>&lt;B&gt;Single-Channel 1-Wire Master&lt;/B&gt;</description>
<wire x1="-7.62" y1="-8.89" x2="7.62" y2="-8.89" width="0.254" layer="94"/>
<wire x1="7.62" y1="-8.89" x2="7.62" y2="8.89" width="0.254" layer="94"/>
<wire x1="7.62" y1="8.89" x2="-7.62" y2="8.89" width="0.254" layer="94"/>
<wire x1="-7.62" y1="8.89" x2="-7.62" y2="-8.89" width="0.254" layer="94"/>
<text x="-7.62" y="9.525" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-9.525" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<pin name="SDA" x="-10.16" y="5.08" length="short"/>
<pin name="SCL" x="-10.16" y="2.54" length="short" direction="in"/>
<pin name="A1" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="A0" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="GND" x="10.16" y="2.54" length="short" direction="pwr" rot="R180"/>
<pin name="PCTLZ" x="10.16" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="IO" x="10.16" y="-5.08" length="short" rot="R180"/>
<pin name="VCC" x="10.16" y="5.08" length="short" direction="nc" rot="R180"/>
</symbol>
<symbol name="ACS712">
<pin name="VCC" x="-12.7" y="7.62" length="middle"/>
<pin name="OUT" x="-12.7" y="2.54" length="middle"/>
<pin name="FILT" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="-12.7" y="-7.62" length="middle"/>
<pin name="IP+" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="IP-" x="12.7" y="-7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="10.668" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.668" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="1.778" layer="96" rot="R90">&gt;IP</text>
</symbol>
<symbol name="STM32F042G">
<description>&lt;h3&gt;STM32F032G - 32-bit MCU, UFQFPN28 (4x4mm)&lt;/h3&gt;</description>
<wire x1="-25.4" y1="25.4" x2="27.94" y2="25.4" width="0.254" layer="94"/>
<wire x1="27.94" y1="25.4" x2="27.94" y2="-27.94" width="0.254" layer="94"/>
<wire x1="27.94" y1="-27.94" x2="-25.4" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-27.94" x2="-25.4" y2="25.4" width="0.254" layer="94"/>
<text x="-25.4" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<text x="-25.4" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<pin name="AVCC" x="-30.48" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-30.48" y="-5.08" length="middle" direction="pwr"/>
<pin name="!NRST!" x="-30.48" y="20.32" length="middle" function="dot"/>
<pin name="VCC" x="-30.48" y="10.16" length="middle" direction="pwr"/>
<pin name="PF0/XTAL/SDA1" x="-30.48" y="-12.7" length="middle"/>
<pin name="PF1/XTAL/SCL1" x="-30.48" y="-15.24" length="middle"/>
<pin name="BOOT0/PB8/T16.1" x="-30.48" y="17.78" length="middle" function="dot"/>
<pin name="T3.4/T14.1/T1.3N/ADC9/PB1" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="WKUP/T2.E/ADC0/PA0" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="T2.2/ADC1/PA1" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="WKUP/TX2/T2.3/ADC2/PA2" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="RX2/T2.4/ADC3/PA3" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="USBNOE/NSS1/T14.1/ADC4/PA4" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="SCK1/T2.E/ADC5/PA5" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="TX1/SCL1/T1.2/PA9" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="RX1/SDA1/T1.3/PA10" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="USBNOE/SWDIO/PA13" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="SWCLK/TX2/PA14" x="33.02" y="20.32" length="middle" rot="R180"/>
<text x="25.4" y="15.24" size="1.524" layer="95" rot="R180">USBDP/SDA1/T1.E/PA12</text>
<text x="25.4" y="10.16" size="1.524" layer="95" rot="R180">USBDM/SCL1/T1.4/PA11</text>
<pin name="T3.3/T1.2N/ADC8/PB0" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="RX2/PA15" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="SCK1/T2.2/PB3" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="PB4/T3.1/MISO1" x="-30.48" y="-25.4" length="middle"/>
<pin name="PB5/T3.2/MOSI1" x="-30.48" y="-22.86" length="middle"/>
<pin name="PB6/T16.1N/SCL1/TX1" x="-30.48" y="-20.32" length="middle"/>
<pin name="PB7/T17.1N/SDA1/RX1" x="-30.48" y="-17.78" length="middle"/>
<pin name="VCC2" x="-30.48" y="7.62" length="middle" direction="pwr"/>
</symbol>
<symbol name="RGBBTN-C+">
<description>Push button with RGB light, common anode (+)</description>
<text x="-12.192" y="-4.064" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<text x="-12.192" y="-6.985" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<pin name="C-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="R+" x="-10.16" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G+" x="-5.08" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="B+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="-10.16" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="0" x2="-10.16" y2="-2.54" width="0.1524" layer="94"/>
<circle x="-5.08" y="-2.54" radius="0.508" width="0" layer="94"/>
<text x="-11.43" y="3.175" size="1.27" layer="94" rot="R180" align="top-center">R</text>
<text x="-6.35" y="3.175" size="1.27" layer="94" rot="R180" align="top-center">G</text>
<text x="-1.27" y="3.175" size="1.27" layer="94" rot="R180" align="top-center">B</text>
<circle x="-5.08" y="-2.54" radius="0.508" width="0" layer="94"/>
<pin name="NO1" x="5.08" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="NO2" x="5.08" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="-2.54" radius="0.508" width="0" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0.762" x2="-3.429" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-3.302" y2="3.302" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="2.159"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="3.302"/>
<vertex x="-2.921" y="2.413"/>
<vertex x="-2.413" y="2.921"/>
</polygon>
<wire x1="-8.89" y1="0" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-11.43" y2="0" width="0.254" layer="94"/>
<wire x1="-8.89" y1="2.54" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-11.43" y2="2.54" width="0.254" layer="94"/>
<wire x1="-8.89" y1="0" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="-11.43" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="-10.16" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-12.192" y1="0.762" x2="-13.589" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-12.065" y1="1.905" x2="-13.462" y2="3.302" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-13.589" y="2.159"/>
<vertex x="-13.208" y="1.27"/>
<vertex x="-12.7" y="1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-13.462" y="3.302"/>
<vertex x="-13.081" y="2.413"/>
<vertex x="-12.573" y="2.921"/>
</polygon>
<wire x1="-3.81" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-6.35" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-6.35" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.112" y1="0.762" x2="-8.509" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="1.905" x2="-8.382" y2="3.302" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-8.509" y="2.159"/>
<vertex x="-8.128" y="1.27"/>
<vertex x="-7.62" y="1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-8.382" y="3.302"/>
<vertex x="-8.001" y="2.413"/>
<vertex x="-7.493" y="2.921"/>
</polygon>
</symbol>
<symbol name="PNP">
<wire x1="2.086" y1="1.678" x2="1.578" y2="2.594" width="0.1524" layer="94"/>
<wire x1="1.578" y1="2.594" x2="0.516" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.516" y1="1.478" x2="2.086" y2="1.678" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.124" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
<text x="1.524" y="4.826" size="1.778" layer="95" align="top-right">&gt;NAME</text>
<text x="1.524" y="-4.826" size="1.778" layer="96" align="bottom-right">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="MOSFET-N">
<wire x1="-1.016" y1="-2.54" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.5334" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.715" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-0.635" x2="4.445" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.762" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="4.445" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="4.191" y2="1.016" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.762" x2="5.969" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="1.524" y="4.826" size="1.778" layer="95" align="top-right">&gt;NAME</text>
<text x="1.524" y="-4.826" size="1.778" layer="96" align="bottom-right">&gt;VALUE</text>
<text x="1.524" y="-3.302" size="0.8128" layer="93">D</text>
<text x="1.524" y="2.54" size="0.8128" layer="93">S</text>
<text x="-2.286" y="1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0.5588" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="-0.254" x2="0.762" y2="0" width="0.3048" layer="94"/>
<wire x1="0.762" y1="0" x2="1.651" y2="0.254" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0.254" x2="1.651" y2="0" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="OPTOCOUPLER">
<wire x1="-2.413" y1="-1.143" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.254" x2="-1.905" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-0.127" x2="-1.397" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.397" y1="-0.635" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="1.397" x2="-2.032" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.016" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="-5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-4.445" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="2.286" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.016" x2="2.286" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-2.286" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-2.286" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<text x="-6.985" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.985" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.381" y1="-2.54" x2="0.381" y2="2.54" layer="94"/>
<pin name="A" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="EMIT" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="COL" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DCDC-BUCK">
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="IN+" x="-12.7" y="5.08" visible="pin" length="middle"/>
<pin name="GND@1" x="-12.7" y="-5.08" visible="pin" length="middle"/>
<pin name="GND@2" x="12.7" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="OUT+" x="12.7" y="5.08" visible="pin" length="middle" rot="R180"/>
<text x="-7.62" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-8.255" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
</symbol>
<symbol name="DCDC">
<description>&lt;h3&gt;Isolated DC-DC Converter, Single output&lt;/h3&gt;</description>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-0.762" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="IN+" x="-12.7" y="5.08" visible="pad" length="short"/>
<pin name="IN-" x="-12.7" y="-5.08" visible="pad" length="short"/>
<pin name="OUT-" x="12.7" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="OUT+" x="12.7" y="5.08" visible="pad" length="short" rot="R180"/>
<text x="0" y="8.382" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.382" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
<wire x1="-0.762" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-7.62" x2="-0.762" y2="7.62" width="0.254" layer="94"/>
<wire x1="0.762" y1="-7.62" x2="0.762" y2="7.62" width="0.254" layer="94"/>
<text x="-9.398" y="5.08" size="1.778" layer="94" align="center-left">IN+</text>
<text x="-9.398" y="-5.08" size="1.778" layer="94" align="center-left">IN-</text>
<text x="9.398" y="5.08" size="1.778" layer="94" align="center-right">OUT+</text>
<text x="9.398" y="-5.08" size="1.778" layer="94" align="center-right">OUT-</text>
<text x="-5.334" y="0" size="1.778" layer="94" align="center">DC</text>
<text x="5.334" y="0" size="1.778" layer="94" align="center">DC</text>
<wire x1="0" y1="-7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0.254" y1="-7.62" x2="0.254" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-7.62" x2="-0.254" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="0.762" y2="7.62" width="0.254" layer="94"/>
<wire x1="0.254" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="-0.254" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="0.254" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0.762" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="DCDC-DUAL">
<description>&lt;h3&gt;Isolated DC-DC Converter, Single output&lt;/h3&gt;</description>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-0.762" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="IN+" x="-12.7" y="5.08" visible="pad" length="short"/>
<pin name="IN-" x="-12.7" y="-5.08" visible="pad" length="short"/>
<pin name="OUT-" x="12.7" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="OUT+" x="12.7" y="5.08" visible="pad" length="short" rot="R180"/>
<text x="0" y="8.382" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.382" size="1.27" layer="96" align="top-center">&gt;VALUE</text>
<wire x1="-0.762" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-7.62" x2="-0.762" y2="7.62" width="0.254" layer="94"/>
<wire x1="0.762" y1="-7.62" x2="0.762" y2="7.62" width="0.254" layer="94"/>
<text x="-9.398" y="5.08" size="1.778" layer="94" align="center-left">IN+</text>
<text x="-9.398" y="-5.08" size="1.778" layer="94" align="center-left">IN-</text>
<text x="9.398" y="5.08" size="1.778" layer="94" align="center-right">OUT+</text>
<text x="9.398" y="-5.08" size="1.778" layer="94" align="center-right">OUT-</text>
<text x="-5.334" y="0" size="1.778" layer="94" align="center">DC</text>
<text x="5.334" y="0" size="1.778" layer="94" align="center">DC</text>
<wire x1="0" y1="-7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0.254" y1="-7.62" x2="0.254" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-7.62" x2="-0.254" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="0.762" y2="7.62" width="0.254" layer="94"/>
<wire x1="0.254" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="-0.254" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="0.254" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0.762" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<pin name="OUT-GND" x="12.7" y="0" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="CON01-1">
<wire x1="-3.81" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<text x="-3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="2.54" x2="-3.81" y2="1.016" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="2.54" x2="-3.81" y2="1.524" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="2.54" x2="-3.81" y2="2.032" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="0.762" y2="1.27" width="0.4064" layer="94"/>
<wire x1="0.762" y1="1.27" x2="1.27" y2="1.27" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="1.27" y2="-1.27" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON01-2">
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<text x="-3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.286" y1="2.54" x2="-3.81" y2="1.016" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="2.54" x2="-3.81" y2="1.524" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="2.54" x2="-3.81" y2="2.032" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON02-1">
<wire x1="-3.81" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="2.54" x2="-3.81" y2="1.016" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="2.54" x2="-3.81" y2="1.524" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="2.54" x2="-3.81" y2="2.032" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="0" width="0.4064" layer="94"/>
<wire x1="0.762" y1="0" x2="1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON02-2">
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.286" y1="2.54" x2="-3.81" y2="1.016" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="2.54" x2="-3.81" y2="1.524" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="2.54" x2="-3.81" y2="2.032" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON03-1">
<wire x1="-3.81" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="5.08" x2="-3.81" y2="3.556" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="5.08" x2="-3.81" y2="4.064" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="5.08" x2="-3.81" y2="4.572" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0.762" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON03-2">
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.286" y1="5.08" x2="-3.81" y2="3.556" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="5.08" x2="-3.81" y2="4.064" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="5.08" x2="-3.81" y2="4.572" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON04-1">
<wire x1="-3.81" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="5.08" x2="-3.81" y2="3.556" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="5.08" x2="-3.81" y2="4.064" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="5.08" x2="-3.81" y2="4.572" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-5.08" x2="0.762" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0.762" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON04-2">
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.286" y1="5.08" x2="-3.81" y2="3.556" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="5.08" x2="-3.81" y2="4.064" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="5.08" x2="-3.81" y2="4.572" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON05-1">
<wire x1="-3.81" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="7.62" x2="-3.81" y2="6.096" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="7.62" x2="-3.81" y2="6.604" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="7.62" x2="-3.81" y2="7.112" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-5.08" x2="0.762" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0.762" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON05-2">
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.286" y1="7.62" x2="-3.81" y2="6.096" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="7.62" x2="-3.81" y2="6.604" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="7.62" x2="-3.81" y2="7.112" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON06-1">
<wire x1="-3.81" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="-2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<text x="-3.81" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="7.62" x2="-3.81" y2="6.096" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="7.62" x2="-3.81" y2="6.604" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="7.62" x2="-3.81" y2="7.112" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-7.62" x2="0.762" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0.762" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON06-2">
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="-2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="11" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.286" y1="7.62" x2="-3.81" y2="6.096" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="7.62" x2="-3.81" y2="6.604" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="7.62" x2="-3.81" y2="7.112" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON08-1">
<wire x1="-3.81" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="10.16" x2="-3.81" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="-3.81" y2="-12.7" width="0.4064" layer="94"/>
<text x="-3.81" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="10.16" x2="-3.81" y2="8.636" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="10.16" x2="-3.81" y2="9.144" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="10.16" x2="-3.81" y2="9.652" width="0.4064" layer="94"/>
<pin name="7" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-1.27" y1="-7.62" x2="-2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-10.16" x2="-2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="0.762" y1="-10.16" x2="0.762" y2="7.62" width="0.4064" layer="94"/>
<wire x1="0.762" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
</symbol>
<symbol name="CON10-1">
<wire x1="-3.81" y1="12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="-2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="1.27" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-2.286" y1="12.7" x2="-3.81" y2="11.176" width="0.4064" layer="94"/>
<wire x1="-2.794" y1="12.7" x2="-3.81" y2="11.684" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="12.7" x2="-3.81" y2="12.192" width="0.4064" layer="94"/>
<pin name="7" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="-2.54" y2="-7.62" width="0.6096" layer="94"/>
<pin name="9" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<wire x1="-1.27" y1="-10.16" x2="-2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-12.7" x2="-2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="0.762" y1="-12.7" x2="0.762" y2="10.16" width="0.4064" layer="94"/>
<wire x1="0.762" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="0.762" y1="-12.7" x2="1.27" y2="-12.7" width="0.4064" layer="94"/>
</symbol>
<symbol name="MOSFET-P">
<wire x1="-1.016" y1="-2.54" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.5334" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-0.762" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-0.762" x2="4.445" y2="0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.635" x2="5.715" y2="0.635" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.635" x2="5.08" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.762" x2="5.08" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-0.762" x2="5.715" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-0.762" x2="5.969" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.762" x2="4.191" y2="-0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="1.524" y="4.826" size="1.778" layer="95" align="top-right">&gt;NAME</text>
<text x="1.524" y="-4.826" size="1.778" layer="96" align="bottom-right">&gt;VALUE</text>
<text x="1.524" y="-3.302" size="0.8128" layer="93">D</text>
<text x="1.524" y="2.54" size="0.8128" layer="93">S</text>
<text x="-2.286" y="1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0.381" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.3622" y1="0" x2="2.413" y2="0" width="0.1524" layer="94"/>
<wire x1="2.413" y1="0" x2="1.143" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.143" y1="-0.508" x2="1.143" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0.508" x2="2.413" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.254" x2="2.159" y2="0" width="0.3048" layer="94"/>
<wire x1="2.159" y1="0" x2="1.27" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="1.524" y2="0" width="0.3048" layer="94"/>
</symbol>
<symbol name="ICM-20602">
<description>&lt;h3&gt;High Performance 6-Axis MEMS MotionTracking Device&lt;/h3&gt;</description>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-12.7" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<pin name="VDD" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="GND" x="-17.78" y="-5.08" length="middle" direction="pwr"/>
<pin name="REGOUT" x="-17.78" y="2.54" length="middle" direction="pwr"/>
<pin name="VDDIO" x="-17.78" y="5.08" length="middle" direction="pwr"/>
<pin name="FSYNC" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="INT" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="CS" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="SA0/SDO" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="SCL/SPC" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="SDA/SDI" x="17.78" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="STM32F030K">
<description>&lt;h3&gt;STM32F032K - 32-bit MCU, LQFP32 (7x7mm)&lt;/h3&gt;</description>
<wire x1="-25.4" y1="25.4" x2="27.94" y2="25.4" width="0.254" layer="94"/>
<wire x1="27.94" y1="25.4" x2="27.94" y2="-27.94" width="0.254" layer="94"/>
<wire x1="27.94" y1="-27.94" x2="-25.4" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-27.94" x2="-25.4" y2="25.4" width="0.254" layer="94"/>
<text x="-25.4" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<text x="-25.4" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<pin name="AVCC" x="-30.48" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-30.48" y="-5.08" length="middle" direction="pwr"/>
<pin name="!NRST!" x="-30.48" y="20.32" length="middle" function="dot"/>
<pin name="VCC" x="-30.48" y="10.16" length="middle" direction="pwr"/>
<pin name="PF0/XTAL" x="-30.48" y="-12.7" length="middle"/>
<pin name="PF1/XTAL" x="-30.48" y="-15.24" length="middle"/>
<pin name="BOOT0" x="-30.48" y="17.78" length="middle" function="dot"/>
<pin name="T3.4/T14.1/T1.3N/ADC9/PB1" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="WKUP/ADC0/PA0" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="ADC1/PA1" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="TX1/ADC2/PA2" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="RX1/ADC3/PA3" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="NSS1/T14.1/ADC4/PA4" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="SCK1/ADC5/PA5" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="TX1/SCL1/T1.2/PA9" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="RX1/SDA1/T1.3/PA10" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="SWDIO/PA13" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="SWCLK/TX1/PA14" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="T3.3/T1.2N/ADC8/PB0" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="VCC2" x="-30.48" y="7.62" length="middle" direction="pwr"/>
<pin name="T1.1/PA8" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="CTS1/T1.4/PA11" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="RTS1/T1.E/PA12" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="RX1/PA15" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="SCK1/PB3" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="PB4/T3.1/MISO1" x="-30.48" y="-25.4" length="middle"/>
<pin name="PB5/T3.2/MOSI1" x="-30.48" y="-22.86" length="middle"/>
<pin name="PB6/T16.1N/SCL1/TX1" x="-30.48" y="-20.32" length="middle"/>
<pin name="PB7/T17.1N/SDA1/RX1" x="-30.48" y="-17.78" length="middle"/>
<pin name="GND2" x="-30.48" y="-7.62" length="middle" direction="pwr"/>
</symbol>
<symbol name="RELAY-K">
<wire x1="1.905" y1="-3.81" x2="1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="3.81" x2="-1.905" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.81" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-3.81" x2="1.905" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0" x2="1.905" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.905" x2="-1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.921" size="1.778" layer="96" align="top-right">&gt;VALUE</text>
<text x="-2.54" y="-2.54" size="1.778" layer="95" align="bottom-right">&gt;PART</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="RELAY-V">
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.3048" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="1.905" width="0.3048" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.635" width="0.3048" layer="94"/>
<wire x1="1.651" y1="1.905" x2="-2.54" y2="0" width="0.3048" layer="94"/>
<text x="-1.27" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="NC" x="5.08" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="C" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="NO" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="RELAY-S">
<wire x1="3.175" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-1.905" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;PART</text>
<pin name="P" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="S" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="FUSE">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.397" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AGND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVDD" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="AVDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-C075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-C075_105-063X106_133" package="C075_105-063X106_133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2 2@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPOL" prefix="C" uservalue="yes">
<description>&lt;B&gt;POLARIZED CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="0"/>
</gates>
<devices>
<device name="SMCA" package="SMC_A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMCB" package="SMC_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMCC" package="SMC_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMCD" package="SMC_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMCE" package="SMC_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMV324?*" prefix="IC">
<description>Quad &lt;b&gt;General Purpose, Low Voltage, Rail-to-Rail Output Operational Amplifiers&lt;/b&gt;&lt;p&gt;
Source: http://www.national.com/mpf/LM/LMV321.html&lt;br&gt;
http://www.national.com/dt/lmv_qual.pdf</description>
<gates>
<gate name="A" symbol="OPAMP" x="-2.54" y="22.86"/>
<gate name="B" symbol="OPAMP" x="-2.54" y="7.62"/>
<gate name="C" symbol="OPAMP" x="-2.54" y="-7.62"/>
<gate name="D" symbol="OPAMP" x="-2.54" y="-22.86"/>
<gate name="P" symbol="PWR+-" x="-2.54" y="22.86" addlevel="always"/>
</gates>
<devices>
<device name="M" package="SO14">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="C" pin="+IN" pad="10"/>
<connect gate="C" pin="-IN" pad="9"/>
<connect gate="C" pin="OUT" pad="8"/>
<connect gate="D" pin="+IN" pad="12"/>
<connect gate="D" pin="-IN" pad="13"/>
<connect gate="D" pin="OUT" pad="14"/>
<connect gate="P" pin="V+" pad="4"/>
<connect gate="P" pin="V-" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MZ" package="TSSOP14">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="C" pin="+IN" pad="10"/>
<connect gate="C" pin="-IN" pad="9"/>
<connect gate="C" pin="OUT" pad="8"/>
<connect gate="D" pin="+IN" pad="12"/>
<connect gate="D" pin="-IN" pad="13"/>
<connect gate="D" pin="OUT" pad="14"/>
<connect gate="P" pin="V+" pad="4"/>
<connect gate="P" pin="V-" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO_SD_SOCKET" prefix="X">
<description>&lt;b&gt;MicroSD Socket&lt;/b&gt;
Attend technology socket for MicroSD card. Push-pull type. Product CODE 112I-TXAR-R.</description>
<gates>
<gate name="G$1" symbol="MICRO_SD" x="0" y="0"/>
</gates>
<devices>
<device name="SD" package="MICRO_SD_SOCKET-PP">
<connects>
<connect gate="G$1" pin="CD" pad="CD"/>
<connect gate="G$1" pin="CD/DAT3" pad="2"/>
<connect gate="G$1" pin="CLK" pad="5"/>
<connect gate="G$1" pin="CMD" pad="3"/>
<connect gate="G$1" pin="DAT0" pad="7"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DTA2" pad="1"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD1"/>
<connect gate="G$1" pin="SHIELD@" pad="SHIELD2"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F042F" prefix="IC">
<description>&lt;h2&gt; STM32F042x&lt;/h2&gt;
&lt;h3&gt;ARM®-based 32-bit MCU, up to 32 KB Flash, crystal-less USB
 FS 2.0, CAN, 8 timers, ADC &amp; comm. interfaces, 2.0 - 3.6 V&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Core: ARM 32-bit Cortex-M0 CPU, frequency up to 48 MHz &lt;/li&gt;
&lt;li&gt;Memories
&lt;ul&gt;
  &lt;li&gt;16 to 32 Kbytes of Flash memory&lt;/li&gt;
  &lt;li&gt;6 Kbytes of SRAM with HW parity&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Digital and I/Os supply: VDD = 2 V to 3.6 V&lt;ul&gt;
  &lt;li&gt;Analog supply: VDDA = VDD to 3.6 V&lt;/li&gt;
  &lt;li&gt;Selected I/Os: V&lt;/li&gt;
  &lt;li&gt;DDIO2 = 1.65 V to 3.6 V&lt;/li&gt;
  &lt;li&gt;Power-on/Power down reset (POR/PDR)&lt;/li&gt;
  &lt;li&gt;Programmable voltage detector (PVD)&lt;/li&gt;
  &lt;li&gt;Low power modes: Sleep, Stop, Standby&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;One 12-bit, 1.0 μs ADC (up to 10 channels)&lt;ul&gt;
  &lt;li&gt;Conversion range: 0 to 3.6 V&lt;/li&gt;
  &lt;li&gt;Separate analog supply: 2.4 V to 3.6 V&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Nine timers&lt;ul&gt;
  &lt;li&gt;One 16-bit advanced-control timer for six channel PWM output&lt;/li&gt;
  &lt;li&gt;One 32-bit and four 16-bit timers, with up to four IC/OC, OCN, usable for IR control decoding&lt;/li&gt;
  &lt;li&gt;Independent and system watchdog timers&lt;/li&gt;
  &lt;li&gt;SysTick timer&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="STM32F042F" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP20">
<connects>
<connect gate="G$1" pin="!NRST!" pad="4"/>
<connect gate="G$1" pin="AVCC" pad="5"/>
<connect gate="G$1" pin="BOOT0/PB8/T16.1" pad="1"/>
<connect gate="G$1" pin="GND" pad="15"/>
<connect gate="G$1" pin="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" pad="12"/>
<connect gate="G$1" pin="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" pad="13"/>
<connect gate="G$1" pin="PF0/XTAL/SDA1" pad="2"/>
<connect gate="G$1" pin="PF1/XTAL/SCL1" pad="3"/>
<connect gate="G$1" pin="RX1/SDA1/T1.3/PA10" pad="18"/>
<connect gate="G$1" pin="RX2/T2.4/ADC3/PA3" pad="9"/>
<connect gate="G$1" pin="SCK1/T2.E/ADC5/PA5" pad="11"/>
<connect gate="G$1" pin="SWCLK/TX2/PA14" pad="20"/>
<connect gate="G$1" pin="T2.2/ADC1/PA1" pad="7"/>
<connect gate="G$1" pin="T3.4/T14.1/T1.3N/ADC9/PB1" pad="14"/>
<connect gate="G$1" pin="TX1/SCL1/T1.2/PA9" pad="17"/>
<connect gate="G$1" pin="USBNOE/NSS1/T14.1/ADC4/PA4" pad="10"/>
<connect gate="G$1" pin="USBNOE/SWDIO/PA13" pad="19"/>
<connect gate="G$1" pin="VCC" pad="16"/>
<connect gate="G$1" pin="WKUP/T2.E/ADC0/PA0" pad="6"/>
<connect gate="G$1" pin="WKUP/TX2/T2.3/ADC2/PA2" pad="8"/>
</connects>
<technologies>
<technology name="4"/>
<technology name="6"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F405RG" prefix="IC">
<description>&lt;B&gt;STM32F4 series ARM cortex-M4 CPU with FPU&lt;/B&gt;&lt;BR&gt;
64pins, LQFP64 (10x10mm) package&lt;BR&gt;
210DMIPS, up to 1MB Flash/192+4KB RAM,&lt;BR&gt;
USB OTG HS/FS, Ethernet, 17 TIMs&lt;BR&gt;
16 ch. 3x12-bit ADCs, 2x12bit DAC&lt;BR&gt;
3 SPI, 3 I&lt;sup&gt;2&lt;/sup&gt;C, 6 UART, 2 CAN, 1 SDIO&lt;BR&gt;
51 GPIO&lt;BR&gt;</description>
<gates>
<gate name="G$1" symbol="STM32F405RG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP64">
<connects>
<connect gate="G$1" pin="!NRST!" pad="7"/>
<connect gate="G$1" pin="(ADC8)PB0" pad="26"/>
<connect gate="G$1" pin="(ADC9)PB1" pad="27"/>
<connect gate="G$1" pin="(BOOT1)PB2" pad="28"/>
<connect gate="G$1" pin="(JT-SWD)PA13" pad="46"/>
<connect gate="G$1" pin="(MCO1/SCL3)PA8" pad="41"/>
<connect gate="G$1" pin="(MISO1/ADC6)PA6" pad="22"/>
<connect gate="G$1" pin="(MISO2)PB14" pad="35"/>
<connect gate="G$1" pin="(MOSI1/ADC7)PA7" pad="23"/>
<connect gate="G$1" pin="(MOSI2)PB15" pad="36"/>
<connect gate="G$1" pin="(NJTRST/MISO1,3)PB4" pad="56"/>
<connect gate="G$1" pin="(NSS1,3/WS3)PA15" pad="50"/>
<connect gate="G$1" pin="(NSS1,3/WS3/ADC4)PA4" pad="20"/>
<connect gate="G$1" pin="(NSS2/SDA1/CANTX1)PB9" pad="62"/>
<connect gate="G$1" pin="(NSS2/SMBA2/CANRX2)PB12" pad="33"/>
<connect gate="G$1" pin="(RX2/ADC3)PA3" pad="17"/>
<connect gate="G$1" pin="(RX4/ADC1)PA1" pad="15"/>
<connect gate="G$1" pin="(SCK1/DAC2/ADC5)PA5" pad="21"/>
<connect gate="G$1" pin="(SCK2/CANTX2)PB13" pad="34"/>
<connect gate="G$1" pin="(SCK2/SCL2/TX3)PB10" pad="29"/>
<connect gate="G$1" pin="(SCL1/CAN2TC/TX1)PB6" pad="58"/>
<connect gate="G$1" pin="(SCL1/CANRX1)PB8" pad="61"/>
<connect gate="G$1" pin="(SDA1/RX1)PB7" pad="59"/>
<connect gate="G$1" pin="(SDA2/RX3)PB11" pad="30"/>
<connect gate="G$1" pin="(SMBA1/CANRX2/MOSI1,3)PB5" pad="57"/>
<connect gate="G$1" pin="(SWCLK)PA14" pad="49"/>
<connect gate="G$1" pin="(TRACESWO/SCK1,3)PB3" pad="55"/>
<connect gate="G$1" pin="(TX1/SMBA3)PA9" pad="42"/>
<connect gate="G$1" pin="(TX2/ADC2)PA2" pad="16"/>
<connect gate="G$1" pin="(USBDM/CANRX1)PA11" pad="44"/>
<connect gate="G$1" pin="(USBDP/CANTX1)PA12" pad="45"/>
<connect gate="G$1" pin="(USBID/RX1)PA10" pad="43"/>
<connect gate="G$1" pin="(WKUP/TX4/ADC0)PA0" pad="14"/>
<connect gate="G$1" pin="AGND" pad="12"/>
<connect gate="G$1" pin="AVCC" pad="13"/>
<connect gate="G$1" pin="BOOT0" pad="60"/>
<connect gate="G$1" pin="GND" pad="18"/>
<connect gate="G$1" pin="GND1" pad="63"/>
<connect gate="G$1" pin="PC0(ADC10)" pad="8"/>
<connect gate="G$1" pin="PC1(ADC11)" pad="9"/>
<connect gate="G$1" pin="PC10(SCK3/TX4/TX3)" pad="51"/>
<connect gate="G$1" pin="PC11(MISO3/RX4/RX3)" pad="52"/>
<connect gate="G$1" pin="PC12(MOSI3/TX5)" pad="53"/>
<connect gate="G$1" pin="PC13(RTC_AF1)" pad="2"/>
<connect gate="G$1" pin="PC2(ADC12/MISO2)" pad="10"/>
<connect gate="G$1" pin="PC3(ADC13/MOSI2/SD2)" pad="11"/>
<connect gate="G$1" pin="PC4(ADC14)" pad="24"/>
<connect gate="G$1" pin="PC5(ADC15)" pad="25"/>
<connect gate="G$1" pin="PC6(TX6)" pad="37"/>
<connect gate="G$1" pin="PC7(RX6)" pad="38"/>
<connect gate="G$1" pin="PC8" pad="39"/>
<connect gate="G$1" pin="PC9(SDA3)" pad="40"/>
<connect gate="G$1" pin="PD2(RX5)" pad="54"/>
<connect gate="G$1" pin="VBAT" pad="1"/>
<connect gate="G$1" pin="VCAP1" pad="31"/>
<connect gate="G$1" pin="VCAP2" pad="47"/>
<connect gate="G$1" pin="VCC" pad="19"/>
<connect gate="G$1" pin="VCC1" pad="32"/>
<connect gate="G$1" pin="VCC2" pad="48"/>
<connect gate="G$1" pin="VCC3" pad="64"/>
<connect gate="G$1" pin="XTAL/PH0" pad="5"/>
<connect gate="G$1" pin="XTAL/PH1" pad="6"/>
<connect gate="G$1" pin="XTAL32/PC14" pad="3"/>
<connect gate="G$1" pin="XTAL32/PC15" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F405VG" prefix="IC">
<description>&lt;B&gt;STM32F4 series ARM cortex-M4 CPU with FPU&lt;/B&gt;&lt;BR&gt;
100pins, LQFP100 (14x14mm) package&lt;BR&gt;
210DMIPS, up to 1MB Flash/192+4KB RAM,&lt;BR&gt;
USB OTG HS/FS, Ethernet, 17 TIMs&lt;BR&gt;
16 ch. 3x12-bit ADCs, 2x12bit DAC&lt;BR&gt;
3 SPI, 3 I&lt;sup&gt;2&lt;/sup&gt;C, 6 UART, 2 CAN, 1 SDIO&lt;BR&gt;
82 GPIO&lt;BR&gt;</description>
<gates>
<gate name="G$1" symbol="STM32F405VG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP100">
<connects>
<connect gate="G$1" pin="!NRST!" pad="14"/>
<connect gate="G$1" pin="(BOOT1)PB2" pad="37"/>
<connect gate="G$1" pin="(JT-SWD)PA13" pad="72"/>
<connect gate="G$1" pin="(MCO1/SCL3/T1.1)PA8" pad="67"/>
<connect gate="G$1" pin="(MISO1/T8.B/T13.1/T3.1/T1.B/ADC6)PA6" pad="31"/>
<connect gate="G$1" pin="(MISO2/T1.2N/T12.1/T8.2N)PB14" pad="53"/>
<connect gate="G$1" pin="(MOSI1/T8.1N/T14.1/T3.2/T1.1N/ADC7)PA7" pad="32"/>
<connect gate="G$1" pin="(MOSI2/T1.3N/T8.3N/T12.2)PB15" pad="54"/>
<connect gate="G$1" pin="(NJTRST/MISO1,3/T3.1)PB4" pad="90"/>
<connect gate="G$1" pin="(NSS1,3/WS3/DAC1/ADC4)PA4" pad="29"/>
<connect gate="G$1" pin="(NSS1,3/WS3/T2.1E)PA15" pad="77"/>
<connect gate="G$1" pin="(NSS2/SDA1/CTX1/T4.4/T11.1)PB9" pad="96"/>
<connect gate="G$1" pin="(NSS2/SMBA2/CRX2/T1.B)PB12" pad="51"/>
<connect gate="G$1" pin="(RX2/T5.4/T9.2/T2.4/ADC3)PA3" pad="26"/>
<connect gate="G$1" pin="(RX4/T5.2/T2.2/ADC1)PA1" pad="24"/>
<connect gate="G$1" pin="(SCK1/DAC2/T2.E/T8.C/DAC2)ADC5)PA5" pad="30"/>
<connect gate="G$1" pin="(SCK2/CTX2/T1.1N)PB13" pad="52"/>
<connect gate="G$1" pin="(SCK2/SCL2/TX3/T2.3)PB10" pad="47"/>
<connect gate="G$1" pin="(SCL1/CRX1/T4.3/T10.1)PB8" pad="95"/>
<connect gate="G$1" pin="(SCL1/CTX2/TX1/T4.1)PB6" pad="92"/>
<connect gate="G$1" pin="(SDA1/RX1/T4.2)PB7" pad="93"/>
<connect gate="G$1" pin="(SDA2/RX3/T2.4)PB11" pad="48"/>
<connect gate="G$1" pin="(SMBA1/CRX2/MOSI1,3/T3.2)PB5" pad="91"/>
<connect gate="G$1" pin="(SWCLK)PA14" pad="76"/>
<connect gate="G$1" pin="(T1.1)PE9" pad="40"/>
<connect gate="G$1" pin="(T1.1N)PE8" pad="39"/>
<connect gate="G$1" pin="(T1.2)PE11" pad="42"/>
<connect gate="G$1" pin="(T1.2N)PE10" pad="41"/>
<connect gate="G$1" pin="(T1.3)PE13" pad="44"/>
<connect gate="G$1" pin="(T1.3N)PE12" pad="43"/>
<connect gate="G$1" pin="(T1.4)PE14" pad="45"/>
<connect gate="G$1" pin="(T1.B)PE15" pad="46"/>
<connect gate="G$1" pin="(T1.E)PE7" pad="38"/>
<connect gate="G$1" pin="(T3.3/T8.2N/T1.2N/ADC8)PB0" pad="35"/>
<connect gate="G$1" pin="(T3.4/T8.3N/T1.3N/ADC9)PB1" pad="36"/>
<connect gate="G$1" pin="(T4.E)PE0" pad="97"/>
<connect gate="G$1" pin="(T9.1)PE5" pad="4"/>
<connect gate="G$1" pin="(T9.2)PE6" pad="5"/>
<connect gate="G$1" pin="(TRACESWO/SCK1,3/T2.2)PB3" pad="89"/>
<connect gate="G$1" pin="(TX2/T5.3/T9.1/T2.3/ADC2)PA2" pad="25"/>
<connect gate="G$1" pin="(USBDM/CRX1/T1.4)PA11" pad="70"/>
<connect gate="G$1" pin="(USBDP/CTX1/T1.E)PA12" pad="71"/>
<connect gate="G$1" pin="(USBID/RX1/T1.3)PA10" pad="69"/>
<connect gate="G$1" pin="(USBVBUS/TX1/SMBA3/T1.2)PA9" pad="68"/>
<connect gate="G$1" pin="(WKUP/TX4/T2.1/T5.1/T8.E/ADC0)PA0" pad="23"/>
<connect gate="G$1" pin="AGND" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="22"/>
<connect gate="G$1" pin="BOOT0" pad="94"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="GND@1" pad="27"/>
<connect gate="G$1" pin="GND@2" pad="74"/>
<connect gate="G$1" pin="GND@3" pad="99"/>
<connect gate="G$1" pin="PC0(ADC10)" pad="15"/>
<connect gate="G$1" pin="PC1(ADC11)" pad="16"/>
<connect gate="G$1" pin="PC10(SCK3/TX4/TX3)" pad="78"/>
<connect gate="G$1" pin="PC11(MISO3/RX4/RX3)" pad="79"/>
<connect gate="G$1" pin="PC12(MOSI3/TX5)" pad="80"/>
<connect gate="G$1" pin="PC13(RTC_AF1)" pad="7"/>
<connect gate="G$1" pin="PC2(ADC12/MISO2)" pad="17"/>
<connect gate="G$1" pin="PC3(ADC13/MOSI2/SD2)" pad="18"/>
<connect gate="G$1" pin="PC4(ADC14)" pad="33"/>
<connect gate="G$1" pin="PC5(ADC15)" pad="34"/>
<connect gate="G$1" pin="PC6(TX6/T8.1/T3.1)" pad="63"/>
<connect gate="G$1" pin="PC7(RX6/T8.2/T3.2)" pad="64"/>
<connect gate="G$1" pin="PC8(T8.3/T3.3)" pad="65"/>
<connect gate="G$1" pin="PC9(SDA3/T8.4/T3.4)" pad="66"/>
<connect gate="G$1" pin="PD0(CRX1)" pad="81"/>
<connect gate="G$1" pin="PD1(CTX1)" pad="82"/>
<connect gate="G$1" pin="PD10" pad="57"/>
<connect gate="G$1" pin="PD11" pad="58"/>
<connect gate="G$1" pin="PD12(T4.1)" pad="59"/>
<connect gate="G$1" pin="PD13(T4.2)" pad="60"/>
<connect gate="G$1" pin="PD14(T4.3)" pad="61"/>
<connect gate="G$1" pin="PD15(T4.4)" pad="62"/>
<connect gate="G$1" pin="PD2(RX5/T3.E)" pad="83"/>
<connect gate="G$1" pin="PD3" pad="84"/>
<connect gate="G$1" pin="PD4" pad="85"/>
<connect gate="G$1" pin="PD5(TX2)" pad="86"/>
<connect gate="G$1" pin="PD6(RX2)" pad="87"/>
<connect gate="G$1" pin="PD7" pad="88"/>
<connect gate="G$1" pin="PD8(TX3)" pad="55"/>
<connect gate="G$1" pin="PD9(RX3)" pad="56"/>
<connect gate="G$1" pin="PE1" pad="98"/>
<connect gate="G$1" pin="PE2" pad="1"/>
<connect gate="G$1" pin="PE3" pad="2"/>
<connect gate="G$1" pin="PE4" pad="3"/>
<connect gate="G$1" pin="VBAT" pad="6"/>
<connect gate="G$1" pin="VCAP1" pad="49"/>
<connect gate="G$1" pin="VCAP2" pad="73"/>
<connect gate="G$1" pin="VCC" pad="11"/>
<connect gate="G$1" pin="VCC@1" pad="19"/>
<connect gate="G$1" pin="VCC@2" pad="28"/>
<connect gate="G$1" pin="VCC@3" pad="50"/>
<connect gate="G$1" pin="VCC@4" pad="75"/>
<connect gate="G$1" pin="VCC@5" pad="100"/>
<connect gate="G$1" pin="VREF+" pad="21"/>
<connect gate="G$1" pin="XTAL/PH0" pad="12"/>
<connect gate="G$1" pin="XTAL/PH1" pad="13"/>
<connect gate="G$1" pin="XTAL32/PC14" pad="8"/>
<connect gate="G$1" pin="XTAL32/PC15" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<description>&lt;b&gt;Inductors&lt;/b&gt;
Basic Inductor/Choke</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="12.3X12.3" package="L1212">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7.5X7.5" package="L0707">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10.1X10.1" package="L1010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D3.5" package="L03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D5.8" package="L05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D7.8" package="L07">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="2X5MM" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="D0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="D0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="D1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="D1210">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F030F" prefix="IC">
<description>&lt;h2&gt; STM32F030F4&lt;/h2&gt;
&lt;h3&gt;Value-line ARM®-based 32-bit MCU with 16-KB Flash,
timers, ADC, communication interfaces, 2.4-3.6 V operation&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Core: ARM 32-bit Cortex-M0 CPU, frequency up to 48 MHz &lt;/li&gt;
&lt;li&gt;Memories
&lt;ul&gt;
  &lt;li&gt;16 Kbytes of Flash memory&lt;/li&gt;
  &lt;li&gt;4 Kbytes of SRAM with HW parity&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Digital and I/Os supply: VDD = 2.4 V to 3.6 V&lt;ul&gt;
  &lt;li&gt;Analog supply: VDDA = VDD to 3.6 V&lt;/li&gt;
  &lt;li&gt;Power-on/Power down reset (POR/PDR)&lt;/li&gt;
  &lt;li&gt;Low power modes: Sleep, Stop, Standby&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;One 12-bit, 1.0 μs ADC (9 channels)&lt;ul&gt;
  &lt;li&gt;Conversion range: 0 to 3.6 V&lt;/li&gt;
  &lt;li&gt;Separate analog supply: 2.4 V to 3.6 V&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Nine timers&lt;ul&gt;
  &lt;li&gt;One 16-bit advanced-control timer for six channel PWM output&lt;/li&gt;
  &lt;li&gt;Four 16-bit timers, with up to four IC/OC, OCN, usable for IR control decoding&lt;/li&gt;
  &lt;li&gt;Independent and system watchdog timers&lt;/li&gt;
  &lt;li&gt;SysTick timer&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="STM32F030F" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP20">
<connects>
<connect gate="G$1" pin="!NRST!" pad="4"/>
<connect gate="G$1" pin="ADC1/PA1" pad="7"/>
<connect gate="G$1" pin="AVCC" pad="5"/>
<connect gate="G$1" pin="BOOT0" pad="1"/>
<connect gate="G$1" pin="GND" pad="15"/>
<connect gate="G$1" pin="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" pad="12"/>
<connect gate="G$1" pin="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" pad="13"/>
<connect gate="G$1" pin="NSS1/T14.1/ADC4/PA4" pad="10"/>
<connect gate="G$1" pin="PF0/XTAL" pad="2"/>
<connect gate="G$1" pin="PF1/XTAL" pad="3"/>
<connect gate="G$1" pin="RX1/ADC3/PA3" pad="9"/>
<connect gate="G$1" pin="RX1/SDA1/T1.3/PA10" pad="18"/>
<connect gate="G$1" pin="SCK1/ADC5/PA5" pad="11"/>
<connect gate="G$1" pin="SWCLK/TX1/PA14" pad="20"/>
<connect gate="G$1" pin="SWDIO/PA13" pad="19"/>
<connect gate="G$1" pin="T3.4/T14.1/T1.3N/ADC9/PB1" pad="14"/>
<connect gate="G$1" pin="TX1/ADC2/PA2" pad="8"/>
<connect gate="G$1" pin="TX1/SCL1/T1.2/PA9" pad="17"/>
<connect gate="G$1" pin="VCC" pad="16"/>
<connect gate="G$1" pin="WKUP/ADC0/PA0" pad="6"/>
</connects>
<technologies>
<technology name="4"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ULN2003A" prefix="IC">
<description>&lt;b&gt;DRIVER ARRAY&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="2001A" x="0" y="0"/>
</gates>
<devices>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V+" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="V+" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC/2" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCC/2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FODM30*" prefix="OK">
<description>&lt;h2&gt;4-Pin Full Pitch Mini-Flat Package Random-Phase Triac 
Driver Output Optocouplers&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Compact 4-pin surface mount package (2.4 mm 
maximum standoff height)&lt;/li&gt;
&lt;li&gt;Peak blocking voltage&lt;br&gt;
250V (FODM301X)&lt;br&gt;
400V (FODM302X)&lt;br&gt;
600V (FODM305X)&lt;/li&gt;
&lt;li&gt;Available in tape and reel quantities of 2500.&lt;/li&gt;
&lt;li&gt;Add “NF098” for new construction version with 260°C 
max. reflow temperature rating&lt;/li&gt;
&lt;li&gt;UL, C-UL and VDE certifications pending&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="OPTOCOUPLER-TR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FODM30*">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="A1" pad="3"/>
<connect gate="G$1" pin="A2" pad="4"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name="11">
<attribute name="IFT" value="10"/>
<attribute name="VDRM" value="250"/>
</technology>
<technology name="12">
<attribute name="IFT" value="10"/>
<attribute name="VDRM" value="250"/>
</technology>
<technology name="22">
<attribute name="IFT" value="5"/>
<attribute name="VDRM" value="400"/>
</technology>
<technology name="23">
<attribute name="IFT" value="5"/>
<attribute name="VDRM" value="400"/>
</technology>
<technology name="52">
<attribute name="IFT" value="10"/>
<attribute name="VDRM" value="600"/>
</technology>
<technology name="53">
<attribute name="IFT" value="5"/>
<attribute name="VDRM" value="600"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ST485" prefix="IC">
<description>&lt;h2&gt;ST485&lt;/h2&gt;
&lt;h3&gt;LOW POWER RS-485/RS-422 TRANSCEIVER&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;OPERATE FROM A SINGLE 5V SUPPLY&lt;/li&gt;
&lt;li&gt;ALLOWS UP TO 64 TRANSCEIVERS ON THE BUS&lt;/li&gt;
&lt;li&gt;CURRENT LIMITING AND THERMAL SHUTDOWN FOR DRIVER OVERLOAD PROTECTION&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="ST485" x="0" y="0"/>
</gates>
<devices>
<device name="D" package="SO8">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="A">
<attribute name="T_RANGE" value="-55-125 C"/>
</technology>
<technology name="B">
<attribute name="T_RANGE" value="-40-85 C"/>
</technology>
<technology name="C">
<attribute name="T_RANGE" value="0-70 C"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="-SOD323" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMA" package="SMA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMB" package="SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMC" package="SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE-SCHOTTKY" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="-SOD323" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMC" package="SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMB" package="SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMA" package="SMA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE-ZENNER" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE-ZENNER" x="2.54" y="0"/>
</gates>
<devices>
<device name="-SOD323" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMC" package="SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMB" package="SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMA" package="SMA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SJ" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value=""/>
</technology>
</technologies>
</device>
<device name="M" package="SJ-M">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value=""/>
</technology>
</technologies>
</device>
<device name="M-NC" package="SJ-M-12">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value="="/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SJ2" prefix="SJ" uservalue="yes">
<description>&lt;b&gt;Solder Jumper 2 way&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="SJ_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value=""/>
<attribute name="23" value=""/>
</technology>
</technologies>
</device>
<device name="M" package="SJ2-M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value=""/>
<attribute name="23" value=""/>
</technology>
</technologies>
</device>
<device name="1-2" package="SJ2-12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value="="/>
<attribute name="23" value=""/>
</technology>
</technologies>
</device>
<device name="2-3" package="SJ2-23">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value=""/>
<attribute name="23" value="="/>
</technology>
</technologies>
</device>
<device name="1-2-3" package="SJ2-123">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value="="/>
<attribute name="23" value="=" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1-2" package="SJ2-M-12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value="="/>
<attribute name="23" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2-3" package="SJ2-M-23">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value=""/>
<attribute name="23" value="="/>
</technology>
</technologies>
</device>
<device name="M1-2-3" package="SJ2-M-123">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="12" value="="/>
<attribute name="23" value="="/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SHORT" prefix="U">
<description>&lt;b&gt;Short Connection&lt;/b&gt;&lt;br&gt;
Force connect two different signals, &lt;i&gt;for evample: GND and AGND&lt;/i&gt;</description>
<gates>
<gate name="G$1" symbol="SHORT" x="0" y="0"/>
</gates>
<devices>
<device name="-6" package="SHORT_SMD6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-16" package="SHORT_SMD16">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS736XX" prefix="IC" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;LDO TPS736xx&lt;/b&gt;
CAP-FREE NMOS 400 mA LOW-DROPOUT REGULATORS&lt;/p&gt;
&lt;p&gt;Input Voltage Range of 1.7 V to 5.5 V&lt;/p&gt;
&lt;p&gt;Fixed Outputs of 1.2 V to 3.3 V&lt;/p&gt;
&lt;p&gt;xx is normal output voltage (for example, 25 = 2.5 V, 01 = Adjustable)</description>
<gates>
<gate name="G$1" symbol="TPS736XX" x="0" y="0"/>
</gates>
<devices>
<device name="DVB" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="NR" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="QSMD3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DS2482-100" prefix="IC">
<description>&lt;B&gt;MAXIM I²C to 1-Wire bridge device&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="DS2482-100" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO8">
<connects>
<connect gate="G$1" pin="A0" pad="8"/>
<connect gate="G$1" pin="A1" pad="7"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="IO" pad="2"/>
<connect gate="G$1" pin="PCTLZ" pad="6"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ADDR" value="20h"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACS712" prefix="IC">
<description>&lt;h2&gt;Hall Effect-Based Linear Current Sensor&lt;/h2&gt;
with 2.1 kVRMS Voltage Isolation and a Low-Resistance Current Conductor</description>
<gates>
<gate name="G$1" symbol="ACS712" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO8">
<connects>
<connect gate="G$1" pin="FILT" pad="6"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="IP+" pad="1 2"/>
<connect gate="G$1" pin="IP-" pad="3 4"/>
<connect gate="G$1" pin="OUT" pad="7"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="-05">
<attribute name="IP" value="5"/>
</technology>
<technology name="-20">
<attribute name="IP" value="20"/>
</technology>
<technology name="-30">
<attribute name="IP" value="30"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F042G" prefix="IC">
<description>&lt;h2&gt; STM32F042G 28 pins UQFPN (4x4mm)&lt;/h2&gt;
&lt;h3&gt;ARM®-based 32-bit MCU, up to 32 KB Flash, crystal-less USB
 FS 2.0, CAN, 8 timers, ADC &amp; comm. interfaces, 2.0 - 3.6 V&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Core: ARM 32-bit Cortex-M0 CPU, frequency up to 48 MHz &lt;/li&gt;
&lt;li&gt;Memories
&lt;ul&gt;
  &lt;li&gt;16 to 32 Kbytes of Flash memory&lt;/li&gt;
  &lt;li&gt;6 Kbytes of SRAM with HW parity&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Digital and I/Os supply: VDD = 2 V to 3.6 V&lt;ul&gt;
  &lt;li&gt;Analog supply: VDDA = VDD to 3.6 V&lt;/li&gt;
  &lt;li&gt;Selected I/Os: V&lt;/li&gt;
  &lt;li&gt;DDIO2 = 1.65 V to 3.6 V&lt;/li&gt;
  &lt;li&gt;Power-on/Power down reset (POR/PDR)&lt;/li&gt;
  &lt;li&gt;Programmable voltage detector (PVD)&lt;/li&gt;
  &lt;li&gt;Low power modes: Sleep, Stop, Standby&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;One 12-bit, 1.0 μs ADC (up to 10 channels)&lt;ul&gt;
  &lt;li&gt;Conversion range: 0 to 3.6 V&lt;/li&gt;
  &lt;li&gt;Separate analog supply: 2.4 V to 3.6 V&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Nine timers&lt;ul&gt;
  &lt;li&gt;One 16-bit advanced-control timer for six channel PWM output&lt;/li&gt;
  &lt;li&gt;One 32-bit and four 16-bit timers, with up to four IC/OC, OCN, usable for IR control decoding&lt;/li&gt;
  &lt;li&gt;Independent and system watchdog timers&lt;/li&gt;
  &lt;li&gt;SysTick timer&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="STM32F042G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UFQFPN28">
<connects>
<connect gate="G$1" pin="!NRST!" pad="4"/>
<connect gate="G$1" pin="AVCC" pad="5"/>
<connect gate="G$1" pin="BOOT0/PB8/T16.1" pad="1"/>
<connect gate="G$1" pin="GND" pad="16"/>
<connect gate="G$1" pin="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" pad="12"/>
<connect gate="G$1" pin="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" pad="13"/>
<connect gate="G$1" pin="PB4/T3.1/MISO1" pad="25"/>
<connect gate="G$1" pin="PB5/T3.2/MOSI1" pad="26"/>
<connect gate="G$1" pin="PB6/T16.1N/SCL1/TX1" pad="27"/>
<connect gate="G$1" pin="PB7/T17.1N/SDA1/RX1" pad="28"/>
<connect gate="G$1" pin="PF0/XTAL/SDA1" pad="2"/>
<connect gate="G$1" pin="PF1/XTAL/SCL1" pad="3"/>
<connect gate="G$1" pin="RX1/SDA1/T1.3/PA10" pad="20"/>
<connect gate="G$1" pin="RX2/PA15" pad="23"/>
<connect gate="G$1" pin="RX2/T2.4/ADC3/PA3" pad="9"/>
<connect gate="G$1" pin="SCK1/T2.2/PB3" pad="24"/>
<connect gate="G$1" pin="SCK1/T2.E/ADC5/PA5" pad="11"/>
<connect gate="G$1" pin="SWCLK/TX2/PA14" pad="22"/>
<connect gate="G$1" pin="T2.2/ADC1/PA1" pad="7"/>
<connect gate="G$1" pin="T3.3/T1.2N/ADC8/PB0" pad="14"/>
<connect gate="G$1" pin="T3.4/T14.1/T1.3N/ADC9/PB1" pad="15"/>
<connect gate="G$1" pin="TX1/SCL1/T1.2/PA9" pad="19"/>
<connect gate="G$1" pin="USBNOE/NSS1/T14.1/ADC4/PA4" pad="10"/>
<connect gate="G$1" pin="USBNOE/SWDIO/PA13" pad="21"/>
<connect gate="G$1" pin="VCC" pad="17"/>
<connect gate="G$1" pin="VCC2" pad="18"/>
<connect gate="G$1" pin="WKUP/T2.E/ADC0/PA0" pad="6"/>
<connect gate="G$1" pin="WKUP/TX2/T2.3/ADC2/PA2" pad="8"/>
</connects>
<technologies>
<technology name="4"/>
<technology name="6"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RGBBTN-C+" prefix="BTN" uservalue="yes">
<description>&lt;h3&gt;RGB LED Light Push Button&lt;/h3&gt;
&lt;p&gt;Common Anode (+)&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RGBBTN-C+" x="-10.16" y="2.54"/>
</gates>
<devices>
<device name="19MM" package="RGBBTN-19">
<connects>
<connect gate="G$1" pin="B+" pad="B$BOT B$TOP" route="any"/>
<connect gate="G$1" pin="C-" pad="C$BOT C$TOP" route="any"/>
<connect gate="G$1" pin="G+" pad="G$BOT G$TOP" route="any"/>
<connect gate="G$1" pin="NO1" pad="NO1$BOT NO1$TOP" route="any"/>
<connect gate="G$1" pin="NO2" pad="NO2$BOT NO2$TOP" route="any"/>
<connect gate="G$1" pin="R+" pad="R$BOT R$TOP" route="any"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="19MM-S" package="RGBBTN-19-1">
<connects>
<connect gate="G$1" pin="B+" pad="B$TOP"/>
<connect gate="G$1" pin="C-" pad="C$TOP"/>
<connect gate="G$1" pin="G+" pad="G$TOP"/>
<connect gate="G$1" pin="NO1" pad="NO1$TOP"/>
<connect gate="G$1" pin="NO2" pad="NO2$TOP"/>
<connect gate="G$1" pin="R+" pad="R$TOP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22MM" package="RGBBTN-22">
<connects>
<connect gate="G$1" pin="B+" pad="B$BOT B$TOP" route="any"/>
<connect gate="G$1" pin="C-" pad="C$BOT C$TOP" route="any"/>
<connect gate="G$1" pin="G+" pad="G$BOT G$TOP" route="any"/>
<connect gate="G$1" pin="NO1" pad="NO1$BOT NO1$TOP" route="any"/>
<connect gate="G$1" pin="NO2" pad="NO2$BOT NO2$TOP" route="any"/>
<connect gate="G$1" pin="R+" pad="R$BOT R$TOP" route="any"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BIPOLAR-PNP" prefix="Q" uservalue="yes">
<description>&lt;b&gt;PNP Transistror&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT323" package="SOT323">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO252" package="TO252-DPAK">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N" prefix="Q" uservalue="yes">
<description>&lt;h3&gt;HEXFET Power MOSFET&lt;h3&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="-TO252" package="TO252-DPAK">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO263" package="TO263-D2PACK">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SO8" package="SO8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT323" package="SOT323">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OPTOCOUPLER-4" prefix="OK" uservalue="yes">
<description>&lt;h3&gt;Photocoupler (Transistor), 4 channels&lt;/h3&gt;</description>
<gates>
<gate name="A" symbol="OPTOCOUPLER" x="0" y="27.94" swaplevel="1"/>
<gate name="B" symbol="OPTOCOUPLER" x="0" y="10.16" swaplevel="1"/>
<gate name="C" symbol="OPTOCOUPLER" x="0" y="-7.62" swaplevel="1"/>
<gate name="D" symbol="OPTOCOUPLER" x="0" y="-25.4" swaplevel="1"/>
</gates>
<devices>
<device name="" package="SO16">
<connects>
<connect gate="A" pin="A" pad="1"/>
<connect gate="A" pin="C" pad="2"/>
<connect gate="A" pin="COL" pad="16"/>
<connect gate="A" pin="EMIT" pad="15"/>
<connect gate="B" pin="A" pad="3"/>
<connect gate="B" pin="C" pad="4"/>
<connect gate="B" pin="COL" pad="14"/>
<connect gate="B" pin="EMIT" pad="13"/>
<connect gate="C" pin="A" pad="5"/>
<connect gate="C" pin="C" pad="6"/>
<connect gate="C" pin="COL" pad="12"/>
<connect gate="C" pin="EMIT" pad="11"/>
<connect gate="D" pin="A" pad="7"/>
<connect gate="D" pin="C" pad="8"/>
<connect gate="D" pin="COL" pad="10"/>
<connect gate="D" pin="EMIT" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DCDC-BUCK" prefix="M" uservalue="yes">
<description>&lt;h2&gt;DC-DC Step down converter, Power Supply Module&lt;/h2&gt;
&lt;p&gt;supplied by Aliexpress&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DCDC-BUCK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE-DCDC-MINI">
<connects>
<connect gate="G$1" pin="GND@1" pad="IN- IN-1"/>
<connect gate="G$1" pin="GND@2" pad="OUT- OUT-1"/>
<connect gate="G$1" pin="IN+" pad="IN+ IN+1"/>
<connect gate="G$1" pin="OUT+" pad="OUT+ OUT+1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="U" package="MODULE-DCDC-UNI">
<connects>
<connect gate="G$1" pin="GND@1" pad="IN- IN-1 IN-2 IN-3 IN-4 IN-5 IN-6 IN-7 IN-8 IN-9 IN-10 IN-11 IN-12 IN-13 IN-14 IN-15 IN-16 IN-17 IN-18 IN-19 IN-20 IN-21 IN-22 IN-23 IN-24 IN-25 IN-26 IN-27 IN-28 IN-29 IN-30 IN-31 IN-32 IN-33 IN-34 IN-35 IN-36 IN-37 IN-38 IN-39 IN-40 IN-41 IN-42 IN-43"/>
<connect gate="G$1" pin="GND@2" pad="OUT- OUT-1 OUT-2 OUT-3 OUT-4 OUT-5 OUT-6 OUT-7 OUT-8 OUT-9 OUT-10 OUT-11 OUT-12 OUT-13 OUT-14 OUT-15"/>
<connect gate="G$1" pin="IN+" pad="IN+ IN+1 IN+2 IN+3 IN+4 IN+5 IN+6 IN+7 IN+8 IN+9 IN+10 IN+11 IN+12 IN+13 IN+14 IN+15 IN+16 IN+17 IN+18 IN+19 IN+20 IN+21 IN+22 IN+23 IN+24 IN+25 IN+26 IN+27 IN+28 IN+29 IN+30 IN+31 IN+32 IN+33 IN+34 IN+35 IN+36 IN+37 IN+38 IN+39 IN+40 IN+41 IN+42 IN+43"/>
<connect gate="G$1" pin="OUT+" pad="OUT+ OUT+1 OUT+2 OUT+3 OUT+4 OUT+5 OUT+6 OUT+7 OUT+8 OUT+9 OUT+10 OUT+11 OUT+12 OUT+13 OUT+14 OUT+15"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MINI360" package="MODULE-DCDC-MINI360">
<connects>
<connect gate="G$1" pin="GND@1" pad="IN-"/>
<connect gate="G$1" pin="GND@2" pad="OUT-"/>
<connect gate="G$1" pin="IN+" pad="IN+"/>
<connect gate="G$1" pin="OUT+" pad="OUT+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DCDC" prefix="U" uservalue="yes">
<description>&lt;h3&gt;Isolated DC-DC Converter, Single output&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="DCDC" x="0" y="0"/>
</gates>
<devices>
<device name="-SIP4" package="SIP4">
<connects>
<connect gate="G$1" pin="IN+" pad="2"/>
<connect gate="G$1" pin="IN-" pad="1"/>
<connect gate="G$1" pin="OUT+" pad="4"/>
<connect gate="G$1" pin="OUT-" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SIP7" package="SIP7-4">
<connects>
<connect gate="G$1" pin="IN+" pad="1"/>
<connect gate="G$1" pin="IN-" pad="2"/>
<connect gate="G$1" pin="OUT+" pad="6"/>
<connect gate="G$1" pin="OUT-" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DCDC-DUAL" prefix="U" uservalue="yes">
<description>&lt;h3&gt;Isolated DC-DC Converter, Dual output&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="DCDC-DUAL" x="0" y="0"/>
</gates>
<devices>
<device name="-SIP7" package="SIP7-5">
<connects>
<connect gate="G$1" pin="IN+" pad="1"/>
<connect gate="G$1" pin="IN-" pad="2"/>
<connect gate="G$1" pin="OUT+" pad="6"/>
<connect gate="G$1" pin="OUT-" pad="4"/>
<connect gate="G$1" pin="OUT-GND" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON01-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON01-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA01-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON01-2" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Double row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON01-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA01-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MF-H" package="MF01-2-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="RCS" value="125129, 125125"/>
</technology>
</technologies>
</device>
<device name="-MF-HP" package="MF01-2-HP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="RCS" value="125129, 125125"/>
</technology>
</technologies>
</device>
<device name="-MF-V" package="MF01-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="RCS" value="125130, 125125"/>
</technology>
</technologies>
</device>
<device name="-MF-VP" package="MF01-2-VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="RCS" value="125130, 125125"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON02-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON02-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA02-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-02-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050205, 00-00050192"/>
</technology>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-02-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050206, 00-00050192"/>
</technology>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-02-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040947, 00-00040946"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-02-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040948, 00-00040946"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-02-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050208, 00-00050192"/>
</technology>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-02-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050207, 00-00050192"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON02-2" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Double row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON02-2" x="0" y="0"/>
</gates>
<devices>
<device name="-MF-V" package="MF02-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047094, 00-00047054"/>
<attribute name="RCS" value="73400, 73413"/>
</technology>
</technologies>
</device>
<device name="-MF-HP" package="MF02-2-HP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047084, 00-00047054"/>
<attribute name="RCS" value="73401, 73413"/>
</technology>
</technologies>
</device>
<device name="-MF-VP" package="MF02-2-VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047094, 00-00047054"/>
<attribute name="RCS" value="73400, 73413"/>
</technology>
</technologies>
</device>
<device name="-MF-H" package="MF02-2-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047084, 00-00047054"/>
<attribute name="RCS" value="73401, 73413"/>
</technology>
</technologies>
</device>
<device name="" package="MA02-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON03-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON03-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-03-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050210, 00-00050193"/>
</technology>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-03-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050209, 00-00050193"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-03-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050212, 00-00050193"/>
</technology>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-03-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050211, 00-00050193"/>
</technology>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-03-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040950, 00-00040949"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-03-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040951, 00-00040949"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON03-2" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Double row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON03-2" x="0" y="0"/>
</gates>
<devices>
<device name="-MF-V" package="MF03-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="5"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047095, 00-00047055"/>
<attribute name="RCS" value="33466, 73397"/>
</technology>
</technologies>
</device>
<device name="-MF-VP" package="MF03-2-VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="5"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047095, 00-00047055"/>
<attribute name="RCS" value="33466, 73397"/>
</technology>
</technologies>
</device>
<device name="-MF-H" package="MF03-2-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="5"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047085, 00-00047055"/>
<attribute name="RCS" value="73402, 73397"/>
</technology>
</technologies>
</device>
<device name="-MF-HP" package="MF03-2-HP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="5"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047085, 00-00047055"/>
<attribute name="RCS" value="73402, 73397"/>
</technology>
</technologies>
</device>
<device name="" package="MA03-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON04-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON04-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-04-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050214, 00-00050194"/>
</technology>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-04-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050213, 00-00050194"/>
</technology>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-04-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040953, 00-00040952"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-04-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040954, 00-00040952"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-04-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-04-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050215, 00-00050194"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON04-2" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Double row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON04-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MF-V" package="MF04-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="6"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="7"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047096, 00-00047056"/>
<attribute name="RCS" value="151855, 102392"/>
</technology>
</technologies>
</device>
<device name="-MF-H" package="MF04-2-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="6"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="7"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047086, 00-00047056" constant="no"/>
<attribute name="RCS" value="19270, 102392"/>
</technology>
</technologies>
</device>
<device name="-MF-VP" package="MF04-2-VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="6"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="7"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047096, 00-00047056"/>
<attribute name="RCS" value="151855, 102392"/>
</technology>
</technologies>
</device>
<device name="-MF-HP" package="MF04-2-HP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="6"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="7"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047086, 00-00047056"/>
<attribute name="RCS" value="19270, 102392"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON05-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON05-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-05-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050218, 00-00050195"/>
</technology>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-05-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050217, 00-00050195"/>
</technology>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-05-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040956, 00-00040955"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-05-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040957, 00-00040955"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-05-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050220, 00-00050195"/>
</technology>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-05-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050219, 00-00050195"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON05-2" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Double row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="CON05-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MF-H" package="MF05-2-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="6"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="7"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="8"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="9"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047087, 00-00047057"/>
<attribute name="RCS" value="102396, 151439"/>
</technology>
</technologies>
</device>
<device name="-MF-HP" package="MF05-2-HP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="6"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="7"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="8"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="9"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047087, 00-00047057"/>
<attribute name="RCS" value="102396, 151439"/>
</technology>
</technologies>
</device>
<device name="-MF-V" package="MF05-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="6"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="7"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="8"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="9"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047097, 00-00047057"/>
<attribute name="RCS" value="91902, 151439"/>
</technology>
</technologies>
</device>
<device name="-MF-VP" package="MF05-2-VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="6"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="7"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="8"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="9"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047097, 00-00047057"/>
<attribute name="RCS" value="91902, 151439"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON06-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON06-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA06-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-06-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050222, 00-00050196"/>
</technology>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-06-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050221, 00-00050196"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-06-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050224, 00-00050196"/>
</technology>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-06-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050223, 00-00050196"/>
</technology>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-06-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040959, 00-00040958"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-06-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040960, 00-00040958"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON06-2" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON06-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA06-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MF-V" package="MF06-2-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="11"/>
<connect gate="G$1" pin="11" pad="6"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="7"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="8"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="9"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="10"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047098, 00-00047058"/>
</technology>
</technologies>
</device>
<device name="-MF-H" package="MF06-2-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="11"/>
<connect gate="G$1" pin="11" pad="6"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="7"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="8"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="9"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="10"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047088, 00-00047058"/>
</technology>
</technologies>
</device>
<device name="-MF-VP" package="MF06-2-VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="11"/>
<connect gate="G$1" pin="11" pad="6"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="7"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="8"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="9"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="10"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047098, 00-00047058"/>
</technology>
</technologies>
</device>
<device name="-MF-HP" package="MF06-2-HP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="11"/>
<connect gate="G$1" pin="11" pad="6"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="7"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="8"/>
<connect gate="G$1" pin="5" pad="3"/>
<connect gate="G$1" pin="6" pad="9"/>
<connect gate="G$1" pin="7" pad="4"/>
<connect gate="G$1" pin="8" pad="10"/>
<connect gate="G$1" pin="9" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00047088, 00-00047058"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON08-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON08-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA08-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-08-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050227, 00-00050198"/>
</technology>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-08-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050226, 00-00050198"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-08-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-08-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-08-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040962, 00-00040961"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-08-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040963, 00-00040961"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON10-1" prefix="X" uservalue="yes">
<description>&lt;h3&gt;PIN HEADER, Single Row&lt;/h3&gt;
&lt;ul&gt;Types:  V - vertical (straight), H - horizontal (right angle), S - SMT, P - with pegs (mounting holes)
&lt;li&gt;MA - Common PCB to PCB or PCB to WIRE connectors 2.54mm pitch&lt;/li&gt;
&lt;li&gt;MF - Mini-Fit PCB to WIRE 4.2mm pitch, 9A&lt;/li&gt;
&lt;li&gt;1mm - PCB to WIRE crimp style with bracket and direction key, 1mm pitch, JST SMxxB-SR, A1001, NX1001&lt;/li&gt;
&lt;li&gt;2mm - PCB to WIRE crimp style with bracket and direction key, 2mm pitch, JST SxxB-PH, NXW&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON10-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA10-1-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1MM-HS" package="JST-SR-10-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040965, 00-00040964"/>
</technology>
</technologies>
</device>
<device name="-1MM-VS" package="JST-SR-10-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00040966, 00-00040964"/>
</technology>
</technologies>
</device>
<device name="-2MM-H" package="JST-PH-10-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050234, 00-00050200"/>
</technology>
</technologies>
</device>
<device name="-2MM-V" package="JST-PH-10-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050233, 00-00050200"/>
</technology>
</technologies>
</device>
<device name="-2MM-VS" package="JST-PH-10-VS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="KOSMODROM" value="00-00050235, 00-00050200"/>
</technology>
</technologies>
</device>
<device name="-2MM-HS" package="JST-PH-10-HS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-P" prefix="Q" uservalue="yes">
<description>&lt;h3&gt;HEXFET Power MOSFET&lt;h3&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-P" x="0" y="0"/>
</gates>
<devices>
<device name="-TO252" package="TO252-DPAK">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SO8" package="SO8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ICM-20602" prefix="IC">
<description>&lt;h3&gt;TDK High Performance 6-Axis MEMS MotionTracking Device&lt;/h3&gt;
The ICM-20602 is a 6-axis MotionTracking device that
combines a 3-axis gyroscope, 3-axis accelerometer, in a small
3 mm x 3 mm x 0.75 mm (16-pin LGA) package.&lt;p&gt;
Communication ports include I2C and high speed SPI at 10 MHz.</description>
<gates>
<gate name="G$1" symbol="ICM-20602" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA-16">
<connects>
<connect gate="G$1" pin="CS" pad="5"/>
<connect gate="G$1" pin="FSYNC" pad="8"/>
<connect gate="G$1" pin="GND" pad="9 10 11 12 13 15"/>
<connect gate="G$1" pin="INT" pad="6"/>
<connect gate="G$1" pin="REGOUT" pad="14"/>
<connect gate="G$1" pin="SA0/SDO" pad="4"/>
<connect gate="G$1" pin="SCL/SPC" pad="2"/>
<connect gate="G$1" pin="SDA/SDI" pad="3"/>
<connect gate="G$1" pin="VDD" pad="16"/>
<connect gate="G$1" pin="VDDIO" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F030K" prefix="IC">
<description>&lt;h2&gt; STM32F030K6&lt;/h2&gt;
&lt;h3&gt;Value-line ARM®-based 32-bit MCU with 32-KB Flash,
timers, ADC, communication interfaces, 2.4-3.6 V operation&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Core: ARM 32-bit Cortex-M0 CPU, frequency up to 48 MHz &lt;/li&gt;
&lt;li&gt;Memories
&lt;ul&gt;
  &lt;li&gt;32 Kbytes of Flash memory&lt;/li&gt;
  &lt;li&gt;4 Kbytes of SRAM with HW parity&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Digital and I/Os supply: VDD = 2.4 V to 3.6 V&lt;ul&gt;
  &lt;li&gt;Analog supply: VDDA = VDD to 3.6 V&lt;/li&gt;
  &lt;li&gt;Power-on/Power down reset (POR/PDR)&lt;/li&gt;
  &lt;li&gt;Low power modes: Sleep, Stop, Standby&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;One 12-bit, 1.0 μs ADC (12 channels)&lt;ul&gt;
  &lt;li&gt;Conversion range: 0 to 3.6 V&lt;/li&gt;
  &lt;li&gt;Separate analog supply: 2.4 V to 3.6 V&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Nine timers&lt;ul&gt;
  &lt;li&gt;One 16-bit advanced-control timer for six channel PWM output&lt;/li&gt;
  &lt;li&gt;Four 16-bit timers, with up to four IC/OC, OCN, usable for IR control decoding&lt;/li&gt;
  &lt;li&gt;Independent and system watchdog timers&lt;/li&gt;
  &lt;li&gt;SysTick timer&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="STM32F030K" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP32-7X7">
<connects>
<connect gate="G$1" pin="!NRST!" pad="4"/>
<connect gate="G$1" pin="ADC1/PA1" pad="7"/>
<connect gate="G$1" pin="AVCC" pad="5"/>
<connect gate="G$1" pin="BOOT0" pad="31"/>
<connect gate="G$1" pin="CTS1/T1.4/PA11" pad="21"/>
<connect gate="G$1" pin="GND" pad="16"/>
<connect gate="G$1" pin="GND2" pad="32"/>
<connect gate="G$1" pin="MISO1/T3.1/T1.B/T16.1/ADC6/PA6" pad="12"/>
<connect gate="G$1" pin="MOSI1/T3.2/T14.1/T17.1/T1.1N/ADC7/PA7" pad="13"/>
<connect gate="G$1" pin="NSS1/T14.1/ADC4/PA4" pad="10"/>
<connect gate="G$1" pin="PB4/T3.1/MISO1" pad="27"/>
<connect gate="G$1" pin="PB5/T3.2/MOSI1" pad="28"/>
<connect gate="G$1" pin="PB6/T16.1N/SCL1/TX1" pad="29"/>
<connect gate="G$1" pin="PB7/T17.1N/SDA1/RX1" pad="30"/>
<connect gate="G$1" pin="PF0/XTAL" pad="2"/>
<connect gate="G$1" pin="PF1/XTAL" pad="3"/>
<connect gate="G$1" pin="RTS1/T1.E/PA12" pad="22"/>
<connect gate="G$1" pin="RX1/ADC3/PA3" pad="9"/>
<connect gate="G$1" pin="RX1/PA15" pad="25"/>
<connect gate="G$1" pin="RX1/SDA1/T1.3/PA10" pad="20"/>
<connect gate="G$1" pin="SCK1/ADC5/PA5" pad="11"/>
<connect gate="G$1" pin="SCK1/PB3" pad="26"/>
<connect gate="G$1" pin="SWCLK/TX1/PA14" pad="24"/>
<connect gate="G$1" pin="SWDIO/PA13" pad="23"/>
<connect gate="G$1" pin="T1.1/PA8" pad="18"/>
<connect gate="G$1" pin="T3.3/T1.2N/ADC8/PB0" pad="14"/>
<connect gate="G$1" pin="T3.4/T14.1/T1.3N/ADC9/PB1" pad="15"/>
<connect gate="G$1" pin="TX1/ADC2/PA2" pad="8"/>
<connect gate="G$1" pin="TX1/SCL1/T1.2/PA9" pad="19"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VCC2" pad="17"/>
<connect gate="G$1" pin="WKUP/ADC0/PA0" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RELAY-SPDT" prefix="K" uservalue="yes">
<gates>
<gate name="G$1" symbol="RELAY-K" x="-7.62" y="0"/>
<gate name="G$2" symbol="RELAY-V" x="-7.62" y="5.08"/>
</gates>
<devices>
<device name="ISO-MINI" package="RELAY-ISO-MINI-5">
<connects>
<connect gate="G$1" pin="1" pad="85"/>
<connect gate="G$1" pin="2" pad="86"/>
<connect gate="G$2" pin="C" pad="30"/>
<connect gate="G$2" pin="NC" pad="87A"/>
<connect gate="G$2" pin="NO" pad="87"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ISO-MICRO" package="RELAY-ISO-MICRO-5">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
<connect gate="G$2" pin="C" pad="3"/>
<connect gate="G$2" pin="NC" pad="4"/>
<connect gate="G$2" pin="NO" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RELAY-SPST" prefix="K" uservalue="yes">
<gates>
<gate name="G$1" symbol="RELAY-K" x="-7.62" y="0"/>
<gate name="G$2" symbol="RELAY-S" x="-7.62" y="7.62"/>
</gates>
<devices>
<device name="ISO-MINI" package="RELAY-ISO-MINI-4">
<connects>
<connect gate="G$1" pin="1" pad="85"/>
<connect gate="G$1" pin="2" pad="86"/>
<connect gate="G$2" pin="P" pad="30"/>
<connect gate="G$2" pin="S" pad="87"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ISO-MINI-5" package="RELAY-ISO-MINI-5">
<connects>
<connect gate="G$1" pin="1" pad="85"/>
<connect gate="G$1" pin="2" pad="86"/>
<connect gate="G$2" pin="P" pad="30"/>
<connect gate="G$2" pin="S" pad="87"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ISO-MICRO" package="RELAY-ISO-MICRO-4">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
<connect gate="G$2" pin="P" pad="3"/>
<connect gate="G$2" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ISO-MICRO-5" package="RELAY-ISO-MICRO-5">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
<connect gate="G$2" pin="P" pad="3"/>
<connect gate="G$2" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="F" uservalue="yes">
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="ATO" package="FUSE-AUTO">
<connects>
<connect gate="G$1" pin="1" pad="1A 1B 1C 1D"/>
<connect gate="G$1" pin="2" pad="2A 2B 2C 2D"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="+3V31" library="main" deviceset="+3V3" device=""/>
<part name="P+4" library="main" deviceset="+5V" device=""/>
<part name="SUPPLY1" library="main" deviceset="AGND" device=""/>
<part name="P+1" library="main" deviceset="AVDD" device=""/>
<part name="C1" library="main" deviceset="C" device="0402" value="100nF"/>
<part name="C3" library="main" deviceset="C" device="0603" value="100nF"/>
<part name="C5" library="main" deviceset="C" device="0805" value="100nF"/>
<part name="C7" library="main" deviceset="C" device="1206" value="100nF"/>
<part name="C9" library="main" deviceset="C" device="1210" value="100nF"/>
<part name="C2" library="main" deviceset="CPOL" device="SMCA" value="100uF 10V"/>
<part name="C4" library="main" deviceset="CPOL" device="SMCB" value="100uF 10v"/>
<part name="C6" library="main" deviceset="CPOL" device="SMCC" value="100uF 10V"/>
<part name="C8" library="main" deviceset="CPOL" device="SMCD" value="100uF 10V"/>
<part name="C10" library="main" deviceset="CPOL" device="SMCE" value="100uF 10V"/>
<part name="GND1" library="main" deviceset="GND" device=""/>
<part name="IC1" library="main" deviceset="LMV324?*" device="M"/>
<part name="IC3" library="main" deviceset="LMV324?*" device="MZ"/>
<part name="X81" library="main" deviceset="MICRO_SD_SOCKET" device="SD"/>
<part name="R1" library="main" deviceset="R" device="0402" value="10k"/>
<part name="R2" library="main" deviceset="R" device="0603" value="10k"/>
<part name="R3" library="main" deviceset="R" device="0805" value="10k"/>
<part name="R4" library="main" deviceset="R" device="1206" value="10k"/>
<part name="R5" library="main" deviceset="R" device="1210" value="10k"/>
<part name="IC7" library="main" deviceset="STM32F042F" device="" technology="6"/>
<part name="IC9" library="main" deviceset="STM32F405RG" device=""/>
<part name="IC10" library="main" deviceset="STM32F405VG" device=""/>
<part name="L1" library="main" deviceset="L" device="0402" value="1uH"/>
<part name="L4" library="main" deviceset="L" device="0603" value="1uH"/>
<part name="L5" library="main" deviceset="L" device="0805" value="1uH"/>
<part name="L8" library="main" deviceset="L" device="1206" value="10uH"/>
<part name="L9" library="main" deviceset="L" device="1210" value="1uH"/>
<part name="L2" library="main" deviceset="L" device="7.5X7.5" value="1uH 1A"/>
<part name="LED1" library="main" deviceset="LED" device="3MM"/>
<part name="LED3" library="main" deviceset="LED" device="5MM"/>
<part name="LED5" library="main" deviceset="LED" device="10MM"/>
<part name="LED7" library="main" deviceset="LED" device="2X5MM"/>
<part name="IC6" library="main" deviceset="STM32F030F" device="" technology="4"/>
<part name="IC2" library="main" deviceset="ULN2003A" device="D"/>
<part name="P+2" library="main" deviceset="V+" device=""/>
<part name="P+3" library="main" deviceset="VCC/2" device=""/>
<part name="OK1" library="main" deviceset="FODM30*" device="" technology="53"/>
<part name="IC5" library="main" deviceset="ST485" device="D" technology="B"/>
<part name="LED2" library="main" deviceset="LED" device="0603"/>
<part name="LED4" library="main" deviceset="LED" device="0805"/>
<part name="LED6" library="main" deviceset="LED" device="1206"/>
<part name="LED8" library="main" deviceset="LED" device="1210"/>
<part name="D1" library="main" deviceset="DIODE" device="-SMA" value="S1M"/>
<part name="D4" library="main" deviceset="DIODE" device="-SMB"/>
<part name="D7" library="main" deviceset="DIODE" device="-SMC"/>
<part name="D10" library="main" deviceset="DIODE" device="-SOD323"/>
<part name="D13" library="main" deviceset="DIODE" device="-SOD123" value="S1M"/>
<part name="D16" library="main" deviceset="DIODE" device="-SOT23" value="BAV99"/>
<part name="D2" library="main" deviceset="DIODE-SCHOTTKY" device="-SMA" value="SS14"/>
<part name="D5" library="main" deviceset="DIODE-SCHOTTKY" device="-SMB" value="SS26"/>
<part name="D8" library="main" deviceset="DIODE-SCHOTTKY" device="-SMC" value="SS34"/>
<part name="D11" library="main" deviceset="DIODE-SCHOTTKY" device="-SOD123"/>
<part name="D14" library="main" deviceset="DIODE-SCHOTTKY" device="-SOD323" value="BAT760"/>
<part name="D17" library="main" deviceset="DIODE-SCHOTTKY" device="-SOT23"/>
<part name="D3" library="main" deviceset="DIODE-ZENNER" device="-SMA"/>
<part name="D6" library="main" deviceset="DIODE-ZENNER" device="-SMB"/>
<part name="D9" library="main" deviceset="DIODE-ZENNER" device="-SMC"/>
<part name="D12" library="main" deviceset="DIODE-ZENNER" device="-SOD123"/>
<part name="D15" library="main" deviceset="DIODE-ZENNER" device="-SOD323"/>
<part name="D18" library="main" deviceset="DIODE-ZENNER" device="-SOT23"/>
<part name="L6" library="main" deviceset="L" device="10.1X10.1" value="3.3uH 2A"/>
<part name="L10" library="main" deviceset="L" device="12.3X12.3" value="10uH 5A"/>
<part name="L3" library="main" deviceset="L" device="D3.5" value="1uH"/>
<part name="L7" library="main" deviceset="L" device="D5.8" value="1uH 0.2A"/>
<part name="L11" library="main" deviceset="L" device="D7.8" value="3uH 0.2A"/>
<part name="SJ2" library="main" deviceset="SJ" device=""/>
<part name="SJ6" library="main" deviceset="SJ" device="M"/>
<part name="SJ9" library="main" deviceset="SJ" device="M-NC"/>
<part name="SJ1" library="main" deviceset="SJ2" device=""/>
<part name="SJ3" library="main" deviceset="SJ2" device="M"/>
<part name="SJ4" library="main" deviceset="SJ2" device="1-2"/>
<part name="SJ7" library="main" deviceset="SJ2" device="1-2-3"/>
<part name="SJ10" library="main" deviceset="SJ2" device="2-3"/>
<part name="U3" library="main" deviceset="SHORT" device="-6"/>
<part name="U4" library="main" deviceset="SHORT" device="-16"/>
<part name="SJ5" library="main" deviceset="SJ2" device="M1-2"/>
<part name="SJ8" library="main" deviceset="SJ2" device="M1-2-3"/>
<part name="SJ11" library="main" deviceset="SJ2" device="M2-3"/>
<part name="IC4" library="main" deviceset="TPS736XX" device="DVB"/>
<part name="Q9" library="main" deviceset="CRYSTAL" device="3225" value="8MHz"/>
<part name="Q10" library="main" deviceset="CRYSTAL" device="HC49S" value="8MHz"/>
<part name="Q11" library="main" deviceset="CRYSTAL" device="HC49UP" value="8MHz"/>
<part name="IC12" library="main" deviceset="DS2482-100" device=""/>
<part name="IC11" library="main" deviceset="ACS712" device="" technology="-05"/>
<part name="IC8" library="main" deviceset="STM32F042G" device="" technology="6"/>
<part name="BTN1" library="main" deviceset="RGBBTN-C+" device="19MM" value="BUTTON-19mm"/>
<part name="Q3" library="main" deviceset="BIPOLAR-PNP" device="-SOT23"/>
<part name="Q5" library="main" deviceset="BIPOLAR-PNP" device="-SOT323"/>
<part name="Q7" library="main" deviceset="BIPOLAR-PNP" device="-TO252"/>
<part name="Q1" library="main" deviceset="MOSFET-N" device="-SO8"/>
<part name="Q2" library="main" deviceset="MOSFET-N" device="-SOT23"/>
<part name="Q4" library="main" deviceset="MOSFET-N" device="-SOT323"/>
<part name="Q6" library="main" deviceset="MOSFET-N" device="-TO252"/>
<part name="Q8" library="main" deviceset="MOSFET-N" device="-TO263"/>
<part name="OK2" library="main" deviceset="OPTOCOUPLER-4" device="" value="PS2801-4"/>
<part name="M1" library="main" deviceset="DCDC-BUCK" device="-MINI360" value="Mini360-5V"/>
<part name="M2" library="main" deviceset="DCDC-BUCK" device="" value="DC/DC 12V"/>
<part name="U1" library="main" deviceset="DCDC" device="-SIP4" value="B1212"/>
<part name="U5" library="main" deviceset="DCDC" device="-SIP7" value="AM1D-2424D"/>
<part name="U2" library="main" deviceset="DCDC-DUAL" device="-SIP7" value="P10CU-0512ZLF"/>
<part name="C12" library="main" deviceset="C" device="-C075-063X106" value="1uF x 63V"/>
<part name="C11" library="main" deviceset="C" device="-C075_105-063X106_133" value="1uF x 63V"/>
<part name="X1" library="main" deviceset="CON01-1" device=""/>
<part name="X7" library="main" deviceset="CON01-2" device=""/>
<part name="X13" library="main" deviceset="CON01-2" device="-MF-H"/>
<part name="X19" library="main" deviceset="CON01-2" device="-MF-HP"/>
<part name="X25" library="main" deviceset="CON01-2" device="-MF-V"/>
<part name="X31" library="main" deviceset="CON01-2" device="-MF-VP"/>
<part name="X38" library="main" deviceset="CON02-1" device=""/>
<part name="X45" library="main" deviceset="CON02-1" device="-1MM-HS"/>
<part name="X52" library="main" deviceset="CON02-1" device="-1MM-VS"/>
<part name="X59" library="main" deviceset="CON02-1" device="-2MM-H"/>
<part name="X66" library="main" deviceset="CON02-1" device="-2MM-HS"/>
<part name="X73" library="main" deviceset="CON02-1" device="-2MM-V"/>
<part name="X80" library="main" deviceset="CON02-1" device="-2MM-VS"/>
<part name="X6" library="main" deviceset="CON02-2" device=""/>
<part name="X12" library="main" deviceset="CON02-2" device="-MF-H"/>
<part name="X18" library="main" deviceset="CON02-2" device="-MF-HP"/>
<part name="X24" library="main" deviceset="CON02-2" device="-MF-V"/>
<part name="X30" library="main" deviceset="CON02-2" device="-MF-VP"/>
<part name="X37" library="main" deviceset="CON03-1" device=""/>
<part name="X44" library="main" deviceset="CON03-1" device="-1MM-HS"/>
<part name="X51" library="main" deviceset="CON03-1" device="-1MM-VS"/>
<part name="X58" library="main" deviceset="CON03-1" device="-2MM-H"/>
<part name="X65" library="main" deviceset="CON03-1" device="-2MM-HS"/>
<part name="X72" library="main" deviceset="CON03-1" device="-2MM-V"/>
<part name="X79" library="main" deviceset="CON03-1" device="-2MM-VS"/>
<part name="X5" library="main" deviceset="CON03-2" device=""/>
<part name="X11" library="main" deviceset="CON03-2" device="-MF-H"/>
<part name="X17" library="main" deviceset="CON03-2" device="-MF-HP"/>
<part name="X23" library="main" deviceset="CON03-2" device="-MF-V"/>
<part name="X29" library="main" deviceset="CON03-2" device="-MF-VP"/>
<part name="X36" library="main" deviceset="CON04-1" device=""/>
<part name="X43" library="main" deviceset="CON04-1" device="-1MM-HS"/>
<part name="X50" library="main" deviceset="CON04-1" device="-1MM-VS"/>
<part name="X57" library="main" deviceset="CON04-1" device="-2MM-H"/>
<part name="X64" library="main" deviceset="CON04-1" device="-2MM-HS"/>
<part name="X71" library="main" deviceset="CON04-1" device="-2MM-V"/>
<part name="X78" library="main" deviceset="CON04-1" device="-2MM-VS"/>
<part name="X4" library="main" deviceset="CON04-2" device=""/>
<part name="X10" library="main" deviceset="CON04-2" device="-MF-H"/>
<part name="X16" library="main" deviceset="CON04-2" device="-MF-HP"/>
<part name="X22" library="main" deviceset="CON04-2" device="-MF-V"/>
<part name="X28" library="main" deviceset="CON04-2" device="-MF-VP"/>
<part name="X35" library="main" deviceset="CON05-1" device=""/>
<part name="X42" library="main" deviceset="CON05-1" device="-1MM-HS"/>
<part name="X49" library="main" deviceset="CON05-1" device="-1MM-VS"/>
<part name="X56" library="main" deviceset="CON05-1" device="-2MM-H"/>
<part name="X63" library="main" deviceset="CON05-1" device="-2MM-HS"/>
<part name="X70" library="main" deviceset="CON05-1" device="-2MM-V"/>
<part name="X77" library="main" deviceset="CON05-1" device="-2MM-VS"/>
<part name="X3" library="main" deviceset="CON05-2" device=""/>
<part name="X9" library="main" deviceset="CON05-2" device="-MF-H"/>
<part name="X15" library="main" deviceset="CON05-2" device="-MF-HP"/>
<part name="X21" library="main" deviceset="CON05-2" device="-MF-V"/>
<part name="X27" library="main" deviceset="CON05-2" device="-MF-VP"/>
<part name="X34" library="main" deviceset="CON06-1" device=""/>
<part name="X41" library="main" deviceset="CON06-1" device="-1MM-HS"/>
<part name="X48" library="main" deviceset="CON06-1" device="-1MM-VS"/>
<part name="X55" library="main" deviceset="CON06-1" device="-2MM-H"/>
<part name="X62" library="main" deviceset="CON06-1" device="-2MM-HS"/>
<part name="X69" library="main" deviceset="CON06-1" device="-2MM-V"/>
<part name="X76" library="main" deviceset="CON06-1" device="-2MM-VS"/>
<part name="X2" library="main" deviceset="CON06-2" device=""/>
<part name="X8" library="main" deviceset="CON06-2" device="-MF-H"/>
<part name="X14" library="main" deviceset="CON06-2" device="-MF-HP"/>
<part name="X20" library="main" deviceset="CON06-2" device="-MF-V"/>
<part name="X26" library="main" deviceset="CON06-2" device="-MF-VP"/>
<part name="X33" library="main" deviceset="CON08-1" device=""/>
<part name="X40" library="main" deviceset="CON08-1" device="-1MM-HS"/>
<part name="X47" library="main" deviceset="CON08-1" device="-1MM-VS"/>
<part name="X54" library="main" deviceset="CON08-1" device="-2MM-H"/>
<part name="X61" library="main" deviceset="CON08-1" device="-2MM-HS"/>
<part name="X68" library="main" deviceset="CON08-1" device="-2MM-V"/>
<part name="X75" library="main" deviceset="CON08-1" device="-2MM-VS"/>
<part name="X32" library="main" deviceset="CON10-1" device=""/>
<part name="X39" library="main" deviceset="CON10-1" device="-1MM-HS"/>
<part name="X46" library="main" deviceset="CON10-1" device="-1MM-VS"/>
<part name="X53" library="main" deviceset="CON10-1" device="-2MM-H"/>
<part name="X60" library="main" deviceset="CON10-1" device="-2MM-HS"/>
<part name="X67" library="main" deviceset="CON10-1" device="-2MM-V"/>
<part name="X74" library="main" deviceset="CON10-1" device="-2MM-VS"/>
<part name="D19" library="main" deviceset="DIODE-ZENNER" device="-SOD523"/>
<part name="D20" library="main" deviceset="DIODE-SCHOTTKY" device="-SOD523" value="SS14"/>
<part name="D21" library="main" deviceset="DIODE" device="-SOD523" value="S1M"/>
<part name="Q12" library="main" deviceset="MOSFET-P" device="-SO8"/>
<part name="Q13" library="main" deviceset="MOSFET-P" device="-SOT23"/>
<part name="Q14" library="main" deviceset="MOSFET-P" device="-TO252"/>
<part name="BTN2" library="main" deviceset="RGBBTN-C+" device="22MM" value="BUTTON-22mm"/>
<part name="IC13" library="main" deviceset="ICM-20602" device=""/>
<part name="IC14" library="main" deviceset="STM32F030K" device=""/>
<part name="K1" library="main" deviceset="RELAY-SPDT" device="ISO-MINI"/>
<part name="K2" library="main" deviceset="RELAY-SPDT" device="ISO-MICRO"/>
<part name="K3" library="main" deviceset="RELAY-SPST" device="ISO-MINI"/>
<part name="K4" library="main" deviceset="RELAY-SPST" device="ISO-MICRO"/>
<part name="F1" library="main" deviceset="FUSE" device="ATO"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="+3V31" gate="G$1" x="22.86" y="83.82"/>
<instance part="P+4" gate="1" x="33.02" y="83.82"/>
<instance part="SUPPLY1" gate="G$1" x="5.08" y="73.66"/>
<instance part="P+1" gate="VCC" x="5.08" y="81.28"/>
<instance part="C1" gate="G$1" x="50.8" y="73.66"/>
<instance part="C3" gate="G$1" x="60.96" y="73.66"/>
<instance part="C5" gate="G$1" x="71.12" y="73.66"/>
<instance part="C7" gate="G$1" x="81.28" y="73.66"/>
<instance part="C9" gate="G$1" x="91.44" y="73.66"/>
<instance part="C2" gate="G$1" x="50.8" y="58.42"/>
<instance part="C4" gate="G$1" x="60.96" y="58.42"/>
<instance part="C6" gate="G$1" x="71.12" y="58.42"/>
<instance part="C8" gate="G$1" x="81.28" y="58.42"/>
<instance part="C10" gate="G$1" x="91.44" y="58.42"/>
<instance part="GND1" gate="1" x="22.86" y="73.66"/>
<instance part="IC1" gate="A" x="7.62" y="-55.88"/>
<instance part="IC1" gate="P" x="7.62" y="-55.88"/>
<instance part="IC3" gate="A" x="38.1" y="-55.88"/>
<instance part="IC3" gate="P" x="38.1" y="-55.88"/>
<instance part="X81" gate="G$1" x="208.28" y="134.62"/>
<instance part="R1" gate="G$1" x="50.8" y="88.9" rot="R90"/>
<instance part="R2" gate="G$1" x="60.96" y="88.9" rot="R90"/>
<instance part="R3" gate="G$1" x="71.12" y="88.9" rot="R90"/>
<instance part="R4" gate="G$1" x="81.28" y="88.9" rot="R90"/>
<instance part="R5" gate="G$1" x="91.44" y="88.9" rot="R90"/>
<instance part="IC7" gate="G$1" x="152.4" y="5.08"/>
<instance part="IC9" gate="G$1" x="231.14" y="45.72"/>
<instance part="IC10" gate="G$1" x="304.8" y="17.78"/>
<instance part="L1" gate="G$1" x="50.8" y="40.64"/>
<instance part="L4" gate="G$1" x="60.96" y="40.64"/>
<instance part="L5" gate="G$1" x="71.12" y="40.64"/>
<instance part="L8" gate="G$1" x="81.28" y="40.64"/>
<instance part="L9" gate="G$1" x="91.44" y="40.64"/>
<instance part="L2" gate="G$1" x="58.42" y="27.94" rot="R90"/>
<instance part="LED1" gate="G$1" x="2.54" y="35.56"/>
<instance part="LED3" gate="G$1" x="12.7" y="35.56"/>
<instance part="LED5" gate="G$1" x="22.86" y="35.56"/>
<instance part="LED7" gate="G$1" x="33.02" y="35.56"/>
<instance part="IC6" gate="G$1" x="152.4" y="73.66"/>
<instance part="IC2" gate="A" x="12.7" y="-30.48"/>
<instance part="P+2" gate="1" x="22.86" y="91.44"/>
<instance part="P+3" gate="G$1" x="33.02" y="91.44"/>
<instance part="OK1" gate="G$1" x="40.64" y="-30.48"/>
<instance part="IC5" gate="G$1" x="96.52" y="-10.16"/>
<instance part="LED2" gate="G$1" x="2.54" y="25.4"/>
<instance part="LED4" gate="G$1" x="12.7" y="25.4"/>
<instance part="LED6" gate="G$1" x="22.86" y="25.4"/>
<instance part="LED8" gate="G$1" x="33.02" y="25.4"/>
<instance part="D1" gate="G$1" x="2.54" y="15.24"/>
<instance part="D4" gate="G$1" x="15.24" y="15.24"/>
<instance part="D7" gate="G$1" x="27.94" y="15.24"/>
<instance part="D10" gate="G$1" x="40.64" y="15.24"/>
<instance part="D13" gate="G$1" x="53.34" y="15.24"/>
<instance part="D16" gate="G$1" x="63.5" y="15.24"/>
<instance part="D2" gate="G$1" x="2.54" y="10.16"/>
<instance part="D5" gate="G$1" x="15.24" y="10.16"/>
<instance part="D8" gate="G$1" x="27.94" y="10.16"/>
<instance part="D11" gate="G$1" x="40.64" y="10.16"/>
<instance part="D14" gate="G$1" x="53.34" y="10.16"/>
<instance part="D17" gate="G$1" x="63.5" y="10.16"/>
<instance part="D3" gate="G$1" x="2.54" y="5.08"/>
<instance part="D6" gate="G$1" x="15.24" y="5.08"/>
<instance part="D9" gate="G$1" x="27.94" y="5.08"/>
<instance part="D12" gate="G$1" x="40.64" y="5.08"/>
<instance part="D15" gate="G$1" x="53.34" y="5.08"/>
<instance part="D18" gate="G$1" x="63.5" y="5.08"/>
<instance part="L6" gate="G$1" x="76.2" y="27.94" rot="R90"/>
<instance part="L10" gate="G$1" x="93.98" y="27.94" rot="R90"/>
<instance part="L3" gate="G$1" x="58.42" y="22.86" rot="R90"/>
<instance part="L7" gate="G$1" x="76.2" y="22.86" rot="R90"/>
<instance part="L11" gate="G$1" x="93.98" y="22.86" rot="R90"/>
<instance part="SJ2" gate="1" x="5.08" y="63.5"/>
<instance part="SJ6" gate="1" x="17.78" y="63.5"/>
<instance part="SJ9" gate="1" x="30.48" y="63.5"/>
<instance part="SJ1" gate="G$1" x="2.54" y="55.88"/>
<instance part="SJ3" gate="G$1" x="5.08" y="45.72"/>
<instance part="SJ4" gate="G$1" x="12.7" y="55.88"/>
<instance part="SJ7" gate="G$1" x="22.86" y="55.88"/>
<instance part="SJ10" gate="G$1" x="33.02" y="55.88"/>
<instance part="U3" gate="G$1" x="10.16" y="76.2"/>
<instance part="U4" gate="G$1" x="15.24" y="76.2"/>
<instance part="SJ5" gate="G$1" x="15.24" y="45.72"/>
<instance part="SJ8" gate="G$1" x="25.4" y="45.72"/>
<instance part="SJ11" gate="G$1" x="35.56" y="45.72"/>
<instance part="IC4" gate="G$1" x="96.52" y="10.16"/>
<instance part="Q9" gate="G$1" x="48.26" y="-7.62"/>
<instance part="Q10" gate="G$1" x="60.96" y="-7.62"/>
<instance part="Q11" gate="G$1" x="73.66" y="-7.62"/>
<instance part="IC12" gate="G$1" x="358.14" y="55.88"/>
<instance part="IC11" gate="G$1" x="358.14" y="86.36"/>
<instance part="IC8" gate="G$1" x="152.4" y="-50.8"/>
<instance part="BTN1" gate="G$1" x="35.56" y="-12.7" rot="R270"/>
<instance part="Q3" gate="G$1" x="-27.94" y="-25.4"/>
<instance part="Q5" gate="G$1" x="-20.32" y="-25.4"/>
<instance part="Q7" gate="G$1" x="-12.7" y="-25.4"/>
<instance part="Q1" gate="G$1" x="-45.72" y="-43.18"/>
<instance part="Q2" gate="G$1" x="-35.56" y="-43.18"/>
<instance part="Q4" gate="G$1" x="-25.4" y="-43.18"/>
<instance part="Q6" gate="G$1" x="-15.24" y="-43.18"/>
<instance part="Q8" gate="G$1" x="-5.08" y="-43.18"/>
<instance part="OK2" gate="A" x="63.5" y="-30.48"/>
<instance part="M1" gate="G$1" x="0" y="-83.82"/>
<instance part="M2" gate="G$1" x="27.94" y="-83.82"/>
<instance part="U1" gate="G$1" x="0" y="-104.14"/>
<instance part="U5" gate="G$1" x="27.94" y="-104.14"/>
<instance part="U2" gate="G$1" x="0" y="-124.46"/>
<instance part="C12" gate="G$1" x="101.6" y="71.12"/>
<instance part="C11" gate="G$1" x="101.6" y="81.28"/>
<instance part="X1" gate="G$1" x="7.62" y="106.68"/>
<instance part="X7" gate="G$1" x="20.32" y="106.68"/>
<instance part="X13" gate="G$1" x="38.1" y="106.68"/>
<instance part="X19" gate="G$1" x="55.88" y="106.68"/>
<instance part="X25" gate="G$1" x="73.66" y="106.68"/>
<instance part="X31" gate="G$1" x="91.44" y="106.68"/>
<instance part="X38" gate="G$1" x="111.76" y="119.38"/>
<instance part="X45" gate="G$1" x="124.46" y="119.38"/>
<instance part="X52" gate="G$1" x="137.16" y="119.38"/>
<instance part="X59" gate="G$1" x="149.86" y="119.38"/>
<instance part="X66" gate="G$1" x="162.56" y="119.38"/>
<instance part="X73" gate="G$1" x="175.26" y="119.38"/>
<instance part="X80" gate="G$1" x="187.96" y="119.38"/>
<instance part="X6" gate="G$1" x="20.32" y="119.38"/>
<instance part="X12" gate="G$1" x="38.1" y="119.38"/>
<instance part="X18" gate="G$1" x="55.88" y="119.38"/>
<instance part="X24" gate="G$1" x="73.66" y="119.38"/>
<instance part="X30" gate="G$1" x="91.44" y="119.38"/>
<instance part="X37" gate="G$1" x="111.76" y="132.08"/>
<instance part="X44" gate="G$1" x="124.46" y="132.08"/>
<instance part="X51" gate="G$1" x="137.16" y="132.08"/>
<instance part="X58" gate="G$1" x="149.86" y="132.08"/>
<instance part="X65" gate="G$1" x="162.56" y="132.08"/>
<instance part="X72" gate="G$1" x="175.26" y="132.08"/>
<instance part="X79" gate="G$1" x="187.96" y="132.08"/>
<instance part="X5" gate="G$1" x="20.32" y="132.08"/>
<instance part="X11" gate="G$1" x="38.1" y="132.08"/>
<instance part="X17" gate="G$1" x="55.88" y="132.08"/>
<instance part="X23" gate="G$1" x="73.66" y="132.08"/>
<instance part="X29" gate="G$1" x="91.44" y="132.08"/>
<instance part="X36" gate="G$1" x="111.76" y="149.86"/>
<instance part="X43" gate="G$1" x="124.46" y="149.86"/>
<instance part="X50" gate="G$1" x="137.16" y="149.86"/>
<instance part="X57" gate="G$1" x="149.86" y="149.86"/>
<instance part="X64" gate="G$1" x="162.56" y="149.86"/>
<instance part="X71" gate="G$1" x="175.26" y="149.86"/>
<instance part="X78" gate="G$1" x="187.96" y="149.86"/>
<instance part="X4" gate="G$1" x="20.32" y="149.86"/>
<instance part="X10" gate="G$1" x="38.1" y="149.86"/>
<instance part="X16" gate="G$1" x="55.88" y="149.86"/>
<instance part="X22" gate="G$1" x="73.66" y="149.86"/>
<instance part="X28" gate="G$1" x="91.44" y="149.86"/>
<instance part="X35" gate="G$1" x="111.76" y="167.64"/>
<instance part="X42" gate="G$1" x="124.46" y="167.64"/>
<instance part="X49" gate="G$1" x="137.16" y="167.64"/>
<instance part="X56" gate="G$1" x="149.86" y="167.64"/>
<instance part="X63" gate="G$1" x="162.56" y="167.64"/>
<instance part="X70" gate="G$1" x="175.26" y="167.64"/>
<instance part="X77" gate="G$1" x="187.96" y="167.64"/>
<instance part="X3" gate="G$1" x="20.32" y="167.64"/>
<instance part="X9" gate="G$1" x="38.1" y="167.64"/>
<instance part="X15" gate="G$1" x="55.88" y="167.64"/>
<instance part="X21" gate="G$1" x="73.66" y="167.64"/>
<instance part="X27" gate="G$1" x="91.44" y="167.64"/>
<instance part="X34" gate="G$1" x="111.76" y="190.5"/>
<instance part="X41" gate="G$1" x="124.46" y="190.5"/>
<instance part="X48" gate="G$1" x="137.16" y="190.5"/>
<instance part="X55" gate="G$1" x="149.86" y="190.5"/>
<instance part="X62" gate="G$1" x="162.56" y="190.5"/>
<instance part="X69" gate="G$1" x="175.26" y="190.5"/>
<instance part="X76" gate="G$1" x="187.96" y="190.5"/>
<instance part="X2" gate="G$1" x="20.32" y="190.5"/>
<instance part="X8" gate="G$1" x="38.1" y="190.5"/>
<instance part="X14" gate="G$1" x="55.88" y="190.5"/>
<instance part="X20" gate="G$1" x="73.66" y="190.5"/>
<instance part="X26" gate="G$1" x="91.44" y="190.5"/>
<instance part="X33" gate="G$1" x="111.76" y="215.9"/>
<instance part="X40" gate="G$1" x="124.46" y="215.9"/>
<instance part="X47" gate="G$1" x="137.16" y="215.9"/>
<instance part="X54" gate="G$1" x="149.86" y="215.9"/>
<instance part="X61" gate="G$1" x="162.56" y="215.9"/>
<instance part="X68" gate="G$1" x="175.26" y="215.9"/>
<instance part="X75" gate="G$1" x="187.96" y="215.9"/>
<instance part="X32" gate="G$1" x="111.76" y="246.38"/>
<instance part="X39" gate="G$1" x="124.46" y="246.38"/>
<instance part="X46" gate="G$1" x="137.16" y="246.38"/>
<instance part="X53" gate="G$1" x="149.86" y="246.38"/>
<instance part="X60" gate="G$1" x="162.56" y="246.38"/>
<instance part="X67" gate="G$1" x="175.26" y="246.38"/>
<instance part="X74" gate="G$1" x="187.96" y="246.38"/>
<instance part="D19" gate="G$1" x="73.66" y="5.08"/>
<instance part="D20" gate="G$1" x="73.66" y="10.16"/>
<instance part="D21" gate="G$1" x="73.66" y="15.24"/>
<instance part="Q12" gate="G$1" x="-45.72" y="-10.16" rot="MR180"/>
<instance part="Q13" gate="G$1" x="-35.56" y="-10.16" rot="MR180"/>
<instance part="Q14" gate="G$1" x="-25.4" y="-10.16" rot="MR180"/>
<instance part="BTN2" gate="G$1" x="17.78" y="-12.7" rot="R270"/>
<instance part="IC13" gate="G$1" x="96.52" y="-35.56"/>
<instance part="IC14" gate="G$1" x="231.14" y="-45.72"/>
<instance part="K1" gate="G$1" x="-43.18" y="7.62"/>
<instance part="K2" gate="G$1" x="-27.94" y="7.62"/>
<instance part="K2" gate="G$2" x="-27.94" y="15.24"/>
<instance part="K3" gate="G$1" x="-43.18" y="27.94"/>
<instance part="K3" gate="G$2" x="-43.18" y="35.56"/>
<instance part="K4" gate="G$1" x="-27.94" y="27.94"/>
<instance part="K4" gate="G$2" x="-27.94" y="35.56"/>
<instance part="F1" gate="G$1" x="-43.18" y="45.72"/>
<instance part="K1" gate="G$2" x="-43.18" y="15.24"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
