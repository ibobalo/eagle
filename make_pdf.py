from __future__ import print_function
import os
from os.path import isfile
from glob import glob
from subprocess import call
from zipfile import ZipFile

"""
Create pdf file from PCBs gerbers for preview
Create zip file with gerbers for upload to pcb manufacture site
Required:
	https://sourceforge.net/projects/gerber2pdf/files/latest/download
	https://sourceforge.net/projects/gerber2pdf/files/Drill2Gerber/Drill2Gerber.exe/download
"""

REQUIRED = [
	(1, ['GML','TXT','GTL']),
	(2, ['GBL']),
	(4, ['G1L','G2L']),
	(6, ['G3L','G4L']),
]
OPTIONAL = ['DRN', 'MLI', 'MLN', 'DSI', 'GPI', 'GTO', 'GTP', 'GBO', 'GBP']
PRESETS = {
'PcbPrint': {
	'GML':'Board.Gerber',
	'TXT':'DrillPlated.NC',
	'DRN':'DrillNoPlated.NC',
	'MLI':'MillPlated.Gerber',
	'MLN':'MillNoPlated.Gerber',
#	'DSI':'DrillStationInfo',
#	'GPI':'PhotoplotterInfo',
	'GTL':'Top.Gerber',
	'GTS':'TopMask.Gerber',
	'GTO':'TopSilk.Gerber',
	'GTP':'TopPaste.Gerber',
	'GBL':'Bot.Gerber',
	'GBS':'BotMask.Gerber',
	'GBO':'BotSilk.Gerber',
	'GBP':'BotPaste.Gerber',
	'G1L':'Inner1.Gerber',
	'G2L':'Inner2.Gerber',
	'G3L':'Inner3.Gerber',
	'G4L':'Inner4.Gerber',
},
'jlcpcb': {
	'GML':'gko',
	'TXT':'xln',
#	'DSI':'dri',
#	'GPI':'gpi',
#	'DRN':'drn',
#	'MLI':'mli',
#	'MLN':'mln',
	'GTL':'gtl',
	'GTS':'gts',
	'GTO':'gto',
	'GTP':'gtp',
	'GBL':'gbl',
	'GBS':'gbs',
	'GBO':'gbo',
	'GBP':'gbp',
	'G1L':'g2l',
	'G2L':'g3l',
	'G3L':'g4l',
	'G4L':'g5l',
},
# SmartPrototypes, SparkFun
'SmartPrototypes': {
	'GML':'gml', # Mill
	'TXT':'txt',
#	'DSI':'dri',
#	'GPI':'gpi',
#	'DRN':'drn',
#	'MLI':'mli',
#	'MLN':'mln',
	'GTL':'gtl',
	'GTS':'gts',
	'GTO':'gto',
	'GTP':'gtp',
	'GBL':'gbl',
	'GBS':'gbs',
	'GBO':'gbo',
	'GBP':'gbp',
	'G1L':'gl2',
	'G2L':'gl3',
	'G3L':'gl4',
	'G4L':'gl5',
},
'SeeedStudio': {
	'GML':'gko', # keepout
	'TXT':'txt',
#	'DSI':'dri',
#	'GPI':'gpi',
#	'DRN':'drn',
#	'MLI':'mli',
#	'MLN':'mln',
	'GTL':'gtl',
	'GTS':'gts',
	'GTO':'gto',
	'GTP':'gtp',
	'GBL':'gbl',
	'GBS':'gbs',
	'GBO':'gbo',
	'GBP':'gbp',
	'G1L':'gl2',
	'G2L':'gl3',
	'G3L':'gl4',
	'G4L':'gl5',
},
}

def log(*args, **kwargs):
	print(*args, **kwargs)

def process_all(preset):
	log('PRESET: ', preset, PRESETS[preset]['GML'])
	for f in glob("*." + PRESETS[preset]['GML']):
		process(f[:-len(PRESETS[preset]['GML'])-1], preset)

def process(f, preset):
	log('processing:', f)
	if not process_pdf(f, preset): return False
	for targetPreset in PRESETS.keys():
		if not targetPreset[0] == '.':
			process_zip(f, preset, targetPreset)
	return True

def generate_negative(f, fneg):
	o = open(fneg, 'w')
	o.write('%IPNEG*%\n');
	for line in open(f):
		if not '%IPPOS*%' in line:
			o.write(line + '\n')

def remove_temp_files(l):
	for f in l:
		os.remove(f)

def layer(f, r, g, b, a=None):
	if a is None:
		return ['-colour={},{},{}'.format(r,g,b), f]
	return ['-colour={},{},{},{}'.format(r,g,b,a), f]

def detect_sides(f, preset):
	sides = 0
	for s, req_ids in REQUIRED:
		for id in req_ids:
			thefile = f + '.' + PRESETS[preset][id]
			if not isfile(thefile):
				if sides == 0:
					log("file {} missed. aborted".format(thefile))
				else:
					log("file {} missed. {} sides detected".format(thefile, sides))
				return sides
		sides = s
	return sides

def process_pdf(f, preset):
	ex = PRESETS[preset]
	sides = detect_sides(f, preset)
	if not sides:
		return False
	temp_files = []
	try:
# !Drill2Gerber is not compatible with Eagle
#		if call(['Drill2Gerber', f + '.' + ex['TXT']]) == 0:
#			temp_files.append(f + '.' + ex['TXT'] + '.grb')
#		else:
#			log('Failed to convert DRILL to gerber.')
		for e in (ex['GML'], ex['GTS'], ex['GBS']):
			if isfile(f + '.' + e):
				fneg = f + '.negative.' + e
				generate_negative(f + '.' + e, fneg)
				temp_files.append(fneg)
		p = ['Gerber2pdf', '-nowarnings' ,'-output={f}', '-silentexit']
		if sides >= 1:
			#page TOP VIEW
			p += ['-nomirror','-combine', '-background=64,64,255']
			p +=   layer('{f}.negative.{GML}', 0,64,0)
			p +=   layer('{f}.{GTS}',          0,0,0)             if isfile(f + '.' + ex['GTS']) else []
			p +=   layer('{f}.{GTL}',          255,224,192,128)
			p +=   layer('{f}.{GTS}',          255,196,255,48)    if isfile(f + '.' + ex['GTS']) else []
			p +=   layer('{f}.{GTO}',          255,255,255)       if isfile(f + '.' + ex['GTO']) else []
		if sides >= 2:
			#page BOT VIEW
			p += ['-mirror','-combine', '-background=64,64,255']
			p +=   layer('{f}.negative.{GML}', 0,64,0)
			p +=   layer('{f}.{GBS}',          0,0,0)            if isfile(f + '.' + ex['GBS']) else []
			p +=   layer('{f}.{GBL}',          255,224,192,128)
			p +=   layer('{f}.{GBS}',          255,196,255,48)   if isfile(f + '.' + ex['GBS']) else []
			p +=   layer('{f}.{GBO}',          255,255,255)      if isfile(f + '.' + ex['GBO']) else []
		if sides >= 4:
			#page INNER1 VIEW
			p += ['-nomirror','-combine', '-background=64,64,255']
			p +=   layer('{f}.negative.{GML}', 0,0,0)
			p +=   layer('{f}.{G1L}',          255,224,192,128)
			#page INNER2 VIEW
			p += ['-nomirror','-combine', '-background=64,64,255']
			p +=   layer('{f}.negative.{GML}', 0,0,0)
			p +=   layer('{f}.{G2L}',          255,224,192,128)
		if isfile(f + '.' + ex['GML']):
			#page OUTLINE BORDERS
			p += ['-nomirror', '-nocombine', '-background=255,255,255']
			p +=   layer('{f}.{GML}',          0,0,0)
		if 'MLI' in ex and isfile(f + '.' + ex['MLI']):
			#page SLOTS PLATED
			p += ['-nomirror', '-combine', '-background=255,255,255']
			p +=   layer('{f}.negative.{GML}', 0,96,0)
			p +=   layer('{f}.{MLI}',          255,255,0)
		if 'MLN' in ex and isfile(f + '.' + ex['MLN']):
			#page SLOTS NOT PLATED
			p += ['-nomirror', '-combine', '-background=255,255,255']
			p +=   layer('{f}.negative.{GML}', 0,96,0)
			p +=   layer('{f}.{MLN}',          0,0,0)
		if isfile(f + '.' + ex['GTL']):
			#page TOP COPPER
			p += ['-nomirror', '-nocombine']
			p +=   layer('{f}.{GTL}',          196,0,0)
		if isfile(f + '.' + ex['GTS']):
			#page TOP MASK
			p += ['-nomirror', '-combine']
			p +=   layer('{f}.negative.{GML}', 0,96,0)
			p +=   layer('{f}.{GTS}',          0,0,0)
		if isfile(f + '.' + ex['GTO']):
			#page TOP SILK
			p += ['-nomirror', '-combine']
			p +=   layer('{f}.negative.{GML}', 0,96,0)
			p +=   layer('{f}.{GTO}',          255,255,255)
		if isfile(f + '.' + ex['GTP']):
			#page TOP PASTE
			p += ['-nomirror', '-nocombine']
			p +=   layer('{f}.{GTP}',          196,0,0)
		if isfile(f + '.' + ex['GBL']):
			#page BOT COPPER
			p += ['-mirror', '-nocombine']
			p +=   layer('{f}.{GBL}',          0,0,128)
		if isfile(f + '.' + ex['GBS']):
			#page BOT MASK
			p += ['-mirror', '-combine']
			p +=   layer('{f}.negative.{GML}', 0,96,0)
			p +=   layer('{f}.{GBS}',          0,0,0)
		if isfile(f + '.' + ex['GBO']):
			#page BOT SILK
			p += ['-mirror', '-combine']
			p +=   layer('{f}.negative.{GML}', 0,96,0)
			p +=   layer('{f}.{GBO}',          255,255,255)
		if isfile(f + '.' + ex['GBP']):
			#page BOT PASTE
			p += ['-mirror', '-nocombine']
			p +=   layer('{f}.{GBP}',          196,0,0)
		for a in ['G{}L'.format(n) for n in range(1,16)]:
			if a in ex and isfile(f + '.' + ex[a]):
				#page INNER 1 COPPER
				p += ['-nomirror', '-nocombine']
				p +=   layer('{f}.{'+a+'}',    196,0,0)
		if isfile(f + '.' + ex['TXT'] + '.grb'):
			#page DRILLS
			p += ['-nomirror', '-combine']
			p +=   layer('{f}.{GML}',          0,0,0)
			p +=   layer('{f}.{TXT}.grb',      0,0,0)
		if call([s.format(f=f, **ex) for s in p]) != 0:
			log('Failed to create pdf.')
			return False
		log('Created pdf ', f, ' from ', preset)
		call(['start', f + '.pdf'], shell=True)
	finally:
		remove_temp_files(temp_files)
	return True

def process_zip(f, preset, targetPreset = None):
	log('Zipping {} for {}'.format(f, targetPreset))
	if targetPreset is None: targetPreset = preset
	with ZipFile(f + '.' + targetPreset + '.zip', 'w') as zip:
		for id, ext in PRESETS[targetPreset].items():
			if id in OPTIONAL and id not in PRESETS[preset]:
				continue
			f_org = f + '.' + PRESETS[preset][id]
			f_tgt = f + '.' + ext
			if isfile(f_org):
#				print('  ', f_org, ' -> ', f_tgt)
				zip.write(f_org, f_tgt)
	return True

if __name__ == '__main__':
	for preset in PRESETS.keys():
		process_all(preset)
