#usage "<b>Switch displayed layers</b>\n"
       "<p>"
       "This program switch displayed layers TOP <-> BOT<p>"
       "supposed to be asigned to F1 key:"
       "ASSIGN F1 RUN view-sw; ASSIGN F2 RUN view-sw M;"
       "<p>"
       "<author>Ihor Bobalo</author>"

//////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2018, Ihor Bobalo
// 
// REVISION HISTORY:
//
// v1.00 - 16/03/2018 - Initial Release
//
//////////////////////////////////////////////////////////////////////////////
//
// THIS PROGRAM IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESSED OR IMPLIED. IF YOU DON'T LIKE IT, DON'T USE IT!
//
//////////////////////////////////////////////////////////////////////////////

string Version = "1.0";

string MODES = "dspcu"; //'c'-copper, 'p'-placement 's'-silk, 'd'-doc, 'u'-unknown
string MODEDESCS[] = {
// mode : unique levels : visible levels : hidden levels
//   # mean all used copper layers : top, inners, bottom
    "Document"
        ":Document,Measures,tDocu,bDocu,tValues,bValues"
        ":Dimention,Milling,tNames,bNames"
        ":#,Pads,Vias,Unrouted,tOrigins,bOrigins,tPlace,bPlace,tKeepout,bKeepout,tStop,bStop,tRestrict,bRestrict,vRestrict",
    "Silk"
        ":tNames,bNames"
        ":Dimention,Milling,tPlace,bPlace,tOrigins,bOrigins,tStop,bStop"
        ":#,Pads,Vias,Unrouted,tKeepout,bKeepout",
    "Placement"
        ":tKeepout,bKeepout"
        ":#,Pads,Vias,Unrouted,Dimention,Milling,tPlace,bPlace,tOrigins,bOrigins"
        ":tValues,bValues,tStop,bStop",
    "Copper"
        ":#,Pads,Vias"
        ":Unrouted,Dimention,Milling,tOrigin,bOrigin,tRestrict,bRestrict,vRestrict"
        ":tPlace,bPlace,tValues,bValues,tStop,bStop,tOrigins,bOrigins",
    "Unknown"
};

string GetModeDesc(char mode)
{
    int n = strchr(MODES, mode);
    return (n < 0) ? "" : MODEDESCS[n];
}

string GetModeName(char mode)
{
    string parts[];
    int nParts = strsplit(parts, GetModeDesc(mode), ':');
    if (!nParts) return "";
    return parts[0];
}

string GetModeDetectionLayers(char mode)
{
    string parts[];
    int nParts = strsplit(parts, GetModeDesc(mode), ':');
    if (nParts <= 1) return "";
    return parts[1];
}

string GetModeLayersToShow(char mode)
{
    string parts[];
    int nParts = strsplit(parts, GetModeDesc(mode), ':');
    if (nParts <= 2) return "";
    return parts[2];
}

string GetModeLayersToHide(char mode)
{
    string parts[];
    int nParts = strsplit(parts, GetModeDesc(mode), ':');
    if (nParts <= 3) return "";
    return parts[3];
}

char GetNextMode(char mode)
{
    int pos = strchr(MODES, mode);
    return (pos < 0 || pos + 2 >= strlen(MODES)) ? MODES[0] : MODES[pos + 1];
}

char GetPrevMode(char mode)
{
    int pos = strchr(MODES, mode);
    return (pos < 1) ? MODES[strlen(MODES)] : MODES[pos - 1];
}

string GetLayerName(UL_BOARD B, int number)
{
    B.layers(L) if (L.number == number) return L.name;
    return "";
}

int GetLayerNumber(UL_BOARD B, string name)
{
    B.layers(L) if (L.name == name) return L.number;
    return 0;
}

int GetLayerIsVisible(UL_BOARD B, string name)
{
    B.layers(L) if (L.name == name) return L.visible;
    return 0;
}

string SIDES = "t23456789ABCDEFba";
string SIDEDESCS[] = {
// Name : Opposite : Next : Nearest
         "Top:b:2:t",  // t
     "Inner 2:F:3:t",  // 2
     "Inner 3:E:4:t",  // 3
     "Inner 4:D:5:t",  // 4
     "Inner 5:C:6:t",  // 5
     "Inner 6:B:7:t",  // 6
     "Inner 7:A:8:t",  // 7
     "Inner 8:9:9:t",  // 8
     "Inner 9:8:A:b",  // 9
    "Inner 10:7:B:b",  // A
    "Inner 11:6:C:b",  // B
    "Inner 12:5:D:b",  // C
    "Inner 13:4:E:b",  // D
    "Inner 14:3:F:b",  // E
    "Inner 15:2:G:b",  // F
      "Bottom:t:t:b",  // b
         "All:a:t:b"   // a
};

string GetSideDesc(char side)
{
    int n = strchr(SIDES, side);
    return (n < 0) ? "" : SIDEDESCS[n];
}

string GetSideName(char side)
{
    string parts[];
    int nParts = strsplit(parts, GetSideDesc(side), ':');
    if (!nParts) return "";
    return parts[0];
}

char GetOppositeSide(char side)
{
    string parts[];
    int nParts = strsplit(parts, GetSideDesc(side), ':');
    if (nParts <= 1) return 'a';
    return parts[1][0];
}

char GetNextSide(char side)
{
    string parts[];
    int nParts = strsplit(parts, GetSideDesc(side), ':');
    if (nParts <= 2) return 't';
    return parts[2][0];
}

char GetNearestSide(char side)
{
    string parts[];
    int nParts = strsplit(parts, GetSideDesc(side), ':');
    if (nParts <= 3) return 'a';
    return parts[3][0];
}

char GetLayerSide(UL_BOARD B, int number) // 'a'-all, 't'-top, 'b'-bottom', '2'-'E'-internals
{
    if (number >= LAYER_TOP && number <= LAYER_BOTTOM) return SIDES[number - LAYER_TOP];

    string name = GetLayerName(B, number);
    if (isupper(name[1])) {
        if (name[0] == 'b') return 'b';
        if (name[0] == 't') return 't';
    }
    return 'a';
}

string GetLayerNameForSide(UL_BOARD B, UL_LAYER L, char targetSide)
{
    int n = strchr(SIDES, targetSide);
    if (L.number >= LAYER_TOP && L.number <= LAYER_BOTTOM)
        if (n >= 0 && LAYER_TOP + n <= LAYER_BOTTOM) return GetLayerName(B, LAYER_TOP + n);
    string name = L.name;
    if (isupper(name[1])) {
        char near = GetNearestSide(targetSide);
        if (name[0] == 'b' && near == 't') return "t" + strsub(name, 1);
        if (name[0] == 't' && near == 'b') return "b" + strsub(name, 1);
    }
    return name;
}

char GetCurrentSide(UL_BOARD B)
{
    B.layers(L) if (L.used && L.visible) {
        char t = GetLayerSide(B, L.number);
        if (t != 'a') return t;
    }
    return 'a';
}

string USEDSIDES = "t2Fb";
char GetNextUsedSide(UL_BOARD B, char side) // 't' -> '2',  '2' -> 'F',  'F' -> 'b', 'b' -> 't'
{
    int pos = strchr(USEDSIDES, side);
    return (pos < 0 || pos + 1 >= strlen(USEDSIDES)) ? 't' : USEDSIDES[pos + 1];
}

char GetCurrentMode(UL_BOARD B)
{
    char mode = 'c';
    for (int m = 0; m < strlen(MODES); m++) {
        mode = MODES[m];
        string layers[];
        int nLayers = strsplit(layers, GetModeDetectionLayers(mode), ',');
        for (int l = 0; l < nLayers; l++)
            if (GetLayerIsVisible(B, layers[l]))
                return mode;
    }
    return mode;
}

void UpdateUsedSides(UL_BOARD B)
{
    string copperLayers;

    USEDSIDES = "";
    B.layers(L) if (L.used) {
        char side = GetLayerSide(B, L.number);
        if (side == 'a') break;

        USEDSIDES += side;
        copperLayers += L.name + ",";
    }
    copperLayers = strsub(copperLayers, 0, strlen(copperLayers) - 1);

    string topUnique;
    int m = 0;
    while (MODEDESCS[m]) {
        string desc = MODEDESCS[m];
        // replace # with real layernames
        int pos = strchr(desc,'#');
        if (pos >= 0) {
            desc = strsub(desc,0,pos) + copperLayers + strsub(desc, pos + 1);
        }

        string parts[];
        int nParts = strsplit(parts, desc, ':');
        if (nParts <= 1) break;

        if (nParts > 2) parts[2] += "," + parts[1]; // visible += uniques
        if (nParts > 3) parts[3] += topUnique;      // hidden += prev uniques
        topUnique += "," + parts[1];

        MODEDESCS[m] = strjoin(parts, ':');
        m++;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Script Entry Point

if (board) board(B)
{
    string logMsg;

    UpdateUsedSides(B);

    char currentMode = GetCurrentMode(B);
    char currentSide = GetCurrentSide(B);

    char targetMode = currentMode;
    char targetSide = (currentSide == 'a') ? 't' : ((targetMode == 'c') ? GetNextUsedSide(B, currentSide) : GetOppositeSide(currentSide));

    for (int a = 1; a < argc; a++) {
        if (argv[a] == "M") {
            targetMode = GetNextMode(currentMode);
            targetSide = currentSide;
        }
        if (argv[a] == "S") {
            targetSide = GetOppositeSide(currentSide);
        }
    }

    string statusMsg;
    sprintf(statusMsg, "switched to MODE '%s', SIDE '%s'\n", GetModeName(targetMode), GetSideName(targetSide));
    status(statusMsg);

    string strTmp;
    sprintf(strTmp, "%c", targetMode);
    string layersToShow[];
    int nLayersToShow = strsplit(layersToShow, GetModeLayersToShow(targetMode), ',');
    string layersToHide[];
    int nLayersToHide = strsplit(layersToHide, GetModeLayersToHide(targetMode), ',');
    
//    string msg;
//    sprintf(msg, "\ncurrent mode: '%c', side: '%c'\ntarget mode: '%c', side: '%c'\nlayers to show:%d, %s\nlayers to hide:%d %s\n\n",
//            currentMode, currentSide, targetMode, targetSide, nLayersToShow, strjoin(layersToShow, ' '), nLayersToHide, strjoin(layersToHide, ' '));
//    logMsg += msg;


    string cmd = "DISPLAY NONE";
    B.layers(L) if (L.used) {
        int v = 0;
        char layerSide = GetLayerSide(B, L.number);

        if (lookup(layersToHide, L.name, 0)) {
            v = 0;
//            sprintf(msg, "%d:'%s'(%c) to hide\n",
//                    L.number, L.name, layerSide);
        } else if (layerSide == 'a') {
            v = L.visible
                || lookup(layersToShow, L.name, 0);
//            sprintf(msg, "%d:'%s'(%c)=%d\n",
//                    L.number, L.name, layerSide, v);
        } else if (layerSide == targetSide) {
            v = GetLayerIsVisible(B, GetLayerNameForSide(B, L, currentSide))
                || lookup(layersToShow, L.name, 0);
//            sprintf(msg, "%d:'%s'(%c)=%d from '%s'\n",
//                    L.number, L.name, layerSide, v, GetLayerPairName(B, L, currentSide));
//        } else {
//            sprintf(msg, "%d:'%s'(%c) is not (%c)\n",
//                    L.number, L.name, layerSide, targetSide);
        }

        if (v) cmd += " " + L.name;

//        if (L.number < 30) logMsg += "\n" + msg;
    }

//    dlgMessageBox(logMsg);
    exit(cmd);
}
else
{
   dlgMessageBox("\n   Start this ULP in a Board    \n");
   exit (0);
}
